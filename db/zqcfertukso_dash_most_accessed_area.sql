-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 07/08/2018 às 15:49
-- Versão do servidor: 5.5.51
-- Versão do PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `menuto`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `zqcfertukso_dash_most_accessed_area`
--

CREATE TABLE IF NOT EXISTS `zqcfertukso_dash_most_accessed_area` (
  `id` bigint(20) NOT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  `1_day_stats` mediumint(9) DEFAULT NULL,
  `7_day_stats` mediumint(9) DEFAULT NULL,
  `30_day_stats` mediumint(9) DEFAULT NULL,
  `all_time_stats` bigint(20) DEFAULT NULL,
  `raw_stats` text
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

--
-- Fazendo dump de dados para tabela `zqcfertukso_dash_most_accessed_area`
--

INSERT INTO `zqcfertukso_dash_most_accessed_area` (`id`, `post_id`, `last_updated`, `1_day_stats`, `7_day_stats`, `30_day_stats`, `all_time_stats`, `raw_stats`) VALUES
(1, 810, '2018-06-28 15:37:00', 2, 3, 5, 5, 'a:3:{s:10:"2018-06-28";i:2;s:10:"2018-07-05";i:1;s:10:"2018-07-06";i:2;}'),
(2, 919, '2018-06-28 16:03:30', 1, 1, 8, 12, 'a:8:{s:10:"2018-06-28";i:1;s:10:"2018-06-29";i:3;s:10:"2018-07-03";i:2;s:10:"2018-07-06";i:1;s:10:"2018-07-09";i:1;s:10:"2018-07-10";i:2;s:10:"2018-07-18";i:1;s:10:"2018-07-30";i:1;}'),
(3, 3, '2018-06-28 16:04:52', 2, 2, 66, 100, 'a:18:{s:10:"2018-06-28";i:2;s:10:"2018-06-29";i:6;s:10:"2018-07-02";i:4;s:10:"2018-07-03";i:10;s:10:"2018-07-04";i:2;s:10:"2018-07-05";i:1;s:10:"2018-07-06";i:9;s:10:"2018-07-08";i:1;s:10:"2018-07-09";i:18;s:10:"2018-07-10";i:5;s:10:"2018-07-11";i:1;s:10:"2018-07-12";i:1;s:10:"2018-07-17";i:1;s:10:"2018-07-18";i:2;s:10:"2018-07-19";i:6;s:10:"2018-07-25";i:2;s:10:"2018-07-30";i:27;s:10:"2018-08-06";i:2;}'),
(5, 642, '2018-06-29 11:16:03', 2, 2, 6, 8, 'a:5:{s:10:"2018-06-29";i:2;s:10:"2018-07-04";i:1;s:10:"2018-07-05";i:2;s:10:"2018-07-19";i:1;s:10:"2018-07-30";i:2;}'),
(6, 846, '2018-06-29 11:42:39', 3, 4, 4, 4, 'a:2:{s:10:"2018-06-29";i:1;s:10:"2018-07-03";i:3;}'),
(7, 1032, '2018-06-29 11:42:45', 1, 5, 37, 38, 'a:7:{s:10:"2018-06-29";i:1;s:10:"2018-07-03";i:3;s:10:"2018-07-06";i:25;s:10:"2018-07-09";i:1;s:10:"2018-07-10";i:3;s:10:"2018-07-30";i:4;s:10:"2018-07-31";i:1;}'),
(8, 8, '2018-07-02 22:30:31', 1, 1, 1, 1, 'a:1:{s:10:"2018-07-03";i:1;}'),
(9, 540, '2018-07-30 14:30:08', 1, 1, 1, 1, 'a:1:{s:10:"2018-07-30";i:1;}');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `zqcfertukso_dash_most_accessed_area`
--
ALTER TABLE `zqcfertukso_dash_most_accessed_area`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `zqcfertukso_dash_most_accessed_area`
--
ALTER TABLE `zqcfertukso_dash_most_accessed_area`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
