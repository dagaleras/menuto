-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 07/08/2018 às 15:47
-- Versão do servidor: 5.5.51
-- Versão do PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `menuto`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `zqcfertukso_dash_total_interaction`
--

CREATE TABLE IF NOT EXISTS `zqcfertukso_dash_total_interaction` (
  `id` bigint(20) NOT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `post_total` bigint(20) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `category_total` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `product_total` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `zqcfertukso_dash_total_interaction`
--

INSERT INTO `zqcfertukso_dash_total_interaction` (`id`, `post_id`, `post_total`, `category_id`, `category_total`, `product_id`, `product_total`) VALUES
(2, 1032, 34, NULL, NULL, NULL, NULL),
(4, 810, 6, NULL, NULL, NULL, NULL),
(5, 642, 9, NULL, NULL, NULL, NULL),
(6, 919, 5, NULL, NULL, NULL, NULL),
(7, 846, 2, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, NULL, 1052, 1),
(9, NULL, NULL, NULL, NULL, 1090, 3),
(10, NULL, NULL, NULL, NULL, 315, 2),
(13, NULL, NULL, 5, 9, NULL, NULL),
(14, NULL, NULL, 4, 10, NULL, NULL),
(15, NULL, NULL, NULL, NULL, 685, 1),
(16, 540, 1, NULL, NULL, NULL, NULL);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `zqcfertukso_dash_total_interaction`
--
ALTER TABLE `zqcfertukso_dash_total_interaction`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `zqcfertukso_dash_total_interaction`
--
ALTER TABLE `zqcfertukso_dash_total_interaction`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
