-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 07/08/2018 às 15:38
-- Versão do servidor: 5.5.51
-- Versão do PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `menuto`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `zqcfertukso_dash_most_popular_post_user`
--

CREATE TABLE IF NOT EXISTS `zqcfertukso_dash_most_popular_post_user` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `post_count` bigint(20) DEFAULT NULL,
  `share_count` bigint(20) DEFAULT NULL,
  `post_category` longtext
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

--
-- Fazendo dump de dados para tabela `zqcfertukso_dash_most_popular_post_user`
--

INSERT INTO `zqcfertukso_dash_most_popular_post_user` (`id`, `user_id`, `post_id`, `post_count`, `share_count`, `post_category`) VALUES
(1, 31, 1032, 3, 0, 'a:1:{i:0;i:5;}'),
(2, 31, 846, 2, 0, 'a:2:{i:0;i:4;i:1;i:3;}'),
(3, 31, 919, 3, 0, 'a:1:{i:0;i:4;}'),
(4, 31, 642, 3, 0, 'a:1:{i:0;i:5;}'),
(5, 31, 810, 3, 0, 'a:1:{i:0;i:3;}'),
(6, 3, 1032, 3, 0, 'a:1:{i:0;i:5;}'),
(7, 3, 919, 1, 0, 'a:1:{i:0;i:4;}'),
(8, 3, 642, 3, 0, 'a:1:{i:0;i:5;}'),
(9, 3, 540, 1, 0, 'a:1:{i:0;i:4;}');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `zqcfertukso_dash_most_popular_post_user`
--
ALTER TABLE `zqcfertukso_dash_most_popular_post_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `zqcfertukso_dash_most_popular_post_user`
--
ALTER TABLE `zqcfertukso_dash_most_popular_post_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
