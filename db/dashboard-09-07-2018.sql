-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09-Jul-2018 às 11:41
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `menuto`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `zqcfertukso_dash_clicks_products`
--

CREATE TABLE IF NOT EXISTS `zqcfertukso_dash_clicks_products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `count` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `zqcfertukso_dash_clicks_products`
--

INSERT INTO `zqcfertukso_dash_clicks_products` (`id`, `user_id`, `product_id`, `post_id`, `sku`, `count`) VALUES
(1, 0, 1052, 1032, 'EUS6999L/3K', 1),
(2, 0, 1090, 1032, 'EU2035YMY/4P', 3),
(3, 0, 315, 3, '753AE/4X', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `zqcfertukso_dash_leads`
--

CREATE TABLE IF NOT EXISTS `zqcfertukso_dash_leads` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `documentType` varchar(255) DEFAULT NULL,
  `document` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `orders` longtext,
  `data_created` datetime DEFAULT NULL,
  `data_updated` datetime DEFAULT NULL,
  `data_last_interaction` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `zqcfertukso_dash_leads`
--

INSERT INTO `zqcfertukso_dash_leads` (`id`, `user_id`, `first_name`, `last_name`, `email`, `documentType`, `document`, `phone`, `orders`, `data_created`, `data_updated`, `data_last_interaction`) VALUES
(1, 31, 'César', 'Menezes', 'contato@cabanacriacao.com', 'cpf', '09124275743', '+552127077020', 'a:6:{s:4:"list";a:3:{i:0;O:8:"stdClass":22:{s:7:"orderId";s:14:"v911748tmcn-01";s:12:"creationDate";s:33:"2018-06-05T15:58:38.0000000+00:00";s:10:"clientName";s:14:"César Menezes";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:72:"Smartwatch Diesel On Híbrido Masculino Marrom - DZT1003/0MI DZT1003/0MI";s:3:"ean";s:13:"7891530473531";s:5:"refId";s:11:"DZT1003/0MI";s:2:"id";s:7:"2011061";s:9:"productId";s:7:"2011032";s:12:"sellingPrice";i:193500;s:5:"price";i:215000;}}s:10:"totalValue";d:193500;s:12:"paymentNames";s:11:"BOLBRADESCO";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"911748";s:12:"salesChannel";s:1:"1";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:143:" Time | Center Pedido cancelado realizado em: 05/06/2018 Olá, César. Seu pedido foi cancelado com sucesso. Pedido nº v911748tmcn-01 Fornecid";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:1;O:8:"stdClass":22:{s:7:"orderId";s:14:"v911697tmcn-01";s:12:"creationDate";s:33:"2018-06-05T14:57:19.0000000+00:00";s:10:"clientName";s:14:"César Menezes";s:5:"items";a:3:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:58:"Relógio Fossil Masculino Townsman - FS5279/0AN FS5279/0AN";s:3:"ean";s:13:"7891530471520";s:5:"refId";s:10:"FS5279/0AN";s:2:"id";s:7:"2011098";s:9:"productId";s:7:"2011069";s:12:"sellingPrice";i:88191;s:5:"price";i:97990;}i:1;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:58:"Relógio Armani Exchange Masculino - AX2172/0AN AX2172/0AN";s:3:"ean";s:13:"7891530342684";s:5:"refId";s:10:"AX2172/0AN";s:2:"id";s:6:"509331";s:9:"productId";s:6:"807730";s:12:"sellingPrice";i:104391;s:5:"price";i:115990;}i:2;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:73:"Smartwatch Diesel On Híbrido Masculino Dourado - DZT1013/1DI DZT1013/1DI";s:3:"ean";s:13:"7891530497131";s:5:"refId";s:11:"DZT1013/1DI";s:2:"id";s:9:"710000162";s:9:"productId";s:9:"910000060";s:12:"sellingPrice";i:207000;s:5:"price";i:230000;}}s:10:"totalValue";d:399582;s:12:"paymentNames";s:11:"BOLBRADESCO";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"911697";s:12:"salesChannel";s:1:"1";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:143:" Time | Center Pedido cancelado realizado em: 05/06/2018 Olá, César. Seu pedido foi cancelado com sucesso. Pedido nº v911697tmcn-01 Fornecid";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:2;O:8:"stdClass":22:{s:7:"orderId";s:14:"v911673tmcn-01";s:12:"creationDate";s:33:"2018-06-05T14:43:43.0000000+00:00";s:10:"clientName";s:14:"César Menezes";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:58:"Relógio Fossil Masculino Townsman - FS5346/1BN FS5346/1BN";s:3:"ean";s:13:"7891530476280";s:5:"refId";s:10:"FS5346/1BN";s:2:"id";s:7:"2011286";s:9:"productId";s:7:"2011257";s:12:"sellingPrice";i:101691;s:5:"price";i:112990;}}s:10:"totalValue";d:101691;s:12:"paymentNames";s:11:"BOLBRADESCO";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"911673";s:12:"salesChannel";s:1:"1";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:143:" Time | Center Pedido cancelado realizado em: 05/06/2018 Olá, César. Seu pedido foi cancelado com sucesso. Pedido nº v911673tmcn-01 Fornecid";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}}s:6:"facets";a:0:{}s:6:"paging";O:8:"stdClass":4:{s:5:"total";i:3;s:5:"pages";i:1;s:11:"currentPage";i:1;s:7:"perPage";i:15;}s:5:"stats";O:8:"stdClass":1:{s:5:"stats";O:8:"stdClass":2:{s:10:"totalValue";O:8:"stdClass":9:{s:5:"Count";i:3;s:3:"Max";d:0;s:4:"Mean";d:0;s:3:"Min";d:0;s:7:"Missing";i:0;s:6:"StdDev";d:0;s:3:"Sum";d:0;s:12:"SumOfSquares";d:0;s:6:"Facets";O:8:"stdClass":0:{}}s:10:"totalItems";O:8:"stdClass":9:{s:5:"Count";i:3;s:3:"Max";d:0;s:4:"Mean";d:0;s:3:"Min";d:0;s:7:"Missing";i:0;s:6:"StdDev";d:0;s:3:"Sum";d:0;s:12:"SumOfSquares";d:0;s:6:"Facets";O:8:"stdClass":0:{}}}}s:11:"countOrders";i:3;s:10:"countItems";i:5;}', '2018-06-05 14:43:44', '2018-06-05 16:41:57', '2018-06-05 16:41:57'),
(2, 11, 'Nathalia', 'Bruno', 'nathaliabruno@grupotechnos.com.br', 'cpf', '13048900748', '+5521964578606', 'a:6:{s:4:"list";a:15:{i:0;O:8:"stdClass":22:{s:7:"orderId";s:14:"v878663tmcn-01";s:12:"creationDate";s:33:"2018-03-29T21:42:30.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:69:"Relógio Technos Militar Masculino Analógico - 2036MEX/8X 2036MEX/8X";s:3:"ean";s:13:"7891530389474";s:5:"refId";s:10:"2036MEX/8X";s:2:"id";s:7:"2004814";s:9:"productId";s:7:"2004869";s:12:"sellingPrice";i:17990;s:5:"price";i:17990;}}s:10:"totalValue";d:18989;s:12:"paymentNames";s:11:"BOLBRADESCO";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"878663";s:12:"salesChannel";s:1:"9";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:142:" Email-Notificacao-Technos Pedido cancelado! realizado em: 29/03/2018 Olá, . Seu pedido foi cancelado com sucesso. Pedido nº v878663tmcn-01 ";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:1;O:8:"stdClass":22:{s:7:"orderId";s:14:"v875249tmcn-01";s:12:"creationDate";s:33:"2018-03-21T15:17:36.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:2:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:65:"Carteira Fossil Masculina Ingram Marrom - ML3562609/1 ML3562609/1";s:3:"ean";s:13:"7891530385216";s:5:"refId";s:11:"ML3562609/1";s:2:"id";s:7:"2004406";s:9:"productId";s:7:"2004461";s:12:"sellingPrice";i:9600;s:5:"price";i:9600;}i:1;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:29:"CC01 - NECESSAIRE FOSSIL CC01";s:3:"ean";s:12:"796483269927";s:5:"refId";s:4:"CC01";s:2:"id";s:7:"2002380";s:9:"productId";s:7:"2002438";s:12:"sellingPrice";i:0;s:5:"price";i:1;}}s:10:"totalValue";d:10564;s:12:"paymentNames";s:4:"Visa";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"875249";s:12:"salesChannel";s:2:"15";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:143:" Fossil Pedido cancelado Olá, Nathalia. Seu pedido de nº v875249tmcn-01, realizado no dia 21/03/2018, foi cancelado. PEDIDO CANCELADO   Prod";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";s:33:"2018-03-21T16:18:11.0000000+00:00";s:22:"callCenterOperatorName";N;}i:2;O:8:"stdClass":22:{s:7:"orderId";s:14:"v874301tmcn-01";s:12:"creationDate";s:33:"2018-03-20T13:28:02.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:64:"Relógio Condor Masculino Casual CO2115VR/2K - Cinza CO2115VR/2K";s:3:"ean";s:13:"7891530405150";s:5:"refId";s:11:"CO2115VR/2K";s:2:"id";s:7:"2007316";s:9:"productId";s:7:"2007299";s:12:"sellingPrice";i:13990;s:5:"price";i:13990;}}s:10:"totalValue";d:14676;s:12:"paymentNames";s:11:"BOLBRADESCO";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"874301";s:12:"salesChannel";s:1:"1";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:142:"Time | Center Pedido canceladorealizado em: 20/03/2018Olá, Nathalia. Seu pedido foi cancelado com sucesso.Pedido nº v874301tmcn-01Fornecido ";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:3;O:8:"stdClass":22:{s:7:"orderId";s:14:"v871602tmcn-01";s:12:"creationDate";s:33:"2018-03-14T14:45:34.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:58:"Relógio Masculino Vasco Preto - VAS13615A/8P VAS13615A/8P";s:3:"ean";s:13:"7891530366994";s:5:"refId";s:12:"VAS13615A/8P";s:2:"id";s:7:"2000615";s:9:"productId";s:7:"2000692";s:12:"sellingPrice";i:11990;s:5:"price";i:11990;}}s:10:"totalValue";d:12668;s:12:"paymentNames";s:16:"Boleto Bancário";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"871602";s:12:"salesChannel";s:1:"1";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:142:"Timecenter Pedido canceladorealizado em: 14/03/2018Olá, Nathalia. Seu pedido foi cancelado com sucesso.Pedido nº v871602tmcn-01Fornecido e e";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:4;O:8:"stdClass":22:{s:7:"orderId";s:14:"v863558tmcn-01";s:12:"creationDate";s:33:"2018-02-26T14:08:06.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:68:"Relógio Technos Feminino Crystal Analógico - 2035MFE/1P 2035MFE/1P";s:3:"ean";s:7:"7.89E12";s:5:"refId";s:10:"2035MFE/1P";s:2:"id";s:7:"2005963";s:9:"productId";s:7:"2005996";s:12:"sellingPrice";i:33990;s:5:"price";i:33990;}}s:10:"totalValue";d:35059;s:12:"paymentNames";s:16:"Boleto Bancário";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"863558";s:12:"salesChannel";s:1:"9";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:142:" Email-Notificacao-Technos Pedido cancelado! realizado em: 26/02/2018 Olá, . Seu pedido foi cancelado com sucesso. Pedido nº v863558tmcn-01 ";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:5;O:8:"stdClass":22:{s:7:"orderId";s:14:"v854513tmcn-01";s:12:"creationDate";s:33:"2018-02-08T17:29:38.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:69:"Carteira Fossil Masculina Ingram Marrom - ML329159222/1 ML329159222/1";s:3:"ean";s:13:"7891530459214";s:5:"refId";s:13:"ML329159222/1";s:2:"id";s:7:"2009919";s:9:"productId";s:7:"2009904";s:12:"sellingPrice";i:20300;s:5:"price";i:20300;}}s:10:"totalValue";d:20739;s:12:"paymentNames";s:16:"Boleto Bancário";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"854513";s:12:"salesChannel";s:2:"15";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:143:" Fossil Pedido cancelado Olá, Nathalia. Seu pedido de nº v854513tmcn-01, realizado no dia 08/02/2018, foi cancelado. PEDIDO CANCELADO   Prod";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:6;O:8:"stdClass":22:{s:7:"orderId";s:14:"v786125tmcn-01";s:12:"creationDate";s:33:"2017-11-01T01:57:30.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:69:"Relógio Technos Militar Masculino Analógico - 2036MEX/8X 2036MEX/8X";s:3:"ean";s:13:"7891530389474";s:5:"refId";s:10:"2036MEX/8X";s:2:"id";s:7:"2004814";s:9:"productId";s:7:"2004869";s:12:"sellingPrice";i:16191;s:5:"price";i:17990;}}s:10:"totalValue";d:17299;s:12:"paymentNames";s:16:"Boleto Bancário";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"786125";s:12:"salesChannel";s:1:"9";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:142:" Email-Notificacao-Technos Pedido cancelado! realizado em: 01/11/2017 Olá, . Seu pedido foi cancelado com sucesso. Pedido nº v786125tmcn-01 ";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:7;O:8:"stdClass":22:{s:7:"orderId";s:14:"v786118tmcn-01";s:12:"creationDate";s:33:"2017-11-01T01:50:50.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:69:"Relógio Technos Militar Masculino Analógico - 2036MEX/8X 2036MEX/8X";s:3:"ean";s:13:"7891530389474";s:5:"refId";s:10:"2036MEX/8X";s:2:"id";s:7:"2004814";s:9:"productId";s:7:"2004869";s:12:"sellingPrice";i:16191;s:5:"price";i:17990;}}s:10:"totalValue";d:17299;s:12:"paymentNames";s:16:"Boleto Bancário";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"786118";s:12:"salesChannel";s:1:"9";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:142:" Email-Notificacao-Technos Pedido cancelado! realizado em: 01/11/2017 Olá, . Seu pedido foi cancelado com sucesso. Pedido nº v786118tmcn-01 ";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:8;O:8:"stdClass":22:{s:7:"orderId";s:14:"v786102tmcn-01";s:12:"creationDate";s:33:"2017-11-01T01:38:14.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:69:"Relógio Technos Militar Masculino Analógico - 2036MEX/8X 2036MEX/8X";s:3:"ean";s:13:"7891530389474";s:5:"refId";s:10:"2036MEX/8X";s:2:"id";s:7:"2004814";s:9:"productId";s:7:"2004869";s:12:"sellingPrice";i:16191;s:5:"price";i:17990;}}s:10:"totalValue";d:17299;s:12:"paymentNames";s:16:"Boleto Bancário";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"786102";s:12:"salesChannel";s:1:"9";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:142:" Email-Notificacao-Technos Pedido cancelado! realizado em: 01/11/2017 Olá, . Seu pedido foi cancelado com sucesso. Pedido nº v786102tmcn-01 ";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:9;O:8:"stdClass":22:{s:7:"orderId";s:14:"v783977tmcn-01";s:12:"creationDate";s:33:"2017-10-27T19:54:13.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:69:"Relógio Technos Militar Masculino Analógico - 2036MEX/8X 2036MEX/8X";s:3:"ean";s:13:"7891530389474";s:5:"refId";s:10:"2036MEX/8X";s:2:"id";s:7:"2004814";s:9:"productId";s:7:"2004869";s:12:"sellingPrice";i:16191;s:5:"price";i:17990;}}s:10:"totalValue";d:17299;s:12:"paymentNames";s:16:"Boleto Bancário";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"783977";s:12:"salesChannel";s:1:"9";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:142:" Email-Notificacao-Technos Pedido cancelado! realizado em: 27/10/2017 Olá, . Seu pedido foi cancelado com sucesso. Pedido nº v783977tmcn-01 ";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:10;O:8:"stdClass":22:{s:7:"orderId";s:14:"v783973tmcn-01";s:12:"creationDate";s:33:"2017-10-27T19:51:44.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:74:"Relógio Euro Feminino Madeira Fashion EU2036MAA/4L - Dourado EU2036MAA/4L";s:3:"ean";s:13:"7891530382420";s:5:"refId";s:12:"EU2036MAA/4L";s:2:"id";s:7:"2004051";s:9:"productId";s:7:"2004106";s:12:"sellingPrice";i:17991;s:5:"price";i:19990;}}s:10:"totalValue";d:19107;s:12:"paymentNames";s:16:"Boleto Bancário";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"783973";s:12:"salesChannel";s:1:"2";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:142:" Email-Notificacao Pedido cancelado Olá, Nathalia. Seu pedido de nº v783973tmcn-01, realizado no dia 27/10/2017, foi cancelado. PEDIDO CANCE";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:11;O:8:"stdClass":22:{s:7:"orderId";s:14:"v783642tmcn-01";s:12:"creationDate";s:33:"2017-10-27T12:59:05.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:3;s:11:"description";s:57:"Relógio Kate Spade Seaport Grand - 1YRU0029/I 1YRU0029/I";s:3:"ean";s:13:"7891530350016";s:5:"refId";s:10:"1YRU0029/I";s:2:"id";s:7:"2000546";s:9:"productId";s:7:"2000623";s:12:"sellingPrice";i:15000;s:5:"price";i:15000;}}s:10:"totalValue";d:45928;s:12:"paymentNames";s:10:"Mastercard";s:6:"status";s:8:"invoiced";s:17:"statusDescription";s:8:"Faturado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"783642";s:12:"salesChannel";s:1:"1";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:143:"Timecenter Seu pedido foi faturado! realizado em: 27/10/2017 Olá, Nathalia. Os produtos abaixo já foram faturados só aguardando envio para a";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";s:33:"2017-10-27T16:14:41.0000000+00:00";s:22:"callCenterOperatorName";N;}i:12;O:8:"stdClass":22:{s:7:"orderId";s:14:"v783339tmcn-01";s:12:"creationDate";s:33:"2017-10-26T18:26:12.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:2;s:11:"description";s:57:"Relógio Kate Spade Seaport Grand - 1YRU0029/I 1YRU0029/I";s:3:"ean";s:13:"7891530350016";s:5:"refId";s:10:"1YRU0029/I";s:2:"id";s:7:"2000546";s:9:"productId";s:7:"2000623";s:12:"sellingPrice";i:15000;s:5:"price";i:15000;}}s:10:"totalValue";d:30863;s:12:"paymentNames";s:10:"Mastercard";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"783339";s:12:"salesChannel";s:1:"1";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:142:"Timecenter Pedido canceladorealizado em: 26/10/2017Olá, Nathalia. Seu pedido foi cancelado com sucesso.Pedido nº v783339tmcn-01Fornecido e e";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:13;O:8:"stdClass":22:{s:7:"orderId";s:14:"v776808tmcn-01";s:12:"creationDate";s:33:"2017-10-11T16:54:24.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:56:"Relógio Euro Vitral Dourado - EU2035XYV/2D EU2035XYV/2D";s:3:"ean";s:13:"7891530371950";s:5:"refId";s:12:"EU2035XYV/2D";s:2:"id";s:7:"2001373";s:9:"productId";s:7:"2001450";s:12:"sellingPrice";i:13990;s:5:"price";i:13990;}}s:10:"totalValue";d:15080;s:12:"paymentNames";s:16:"Boleto Bancário";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"776808";s:12:"salesChannel";s:1:"2";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:142:" Email-Notificacao Pedido cancelado Olá, Nathalia. Seu pedido de nº v776808tmcn-01, realizado no dia 11/10/2017, foi cancelado. PEDIDO CANCE";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}i:14;O:8:"stdClass":22:{s:7:"orderId";s:14:"v776804tmcn-01";s:12:"creationDate";s:33:"2017-10-11T16:53:40.0000000+00:00";s:10:"clientName";s:14:"Nathalia Bruno";s:5:"items";a:1:{i:0;O:8:"stdClass":9:{s:6:"seller";s:1:"1";s:8:"quantity";i:1;s:11:"description";s:56:"Relógio Technos Boutique Feminino 2035MKM/4G 2035MKM/4G";s:3:"ean";s:13:"7891530448003";s:5:"refId";s:10:"2035MKM/4G";s:2:"id";s:7:"2009157";s:9:"productId";s:7:"2009142";s:12:"sellingPrice";i:29990;s:5:"price";i:29990;}}s:10:"totalValue";d:31149;s:12:"paymentNames";s:16:"Boleto Bancário";s:6:"status";s:8:"canceled";s:17:"statusDescription";s:9:"Cancelado";s:18:"marketPlaceOrderId";N;s:8:"sequence";s:6:"776804";s:12:"salesChannel";s:1:"9";s:11:"affiliateId";s:0:"";s:6:"origin";s:11:"Marketplace";s:20:"workflowInErrorState";b:0;s:15:"workflowInRetry";b:0;s:17:"lastMessageUnread";s:142:" Email-Notificacao-Technos Pedido cancelado! realizado em: 11/10/2017 Olá, . Seu pedido foi cancelado com sucesso. Pedido nº v776804tmcn-01 ";s:21:"ShippingEstimatedDate";N;s:15:"orderIsComplete";b:1;s:6:"listId";N;s:8:"listType";N;s:14:"authorizedDate";N;s:22:"callCenterOperatorName";N;}}s:6:"facets";a:0:{}s:6:"paging";O:8:"stdClass":4:{s:5:"total";i:21;s:5:"pages";i:2;s:11:"currentPage";i:1;s:7:"perPage";i:15;}s:5:"stats";O:8:"stdClass":1:{s:5:"stats";O:8:"stdClass":2:{s:10:"totalValue";O:8:"stdClass":9:{s:5:"Count";i:21;s:3:"Max";d:0;s:4:"Mean";d:0;s:3:"Min";d:0;s:7:"Missing";i:0;s:6:"StdDev";d:0;s:3:"Sum";d:0;s:12:"SumOfSquares";d:0;s:6:"Facets";O:8:"stdClass":0:{}}s:10:"totalItems";O:8:"stdClass":9:{s:5:"Count";i:21;s:3:"Max";d:0;s:4:"Mean";d:0;s:3:"Min";d:0;s:7:"Missing";i:0;s:6:"StdDev";d:0;s:3:"Sum";d:0;s:12:"SumOfSquares";d:0;s:6:"Facets";O:8:"stdClass":0:{}}}}s:11:"countOrders";i:15;s:10:"countItems";i:16;}', '2017-10-16 14:12:50', '2018-07-05 19:06:46', '2018-07-05 19:06:46');

-- --------------------------------------------------------

--
-- Estrutura da tabela `zqcfertukso_dash_most_accessed_area`
--

CREATE TABLE IF NOT EXISTS `zqcfertukso_dash_most_accessed_area` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  `1_day_stats` mediumint(9) DEFAULT NULL,
  `7_day_stats` mediumint(9) DEFAULT NULL,
  `30_day_stats` mediumint(9) DEFAULT NULL,
  `all_time_stats` bigint(20) DEFAULT NULL,
  `raw_stats` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `zqcfertukso_dash_most_accessed_area`
--

INSERT INTO `zqcfertukso_dash_most_accessed_area` (`id`, `post_id`, `last_updated`, `1_day_stats`, `7_day_stats`, `30_day_stats`, `all_time_stats`, `raw_stats`) VALUES
(1, 810, '2018-06-28 15:37:00', 2, 3, 5, 5, 'a:3:{s:10:"2018-06-28";i:2;s:10:"2018-07-05";i:1;s:10:"2018-07-06";i:2;}'),
(2, 919, '2018-06-28 16:03:30', 1, 3, 7, 7, 'a:4:{s:10:"2018-06-28";i:1;s:10:"2018-06-29";i:3;s:10:"2018-07-03";i:2;s:10:"2018-07-06";i:1;}'),
(3, 3, '2018-06-28 16:04:52', 1, 27, 35, 35, 'a:8:{s:10:"2018-06-28";i:2;s:10:"2018-06-29";i:6;s:10:"2018-07-02";i:4;s:10:"2018-07-03";i:10;s:10:"2018-07-04";i:2;s:10:"2018-07-05";i:1;s:10:"2018-07-06";i:9;s:10:"2018-07-08";i:1;}'),
(5, 642, '2018-06-29 11:16:03', 2, 5, 5, 5, 'a:3:{s:10:"2018-06-29";i:2;s:10:"2018-07-04";i:1;s:10:"2018-07-05";i:2;}'),
(6, 846, '2018-06-29 11:42:39', 3, 4, 4, 4, 'a:2:{s:10:"2018-06-29";i:1;s:10:"2018-07-03";i:3;}'),
(7, 1032, '2018-06-29 11:42:45', 25, 28, 29, 29, 'a:3:{s:10:"2018-06-29";i:1;s:10:"2018-07-03";i:3;s:10:"2018-07-06";i:25;}'),
(8, 8, '2018-07-02 22:30:31', 1, 1, 1, 1, 'a:1:{s:10:"2018-07-03";i:1;}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `zqcfertukso_dash_most_popular_post_user`
--

CREATE TABLE IF NOT EXISTS `zqcfertukso_dash_most_popular_post_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `post_count` bigint(20) DEFAULT NULL,
  `share_count` bigint(20) DEFAULT NULL,
  `post_category` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `zqcfertukso_dash_most_popular_post_user`
--

INSERT INTO `zqcfertukso_dash_most_popular_post_user` (`id`, `user_id`, `post_id`, `post_count`, `share_count`, `post_category`) VALUES
(1, 31, 1032, 27, 0, 'a:1:{i:0;i:5;}'),
(2, 31, 846, 2, 0, 'a:2:{i:0;i:4;i:1;i:3;}'),
(3, 31, 919, 2, 0, 'a:1:{i:0;i:4;}'),
(4, 31, 642, 3, 0, 'a:1:{i:0;i:5;}'),
(5, 31, 810, 3, 0, 'a:1:{i:0;i:3;}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `zqcfertukso_dash_total_interaction`
--

CREATE TABLE IF NOT EXISTS `zqcfertukso_dash_total_interaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) DEFAULT NULL,
  `post_total` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `product_total` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `zqcfertukso_dash_total_interaction`
--

INSERT INTO `zqcfertukso_dash_total_interaction` (`id`, `post_id`, `post_total`, `product_id`, `product_total`) VALUES
(2, 1032, 31, NULL, NULL),
(4, 810, 6, NULL, NULL),
(5, 642, 6, NULL, NULL),
(6, 919, 3, NULL, NULL),
(7, 846, 2, NULL, NULL),
(8, NULL, NULL, 1052, 1),
(9, NULL, NULL, 1090, 3),
(10, NULL, NULL, 315, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `zqcfertukso_dash_visitors`
--

CREATE TABLE IF NOT EXISTS `zqcfertukso_dash_visitors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `referrer` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=193 ;

--
-- Extraindo dados da tabela `zqcfertukso_dash_visitors`
--

INSERT INTO `zqcfertukso_dash_visitors` (`id`, `post_id`, `ip`, `referrer`, `browser`, `time`) VALUES
(1, 122, '::1', 'https://localhost/menuto/pra-combinar/arm-party-relogios-como-combinar-com-estilo/', 'Google Chrome', '2018-07-06 15:38:20'),
(2, 3, '::1', 'https://localhost/menuto/pra-combinar/arm-party-relogios-como-combinar-com-estilo/', 'Google Chrome', '2018-07-06 15:39:45'),
(3, 3, '::1', 'https://localhost/menuto/', 'Google Chrome', '2018-07-06 15:43:24'),
(4, 3, '::1', 'https://localhost/menuto/', 'Google Chrome', '2018-07-06 15:44:24'),
(5, 1198, '::1', '', 'Google Chrome', '2018-07-06 15:44:51'),
(6, 1198, '::1', '', 'Google Chrome', '2018-07-06 16:24:42'),
(7, 1198, '::1', '', 'Google Chrome', '2018-07-06 16:47:22'),
(8, 1198, '::1', '', 'Google Chrome', '2018-07-06 16:49:36'),
(9, 1198, '::1', '', 'Google Chrome', '2018-07-06 16:51:41'),
(10, 1198, '::1', '', 'Google Chrome', '2018-07-06 17:07:43'),
(11, 1198, '::1', '', 'Google Chrome', '2018-07-06 17:08:38'),
(12, 1198, '::1', '', 'Google Chrome', '2018-07-06 17:12:25'),
(13, 1198, '::1', '', 'Google Chrome', '2018-07-06 17:19:29'),
(14, 1198, '::1', '', 'Google Chrome', '2018-07-06 17:25:36'),
(15, 1198, '::1', '', 'Google Chrome', '2018-07-06 17:32:14'),
(16, 1198, '::1', '', 'Google Chrome', '2018-07-06 17:41:28'),
(17, 1198, '::1', '', 'Google Chrome', '2018-07-06 17:49:54'),
(18, 1198, '::1', '', 'Google Chrome', '2018-07-06 17:52:58'),
(19, 1198, '::1', '', 'Google Chrome', '2018-07-06 17:53:26'),
(20, 1198, '::1', '', 'Google Chrome', '2018-07-06 17:55:51'),
(21, 1198, '::1', '', 'Google Chrome', '2018-07-06 18:04:25'),
(22, 1198, '::1', '', 'Google Chrome', '2018-07-06 18:05:20'),
(23, 1198, '::1', '', 'Google Chrome', '2018-07-06 18:11:22'),
(24, 1198, '::1', '', 'Google Chrome', '2018-07-06 18:12:52'),
(25, 1198, '::1', '', 'Google Chrome', '2018-07-06 18:13:30'),
(26, 1198, '::1', '', 'Google Chrome', '2018-07-06 18:50:52'),
(27, 1198, '::1', '', 'Google Chrome', '2018-07-06 18:51:59'),
(28, 1198, '::1', '', 'Google Chrome', '2018-07-06 18:54:40'),
(29, 1198, '::1', '', 'Google Chrome', '2018-07-06 18:55:28'),
(30, 1198, '::1', '', 'Google Chrome', '2018-07-06 18:57:10'),
(31, 1198, '::1', '', 'Google Chrome', '2018-07-06 18:57:21'),
(32, 1198, '::1', '', 'Google Chrome', '2018-07-06 18:58:44'),
(33, 1198, '::1', '', 'Google Chrome', '2018-07-06 19:19:15'),
(34, 1198, '::1', '', 'Google Chrome', '2018-07-06 19:22:08'),
(35, 1198, '::1', '', 'Google Chrome', '2018-07-06 19:30:49'),
(36, 1198, '::1', '', 'Google Chrome', '2018-07-06 19:44:32'),
(37, 1198, '::1', '', 'Google Chrome', '2018-07-06 19:46:09'),
(38, 1198, '::1', '', 'Google Chrome', '2018-07-06 19:54:05'),
(39, 1198, '::1', '', 'Google Chrome', '2018-07-06 19:59:50'),
(40, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:02:20'),
(41, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:04:22'),
(42, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:10:18'),
(43, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:11:08'),
(44, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:12:19'),
(45, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:12:53'),
(46, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:13:41'),
(47, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:15:09'),
(48, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:15:46'),
(49, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:21:24'),
(50, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:22:42'),
(51, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:23:41'),
(52, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:24:22'),
(53, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:24:55'),
(54, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:25:19'),
(55, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:25:49'),
(56, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:26:18'),
(57, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:29:02'),
(58, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:32:29'),
(59, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:33:11'),
(60, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:37:30'),
(61, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:38:06'),
(62, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:43:39'),
(63, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:48:54'),
(64, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:50:41'),
(65, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:54:01'),
(66, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:58:22'),
(67, 1198, '::1', '', 'Google Chrome', '2018-07-06 20:59:25'),
(68, 1198, '::1', '', 'Google Chrome', '2018-07-06 21:00:16'),
(69, 1198, '::1', '', 'Google Chrome', '2018-07-06 21:01:07'),
(70, 1198, '::1', '', 'Google Chrome', '2018-07-06 21:03:47'),
(71, 1198, '::1', '', 'Google Chrome', '2018-07-06 21:05:09'),
(72, 1198, '::1', '', 'Google Chrome', '2018-07-06 21:16:08'),
(73, 1198, '::1', '', 'Google Chrome', '2018-07-06 21:43:25'),
(74, 1198, '::1', '', 'Google Chrome', '2018-07-06 21:45:09'),
(75, 1198, '::1', '', 'Google Chrome', '2018-07-06 21:48:49'),
(76, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-06 21:49:18'),
(77, 1198, '::1', 'https://localhost/menuto/dashboard/?view=Usuarios/', 'Google Chrome', '2018-07-06 21:49:37'),
(78, 1198, '::1', 'https://localhost/menuto/dashboard/?view=Usuarios/', 'Google Chrome', '2018-07-06 22:09:03'),
(79, 1198, '::1', '', 'Google Chrome', '2018-07-06 22:09:12'),
(80, 1198, '::1', '', 'Google Chrome', '2018-07-06 22:45:52'),
(81, 1198, '::1', '', 'Google Chrome', '2018-07-06 22:57:02'),
(82, 1198, '::1', '', 'Google Chrome', '2018-07-06 23:03:26'),
(83, 1198, '::1', '', 'Google Chrome', '2018-07-06 23:14:34'),
(84, 1198, '::1', '', 'Google Chrome', '2018-07-06 23:26:45'),
(85, 1198, '::1', '', 'Google Chrome', '2018-07-06 23:29:00'),
(86, 1198, '::1', '', 'Google Chrome', '2018-07-06 23:31:51'),
(87, 1198, '::1', '', 'Google Chrome', '2018-07-07 11:29:46'),
(88, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 11:29:59'),
(89, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 11:30:19'),
(90, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 11:35:12'),
(91, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 11:37:18'),
(92, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 11:38:44'),
(93, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 11:48:43'),
(94, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 11:49:17'),
(95, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 11:49:42'),
(96, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 11:54:47'),
(97, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 11:55:00'),
(98, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 11:56:42'),
(99, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 12:01:49'),
(100, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 12:05:45'),
(101, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 12:06:14'),
(102, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 12:20:41'),
(103, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 12:23:57'),
(104, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 12:27:41'),
(105, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 12:29:20'),
(106, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 12:30:16'),
(107, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 12:31:42'),
(108, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 12:32:31'),
(109, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 12:35:58'),
(110, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 12:37:49'),
(111, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 18:03:29'),
(112, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 18:05:39'),
(113, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 18:06:24'),
(114, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 18:12:11'),
(115, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 18:13:01'),
(116, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 18:13:28'),
(117, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 18:13:42'),
(118, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 18:14:55'),
(119, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 18:22:01'),
(120, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 18:29:07'),
(121, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 18:33:51'),
(122, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 18:40:05'),
(123, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 18:40:43'),
(124, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 19:02:30'),
(125, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 19:06:56'),
(126, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 19:29:46'),
(127, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 19:38:51'),
(128, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 19:49:17'),
(129, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 19:52:51'),
(130, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 19:54:45'),
(131, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 20:01:01'),
(132, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 20:03:23'),
(133, 1198, '::1', 'https://localhost/menuto/dashboard/', 'Google Chrome', '2018-07-07 20:05:46'),
(134, 3, '::1', 'https://localhost/menuto/technosadm/about.php', 'Google Chrome', '2018-07-08 21:34:25'),
(135, 1198, '::1', '', 'Google Chrome', '2018-07-08 21:34:53'),
(136, 1198, '::1', '', 'Google Chrome', '2018-07-08 21:57:11'),
(137, 1198, '::1', '', 'Google Chrome', '2018-07-09 00:09:25'),
(138, 1198, '::1', '', 'Google Chrome', '2018-07-09 00:12:59'),
(139, 1198, '::1', '', 'Google Chrome', '2018-07-09 00:19:28'),
(140, 1198, '::1', '', 'Google Chrome', '2018-07-09 00:21:14'),
(141, 1198, '::1', '', 'Google Chrome', '2018-07-09 00:26:54'),
(142, 1198, '::1', '', 'Google Chrome', '2018-07-09 01:09:32'),
(143, 1198, '::1', '', 'Google Chrome', '2018-07-09 01:33:58'),
(144, 1198, '::1', '', 'Google Chrome', '2018-07-09 01:43:03'),
(145, 1198, '::1', '', 'Google Chrome', '2018-07-09 01:47:48'),
(146, 1198, '::1', '', 'Google Chrome', '2018-07-09 02:05:40'),
(147, 1198, '::1', '', 'Google Chrome', '2018-07-09 02:12:39'),
(148, 1198, '::1', '', 'Google Chrome', '2018-07-09 02:15:40'),
(149, 1198, '::1', '', 'Google Chrome', '2018-07-09 02:25:03'),
(150, 1198, '::1', '', 'Google Chrome', '2018-07-09 02:26:48'),
(151, 1198, '::1', '', 'Google Chrome', '2018-07-09 02:33:02'),
(152, 1198, '::1', '', 'Google Chrome', '2018-07-09 02:40:16'),
(153, 1198, '::1', '', 'Google Chrome', '2018-07-09 02:51:33'),
(154, 1198, '::1', '', 'Google Chrome', '2018-07-09 03:17:08'),
(155, 1198, '::1', '', 'Google Chrome', '2018-07-09 03:36:48'),
(156, 1198, '::1', '', 'Google Chrome', '2018-07-09 03:46:01'),
(157, 1198, '::1', '', 'Google Chrome', '2018-07-09 03:47:12'),
(158, 1198, '::1', '', 'Google Chrome', '2018-07-09 03:51:54'),
(159, 1198, '::1', '', 'Google Chrome', '2018-07-09 03:57:47'),
(160, 1198, '::1', '', 'Google Chrome', '2018-07-09 04:01:52'),
(161, 1198, '::1', '', 'Google Chrome', '2018-07-09 04:13:34'),
(162, 1198, '::1', '', 'Google Chrome', '2018-07-09 04:25:05'),
(163, 1198, '::1', '', 'Google Chrome', '2018-07-09 04:43:57'),
(164, 1198, '::1', '', 'Google Chrome', '2018-07-09 05:23:17'),
(165, 1198, '::1', '', 'Google Chrome', '2018-07-09 05:27:29'),
(166, 1198, '::1', '', 'Google Chrome', '2018-07-09 05:39:12'),
(167, 1198, '::1', '', 'Google Chrome', '2018-07-09 05:45:49'),
(168, 1198, '::1', '', 'Google Chrome', '2018-07-09 05:46:09'),
(169, 1198, '::1', '', 'Google Chrome', '2018-07-09 05:52:47'),
(170, 1198, '::1', '', 'Google Chrome', '2018-07-09 06:05:27'),
(171, 1198, '::1', '', 'Google Chrome', '2018-07-09 06:38:48'),
(172, 1198, '::1', '', 'Google Chrome', '2018-07-09 06:46:58'),
(173, 1198, '::1', '', 'Google Chrome', '2018-07-09 07:02:31'),
(174, 1198, '::1', '', 'Google Chrome', '2018-07-09 07:08:40'),
(175, 1198, '::1', '', 'Google Chrome', '2018-07-09 07:13:07'),
(176, 1198, '::1', '', 'Google Chrome', '2018-07-09 07:20:37'),
(177, 1198, '::1', '', 'Google Chrome', '2018-07-09 07:26:00'),
(178, 1198, '::1', '', 'Google Chrome', '2018-07-09 07:26:39'),
(179, 1198, '::1', '', 'Google Chrome', '2018-07-09 08:16:43'),
(180, 1198, '::1', '', 'Google Chrome', '2018-07-09 08:20:54'),
(181, 1198, '::1', '', 'Google Chrome', '2018-07-09 08:25:47'),
(182, 1198, '::1', '', 'Google Chrome', '2018-07-09 08:26:35'),
(183, 1198, '::1', '', 'Google Chrome', '2018-07-09 08:33:41'),
(184, 1198, '::1', '', 'Google Chrome', '2018-07-09 08:35:10'),
(185, 1198, '::1', '', 'Google Chrome', '2018-07-09 08:41:29'),
(186, 1198, '::1', '', 'Google Chrome', '2018-07-09 08:47:58'),
(187, 1198, '::1', '', 'Google Chrome', '2018-07-09 08:58:22'),
(188, 1198, '::1', '', 'Google Chrome', '2018-07-09 09:01:10'),
(189, 1198, '::1', '', 'Google Chrome', '2018-07-09 09:10:07'),
(190, 1198, '::1', '', 'Google Chrome', '2018-07-09 09:13:15'),
(191, 1198, '::1', '', 'Google Chrome', '2018-07-09 09:27:18'),
(192, 1198, '::1', '', 'Google Chrome', '2018-07-09 09:37:25');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
