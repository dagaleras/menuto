-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 07/08/2018 às 15:30
-- Versão do servidor: 5.5.51
-- Versão do PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `menuto`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `zqcfertukso_dash_clicks_products`
--

CREATE TABLE IF NOT EXISTS `zqcfertukso_dash_clicks_products` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `count` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Fazendo dump de dados para tabela `zqcfertukso_dash_clicks_products`
--

INSERT INTO `zqcfertukso_dash_clicks_products` (`id`, `user_id`, `product_id`, `post_id`, `sku`, `count`) VALUES
(1, 0, 1052, 1032, 'EUS6999L/3K', 1),
(2, 0, 1090, 1032, 'EU2035YMY/4P', 3),
(3, 0, 315, 3, '753AE/4X', 1),
(4, 3, 685, 642, 'E6001FC749/8B', 1);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `zqcfertukso_dash_clicks_products`
--
ALTER TABLE `zqcfertukso_dash_clicks_products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `zqcfertukso_dash_clicks_products`
--
ALTER TABLE `zqcfertukso_dash_clicks_products`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
