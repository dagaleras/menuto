<!DOCTYPE html>
<!-- add a class to the html tag if the site is being viewed in IE, to allow for any big fixes -->
<!--[if lt IE 8]><html class="ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 9]><html class="ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if gt IE 9]><html <?php language_attributes(); ?>><![endif]-->
<!--[if !IE]><html <?php language_attributes(); ?>><![endif]-->
<html <?php language_attributes(); ?> class="no-js">
    <head>        
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta http-equiv="Content-Type"                    content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
        <meta http-equiv="X-UA-Compatible"                 content="IE=edge" />
        <meta name="apple-mobile-web-app-capable"          content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="viewport"                              content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="icon" href="<?php bloginfo( 'template_url' ); ?>/images/logo/favico/favicon.ico" />
        <link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/images/logo/favico/favicon.ico" />
        <!--[if IE]><link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/images/logo/favico/favicon.ico" /><![endif]-->
        
        <?php wp_head(); ?>        
    </head>
    <body <?php body_class( 'appear-animate' ) ?>>
        
        <!-- Load Facebook SDK for JavaScript -->
        <div id="fb-root"></div>

        <div id="preloader">
            <div class="loadingGIF"></div>
        </div>

        <div id="site" class="main" tabindex="-1">

            <header id="header" class="header">
                <div class="container-fluid">
                    
                    <div class="elements">                        
                        <div class="elements__left">
                            <div class="header--buttons">
                                <button id="button-menu" class="header--buttons-bt">
                                    <i class="i-hamburguer"></i>
                                </button>
                            </div>
                            <div class="header--brand">
                                <a href="<?php echo esc_url(home_url('/')) ?>" class="header--brand-lik">
                                    <img class="is-menu-close" src="<?php bloginfo( 'template_url' ); ?>/images/menuto-logo.png" alt="<?php bloginfo( 'name' ); ?>" />
                                    <img class="is-menu-open" src="<?php bloginfo( 'template_url' ); ?>/images/menuto-logo-novatagline.svg" alt="<?php bloginfo( 'name' ); ?>" />
                                </a>
                            </div>
                        </div>
                        <div class="elements__center">
                            <nav id="menu_category" class="header--categoies">
                                <?php 
                                    wp_nav_menu( array(
                                        'theme_location'  => 'category-header',
                                        'menu'            => 'category-header',
                                        'container'       => false,
                                        'container_class' => false,
                                        'menu_class'      => 'menu',
                                        'echo'            => true,
                                        'fallback_cb'     => 'wp_page_menu',
                                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                        'depth'           => 0,
                                        'walker'          => '',
                                    ) );
                                ?>
                            </nav>
                        </div>
                        <div class="elements__right">
                            <div id="menu_newsletter" class="header--newsletter">
                                <div class="header--newsletter-box">
                                    <button id="button-newsletter">
                                        <span><?php echo esc_html( __( 'Não perca nenhuma novidade!', 'menuto' ) ); ?></span>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>                          
                            <div class="header--login">
                                <div class="header--login-box">
                                    <?php if(is_user_logged_in()) : ?>
                                        <button id="button-login" class="open-menubox">
                                            <?php 
                                                global $current_user; 
                                                wp_get_current_user(); 
                                            ?>
                                            <span><?php echo esc_html( __( trunc($current_user->user_login, 1), 'menuto' ) ); ?></span>
                                            <figure><a href="#" alt="Sair"><i class="i-logout"></i></a></figure>
                                        </button>  
                                        <div class="menubox">
                                            <a href="<?php echo esc_url(site_url('minha-conta')) ?>"><?php echo __( 'Minha Conta', 'menuto' ); ?></a>
                                            <?php if ( in_array( 'administrator', (array)$current_user->roles ) || in_array( 'analyst', (array)$current_user->roles ) ) : ?>
                                                <a href="<?php echo esc_url(site_url('dashboard')) ?>"><?php echo __( 'Dashboard', 'menuto' ); ?></a>
                                            <?php endif; ?>
                                            <a href="<?php echo wp_logout_url( home_url('/') ); ?>" alt="Sair"><?php echo __( 'Sair', 'menuto' ); ?></a>
                                        </div>                                     
                                    <?php else : ?>    
                                        <button id="button-login" data-modal="modal" data-target="#login">                                   
                                            <span><?php echo esc_html( __( 'Login', 'menuto' ) ); ?></span>
                                            <figure><i class="i-login"></i></figure>                                    
                                        </button>
                                    <?php endif; ?>
                                </div>
                            </div>                           
                            <div class="header--search">
                                <div class="header--search-box">   
                                    <button id="button-search">
                                        <span><?php echo esc_html( __( 'Buscar', 'menuto' ) ); ?></span>
                                        <figure><i class="i-search"></i></figure>                                    
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>  

                    <div id="drop-menu">
                        <div class="container-fluid">
                            <div class="dropmenu">
                                <div class="dropmenu--nav">
                                    <nav id="dropmenu" class="dropmenu--nav-links">
                                        <?php 
                                            wp_nav_menu( array(
                                                'theme_location'  => 'dropdow-header',
                                                'menu'            => 'dropdow-header',
                                                'container'       => false,
                                                'container_class' => false,
                                                'menu_class'      => 'menu-drop row',
                                                'echo'            => true,
                                                'fallback_cb'     => 'wp_page_menu',
                                                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                                'depth'           => 0,
                                                'walker'          => '',
                                            ) );
                                        ?>
                                    </nav>
                                </div>       
                                <div class="dropmenu--news">
                                    <div class="newsletter">
                                        <?php if( get_field('header__newsletter_title', 'option') ) : ?>
                                            <h3 class="newsletter--title"><?php the_field('header__newsletter_title', 'option'); ?></h3>
                                        <?php endif; ?>
                                        <div class="newsletter--form">
                                            <div class="form">
                                                <?php get_template_part( 'content/newsletter' ); ?>
                                            </div>
                                        </div>
                                    </div>     
                                </div> 
                            </div>
                        </div>   
                    </div>

                    <div id="drop-search">
                        <div class="container-fluid">
                            <div class="search">
                                <div class="search--area">
                                    <?php get_search_form(); ?>
                                </div> 
                            </div>
                        </div>
                    </div>

                    <div id="drop-newsletter">
                        <section class="container-fluid">
                            <div class="newsletter">
                                <?php if( get_field('newsletter__title', 'option') ) : ?>
                                    <h3 class="newsletter--title"><?php the_field('newsletter__title', 'option'); ?></h3>
                                <?php endif; ?>
                                <div class="newsletter--form">
                                    <div class="form">
                                        <?php get_template_part( 'content/newsletter' ); ?>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                </div>
            </header>

            <?php get_template_part( 'modules/modal' ); ?>