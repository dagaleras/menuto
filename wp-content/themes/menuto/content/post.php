<div class="post--card">
	<a href="<?php echo esc_url(get_permalink()); ?>" class="post--card-link">
		<?php if( has_post_thumbnail() ) : ?>
			<figure class="post--card-figure">

				<img class="post--card-image" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
				
				<?php if( $category = get_the_terms( $post->ID, 'category' ) ): ?>
					<span class="post--card-category">
						<?php foreach ( $category as $cat ): ?>
							<?php echo $cat->name; ?>
						<?php endforeach; ?>
					</span>
				<?php endif; ?>

			</figure>
		<?php endif; ?>
		<div class="post--card-content">
			<h3 class="post--card-title"><?php the_title(); ?></h3>
			<div class="post--card-text"><?php echo wp_trim_words( do_shortcode($post->post_content), 25, '...' ); ?></div>
		</div>
	</a>
</div>