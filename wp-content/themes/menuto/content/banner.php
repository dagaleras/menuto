<div class="banners">
	<div class="banners--area">
		<div class="banners--area-loop">
			<div class="banner">
				<div class="mask"></div>

				<?php if( has_post_thumbnail() ) : ?>
					<figure class="banner--figure">
						<img class="banner--figure-img" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
					</figure>
				<?php endif; ?>
				
				<?php if( $category = get_the_terms( $post->ID, 'category' ) ): ?>
					<div class="banner--category">
						<?php foreach ( $category as $cat ): ?>
							<?php echo $cat->name; ?>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>