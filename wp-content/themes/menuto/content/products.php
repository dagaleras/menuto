<div class="product--card">
	<a href="<?php echo get_post_meta( $post->ID, 'product-text', true ); ?>" class="product--card-link" target="_blank">
		<?php if( has_post_thumbnail() ) : ?>
			<figure class="product--card-figure">
				<img class="product--card-image" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
			</figure>
		<?php endif; ?>
		<div class="product--card-content">
			<h3 class="product--card-title"><?php the_title(); ?></h3>
			<div class="product--card-text"><?php echo wp_trim_words( do_shortcode($post->post_content), 25, '...' ); ?></div>
		</div>
	</a>
</div>