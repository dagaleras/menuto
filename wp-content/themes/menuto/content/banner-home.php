<?php global $post, $show_content, $show_category; ?>
<div class="banners">
	<div class="banners--area">
		<?php
			$args = array(
		        'posts_per_page'      => 1,                     // Máximo de 5 artigos
		        'no_found_rows'       => true,                  // Não conta linhas
		        'post_status'         => 'publish',             // Somente posts publicados
		        'ignore_sticky_posts' => true,                  // Ignora posts fixos
		        'orderby'             => 'meta_value_num',      // Ordena pelo valor da post meta
		        'meta_key'            => 'data_most_read_post', // A nossa post meta
		        'order'               => 'DESC',                // Ordem decrescente
			);

			$featured = new WP_Query( apply_filters( 'most_read_posts_args', $args ) );		 	
		?>
		<?php if ($featured->have_posts()): ?>
			<div class="banners--area-loop">
				<?php while($featured->have_posts()): $featured->the_post();  ?>
					<div class="banner">
						<div class="mask"></div>
						<?php if( has_post_thumbnail() ) : ?>
							<figure class="banner--figure">
								<img class="banner--figure-img" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
							</figure>
						<?php endif; ?>	
						<div class="content">
							<a href="<?php echo esc_url(get_permalink()); ?>" class="banner--link">
								<h2 class="banner--title"><?php the_title(); ?></h2>
								<div class="banner--paragraph">
									<div class="arrow-up"></div>
									<div class="banner--paragraph-summary">
										<?php echo wp_trim_words( do_shortcode($post->post_content), 20, '...' ); ?>
									</div>
									<button class="banner--paragraph-button"><?php _e('leia mais'); ?></button>
								</div>
							</a>
						</div>
						<?php if( $show_category ): ?>
							<?php  
								$category = get_the_terms( $post->ID, 'category' );
								$products = get_the_terms( $post->ID, 'product_cat' );    					
							?>
							<?php if($category): ?>
								<div class="banner--category">
									<?php foreach ( $category as $cat ): ?>
										<?php echo $cat->name; ?>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
							<?php if($products): ?>
								<div class="banner--category">
									<?php foreach ( $products as $cat ): ?>
										<?php echo $cat->name; ?>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
		<?php endif; ?>
	</div>
</div>