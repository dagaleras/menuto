<div class="featured--card">
	<a href="<?php echo esc_url(get_permalink()); ?>" class="featured--card-link">
		<?php if( has_post_thumbnail() ) : ?>
			<figure class="featured--card-figure">
				<img class="featured--card-image" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
			</figure>
		<?php endif; ?>

		<div class="featured--card-content">
			<div class="entry-content">
				<div class="arrow-up"></div>
				<h3 class="featured--card-title"><?php the_title(); ?></h3>
				
				<p class="featured--card-text"><?php echo wp_trim_words( do_shortcode($post->post_content), 15, '...' ); ?></p>

				<?php if( $category = get_the_terms( $post->ID, 'category' ) ): ?>
					<span class="featured--card-category">
						<?php foreach ( $category as $cat ): ?>
							<?php echo $cat->name; ?>
						<?php endforeach; ?>
					</span>
				<?php endif; ?>

			</div>
		</div>	
	</a>
</div>