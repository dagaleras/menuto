<div class="instagram">
	<div class="instagram--area">
		<?php if( get_field('home__instagram_title', get_the_ID()) ) : ?>
			<h3 class="instagram--area-title"><div class="tile"><?php the_field('home__instagram_title', get_the_ID()); ?></div></h3>
		<?php endif ?>
		<?php if( get_field('home__instagram_text', get_the_ID()) ) : ?>
			<div class="instagram--area-text"><?php the_field('home__instagram_text', get_the_ID()); ?></div>
		<?php endif ?>		
		<?php 

		?>
		<div class="instagram--area-loop">								
			<ul id="instagramFeed" class="row"></ul>
		</div>

	</div>
</div>