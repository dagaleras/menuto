<aside class="aside">
	
	<div id="asideShare" class="aside--shares">
		<div id="counter" class="aside--shares-count">
			<?php echo facebook_share( get_permalink(get_the_ID()) ); ?>	
		</div>
		<div class="aside--shares-label"><?php echo esc_html( __( 'COPARTILHAMENTOS', 'menuto' ) ); ?></div>
		<div class="aside--shares-icons">
			<ul>
				<li>
					<?php $fb_image = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ), 'facebook'); ?>
					<a href="<?php echo get_permalink(); ?>" data-image="<?php echo $fb_image[0]; ?>" data-title="<?php echo get_the_title();?>" data-desc="<?php echo wp_trim_words( do_shortcode(get_the_content()), 25, '...' ); ?>" class="get-shares"><i class="fa fa-facebook"></i></a>
				</li>
			</ul>
		</div>
	</div>

	<div id="asideReading" class="aside--reading">
		<header class="aside--reading--header">
			<h3 class="header__title"><?php echo esc_html( __( 'Lista de leitura', 'menuto' ) ); ?></h3>
		</header>
		<div id="reading" class="aside--reading-list">
			<ul>
				<li id="<?php echo $post->ID; ?>" class="active">	
					<a href="#<?php echo $post->ID; ?>">			
						<div class="progress"><span>1</span></div>
						<div class="post-title"><?php echo get_the_title(get_the_ID()); ?></div>
					</a>					
				</li>
				<?php
			        global $post;
			        $current_post = $post;

			        for($i = 1; $i <= 2; $i++):
				        $post = get_previous_post();
				        setup_postdata($post);
				        if($post):
					        $n = $i+1;
					        echo '<li id="'.$post->ID.'">';
					        	echo '<a href="#'.$post->ID.'">';
						        	echo '<div class="progress"><span>'.$n.'</span></div>';
						        	echo '<div class="post-title">' . get_the_title() . '</div>';
						        echo '</a>';
					        echo '</li>';
					    endif;

			        endfor;
			        $post = $current_post;
			    ?>
			</ul>
		</div>
	</div>

	<div id="asideNewsletter" class="aside--newsletter">
		<header class="aside--newsletter--header">
			<?php if( get_field('sidebar__newsletter_titulo', 'option') ): ?>
				<h3 class="header__title"><?php the_field('sidebar__newsletter_titulo', 'option'); ?></h3>
			<?php endif; ?>
			<?php if( get_field('sidebar__newsletter_texto', 'option') ): ?>
				<p class="header__text"><?php the_field('sidebar__newsletter_texto', 'option'); ?></p>
			<?php endif; ?>
		</header>		
		<div class="aside--newsletter-form dark">
			<div class="form">
				<?php get_template_part( 'content/newsletter' ); ?>
			</div>
		</div>
	</div>

	<button id="open-sidebar" class="aside--button"><span><?php _e( 'recolher aba' ); ?></span><i class="arrow open"></i></button>
</aside>