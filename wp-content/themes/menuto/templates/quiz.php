<?php
/**
 * Template Name: Quiz
 */
?>

<?php get_header(); ?>

	<main role="main" id="main" class="site-main login">
		<?php if ( have_posts() ) : ?> 

	        <?php while ( have_posts() ) : the_post(); ?>
				
				<section class="content">
					<div class="quizcontainer">
							<h1 class="page__title"><?php the_title() ?></h1>
							<?php if( is_user_logged_in() ): ?>
								<?php 
									$current_user = wp_get_current_user();
								    $category = get_field( 'user__categoria', 'user_'.$current_user->ID);
								    $rotina = get_field( 'user__rotina', 'user_'.$current_user->ID);

								    echo 'Username: ' . $current_user->user_login . '<br />';
								    echo 'User email: ' . $current_user->user_email . '<br />';
								    echo 'User display name: ' . $current_user->display_name . '<br />';
								    echo 'User ID: ' . $current_user->ID . '<br />';
								    if($category[0]){
								    	echo 'User Category: ' . $category[0]->name . '<br />';
								    }
									$acf_key_cats = "field_5a5ce32b14683";
									$acf_cats    = get_field_object($acf_key_cats);

									$acf_key_activities = "field_5a5ce36d14684";
									$acf_activities    = get_field_object($acf_key_activities);

									$acf_key_model = "field_5a5ce39a14685";
									$acf_model    = get_field_object($acf_key_model);
								?>
							
								<div class="formquiz">
									<form id="quiz">
										<fieldset>
											<legend><?php echo $acf_cats['label']; ?></legend>
											<div class="f_checkboxe">
												<ul>
													<?php $categories = get_terms( $acf_cats['taxonomy'], 'hide_empty=0&exclude=1' ); ?>
													<?php foreach ($categories as $key => $value) : ?>
														<li>
															<input type="checkbox" name="<?php echo $acf_cats['name']; ?>" id="<?php echo $acf_cats['name']; ?>" value="<?php echo $value->term_id; ?>" <?php if($category[0]){ checked( $category[0]->term_id, $value->term_id ); } ?> />
															<label for="<?php echo $value->slug; ?>"><?php echo $value->name; ?></label>
														</li>
													<?php endforeach; ?>												
												</ul>
											</div>
										</fieldset>
										<fieldset>
											<legend><?php echo $acf_activities['label']; ?></legend>
											<div class="f_radio">
												<ul>
													<?php foreach ($acf_activities['choices'] as $key => $value) : ?>
														<li>
															<img src="" alt="" />
															<input type="checkbox" name="<?php echo $acf_activities['name']; ?>" id="<?php echo $acf_activities['name']; ?>" value="<?php echo $key; ?>" />
															<label for="<?php echo $value; ?>"><?php echo $value; ?></label>
														</li>
													<?php endforeach; ?>
												</ul>
											</div>
										</fieldset>
										<fieldset>
											<legend><?php echo $acf_model['label']; ?></legend>
											<div class="f_textarea">
												<textarea name="<?php echo $acf_model['name']; ?>" id="<?php echo $acf_model['name']; ?>">
												</textarea>
											</div>
										</fieldset>

										<?php wp_nonce_field( 'ajax-quiz-nonce', 'security' ); ?>

										<input type="hidden" name="user_id" id="user_id" value="<?php echo $current_user->ID; ?>" />

				                        <div id="carregando">
				                        	<p class="status"></p>
				                        </div>
										<input type="submit" value="Enviar" />
									</form>
								</div>

							<?php else: ?>
								<p></p>
							<?php endif; ?>

						</div>
					</div>
				</section>	
			        				
			<?php endwhile; ?>

	    <?php else : ?>

	        <?php get_template_part( 'content', 'none' ); ?>

	    <?php endif; ?>
	</main><!-- .site-main -->

<?php get_footer(); ?>