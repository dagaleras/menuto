<?php
/**
 * Template Name: Contact
 */
?>

<?php get_header(); ?>

	<main role="main" id="main" class="site-main page contact">
		<?php if ( have_posts() ) : ?> 

	        <?php while ( have_posts() ) : the_post(); ?>
				
				<?php if( has_post_thumbnail() ): ?>
					<section class="container-fluid">
						<div class="banners">
							<div class="banners--area">
								<div class="banners--area-loop">
									<div class="intro">
										<figure class="intro--figure">
											<img class="intro--figure-img" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
											<h1 class="intro--figure-title"><?php the_title(); ?></h1>
										</figure>
									</div>
								</div>
							</div>
						</div>
					</section>	
				<?php endif; ?>

				<section class="content">
					<div class="container-small">
						<div class="content--area">
							<div class="content--area-loop">
								<div class="entry">
									<div class="entry--content">
										
										<div class="social">
											<ul>
												<?php
						                            $html = "";
						                            $links = get_field('footer__redes_sociais', 'option');

						                            if(isset($links) && strlen($links) > 0) {
						                                $links_array = explode("\n", $links);
						                                foreach($links_array as $link){
						                                    $html .= '
						                                        <li>
						                                            <a href="'.trim($link).'" target="_blank" title="'.get_social_network_sherad_id($link).'" id="'.get_social_network_sherad_id($link).'">
						                                                <i class="fa fa-'.get_social_network_sherad_id($link).'"></i>
						                                            </a>
						                                        </li>
						                                    ';
						                                }
						                            }
						                            echo $html;
						                        ?>
											</ul>
										</div>

										<?php the_content(); ?>

										<div class="form form-default">
											<?php echo do_shortcode( '[contact-form-7 id="146" title="Contato"]' ); ?>
										</div>

									</div>
								</div>
								
							</div>
						</div>
					</div>
				</section>	
			        				
			<?php endwhile; ?>

	    <?php else : ?>

	        <?php get_template_part( 'content', 'none' ); ?>

	    <?php endif; ?>
	</main><!-- .site-main -->

<?php get_footer(); ?>