<?php
/**
 * Template Name: Sucesso
 */
?>

<?php get_header(); ?>

	<main role="main" id="main" class="site-main page success">
		<?php if ( have_posts() ) : ?> 

	        <?php while ( have_posts() ) : the_post(); ?>
				
				<section class="content">
					<div class="container-small">
						<div class="content--area">
							<div class="content--area-loop">
								<div class="entry">
									<div class="entry--content">
										<?php the_content(); ?>
									</div>
								</div>
								
							</div>

						</div>
					</div>				
					<div class="area--posts">
						<?php
							$args = array(
								'post_type'           => 'post',
								'posts_per_page'      => 4,
						        'no_found_rows'       => true,                  // Não conta linhas
						        'post_status'         => 'publish',             // Somente posts publicados
						        'ignore_sticky_posts' => true,                  // Ignora posts fixos
						        'orderby'             => 'meta_value_num',      // Ordena pelo valor da post meta
						        'meta_key'            => 'data_most_read_post', // A nossa post meta
						        'order'               => 'DESC',                 // Ordem decrescente
							);
							$othersPosts = new WP_Query($args);	 	
						?>
						<div class="row">
							<?php while( $othersPosts->have_posts() ) : $othersPosts->the_post(); ?>
								<div class="col-md-4">
									<div class="post">
										<div class="post--card">
											<a href="<?php echo esc_url(get_permalink()); ?>" class="post--card-link">
												<?php if( has_post_thumbnail() ) : ?>
													<figure class="post--card-figure">

														<img class="post--card-image" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
														
														<?php if( $category = get_the_terms( $post->ID, 'category' ) ): ?>
															<span class="post--card-category">
																<?php foreach ( $category as $cat ): ?>
																	<?php echo $cat->name; ?>
																<?php endforeach; ?>
															</span>
														<?php endif; ?>

													</figure>
												<?php endif; ?>
												<div class="post--card-content">
													<h3 class="post--card-title"><?php the_title(); ?></h3>
												</div>
											</a>
										</div>
									</div>
								</div>
							<?php endwhile; wp_reset_postdata(); ?>
						</div>
					</div>

				</section>	
			        				
			<?php endwhile; ?>

	    <?php else : ?>

	        <?php get_template_part( 'content', 'none' ); ?>

	    <?php endif; ?>
	</main><!-- .site-main -->

<?php get_footer(); ?>