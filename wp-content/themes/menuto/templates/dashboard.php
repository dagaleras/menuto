<?php
/**
 * Template Name: Dashboard
 */
?>

<?php get_header('dashboard'); ?>
	
	<?php $user = wp_get_current_user(); ?>

	<?php if ( in_array( 'administrator', (array)$user->roles ) || in_array( 'analyst', (array)$user->roles ) ) : ?>

		<main role="dashboard" id="dashboard" class="dashboard-main"></main>
	
	<?php else : ?>			
		<h3>Você não tem permissão para acessar essa área.</h3>
	<?php endif; ?>

<?php get_footer('dashboard'); ?>