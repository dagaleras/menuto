<?php
/**
 * Template Name: Login
 */
?>

<?php get_header(); ?>

	<main role="main" id="main" class="site-main login">
		<?php if ( have_posts() ) : ?> 

	        <?php while ( have_posts() ) : the_post(); ?>
				
				<section class="content">
					<div class="logincontainer">
							<h1 class="page__title"><?php the_title() ?></h1>
							<?php if( !is_user_logged_in() ): ?>
								<?php 
									$page_showing = basename($_SERVER['REQUEST_URI']);

									if (strpos($page_showing, 'failed') !== false) {
										echo '<p class="error-msg">E-mail inválido e/ou senha.</p>';
									}
									elseif (strpos($page_showing, 'blank') !== false ) {
										echo '<p class="error-msg">E-mail e/ou senha em branco.</p>';
									}
									elseif (strpos($page_showing, 'logout') !== false ) {
										echo '<p class="error-msg">Você foi desconectado com sucesso!</p>';
									}
								?>
								<div class="form">
									<form id="login">
										<div class="inputText">
										    <input type="text" class="input-text" name="username" id="l_username" autocomplete="off" />
											<div class="floating-label">Usuário</div>
										</div>
										<div class="inputText">
										    <input class="input-text" type="password" name="password" id="l_password" autocomplete="off" />
											<div class="floating-label">Senha</div>
										</div>
										<div class="inputSubmit">
										   <input type="submit" class="input-submit" name="login" value="<?php esc_attr_e( 'Login' ); ?>"/>
										</div>
			                            
				                        <a class="lost" href="<?php echo wp_lostpassword_url(); ?>"><?php _e('Esqueceu a senha?'); ?></a>

				                        <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>

				                        <div id="carregando">
				                        	<p class="status"></p>
				                        </div>
				                    </form>
				                </div>
							<?php else: ?>
								<p>Você já está logado, <a href="<?php echo wp_logout_url( get_permalink() ); ?>">Clique aqui</a> para sair</p>
							<?php endif; ?>

						</div>
					</div>
				</section>	
			        				
			<?php endwhile; ?>

	    <?php else : ?>

	        <?php get_template_part( 'content', 'none' ); ?>

	    <?php endif; ?>
	</main><!-- .site-main -->

<?php get_footer(); ?>