<?php
/**
 * Template Name: Cadastro
 */
?>

<?php get_header(); ?>

	<main role="main" id="main" class="site-main login">
		<?php if ( have_posts() ) : ?> 

	        <?php while ( have_posts() ) : the_post(); ?>
				
				<section class="content">
					<div class="logincontainer">
							<h1 class="page__title"><?php the_title() ?></h1>
							<div class="form">
								<div id="error-message"></div>

								<form id="register">
									<div class="row">
										<div class="col-md-6">
											<div class="inputText">
											    <input type="text" class="input-text" name="fname" id="fname" autocomplete="off" />
												<div class="floating-label">Primeiro nome</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="inputText">
											    <input type="text" class="input-text" name="lname" id="lname" autocomplete="off" />
												<div class="floating-label">Último nome</div>
											</div>
										</div>
									</div>
									<div class="inputText">
									    <input type="email" class="input-text" name="email" id="email" autocomplete="off" />
										<div class="floating-label">E-mail</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="inputText">
											    <input class="input-text" type="password" name="pwd" id="pwd" autocomplete="off" />
												<div class="floating-label">Senha</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="inputText">
											    <input class="input-text" type="password" name="cpwd" id="cpwd" autocomplete="off" />
												<div class="floating-label">Confirmar Senha</div>
											</div>
										</div>
									</div>
									<div class="inputSubmit">
									   <input type="submit" class="input-submit" name="login" value="<?php esc_attr_e( 'Cadastre-se' ); ?>"/>
									</div>
		                            
			                        <?php wp_nonce_field( 'ajax-register-nonce', 'security' ); ?>

			                        <div id="carregando">
			                        	<p class="status"></p>
			                        </div>
			                    </form>
			                </div>
						</div>
					</div>
				</section>	
			        				
			<?php endwhile; ?>

	    <?php else : ?>

	        <?php get_template_part( 'content', 'none' ); ?>

	    <?php endif; ?>
	</main><!-- .site-main -->

<?php get_footer(); ?>