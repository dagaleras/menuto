<?php
/**
 * Template Name: Social
 */
?>

<?php get_header(); ?>
	<main role="main" id="main" class="site-main login">	
		<div class="container">
					
			<button class="button" data-modal="modal" data-target="#login2">Login</button>
			<button class="button" data-modal="modal" data-target="#loginemail">Login E-mail</button>
			<button class="button" data-modal="modal" data-target="#lostpassword">Recuperar de senha</button>
			<button class="button" data-modal="modal" data-target="#register">Cadastro</button>
			<button class="button" data-modal="modal" data-target="#gender">Sexo</button>
			<button class="button" data-modal="modal" data-target="#quiz">Quiz</button>

			<div id="status"></div>
		
		</div>
	</main>
<?php get_footer(); ?>