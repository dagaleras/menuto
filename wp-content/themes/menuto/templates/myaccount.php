<?php
/**
 * Template Name: Minha Conta
 */
?>

<?php get_header(); ?>

	<main role="main" id="main" class="site-main myaccount">
		<?php if ( have_posts() ) : ?> 

	        <?php while ( have_posts() ) : the_post(); ?>
				
				<div class="myaccount-wrapper">
					<div class="myaccount--container">
						<?php if(is_user_logged_in()) : ?>
							<?php 
								global $wpdb, $wp;
								global $current_user; wp_get_current_user();
							?>							

							<h1 class="page__title"><?php the_title() ?></h1>
							
							<section class="myaccount--content myaccount--quiz">
								<h2 class="myaccount--subtitle"><?php echo __( 'Quiz', 'menuto' ); ?></h2>
								<p class="myaccount--text"><?php echo __( 'Quer redefinir sua curadoria de conteúdo? Refaça o quiz!', 'menuto' ); ?></p>
								<button class="myaccount--button" data-modal="modal" data-target="#quiz"><?php echo esc_html( __( 'RESPONDER O QUIZ NOVAMENTE', 'menuto' ) ); ?></button>
							</section>

							<section class="myaccount--content myaccount--accounts">
								<h2 class="myaccount--subtitle"><?php echo __( 'Contas', 'menuto' ); ?></h2>
								<p class="myaccount--text"><?php echo __( 'Conecte ou desconecte sua conta Me’nuto das suas redes sociais.', 'menuto' ); ?></p>
								
								<?php if(class_exists('NextendSocialLogin', false)){ ?>
									<div class="myaccount--socialconnect">
										<?php
											echo NextendSocialLogin::renderLinkAndUnlinkButtons($heading = false);
											echo '<span>'.__( 'ou', 'menuto' ).'</span>';
										?>
									</div>
								<?php } ?>

								<div class="myaccount--editemail">
									<form id="form-editemail" method="post">
							            <div class="form-group">
							                <input type="text" name="youremail" id="youremail" class="form-control required" value="<?php echo $current_user->data->user_email; ?>" required />
							                <label class="form-control-placeholder" for="youremail"><?php echo __('E-mail', 'menuto') ?></label>
							                <div class="msg"></div>
							            </div>
						                <a href="javascript:void(0)" class="myaccount--button editemail"><?php echo __('EDITAR E-MAIL', 'menuto') ?></a>

						                <div class="res"></div>
										
						                <input type="hidden" name="edit_user_id" id="edit_user_id" value="<?php echo $current_user->ID; ?>" />

						                <div id="editEmailUserConfirm" class="modal" tabindex="-1" role="dialog">
										  	<div class="modal-content">
										  		<div class="wrapper">
											  		<div class="modal-header">
														<h5 class="modal-title"><?php echo  __( 'Você deseja usar o <span></span> para fazer login?', 'menuto' ); ?></h5>				
													</div>
													<div class="modal-body">
														<a href="javascript:void(0)" class="button--no" data-dismiss="modal" aria-label="Close"><?php echo __('Não', 'menuto') ?></a>
														<input type="submit" name="submit" class="button--yes" value="<?php esc_attr_e('Sim','menuto') ?>" />

														<div class="status"></div>
													</div>
												</div>
										  	</div>
										</div>
						                <?php wp_nonce_field( 'ajax-editemail-nonce', 'editemail' ); ?>
						            </form>	
								</div>
								
							</section>

							<section class="myaccount--content myaccount--password">
								<h2 class="myaccount--subtitle"><?php echo __( 'Senhas', 'menuto' ); ?></h2>
								<p class="myaccount--text"><?php echo __( 'Redefina a sua senha.', 'menuto' ); ?></p>
								
								<form id="form-changepassword" method="post">
						            <div class="form-group">
						                <input type="password" name="currentpassword" id="currentpassword" class="form-control password-toggle required" required />
						                <label class="form-control-placeholder" for="currentpassword"><?php esc_attr_e('Sua senha atual*', 'menuto') ?></label>
					                	<span toggle=".password-toggle" class="field-icon toggle-password"></span>
						                <em><?php echo __( 'Mínimo de 6 caracteres', 'menuto' ); ?></em>
						                <div class="msg"></div>
						            </div>
						            <div class="form-group">
						                <input type="password" name="newpassword" id="newpassword" class="form-control password-toggle required" required />
						                <label class="form-control-placeholder" for="newpassword"><?php esc_attr_e('Sua nova senha*', 'menuto') ?></label>
					                	<span toggle=".password-toggle" class="field-icon toggle-password"></span>
						                <em><?php echo __( 'Mínimo de 6 caracteres', 'menuto' ); ?></em>
						                <div class="msg"></div>
						            </div>
	
									<a href="javascript:void(0)" class="myaccount--button myaccount--changepassword"><?php echo __( 'REDEFENIR SENHA', 'menuto' ); ?></a>

									<div class="statususer"></div>

					                <div id="moda_changepassword" class="modal" tabindex="-1" role="dialog">
									  	<div class="modal-content">
									  		<div class="wrapper">
										  		<div class="modal-header">
													<h5 class="modal-title">Confirme a nova senha para alterar.</h5>				
												</div>
												<div class="modal-body">
													<div class="form-group">
										                <input type="password" name="c_newpassword" id="c_newpassword" class="form-control password-toggle required" required />
										                <label class="form-control-placeholder" for="c_newpassword"><?php esc_attr_e('Confirmar nova senha*', 'menuto') ?></label>
									                	<span toggle=".password-toggle" class="field-icon toggle-password"></span>
										                <em><?php echo __( 'Mínimo de 6 caracteres', 'menuto' ); ?></em>
										                <div class="msg"></div>
										            </div>																								
					                				<input type="submit" name="submit" id="submitpassword" class="myaccount--button" value="<?php esc_attr_e('CONFIRMAR','menuto') ?>" />

													<div class="status"></div>
												</div>
												<div class="modal-footer">
													<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close"></button>
												</div>
											</div>
									  	</div>
									</div>

					                <input type="hidden" name="changepassword_userid" id="changepassword_userid" value="<?php echo $current_user->ID; ?>" />
					                <?php wp_nonce_field( 'ajax-changepassword-nonce', 'changepassword' ); ?>
					            </form>	
								
							</section>

							<section class="myaccount--content myaccount--delete">
								<a href="javascript:void(0)" data-modal="modal" data-target="#deleteaccount" class="myaccount--delete-button"><?php echo __( 'APAGAR CONTA', 'menuto' ); ?></a>

								<div id="deleteaccount" class="modal" tabindex="-1" role="dialog">
								  	<div class="modal-content">
								  		<div class="wrapper">
									  		<div class="modal-header">
												<h5 class="modal-title">Poxa, tem certeza que você quer<br />deletar sua conta?</h5>				
											</div>
											<div class="modal-body">
												<p>Atenção: Esta ação não pode ser desfeita.</p>
												<a href="javascript:void(0)" class="myaccount--delete-confirm" data-id="<?php echo $current_user->ID; ?>" data-nonce="<?php echo wp_create_nonce('wp_delete_user_nonce') ?>">SIM, QUERO DELETAR A MINHA CONTA</a>
												<button class="myaccount--button" data-dismiss="modal" aria-label="Close"><?php echo esc_html( __( 'NÃO, QUERO CONTINUAR ', 'menuto' ) ); ?></button>

												<div class="status"></div>
											</div>
											<div class="modal-footer">
												<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close"></button>
											</div>
										</div>
								  	</div>
								</div>

							</section>
						
						<?php else : ?> 
							<p><?php echo __( 'Você tem que estar registrado e logado para acessar está página.', 'menuto' ); ?></p>  
	                        <button id="button-login" data-modal="modal" data-target="#login">                                   
	                            <span><?php echo esc_html( __( 'Login', 'menuto' ) ); ?></span>
	                            <figure><i class="i-login"></i></figure>                                    
	                        </button>
	                    <?php endif; ?>
					</div>
				</div>	
			        				
			<?php endwhile; ?>

	    <?php else : ?>

	        <?php get_template_part( 'content', 'none' ); ?>

	    <?php endif; ?>
	</main><!-- .site-main -->

<?php get_footer(); ?>