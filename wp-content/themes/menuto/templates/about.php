<?php
/**
 * Template Name: Sobre
 */
?>

<?php get_header(); ?>

	<main role="main" id="main" class="site-main page about">
		<?php if ( have_posts() ) : ?> 

	        <?php while ( have_posts() ) : the_post(); ?>
				
				<?php if( has_post_thumbnail() ): ?>
					<section class="container-fluid">
						<div class="page--banner">
							<div class="banners">
								<div class="banners--area">
									<div class="banners--area-loop">
										<div class="intro">
											<figure class="intro--figure">
												<img class="intro--figure-img" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
												<h1 class="intro--figure-title"><?php the_title(); ?></h1>
											</figure>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				<?php endif; ?>

				<section class="content">
					<div class="container-small">
						<div class="page--entry">
							<?php the_content(); ?>
						</div>
					</div>
				</section>	
			        				
			<?php endwhile; ?>

	    <?php else : ?>

	        <?php get_template_part( 'content', 'none' ); ?>

	    <?php endif; ?>
	</main><!-- .site-main -->

<?php get_footer(); ?>