<?php get_header(); ?>
	
	<main role="main" id="main" class="site-main search">

		<section class="container-fluid">					
			<div class="posts">
				<div class="posts--area listing">
					<div class="posts--area-loop container">
						<?php if (have_posts()) : ?>	
							<h2 class="search--title">Resultados para <span><?php echo get_search_query() ?></span></h2>										
							<div class="row">
								<?php 
									$rowCount  = 0;
									$numOfCols = 3;
								?>
								<?php while( have_posts() ) : the_post(); ?>
									<div class="col col-3">
										<div class="post">
											<div class="post--card">
												<a href="<?php echo esc_url(get_permalink()); ?>" class="post--card-link">
													<?php if( has_post_thumbnail() ) : ?>
														<figure class="post--card-figure">

															<img class="post--card-image" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
															
															<?php if( $category = get_the_terms( $post->ID, 'category' ) ): ?>
																<span class="post--card-category">
																	<?php foreach ( $category as $cat ): ?>
																		<?php echo $cat->name; ?>
																	<?php endforeach; ?>
																</span>
															<?php endif; ?>

														</figure>
													<?php endif; ?>
													<div class="post--card-content">
														<h3 class="post--card-title"><?php the_title(); ?></h3>
													</div>
												</a>
											</div>
										</div>
									</div>
									<?php
									    $rowCount++;
									    if($rowCount % $numOfCols == 0) echo '</div><div class="row last">';
									?>
								<?php endwhile; ?>
							</div>
							<div class="row">
								<?php echo get_pagination_links(); ?>
							</div>
						<?php else : ?>	
							<div class="row">
								<div class="col-1">
									<h3 class="search--title"> <?php printf(__( 'Nenhum artigo encontrado com a palavra <span>%1$s</span> ', 'menuto' ), get_search_query() );?> </h3>
									<p class="search--text">Fizemos uma pequena seleção de artigos que talvez possam te interessar:</p>
								</div>
							</div>
							<?php
								$args = array(
									'post_type'           => 'post',
									'posts_per_page'      => 3,
							        'no_found_rows'       => true,                  // Não conta linhas
							        'post_status'         => 'publish',             // Somente posts publicados
							        'ignore_sticky_posts' => true,                  // Ignora posts fixos
							        'orderby'             => 'meta_value_num',      // Ordena pelo valor da post meta
							        'meta_key'            => 'data_most_read_post', // A nossa post meta
							        'order'               => 'DESC',                 // Ordem decrescente
								);
								$othersPosts = new WP_Query($args);	 	
							?>
							<div class="row">
								<?php while( $othersPosts->have_posts() ) : $othersPosts->the_post(); ?>
									<div class="col col-3">
										<div class="post">
											<div class="post--card">
												<a href="<?php echo esc_url(get_permalink()); ?>" class="post--card-link">
													<?php if( has_post_thumbnail() ) : ?>
														<figure class="post--card-figure">

															<img class="post--card-image" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
															
															<?php if( $category = get_the_terms( $post->ID, 'category' ) ): ?>
																<span class="post--card-category">
																	<?php foreach ( $category as $cat ): ?>
																		<?php echo $cat->name; ?>
																	<?php endforeach; ?>
																</span>
															<?php endif; ?>

														</figure>
													<?php endif; ?>
													<div class="post--card-content">
														<h3 class="post--card-title"><?php the_title(); ?></h3>
													</div>
												</a>
											</div>
										</div>
									</div>
								<?php endwhile; wp_reset_postdata(); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>		

	</main><!-- .site-main -->

<?php get_footer(); ?>