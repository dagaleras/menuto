<?php
class WP_Dashboard_Overview {
    
    /**
	 * Action event ajax.
	 *
	 * @var string
	 */
    const ACTION = 'load_overview';
	
	/**
	 * Nonce ajax referer.
	 *
	 * @var string
	 */
    const NONCE  = 'nonce-overview-ajax';

    /**
	 * Counter ID.
	 *
	 * @var string
	 */
	const FB_id = 'facebook';

	/**
	 * API URL.
	 *
	 * @var string
	 */
	const FB_api_url = 'https://graph.facebook.com';

	/**
	 * API Version.
	 *
	 * @var string
	 */
	const FB_api_version = 'v3.0';

 
    public static function register() {
        $handler = new self();
 
        add_action( 'wp_ajax_' . self::ACTION, array( $handler, 'build' ) );
        add_action( 'wp_ajax_nopriv_' . self::ACTION, array( $handler, 'build' ) );
        add_action( 'wp_head', array( $handler, 'register_script' ) );
    } 
 
    public function register_script() {
        wp_localize_script( 'dashboard', 'overview_ajax', $this->get_ajax_data() );
    }
 
    private function get_ajax_data() {
        return array(
        	'url'    => admin_url( 'admin-ajax.php' ),
            'action' => self::ACTION,
            'nonce'  => wp_create_nonce( WP_Dashboard_Overview::NONCE )
        );
    }
 
    private function get_post_id() {
        $post_id = 0;

        if (isset($_POST['post_id'])) {
            $post_id = absint( filter_var( $_POST['post_id'], FILTER_SANITIZE_NUMBER_INT ) );
        }
 
        return $post_id;
    }

    public function build() {
        check_ajax_referer( self::NONCE, 'nonce' );
 
		global $wpdb;
		$user = wp_get_current_user();

		$count_users = count_users();
		$count_posts = wp_count_posts();
		$count_terms = wp_count_terms( 'category', array( 'hide_empty' => true ) );
		$ip          = get_client_ip();

	    $args = array();

	    $args['overview'] = array(
			'total_users'               => $count_users['total_users'],
			'total_visitors'            => $this->get_number_total_visitors(),
			'unique_accesses'           => $this->get_number_total_unique_visitors(),
			'total_posts'               => $count_posts->publish,
			'total_categories'          => $count_terms,
			'get_facebook_fan_count'    => $this->get_facebook_fan_count(),
			'instagram_followers_count' => $this->get_instagram_followers_count(),
			'postMostPopular'           => $this->posts_most_popular(),
			'productsClicks'            => $this->porducts_most_clicks(),
			'recentsUsers'              => $this->get_users_recents(),
			'recentsLeads'              => $this->get_leads_recents(),
			'recentsClients'            => $this->get_clients_recents(),
			'recentsEvangelizers'       => $this->get_evangelizers_recents(),
			'postsMostAccessed'         => $this->posts_most_accessed(),
			'categoriesMostAccessed'    => $this->categories_most_accessed()
	    );

	    wp_send_json_success($args);
	    wp_die();
    }

    private function get_facebook_access_token( $settings ) {
		// How to get an access token: https://developers.facebook.com/docs/facebook-login/access-tokens/
		
		$url = self::FB_api_url . '/oauth/access_token';

		$access_token = wp_remote_get( $url, array(
            'timeout' => 60,
            'body'    => array(
				'client_id'     => sanitize_text_field( $settings['facebook_app_id'] ),
				'client_secret' => sanitize_text_field( $settings['facebook_app_secret'] ),
				'grant_type'    => 'client_credentials'				
            )
        ));

		if ( is_wp_error( $access_token ) || ( isset( $access_token['response']['code'] ) && 200 != $access_token['response']['code'] ) ) {
			return '';
		} else {
			$access_token = json_decode( $access_token['body'], true );
			return sanitize_text_field( $access_token['access_token'] );
		}
	}

	private function get_facebook_fan_count() {
		$settings = array(
			'facebook_active'     => true,
			'fanpage_name'        => 'meumenuto',
			'facebook_id'         => '1588355647896704',
			'facebook_app_id'     => '153233695441742',
			'facebook_app_secret' => '9beb13e1162f1882048a2132f3a6889c'
    	);

		$url = self::FB_api_url . '/' . self::FB_api_version . '/' . sanitize_text_field( $settings['facebook_id'] );

		$connection = wp_remote_get( $url, array(
            'timeout' => 60,
            'body'    => array(
				'access_token' => $this->get_facebook_access_token( $settings ),
				'fields'       => 'fan_count'
            )
        ));

        if ( is_wp_error( $connection ) || ( isset( $connection['response']['code'] ) && 200 != $connection['response']['code'] ) ) {
			return $connection['response']['message'];
        } else{
			$body   = wp_remote_retrieve_body( $connection );
			$result = json_decode( $body );

			return $result['fan_count'];
        }		
	}

    private function get_instagram_followers_count() {
    	global $wpdb;	
		
		$url = 'https://api.instagram.com/v1/users/self/';
	    $get = wp_remote_get( esc_url_raw( $url ), array(
            'body'    => array(
                'access_token'   => '6650873925.4b8470f.d252766a30c1447bbd9837b44886dad9'
            )   
        ));
		$body    = wp_remote_retrieve_body( $get );
		$results = json_decode( $body );


		$followed_by = $results->data->counts->followed_by;

	    if( $followed_by >= 1 ){
    		return $followed_by;
    	} else{
    		return 0;
    	}	    
    }

    private function get_number_total_visitors() {
    	global $wpdb;	
		
		$tbname   = $wpdb->prefix.'dash_visitors';

		$visitors = $wpdb->get_var("SELECT COUNT(*)  FROM {$tbname}");

	    if( $visitors >= 1 ){
    		return $visitors;
    	}

	    return 0;
    }

    private function get_number_total_unique_visitors() {
    	global $wpdb;	
		
		$tbname   = $wpdb->prefix.'dash_visitors';
		
		$sql = "SELECT ip, COUNT(*) AS num_ip FROM {$tbname} GROUP BY ip DESC";
		$visitors = $wpdb->get_results( $sql );
    	
    	if( $visitors >= 1 ){
    		return count( $visitors );
    	}

	    return 0;
    }

    private function posts_most_popular() {
    	global $wpdb;

    	$tbname = $wpdb->prefix.'dash_total_interaction';
    	$query  = $wpdb->get_results( "SELECT post_id, post_total FROM {$tbname}" );

        $posts    = array();
        $mostpost = array();

        foreach ( $query as $res ){
        	if( $res->post_id != NULL && $res->post_total != NULL ){
				$posts[$res->post_id] = $res->post_total;
	            arsort($posts);
        	}
        }
        $post_id = max($posts);

		$mostpost['post_id']    = array_search( $post_id, $posts );
		$mostpost['post_title'] = get_the_title( array_search( $post_id, $posts ) );
		$mostpost['post_link']  = get_permalink( array_search( $post_id, $posts ) );
		$mostpost['post_count'] = $post_id;

        return $mostpost;
    }

    private function porducts_most_clicks() {
    	global $wpdb;

    	$tbname = $wpdb->prefix.'dash_total_interaction';
    	$query  = $wpdb->get_results( "SELECT product_id, product_total FROM {$tbname} ORDER BY product_total DESC" );

        $products = array();

        foreach ( $query as $res ){
        	if( $res->product_id != NULL && $res->product_total != NULL ){
        		if( get_field( 'product__url', $res->product_id ) ){
		    		$link = get_field( 'product__url', $res->product_id );
		    	} else{
		    		$link = '#';
		    	}
        		array_push( $products, array(
        			'product_count' => $res->product_total,
        			'product_name'  => get_the_title( $res->product_id ),
        			'product_link'  => $link

        		));
        	}
        }

        return $products;
    }

    private function categories_most_accessed() {
    	global $wpdb;

    	$tbname = $wpdb->prefix.'dash_total_interaction';
    	$query  = $wpdb->get_results( "SELECT category_id, category_total FROM {$tbname} ORDER BY category_total DESC LIMIT 5" );

        $categories    = array();       

        foreach ( $query as $res ){
        	if( $res->category_id != NULL && $res->category_total != NULL ){
        		array_push( $categories, array(
        			'category_count' => $res->category_total,
        			'category_name'  => get_cat_name( $res->category_id ),
        			'category_link'  => get_category_link( $res->category_id )
        		));
        	}
        }

        return $categories;
    }

    private function posts_most_accessed() {
		global $post;

		if ( isset( $_POST['period'] ) ) {
            $period = $_POST['period'];
        } else{
			$period = 'all_time';
		}

		$posts = wmp_get_popular(
			array( 
				'limit'     => 5, 
				'post_type' => array( 'post' ),
				'range'     => $period
			) 
		);

		$period = array();

		foreach ( $posts as $post ):
			setup_postdata( $post );
			array_push( $period, array(
				'post_id'       => $post->ID, 
				'post_type'     => get_post_type( $post->ID ), 
				'post_category' => get_the_category( $post->ID ), 
				'post_href'     => get_permalink(),
				'post_title'    => get_the_title()
	        ));
		endforeach;

		if ( $period ) {
			return $period;
		} else {
			return 0;
		}
    }

    private function get_users_recents() {
    	global $wpdb;
    	
    	$data = $wpdb->get_results( "SELECT * FROM $wpdb->users ORDER BY user_registered DESC" );
    	$args = array();

        if( $data ){
	 		foreach ($data as $user) {
	    		array_push( $args, array(
					'userid'   => $user->ID,
					'username' => $user->display_name
		        ));		
	        }		    

			if( count( $args ) > 0 ){

				$total = array(
					'total'      => count( $args ),					
					'dataUsers' => $args
				);

			    return $total;
		    } else{
		    	return 0;
		    }
	    }
    }

    private function get_leads_recents() {
    	global $wpdb;

    	$tbname = $wpdb->prefix.'dash_leads';
    	
    	$data = $wpdb->get_results( "SELECT * FROM {$tbname} ORDER BY {$tbname}.data_created DESC" );
    	$args = array();

    	if( $data ){
	 		foreach ($data as $user) {
	    		$username = $user->first_name . ' ' . $user->last_name;
	    		array_push( $args, array(
					'user_id'  => $user->user_id,
					'username' => $username
		        ));		
	        }		    

			if( count( $args ) > 0 ){

				$total = array(
					'total'       => count( $args ),					
					'dataLeads' => $args
				);

			    return $total;
		    } else{
		    	return 0;
		    }
	    }
    }

    private function get_clients_recents(){
    	global $wpdb;
		
	    $tbname = $wpdb->prefix.'dash_leads';
	    $data   = $wpdb->get_results( "SELECT * FROM {$tbname} ORDER BY data_created DESC" );
		
		$args   = array();

	 	if( $data ){
	 		foreach ( $data as $key => $value ) {
				$orders = unserialize( $value->orders );
				foreach ( $orders['list'] as $item ) {	

					if( $item->status === 'invoiced' ){
						$username = $value->first_name . ' ' . $value->last_name;
			    		array_push( $args, array(
							'user_id'  => $value->user_id,
							'username' => $username
				        ));
			    	}				
		    	
		    	}
	 		}		    

			if( count( $args ) > 0 && count( $args ) < 2 ){

				$total = array(
					'total'       => count( $args ),					
					'dataClients' => $args
				);

			    return $total;
		    } else{
		    	return 0;
		    }
	    }	    
    }

    private function get_evangelizers_recents(){
    	global $wpdb;
		
	    $tbname = $wpdb->prefix.'dash_leads';
	    $data   = $wpdb->get_results( "SELECT * FROM {$tbname} ORDER BY data_created DESC LIMIT 5" );
		
		$args   = array();

	 	if( $data ){
	 		foreach ( $data as $key => $value ) {
	 			$fullname = $value->first_name . ' ' . $value->last_name;

				$orders = unserialize( $value->orders );
				foreach ( $orders['list'] as $item ) {
					if( $item->status === 'invoiced' ){					
			    		array_push( $args, array(
							'user_id'           => $value->user_id,
							'username'          => $fullname,
							'countItems'        => count( $orders['list'] ),
							'origin'            => $item->origin,
							'status'            => $item->status,
							'statusDescription' => $item->statusDescription,
							'items'             => $item->items,
							'countItems'        => count( $item->items ),
							'totalValue'        => $item->totalValue,
							'creationDate'      => mysql2date( 'd.m.Y g:i A', $item->creationDate )
				        ));
			    	}				
		    	
		    	}
	 		}

	 		$counter = count( $args );

	 		if( $counter > 2 ){
				$total = array(
					'total'            => $counter,					
					'dataEvangelizers' => $args
				);
			    return $total;
		    } else{
		    	return 0;
		    }
	    }
    }
}