<?php
class WP_Dashboard_Users {
    const ACTION = 'load_users';
    const NONCE  = 'nonce-users-ajax';
    const AJAX   = 'data_users_ajax';
 
    public static function register() {
        $handler = new self();
 
        add_action( 'wp_ajax_' . self::ACTION, array( $handler, 'build' ) );
        add_action( 'wp_ajax_nopriv_' . self::ACTION, array( $handler, 'build' ) );
        add_action( 'wp_head', array( $handler, 'register_script' ) );
    } 
 
    public function register_script() {
        wp_localize_script( 'dashboard', WP_Dashboard_Users::AJAX, $this->get_ajax_data() );
    }

    private function get_ajax_data() {
        return array(
        	'url'    => admin_url( 'admin-ajax.php' ),
            'action' => self::ACTION,
            'nonce'  => wp_create_nonce( WP_Dashboard_Users::NONCE )
        );
    }
 
    private function get_post_id() {
        $post_id = 0;

        if (isset($_POST['post_id'])) {
            $post_id = absint( filter_var( $_POST['post_id'], FILTER_SANITIZE_NUMBER_INT ) );
        }
 
        return $post_id;
    }


    public function build() {
        check_ajax_referer( self::NONCE, 'nonce' );
 
        global $wpdb;

	    $args = array();

		$user_id = $this->get_user_id();
	    $count_users = count_users();

	    $args['users'] = array(
			'total_users'         => $count_users[ 'total_users' ],
			'recents_users'        => $this->get_users_recents(),
			'posts_most_accessed' => $this->get_most_popular_post( $user_id ),
			'search_user'         => $this->get_info_from_user( $user_id ),
			'search_user_leads'   => $this->get_info_from_user_leads( $user_id )
	    );

	    wp_send_json_success( $args );
	    wp_die();
    }

    private function get_user_id() {
        $user_id = 0;

        if (isset($_POST['user_id'])) {
            $user_id = absint( filter_var( $_POST['user_id'], FILTER_SANITIZE_NUMBER_INT ) );
        }
 
        return $user_id;
    }

	private function get_users_recents() {
    	global $wpdb;
    	
    	$data = $wpdb->get_results( "SELECT * FROM $wpdb->users ORDER BY user_registered DESC" );
    	$users = array();

    	foreach ($data as $user) {
    		array_push( $users, array(
				'userid'   => $user->ID,
				'username' => $user->display_name
	        ));		
        }

        return $users; 

    }

	private function get_most_popular_post( $user_id ) {
    	global $wpdb;

		if ( $user_id ) {
			$tb_name = $wpdb->prefix.'dash_most_popular_post_user';
			$consult = $wpdb->get_results( "SELECT * FROM {$tb_name} WHERE user_id = {$user_id} ORDER BY {$tb_name}.post_count DESC" );

			$posts    = array();

		    foreach ( $consult as $value ) {
		    	$cat_id = unserialize( $value->post_category );
		        array_push( $posts, array(
					'post_id'       => $value->post_id, 
					'post_count'    => $value->post_count, 
					'post_title'    => get_the_title( $value->post_id ), 
					'post_link'     => get_permalink( $value->post_id ),
					'post_category' => get_cat_name( $cat_id )
		        ));
		    }
			return $posts;
		}
		return 0;
    }

    private function get_info_from_user( $user_id ) {
    	global $wpdb;	
		
		if ( $user_id ) {

			$args = array();

		    $tbname = $wpdb->prefix.'dash_leads';
		    $data   = $wpdb->get_results( "SELECT * FROM {$tbname} WHERE user_id = {$user_id}" );

			$meta   = get_user_by('id', $user_id);
		    $cat_id = get_field( 'user_categoria', 'user_'.$user_id );

			$args['leads']['profile']['meta']            = $meta;
			$args['leads']['profile']['user_avatar']     = get_avatar($user_id, 200);
			$args['leads']['profile']['display_name']    = $meta->data->display_name;
			$args['leads']['profile']['user_login']      = $meta->data->user_login;
			$args['leads']['profile']['user_email']      = $meta->data->user_email;
			$args['leads']['profile']['user_registered'] = mysql2date( 'd.m.Y g:i A', $meta->data->user_registered );
			$args['leads']['profile']['user_category']   = get_cat_name( $cat_id );
			$args['leads']['profile']['user_url']        = $meta->data->user_url;
			$args['leads']['profile']['user_genero']     = get_field( 'user_gender', 'user_'.$user_id );

			$args['leads']['profile']['api_fullname']     = $data[0]->first_name . ' ' . $data[0]->last_name;
			$args['leads']['profile']['leads']['api_email']        = $data[0]->email;
			$args['leads']['profile']['api_documentType'] = $data[0]->documentType;
			$args['leads']['profile']['api_document']     = $data[0]->document;
			$args['leads']['profile']['api_phone']        = $data[0]->phone;

			$orders = unserialize( $data[0]->orders );
			
			
		 	if( $orders['list'] ){
				
				$a = array();

			    foreach ( $orders['list'] as $item ) {					
			    	if( $item->status === 'invoiced' ){			    		
						array_push( $a, array(
							'countItems'        => count( $orders['list'] ),
							'origin'            => $item->origin,
							'status'            => $item->status,
							'statusDescription' => $item->statusDescription,
							'items'             => $item->items,
							'countItems'        => count( $item->items ),
							'totalValue'        => $item->totalValue,
							'creationDate'      => mysql2date( 'd.m.Y g:i A', $item->creationDate )
				        ));
			    	}
			    }

			    if( count( $a ) >= 2 ){			    	
					$args['leads']['type'] ='Evangelizador';
				}

				if( count( $a ) > 0 && count( $a ) < 2 ){			    	
					$args['leads']['type'] ='Cliente';		
				}

				if( count( $a ) <= 0 ){  		    	
					$args['leads']['type'] ='Leads';
				}
				
				$args['leads']['count_orders'] = count( $a );

				return $args;
		    } else{
				$args['leads']['type'] = "Usuário";
		    }

		    return $args;
		}

		return 0;

    }

    private function get_info_from_user_leads( $user_id ) {
    	global $wpdb;	
		
		if ( $user_id ) {

			$args = array();

		    $tbname = $wpdb->prefix.'dash_leads';
		    $data   = $wpdb->get_results( "SELECT * FROM {$tbname} WHERE user_id = {$user_id}" );

			$orders = unserialize( $data[0]->orders );

			$args = array();

		 	if( $orders['list'] ){

			    foreach ( $orders['list'] as $item ) {					
			    	if( $item->status === 'invoiced' ){			    		
						array_push( $args, array(
							'countItems'        => count( $orders['list'] ),
							'origin'            => $item->origin,
							'status'            => $item->status,
							'statusDescription' => $item->statusDescription,
							'items'             => $item->items,
							'countItems'        => count( $item->items ),
							'totalValue'        => $item->totalValue,
							'creationDate'      => mysql2date( 'd.m.Y g:i A', $item->creationDate )
				        ));
			    	}
			    }

			    if( count( $args ) >= 2 ){			    	
					$type ='Evangelizador';
				}

				if( count( $args ) > 0 && count( $args ) < 2 ){			    	
					$type ='Cliente';		
				}

				if( count( $args ) <= 0 ){  		    	
					$type ='Leads';
				}

				$leads = array(
					'type'         => $type,
					'count_orders' => count( $args ),
					'objects'       => $args
				);

			    return $leads;
		    }
		}

		return 0;
    }
 
}