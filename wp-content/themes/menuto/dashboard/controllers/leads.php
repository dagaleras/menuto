<?php
class WP_Dashboard_Leads {
    const ACTION = 'load_leads';
    const NONCE  = 'nonce-leads-ajax';
    const AJAX   = 'data_leads_ajax';
 
    public static function register() {
        $handler = new self();
 
        add_action( 'wp_ajax_' . self::ACTION, array( $handler, 'build' ) );
        add_action( 'wp_ajax_nopriv_' . self::ACTION, array( $handler, 'build' ) );
        add_action( 'wp_head', array( $handler, 'register_script' ) );
    } 
 
    public function register_script() {
        wp_localize_script( 'dashboard', WP_Dashboard_Leads::AJAX, $this->get_ajax_data() );
    }

    private function get_ajax_data() {
        return array(
        	'url'    => admin_url( 'admin-ajax.php' ),
            'action' => self::ACTION,
            'nonce'  => wp_create_nonce( WP_Dashboard_Leads::NONCE )
        );
    }
 
    private function get_post_id() {
        $post_id = 0;

        if (isset($_POST['post_id'])) {
            $post_id = absint( filter_var( $_POST['post_id'], FILTER_SANITIZE_NUMBER_INT ) );
        }
 
        return $post_id;
    }


    public function build() {
        check_ajax_referer( self::NONCE, 'nonce' );
 
        global $wpdb;

	    $args = array();

		$user_id = $this->get_user_id();

	    $args['leads'] = array(
			'data'                     => self::buildDataFromDatabase(),
			'leads_users_total'        => $this->leads_users_total(),
			'leads_users_recents'      => $this->leads_users_recents(),
			'leads_users_popupar_post' => $this->leads_users_popupar_post( $user_id ),
			'search_user'              => $this->get_info_from_user( $user_id ),
			'search_user_leads'        => $this->get_info_from_user_leads( $user_id )
	    );

	    wp_send_json_success( $args );
	    wp_die();
    }

    private function get_user_id() {
        $user_id = 0;

        if (isset($_POST['user_id'])) {
            $user_id = absint( filter_var( $_POST['user_id'], FILTER_SANITIZE_NUMBER_INT ) );
        }
 
        return $user_id;
    }

    private function leads_users_total(){
    	global $wpdb;
		
		$tbname = $wpdb->prefix.'dash_leads';

		$total = $wpdb->get_var("SELECT COUNT(*)  FROM {$tbname}");

		if( $total >= 1 ){

		    return $total;
	    } else{
	    	return 0;
	    }	    
    }

    public static function buildDataFromDatabase() {
		global $wpdb;
		
		$tbname = $wpdb->prefix.'dash_leads';
		$sql  = $wpdb->get_results( "SELECT * FROM {$tbname} ORDER BY data_created DESC" );
		$leads  = array();

		if( $sql ){
			foreach ( $sql as $res ) {
				$orders = unserialize( $res->orders );

				array_push( $leads, array(
					'user_id'      => $res->user_id,
					'first_name'   => $res->first_name,
					'last_name'    => $res->last_name,
					'email'        => $res->email,
					'documentType' => $res->documentType,
					'document'     => $res->document,
					'phone'        => $res->phone,
					'orders'       => $orders
		        ));
		    }

		    return $leads;
	    }

	    return 0;
	}

	private function leads_users_recents() {
    	global $wpdb;

        $tbname = $wpdb->prefix.'dash_leads';
		$sql    = $wpdb->get_results( "SELECT * FROM {$tbname} ORDER BY data_created DESC" );
		$users  = array();

		if( $sql ){
			foreach ( $sql as $res ) {
				array_push( $users, array(
					'user_id'  => $res->user_id,
					'username' => $res->first_name . ' ' . $res->last_name
		        ));
		    }

		    return $users;
	    }

	    return 0;
    }

	private function leads_users_popupar_post( $user_id ) {
    	global $wpdb;

		if ( $user_id ) {
			$tbposts = $wpdb->prefix.'dash_most_popular_post_user';			
			$tbleads = $wpdb->prefix.'dash_leads';

			$sql = "
				SELECT {$tbposts}.user_id, {$tbposts}.post_id, {$tbposts}.post_count, {$tbposts}.share_count, {$tbposts}.post_category
				FROM {$tbleads} INNER JOIN {$tbposts} ON ( {$tbleads}.user_id = {$tbposts}.user_id )
				WHERE {$tbleads}.user_id = {$user_id} AND {$tbposts}.user_id = {$user_id}  ORDER BY {$tbposts}.post_count DESC
			";

			$users = $wpdb->get_results($sql);

			$posts    = array();

		    foreach ( $users as $value ) {
		    	$cat_id = unserialize( $value->post_category );
		        array_push( $posts, array(
					'post_id'       => $value->post_id, 
					'post_count'    => $value->post_count, 
					'post_title'    => get_the_title( $value->post_id ), 
					'post_link'     => get_permalink( $value->post_id ),
					'post_category' => get_cat_name( $cat_id )
		        ));
		    }

			return $posts;
		}
		return 0;
    }

    private function get_info_from_user( $user_id ) {
    	global $wpdb;	
		
		if ( $user_id ) {

			$args = array();

		    $tbname = $wpdb->prefix.'dash_leads';
		    $data   = $wpdb->get_results( "SELECT * FROM {$tbname} WHERE user_id = {$user_id}" );

			$meta   = get_user_by('id', $user_id);
		    $cat_id = get_field( 'user_categoria', 'user_'.$user_id );

			$args['meta']     = $meta;
			$args['user_avatar']     = get_avatar($user_id, 200);
			$args['display_name']      = $meta->data->display_name;
			$args['user_login']      = $meta->data->user_login;
			$args['user_email']      = $meta->data->user_email;
			$args['user_registered'] = mysql2date( 'd.m.Y g:i A', $meta->data->user_registered );
			$args['user_category']   = get_cat_name( $cat_id );
			$args['user_url']        = $meta->data->user_url;
			$args['user_genero']     = get_field( 'user_gender', 'user_'.$user_id );

			$args['api_fullname']     = $data[0]->first_name . ' ' . $data[0]->last_name;
			$args['api_email']        = $data[0]->email;
			$args['api_documentType'] = $data[0]->documentType;
			$args['api_document']     = $data[0]->document;
			$args['api_phone']        = $data[0]->phone;

			$orders = unserialize( $data[0]->orders );
			$args['api_orders']              = $orders;
			$args['api_orders_list']         = $orders['list'];
			$args['api_orders_count_orders'] = $orders['countOrders'];
			$args['api_orders_count_items']  = $orders['countItems'];

			if( $orders['countOrders'] >= 2 ){
				$args['user_type'] = 'Evangelizador';
			}

			if( $orders['countOrders'] > 0 && $orders['countOrders'] < 2 ){
				$args['user_type'] = 'Clientes';
			}

			if( $orders['countOrders'] <= 0 ){
				$args['user_type'] = 'Leads';
			}


		    return $args;
		}

		return 0;

    }

    private function get_info_from_user_leads( $user_id ) {
    	global $wpdb;	
		
		if ( $user_id ) {

			$args = array();

		    $tbname = $wpdb->prefix.'dash_leads';
		    $data   = $wpdb->get_results( "SELECT * FROM {$tbname} WHERE user_id = {$user_id}" );

			$orders = unserialize( $data[0]->orders );

			$args = array();

		 	if( $orders['list'] ){

			    foreach ( $orders['list'] as $item ) {					
			    	if( $item->status === 'invoiced' ){			    		
						array_push( $args, array(
							'countItems'        => count( $orders['list'] ),
							'origin'            => $item->origin,
							'status'            => $item->status,
							'statusDescription' => $item->statusDescription,
							'items'             => $item->items,
							'countItems'        => count( $item->items ),
							'totalValue'        => $item->totalValue,
							'creationDate'      => mysql2date( 'd.m.Y g:i A', $item->creationDate )
				        ));
			    	}
			    }

			    if( count( $args ) >= 2 ){			    	
					$type ='Evangelizador';
				}

				if( count( $args ) > 0 && count( $args ) < 2 ){			    	
					$type ='Cliente';		
				}

				if( count( $args ) <= 0 ){  		    	
					$type ='Leads';
				}

				$leads = array(
					'type'         => $type,
					'count_orders' => count( $args ),
					'objects'       => $args
				);

			    return $leads;
		    }
		}

		return 0;
    }
 
}