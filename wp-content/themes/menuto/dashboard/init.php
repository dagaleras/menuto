<?php 
// add_action( 'shutdown', function(){
//     foreach( $GLOBALS['wp_actions'] as $action => $count )
//         printf( '%s (%d) <br/>' . PHP_EOL, $action, $count );

// });
/**
 * Constantes do projeto
 */

// Caminhos do front end
define('DASHBOARD_PATH', get_template_directory() . '/dashboard/');
define('DASHBOARD_URI', get_template_directory_uri() . '/dashboard');

define('DASHBOARD_ASSETS_PATH', DASHBOARD_URI . '/assets');
define('DASHBOARD_VIEWS', DASHBOARD_URI . '/views/');

define( 'APP_KEY', 'vtexappkey-timecenter-KCSHMI' );
define( 'APP_TOKEN', 'VWLURHHACSOVODFSCHRBIUVUGVRLNIKBJQPLARNSRPERRWYRHPWBASTABROWPVMUINMXNTRLUYJSQDCJXADYLIAQXPKJDRDVDJIEKADOXLHCJAJXPEYOSNJZNWAXIIHC' );

// Class for installation and uninstallation
class WP_Dasboard{
	public static function init() {
		// Lista de diretórios dentro da pasta functions a serem incluídos
		$directories = array(
		    'enqueue',
		    'helpers',
		    'modules'
		);

		// Inclui todos os diretórios listados acima
		foreach ( $directories as $directory ) {
		    self::include_php_dir( DASHBOARD_PATH . $directory );
		}
	}

	public static function include_php_dir( $folder ) {
		foreach ( glob( "{$folder}/*.php" ) as $filename ) {
	        include $filename;
	    }
	}

	public static function actions() {
		// Check for token
		if ( ! wp_verify_nonce( $_POST['token'], 'wp_token' ) ) die();

		include_once( DASHBOARD_PATH . 'controllers/track.php' );
		$track = new WP_Dashobard_track( intval( $_POST['post_id'] ) );
	}

	public static function javascript() {
		global $wp_query;
		wp_reset_query();
		wp_print_scripts( 'jquery' );
		$token = wp_create_nonce( 'wp_token' );
		//if ( ! is_front_page() && ( is_page() || is_single() ) ) {
		if ( ! is_page_template( 'templates/dashboard.php' ) ){
			echo '
				<script type="text/javascript">
					/* <![CDATA[ */ 
					jQuery.post("' . admin_url( 'admin-ajax.php ') . '", { 
						action: "wmp_update",
						post_id: ' . $wp_query->post->ID . ', 
						token: "' . $token . '" 
					}); 
					/* ]]> */
				</script>
			';
		}
	}

	public static function create_tables() {
		global $wpdb;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$charset_collate = 'ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4';

		$dash_clicks_products = $wpdb->prefix . 'dash_clicks_products';
		$dash_leads = $wpdb->prefix . 'dash_leads';
		$dash_most_accessed_area = $wpdb->prefix . 'dash_most_accessed_area';
		$dash_most_popular_post_user = $wpdb->prefix . 'dash_most_popular_post_user';
		$dash_total_interaction = $wpdb->prefix . 'dash_total_interaction';
		$dash_visitors = $wpdb->prefix . 'dash_visitors';

		$sql = "CREATE TABLE IF NOT EXISTS $dash_clicks_products (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			user_id bigint(20) DEFAULT NULL,
			product_id bigint(20) DEFAULT NULL,
			post_id bigint(20) DEFAULT NULL,
			sku varchar(255) DEFAULT NULL,
			count bigint(20) DEFAULT NULL,
			PRIMARY KEY (id)
		) $charset_collate;";
		dbDelta( $sql );

		$sql = "CREATE TABLE IF NOT EXISTS $dash_leads (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			user_id bigint(20) DEFAULT NULL,
			first_name varchar(255) DEFAULT NULL,
			last_name varchar(255) DEFAULT NULL,
			email varchar(255) DEFAULT NULL,
			documentType varchar(255) DEFAULT NULL,
			document varchar(255) DEFAULT NULL,
			phone varchar(255) DEFAULT NULL,
			orders longtext,
			data_created datetime DEFAULT NULL,
			data_updated datetime DEFAULT NULL,
			data_last_interaction datetime DEFAULT NULL,
			PRIMARY KEY  (id)
		) $charset_collate;";
		dbDelta( $sql );

		$sql = "CREATE TABLE IF NOT EXISTS $dash_most_popular_post_user (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			user_id bigint(20) DEFAULT NULL,
			post_id bigint(20) DEFAULT NULL,
			post_count bigint(20) DEFAULT NULL,
			share_count bigint(20) DEFAULT NULL,
			post_category longtext,
			PRIMARY KEY  (id)
		) $charset_collate;";
		dbDelta( $sql );

		$sql = "CREATE TABLE IF NOT EXISTS $dash_visitors (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			post_id bigint(20) DEFAULT NULL,
			ip varchar(15) DEFAULT NULL,
			referrer varchar(255) DEFAULT NULL,
			browser varchar(255) DEFAULT NULL,
			time datetime DEFAULT NULL,
			PRIMARY KEY  (id)
		) $charset_collate;";
		dbDelta( $sql );

		$sql = "CREATE TABLE IF NOT EXISTS $dash_total_interaction (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			post_id bigint(20) DEFAULT NULL,
			post_total bigint(20) DEFAULT NULL,
			category_id bigint(20) DEFAULT NULL,
			category_total bigint(20) DEFAULT NULL,
			product_id bigint(20) DEFAULT NULL,
			product_total bigint(20) DEFAULT NULL,
			PRIMARY KEY  (id)
		) $charset_collate;";
		dbDelta( $sql );
  
  		$sql = "CREATE TABLE IF NOT EXISTS $dash_most_accessed_area (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			post_id bigint(20) DEFAULT NULL,
			last_updated datetime DEFAULT NULL,
			1_day_stats mediumint(9) DEFAULT NULL,
			7_day_stats mediumint(9) DEFAULT NULL,
			30_day_stats mediumint(9) DEFAULT NULL,
			all_time_stats bigint(20) DEFAULT NULL,
			raw_stats text,
			PRIMARY KEY  (id)
		) $charset_collate;";
		dbDelta( $sql );
	}

}

// Use ajax for tracking popular posts
WP_Dasboard::create_tables();
add_action( 'init', 'WP_Dasboard::init' );
add_action( 'wp_head', 'WP_Dasboard::javascript' );

add_action( 'wp_ajax_wmp_update', 'WP_Dasboard::actions' );
add_action( 'wp_ajax_nopriv_wmp_update', 'WP_Dasboard::actions' );

include_once( DASHBOARD_PATH . 'controllers/overview.php' );
$overview = WP_Dashboard_Overview::register();

include_once( DASHBOARD_PATH . 'controllers/users.php' );
$users = WP_Dashboard_Users::register();

include_once( DASHBOARD_PATH . 'controllers/leads.php' );
$leads = WP_Dashboard_Leads::register();

include_once( DASHBOARD_PATH . 'controllers/clients.php' );
$clients = WP_Dashboard_Clients::register();

include_once( DASHBOARD_PATH . 'controllers/evangelizers.php' );
$evangelizers = WP_Dashboard_Evangelizers::register();