<div id="leads" class="dashboard--leads">
	<div class="row">
		<div class="col-xs-12 col-sm-4 col-md-3 col-lg-4">
			<div class="card-stats">
				<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>	
				<div class="card-stats--header">
					<h2 id="leads--total_users_leads"></h2>
					<small><?php echo __( 'Total Leads', 'menuto' ); ?></small>
				</div>
				<div class="card-stats--icon">
					<i class="material-icons">assignment_ind</i>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-9 col-lg-8">
			<div class="card">
				<div class="card-header">	
					<h2><?php echo __( 'Buscar por Leads', 'menuto' ); ?></h2>
				</div>
				<div class="card-body">
					<div class="row clearfix">
						<div class="col-md-12">
							<?php do_action( 'user_leads_select' );	?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row" id="leads--row_list">
		<div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-12 col-md-offset-2 col-md-8 col-md-offset-2 col-lg-8">
			<div id="" class="card">
				<div class="card-header">	
					<h2><i class="material-icons">account_box</i> <?php echo __( 'Usuários Leads Recentes', 'menuto' ); ?></h2>
				</div>
				<div class="card-body">
					<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>
					<div id="leads--recents_users" class="list"><ul></ul></div>
				</div>
			</div>
		</div>
	</div>

	<div class="row" id="leads--row_search" style="display: none;">
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-4">
			<div class="card card-user">
				<div class="card-image">
					<img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&amp;fm=jpg&amp;h=300&amp;q=75&amp;w=400" alt="" />
				</div>
				<div class="card-body">
					<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>
					<div id="leads--load_author" class="author"></div>
					<div id="leads--load_description"class="description"><ul></ul></div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-8">

			<div id="is_popular_posts_leads" class="card" style="display: none;">
				<div class="card-header">	
					<h2><i class="material-icons">account_box</i> <?php echo __( 'Interações mais recentes', 'menuto' ); ?></h2>
				</div>
				<div class="card-body">
					<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>
					<div id="leads--most_popular_posts" class="list"><ul></ul></div>
				</div> 
			</div>

			<div id="is_leads" class="card" style="display: none;">
				<div class="card-header">	
					<h2><i class="material-icons">account_box</i> <?php echo __( 'Compras', 'menuto' ); ?></h2>
				</div>
				<div class="card-body">
					<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>
					<div class="table-responsive">
                        <table id="leads--table_leads" class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Origem</th>
                                    <th>Status</th>
                                    <th>Data</th>
                                    <th>Nº de itens</th>
                                    <th>Valor Total</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
					</div>
				</div>
			</div>

		</div>
	</div>

</div>