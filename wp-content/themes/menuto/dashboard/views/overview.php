<div id="overview" class="dashboard--overview">
	<!--/ Card Stats -->
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div id="card-total_users" class="card-stats">
				<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>	
				<div class="card-stats--header">
					<h2 id="total_users"></h2>
					<small><?php echo __( 'Usuários', 'menuto' ); ?></small>
				</div>
				<div class="card-stats--icon">
					<i class="material-icons">supervisor_account</i>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div id="card-total_posts" class="card-stats">
				<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>	
				<div class="card-stats--header">
					<h2 id="total_posts"></h2>
					<small><?php echo __( 'Artigos', 'menuto' ); ?></small>
				</div>
				<div class="card-stats--icon">
					<i class="material-icons">assignment</i>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div id="card-total_views" class="card-stats">
				<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>	
				<div class="card-stats--header">
					<h2 id="total_visitors"></h2>
					<small><?php echo __( 'Visualizações', 'menuto' ); ?></small>
				</div>
				<div class="card-stats--icon">
					<i class="material-icons">visibility</i>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div id="card-unique_visitors" class="card-stats">
				<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>	
				<div class="card-stats--header">
					<h2 id="unique_accesses"></h2>
					<small><?php echo __( 'Visitantes únicos', 'menuto' ); ?></small>
				</div>
				<div class="card-stats--icon">
					<i class="material-icons">vpn_lock</i>
				</div>
			</div>
		</div>
	</div>

    <!--/ Card Social -->
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-lg-3">	
            <div id="card-facebook" class="card-social facebook">
				<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>
                <i class="fa fa-facebook"></i>
                <ul>
                	<li>
                        <h4>Facebook</h4>
                        <label>
                        	<span class="count">5</span>
                        	<span>Amigos</span>
                        </label>                        
                    </li>
                </ul>
            </div>
        </div>

		<div class="col-xs-12 col-sm-6 col-lg-6">
            <div id="card-post_mostread" class="card-featured">
				<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>	
            	<div class="card-featured--header">
            		<div class="inner">
		                <i class="material-icons">touch_app</i>
		                <h3>Artigo com maior interação</h3>
	                </div>
                </div>
                <div class="card-featured--body">
	                <ul id="popularpost"></ul>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
            <div id="card-instagram" class="card-social instagram">
				<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>	
                <i class="fa fa-instagram"></i>
                <ul>
                	<li>
                        <h4>Instagram</h4>
                        <label>
                        	<span id="instagram_followers_count" class="count">2</span>
                        	<span>Seguidores</span>
                        </label>                        
                    </li>
                </ul>
            </div>
        </div>
	</div>

	<div class="row">			
		
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div id="card-categories_mostread" class="card">
				<div class="card-header">	
					<h2><i class="material-icons">assessment</i> <?php echo __( 'Categorias mais acessadas', 'menuto' ); ?></h2>					
					<div class="card-controls">
						<div class="card-controls--item">Acessos</div>
					</div>
				</div>
				<div class="card-body">
					<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>
					<div id="categories_mostread" class="list"><ul></ul></div>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div id="card-products_clicks" class="card">
				<div class="card-header">	
					<h2><i class="material-icons">assessment</i> <?php echo __( 'Produtos relacionados', 'menuto' ); ?></h2>
					<div class="card-controls">
						<div class="card-controls--item">Cliques</div>
					</div>
				</div>
				<div class="card-body">
					<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>
					<div id="products_clicks" class="list"><ul></ul></div>
				</div>
			</div>
		</div>


	</div>

	<div class="row">	

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
			<div id="card-users_recents" class="card">
				<div class="card-header">	
					<h2><i class="material-icons">account_box</i> <?php echo __( 'Usuários', 'menuto' ); ?></h2>
					<div class="card-controls">
						<a href="javascript:void(0)" class="card-controls--item" data-toggle="tooltip" title="São pessoas cadastradas na plataforma Me'nuto."><i class="material-icons">help_outline</i></a>
					</div>
				</div>
				<div class="card-body">
					<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>				
					<div id="users_recents" class="box" style="display: none;"></div>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
			<div id="card-leads_recents" class="card">
				<div class="card-header">	
					<h2><i class="material-icons">account_box</i> <?php echo __( 'Leads', 'menuto' ); ?></h2>
					<div class="card-controls">
						<a href="javascript:void(0)" class="card-controls--item" data-toggle="tooltip" title="São pessoas cadastradas no Me'nuto e no Timecenter."><i class="material-icons">help_outline</i></a>
					</div>
				</div>
				<div class="card-body">
					<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>
					<div id="leads_recents" class="box" style="display: none;"></div>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
			<div id="card-clients_recents" class="card">
				<div class="card-header">	
					<h2><i class="material-icons">account_box</i> <?php echo __( 'Clientes', 'menuto' ); ?></h2>
					<div class="card-controls">
						<a href="javascript:void(0)" class="card-controls--item" data-toggle="tooltip" data-placement="left" title="São pessoas cadastradas no Me'nuto e no Timecenter que já realizaram uma compra."><i class="material-icons">help_outline</i></a>
					</div>
				</div>
				<div class="card-body">
					<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>
					<div id="clients_recents" class="box" style="display: none;"></div>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div id="card-evangelizers_recents" class="card">
				<div class="card-header">	
					<h2><i class="material-icons">assignment_ind</i> <?php echo __( 'Evangelizadores', 'menuto' ); ?></h2>
					<div class="card-controls">
						<a href="javascript:void(0)" class="card-controls--item" data-toggle="tooltip" data-placement="left" title="São pessoas cadastradas no Me'nuto e no Timecenter que já realizaram X compras."><i class="material-icons">help_outline</i></a>
					</div>
				</div>
				<div class="card-body">
					<div class="loading"><div class="loading-icon fa fa-circle-o-notch fa-spin fa-3x fa-fw"></div></div>
					<div class="table-responsive">
                        <table id="overview--evangelizers_recents" class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Clientes</th>
                                    <th>Status</th>
                                    <th>Data</th>
                                    <th>Nº de itens</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>