<?php
/**
 * Load a requested modal via AJAX.
 * 
 * @since 1.0.0
 * 
 * @return void
 */
function handler_ajax_load_view(){
	ob_start();

    switch( $_POST['view'] ) {
    	CASE 'overview':
			get_template_part( 'dashboard/views/overview' );
			break;
		CASE 'users':
			get_template_part( 'dashboard/views/users' );
			break;
		CASE 'clients':
			get_template_part( 'dashboard/views/clients' );
			break;
		CASE 'leads':
			get_template_part( 'dashboard/views/leads' );
			break;
		CASE 'evangelizers':
			get_template_part( 'dashboard/views/evangelizers' );
			break;
		DEFAULT:
			get_template_part( 'dashboard/views/overview' );
			break;
	}

	$view = ob_get_clean();

	if ( $view ) {
		echo $view;
	} else {
		echo 0;
	}
	
	die();
}
add_action( 'wp_ajax_load_view', 'handler_ajax_load_view' );
add_action( 'wp_ajax_nopriv_load_view', 'handler_ajax_load_view' );

/**
 * Load a requested modal via AJAX.
 * 
 * @since 1.0.0
 * 
 * @return void
 */
function handler_ajax_load_modal() {
	
	ob_start();
	
	get_template_part( 'parts/modal', $_POST['modal'] );
		
	$modal = ob_get_clean();
	
	if ( $modal ) {
		echo $modal;
	} else {
		echo 0;
	}
	
	die();
}
add_action( 'wp_ajax_load_modal',        'handler_ajax_load_modal' );
add_action( 'wp_ajax_nopriv_load_modal', 'handler_ajax_load_modal' );

/**
 * Load a requested modal via AJAX.
 * 
 * @since 1.0.0
 * 
 * @return void
 */
function wmp_get_popular( $args = array() ) {
	// This function is just for backwards compatibility
	return wp_most_popular_get_popular( $args );
}

function wp_most_popular_get_popular( $args = array() ) {
	global $wpdb;
	
	// Default arguments
	$limit = 5;
	$post_type = array( 'post' );
	$range = 'all_time';
	
	if ( isset( $args['limit'] ) ) {
		$limit = $args['limit'];
	}
	
	if ( isset( $args['post_type'] ) ) {
		if ( is_array( $args['post_type'] ) ) {
			$post_type = $args['post_type'];
		} else {
			$post_type = array( $args['post_type'] );
		}
	}
	
	if ( isset( $args['range'] ) ) {
		$range = $args['range'];
	}
	
	switch( $range ) {
		CASE 'all_time':
			$order = "ORDER BY all_time_stats DESC";
			break;
		CASE 'monthly':
			$order = "ORDER BY 30_day_stats DESC";
			break;
		CASE 'weekly':
			$order = "ORDER BY 7_day_stats DESC";
			break;
		CASE 'daily':
			$order = "ORDER BY 1_day_stats DESC";
			break;
		DEFAULT:
			$order = "ORDER BY all_time_stats DESC";
			break;
	}

	$holder = implode( ',', array_fill( 0, count( $post_type ), '%s') );
	
	$sql = "
		SELECT
			p.*
		FROM
			{$wpdb->prefix}dash_most_accessed_area mp
			INNER JOIN {$wpdb->prefix}posts p ON mp.post_id = p.ID
		WHERE
			p.post_type IN ( $holder ) AND
			p.post_status = 'publish'
		{$order}
		LIMIT %d
	";

	$result = $wpdb->get_results( $wpdb->prepare( $sql, array_merge( $post_type, array( $limit ) ) ), OBJECT );
	
	if ( ! $result) {
		return array();
	}
	
	return $result;
}

/**
 * Load a requested modal via AJAX.
 * 
 * @since 1.0.0
 * 
 * @return void
 */
function remote_headers(){
	$APP_KEY   = 'vtexappkey-timecenter-KCSHMI';
	$APP_TOKEN = 'VWLURHHACSOVODFSCHRBIUVUGVRLNIKBJQPLARNSRPERRWYRHPWBASTABROWPVMUINMXNTRLUYJSQDCJXADYLIAQXPKJDRDVDJIEKADOXLHCJAJXPEYOSNJZNWAXIIHC';

	$params = array(
        'cache-control'       => 'no-cache',
        'x-vtex-api-appkey'   => $APP_KEY,
        'x-vtex-api-apptoken' => $APP_TOKEN,
        'content-type'        => 'application/json',
        'accept'              => 'application/json'
    );

	return $params;
}

function consult_leads( $params ){
	$url = 'http://api.vtex.com/timecenter/dataentities/CL/search';
    $get = wp_remote_get( esc_url_raw( $url ), array( 
        'timeout' => 30,
        'headers' => remote_headers(),
        'body'    => array(
            'email'   => $params,
            '_fields' => '_all'
        )
    ));
	$body    = wp_remote_retrieve_body( $get );
	$results = json_decode( $body );

    return $results;
}

function consult_orders( $params ){
	if( $params ){
        $url = 'http://timecenter.vtexcommercestable.com.br/api/oms/pvt/orders';
        $get = wp_remote_get( esc_url_raw( $url ), array( 
            'timeout' => 30,
            'headers' => remote_headers(),
            'body'    => array(
                'q'   => $params
            )   
        ));
        $body     = wp_remote_retrieve_body( $get );
        $results  = json_decode( $body );

        return $results;
    }
}

function capture_leads() {
    global $wpdb;

    $users = $wpdb->get_results( "SELECT * FROM $wpdb->users ORDER BY ID DESC" );
    $tbname = $wpdb->prefix.'dash_leads';
    $dataLeads  = array();
    $dataOrders = array();
    $data       = array();

	foreach ( $users as $user ) {
	    $results = consult_leads( $user->user_email );

	    if( $results ){

	    	$listLeads = array(
				'user_id'               => $user->ID,
				'user_email'            => $user->user_email,
				'first_name'            => $results[0]->firstName,
				'last_name'             => $results[0]->lastName,
				'email'                 => $results[0]->email,
				'documentType'          => $results[0]->documentType,
				'cpf'                   => $results[0]->document,
				'phone'                 => $results[0]->homePhone,
				'data_created'          => $results[0]->createdIn,
				'data_updated'          => $results[0]->updatedIn,
				'data_last_interaction' => $results[0]->lastInteractionIn
		    );
		    array_push( $dataLeads, $listLeads );

		    $tbname  = $wpdb->prefix.'dash_leads';
		    $colname = 'ID';
		    $query   = $wpdb->prepare( "SELECT {$colname} FROM {$tbname} WHERE user_id = %d", $user->ID );
		    $return  = $wpdb->get_col( $query );

		    if($return){
		    	$wpdb->update( $tbname,
		            array( 
						'first_name'            => $listLeads['first_name'],
						'last_name'             => $listLeads['last_name'],
						'email'                 => $listLeads['email'],
						'documentType'          => $listLeads['documentType'],
						'document'              => $listLeads['cpf'],
						'phone'                 => $listLeads['phone'],
						'data_created'          => $listLeads['data_created'],
						'data_updated'          => $listLeads['data_updated'],
						'data_last_interaction' => $listLeads['data_last_interaction']
		            ), 
		            array( 
		                'user_id' => $user->ID
		            )                        
		        );
			} else{
				$wpdb->insert( $tbname, array(
					'user_id'               => $listLeads['user_id'],
					'first_name'            => $listLeads['first_name'],
					'last_name'             => $listLeads['last_name'],
					'email'                 => $listLeads['email'],
					'documentType'          => $listLeads['documentType'],
					'document'              => $listLeads['cpf'],
					'phone'                 => $listLeads['phone'],
					'data_created'          => $listLeads['data_created'],
					'data_updated'          => $listLeads['data_updated'],
					'data_last_interaction' => $listLeads['data_last_interaction']
		        ));
			}
		}		
    }

    foreach ( $dataLeads as $meta ) {
    	$orders = consult_orders( $meta['cpf'] ); 	

    	if( $orders ){
    		$order   = array();
	        $items   = array();

	        foreach ( $orders->list as $value ) {
	            array_push( $order, $value->items );

	            foreach ( $value->items as $item ) {
	                array_push( $items, $item );
	            }
	        }

	        $listOrder = array(
				'list'        => $orders->list,
				'facets'      => $orders->facets,
				'paging'      => $orders->paging,
				'stats'       => $orders->stats,
				'countOrders' => count( $orders->list ),
				'countItems'  => count( $items )
	        );
	        array_push( $dataOrders, $listOrder);

	        if( $meta['user_id'] ){
				$wpdb->update( $tbname,
		            array( 
						'orders' => serialize( $listOrder )
		            ), 
		            array( 
		                'user_id' => $meta['user_id']
		            )                        
		        );
	        }
		}
    }

}
add_action( 'wp_head', 'capture_leads' );

function returnObjectDBLeads() {
	global $wpdb;
	
	$tbname = $wpdb->prefix.'dash_leads';
	$query  = $wpdb->get_results( "SELECT * FROM {$tbname} ORDER BY ID DESC" );
	$leads  = array();

	foreach ($query as $data) {
		$orders = unserialize( $data->orders );
		array_push( $leads, array(
			'user_id'      => $data->user_id,
			'first_name'   => $data->first_name,
			'last_name'    => $data->last_name,
			'email'        => $data->email,
			'documentType' => $data->documentType,
			'document'     => $data->document,
			'phone'        => $data->phone,
			'orders'       => $orders['list']
        ));
    }

    return $leads;

}


/**
 * Return ip visit.
 * 
 * @since 1.0.0
 * 
 * @return void
 */
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP'])){
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    }
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else if(isset($_SERVER['HTTP_X_FORWARDED'])){
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    }
    else if(isset($_SERVER['HTTP_FORWARDED_FOR'])){
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    }
    else if(isset($_SERVER['HTTP_FORWARDED'])){
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    }
    else if(isset($_SERVER['REMOTE_ADDR'])){
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    }
    else{
        $ipaddress = 'UNKNOWN';
    }
    return $ipaddress;
}

function get_client_browser() {
    $browser = '';
	if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE){
		$browser = 'Internet explorer';
	}
	elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== FALSE){ //For Supporting IE 11
		$browser = 'Internet explorer';
	}
	elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== FALSE){
		$browser = 'Mozilla Firefox';
	}
	elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== FALSE){
		$browser = 'Google Chrome';
	}
	elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== FALSE){
		$browser = "Opera Mini";
	}
	elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Opera') !== FALSE){
		$browser = "Opera";
	}
	elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') !== FALSE){
		$browser = "Safari";
	}
	else{
		$browser = 'Something else';
	}
    return $browser;
}

function detectDevice(){
	$userAgent = $_SERVER["HTTP_USER_AGENT"];
	$devicesTypes = array(
        "computer" => array("msie 10", "msie 9", "msie 8", "windows.*firefox", "windows.*chrome", "x11.*chrome", "x11.*firefox", "macintosh.*chrome", "macintosh.*firefox", "opera"),
        "tablet"   => array("tablet", "android", "ipad", "tablet.*firefox"),
        "mobile"   => array("mobile ", "android.*mobile", "iphone", "ipod", "opera mobi", "opera mini"),
        "bot"      => array("googlebot", "mediapartners-google", "adsbot-google", "duckduckbot", "msnbot", "bingbot", "ask", "facebook", "yahoo", "addthis")
    );
 	foreach( $devicesTypes as $deviceType => $devices ) {           
        foreach( $devices as $device ) {
            if( preg_match( "/" . $device . "/i", $userAgent ) ) {
                $deviceName = $deviceType;
            }
        }
    }
    return ucfirst( $deviceName );
}

function save_visitors() {
	global $wpdb, $post;

	$tbname = $wpdb->prefix.'dash_visitors';

	// visitor information
	$visitorIP       = get_client_ip();
	$visitorReferrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
	$visitorBrowser  = $_SERVER['HTTP_USER_AGENT'];

	$wpdb->insert( $tbname, array(
		'post_id'  => $post->ID,
		'ip'       => $visitorIP,
		'referrer' => $visitorReferrer,
		'browser'  => $visitorBrowser,
		'time'     => date("Y-m-d H:i:s")
    ));
}
add_action( 'wp_head', 'save_visitors' );


function get_visitors( $ip ) {
    global $wpdb;

	$tbname   = $wpdb->prefix.'dash_visitors';
    $rowcount = $wpdb->get_results( "SELECT * FROM {$tbname} WHERE ip = {$ip} " );

    return $rowcount;
}
//get_visitors( $post->ID, get_client_ip() );

function setClickProductCount(){
	// Cria ou obtém o valor da chave para contarmos
    global $wpdb;

    $status  = false;

	$user_id    = $_POST['user_id'];
	$product_id = $_POST['product_id'];
	$post_id    = $_POST['post_id'];
	$sku        = $_POST['sku'];

    $tbname   = $wpdb->prefix.'dash_clicks_products';
    $colname  = 'count';
    $prepared = $wpdb->prepare( "SELECT {$colname} FROM {$tbname} WHERE product_id = %d AND post_id = %d", $_POST['product_id'], $_POST['post_id'] );
    $count    = $wpdb->get_col( $prepared );

    if( $count ){
        $count = $count[0];
        $count += 1;            
        $wpdb->update( $tbname,
            array( 
                'count' => $count
            ), 
            array( 
                'product_id' => $product_id,
                'post_id' => $post_id
            )                        
        );

        $status = true;
    } else{
        $count = 1;
        $wpdb->insert( $tbname, array(
			'user_id'    => $user_id,
			'product_id' => $product_id,
			'post_id'    => $post_id,
			'sku'        => $sku,
			'count'      => $count
        ));

        $status = true;
    }

	$tbtotal      = $wpdb->prefix.'dash_total_interaction';
	$colproduct   = 'product_total';
	$sqlProduct   = $wpdb->prepare( "SELECT {$colproduct} FROM {$tbtotal} WHERE product_id = %d", $_POST['product_id'] );
	$totalProduct = $wpdb->get_col( $sqlProduct );

    if($totalProduct){
    	$count = $totalProduct[0];
        $count += 1;
        $wpdb->update( $tbtotal,  
            array( 
                'product_total' => $count
            ), 
            array( 
                'product_id' => $product_id
            )                        
        );
        $status = true;
    } else{
        $count = 1;
        $wpdb->insert( $tbtotal, array( 
			'product_id'    => $product_id,
			'product_total' => $count
        ));
        $status = true;  	
    }

    if( $status ){
    	echo $_POST;
    } else{
    	echo 0;
    }

    exit();
}
add_action( 'wp_ajax_click_product', 'setClickProductCount' );
add_action( 'wp_ajax_nopriv_click_product', 'setClickProductCount' );