<?php function module_area_popular(){ ?>
	<div class="listview">
        <div class="listview-header">
            <h2 class="listview--title">Áreas acessadas</h2>
			<div class="listview--actions">										
                <a href="javascript:void(0)" class="timeline active" data-timeline="daily">Hoje</a>
                <a href="javascript:void(0)" class="timeline" data-timeline="weekly">7 Dias</a>
                <a href="javascript:void(0)" class="timeline" data-timeline="monthly">30 Dias</a>
                <a href="javascript:void(0)" class="timeline" data-timeline="all_time">Todos</a>
                <div class="listview--actions__item">
                    <div class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="javascript:void(0)" class="dropdown-item" data-type="*">Todas</a>
                            <a href="javascript:void(0)" class="dropdown-item" data-type="page">Páginas</a>
                            <a href="javascript:void(0)" class="dropdown-item" data-type="post">Artigos</a>
                        </div>
                    </div>
                </div>	
			</div>	                            		                            
        </div>
        <div class="listview-body">
        	<div id="mostarea" class="listview--area">
        		<?php 
        			global $post;
					$posts = wmp_get_popular( 
						array( 
							'limit'     => 5, 
							'post_type' => array('post','page'),
							'range'     => 'daily'
						) 
					);
        		?>
        		<?php if ( count( $posts ) > 0 ) : ?>
            		<ul class="listview--striped" id="timeline">
                        <?php  foreach ( $posts as $post ): ?>
                        	<?php 
                        		setup_postdata( $post ); 
                        		$category = get_the_category( $post->ID );
                        		$posttype = get_post_type( $post->ID );
                        	?>
							<li class="listview__item">
								<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
							</li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</div>
        </div>
    </div>
<?php } ?>
<?php add_action('area_popular', 'module_area_popular', 10); ?>