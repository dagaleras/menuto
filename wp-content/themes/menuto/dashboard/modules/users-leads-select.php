<?php 

function dashboard_user_leads_select() { 
	if( !is_user_logged_in() ) 
		return; 

	global $wpdb;
		
	$tbname = $wpdb->prefix.'dash_leads';
	$query  = $wpdb->get_results( "SELECT * FROM {$tbname} ORDER BY data_created DESC" );
	$leads  = array();

	if( $query ){
	    $_html = '<select name="user_leads_select" id="user_leads_select" class="selectpicker" title="- Selecione um usuário lead -">';

			foreach ( $query as $data ) {
	            $_html .= '<option value="'.$data->user_id.'" data-tokens="'.$data->first_name . '' .$data->last_name.'">'.$data->first_name . ' ' .$data->last_name.'</option>';
	        }

	    $_html .= '</select>';		
		echo $_html;
    }
	
}

add_action('user_leads_select', 'dashboard_user_leads_select', 5);