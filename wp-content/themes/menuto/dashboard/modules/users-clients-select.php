<?php 

function dashboard_user_clients_select() { 
	if( !is_user_logged_in() ) 
		return; 

	global $wpdb;
		
	$tbname = $wpdb->prefix.'dash_leads';
    $data   = $wpdb->get_results( "SELECT * FROM {$tbname} ORDER BY {$tbname}.data_created DESC" );
	
	$args  = array();
	$_html = '';

 	if( $data ){
 		foreach ( $data as $key => $value ) {
			$orders = unserialize( $value->orders );
			foreach ( $orders['list'] as $item ) {	

				if( $item->status === 'invoiced' ){
					$username = $value->first_name . ' ' . $value->last_name;
		    		array_push( $args, array(
						'user_id'  => $value->user_id,
						'username' => $username
			        ));
		    	}				
	    	
	    	}
 		}	    

		if( count( $args ) > 0 && count( $args ) < 2 ){

			$_html .= '<select name="user_clients_select" id="user_clients_select" class="selectpicker" title="- Selecione um usuário Cliente -">';	
	    		foreach ( $args as $item ) {
					$_html .= '<option value="' . $item['user_id'] . '" data-tokens="'. $item['username'] . '">' . $item['username'] . '</option>';	
				}			
			$_html .= '</select>';
	    }
    }
	echo $_html;
}

add_action( 'user_clients_select', 'dashboard_user_clients_select', 5 );