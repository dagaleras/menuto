<?php function dashboard_user_info() { ?>
	<?php 
		if( !is_user_logged_in() ) 
			return; 
	
		$user_id  = get_current_user_id();
		$user_obj = get_user_by('id', $user_id);
	?>
	<div class="widget--userinfo">
		<div class="loading-overlay"><i class="loading-icon fa fa-refresh fa-spin fa-lg"></i></div>
		<div id="loadUser">
	    	<div class="user">
	            <figure class="user__gravatar"><?php echo get_avatar( $user_id, 100 ); ?></figure>
	            <h2 class="user__name"><?php echo $user_obj->data->display_name; ?></h2>
	        </div>
	    </div>
	</div>
<?php } ?>
<?php add_action('user_info', 'dashboard_user_info', 1); ?>