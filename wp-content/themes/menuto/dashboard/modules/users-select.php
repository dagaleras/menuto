<?php 

function dashboard_select_user( $attr ) { 
	if( !is_user_logged_in() ) 
		return; 

	extract( shortcode_atts( array(
		'name'      => 'select_user_name',
		'id'        => 'select_user_id',
		'className' => 'select_user',
		'title'     => __( '- Selecione um usuário -', 'menuto' )
    ), $attr ) );

	global $wpdb;
	$usernames = $wpdb->get_results("SELECT * FROM $wpdb->users ORDER BY ID DESC");

	if ( $name ) {
        $name = esc_attr( $name );
    }

	if ( $id ) {
        $id = esc_attr( $id );
    }

    if ( $className ) {
        $className = esc_attr( $className );
    }

    if ( $title ) {
        $title = esc_attr( $title );
    }

	$_html = '<select name="'.$name.'" id="'.$id.'" class="'.$id.' selectpicker" title="'.$title.'">';

		foreach ($usernames as $username) {
            $_html .= '<option value="'.$username->ID.'" data-tokens="'.$username->user_login.'">'.$username->user_login.'</option>';
        }

    $_html .= '</select>';
	
	echo $_html;	
}

add_action('user_select', 'dashboard_select_user', 5);