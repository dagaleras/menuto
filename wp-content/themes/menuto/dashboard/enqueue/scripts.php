<?php
/**
 * Scripts personalizados. Nesta página contém todos os arquivos javascripts customizados do site
 *
 * @package Menuto Theme
 * @subpackage Menuto
 * @since Menuto Theme - 1.0
 */
function dashboard_assets_scripts() {
    
    if( is_page_template( 'templates/dashboard.php' ) ){
        
        // Register script css file to be enqueued
        wp_register_style( 'dashstyle', DASHBOARD_ASSETS_PATH . '/css/dashboard.min.css', false, '1.0' );
        wp_enqueue_style( 'dashstyle' ); // Enqueue it!

        // Register script js file to be enqueued
        wp_register_script( 'dashboard', DASHBOARD_ASSETS_PATH . '/js/dashboard.min.js', false, '1.0', true );
        wp_enqueue_script(  'dashboard'); // Enqueue it! 

        // Localize script exposing $data contents
        wp_localize_script( 'dashboard', 'dashoard_ajax', array(
            'url' => admin_url( 'admin-ajax.php' )
        ));
    }

}

//Inclui o javascript do assets
add_action('wp_enqueue_scripts', 'dashboard_assets_scripts', 30, 2);