<div id="login" class="modal" tabindex="-1" role="dialog">
  	<div class="modal-content">
  		<div class="wrapper">
	  		<div class="modal-header">
				<h5 class="modal-title">Conecte-se agora e receba <br /> conteúdo personalizado!</h5>				
			</div>
			<div class="modal-body">
				<!-- Facebook login or logout button -->
				<a href="<?php echo esc_url(site_url('wp-login.php?loginSocial=facebook')); ?>" class="link facebook" data-plugin="nsl" data-action="connect" data-redirect="current" data-provider="facebook" data-popupwidth="475" data-popupheight="175">
					Entrar com Facebook
				</a>
				<!-- Google login or logout button -->
				<a href="<?php echo esc_url(site_url('wp-login.php?loginSocial=google')) ?>" class="link google" data-plugin="nsl" data-action="connect" data-redirect="current" data-provider="google" data-popupwidth="600" data-popupheight="600">
					Entrar com conta Google
				</a>
				<span class="or"><?php echo __( 'ou', 'menuto' ); ?></span>
				<!-- WordPress login or logout button -->
				<a href="javascript:void(0);" class="link email change-modal" data-target="#loginemail">Entrar com E-mail</a>

				<div class="socialstatus"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close"></button>
			</div>
		</div>
  	</div>
</div>

<?php get_template_part( 'modals/wplogin' ); ?>

<?php get_template_part( 'modals/lostpassword' ); ?>

<?php get_template_part( 'modals/register' ); ?>

<?php get_template_part( 'modals/quiz' ); ?>