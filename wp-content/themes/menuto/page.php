<?php get_header(); ?>

	<main role="main" id="main" class="site-main">
		<?php if ( have_posts() ) : ?> 

	        <?php while ( have_posts() ) : the_post(); ?>
				
				<?php if( has_post_thumbnail() ): ?>
					<section class="container-fluid">
						<div class="banners">
							<div class="banners--area">
								<div class="banners--area-loop">
									<div class="intro">
										<figure class="intro--figure">
											<img class="intro--figure-img" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
											<h1 class="intro--figure-title"><?php the_title(); ?></h1>
										</figure>
									</div>
								</div>
							</div>
						</div>
					</section>	
				<?php endif; ?>

				<section class="content">
					<div class="container-small">
						<div class="content--area">
							<div class="content--area-loop">
								<div class="entry">
									<div class="entry--content">

										<?php the_content(); ?>

									</div>
								</div>
								
							</div>
						</div>
					</div>
				</section>	
			        				
			<?php endwhile; ?>

	    <?php else : ?>

	        <?php get_template_part( 'content', 'none' ); ?>

	    <?php endif; ?>
	</main><!-- .site-main -->

<?php get_footer(); ?>