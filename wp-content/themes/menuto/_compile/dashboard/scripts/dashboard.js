(function($) {
    'use strict';

    function startTime() {
        var today = new Date();

        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();

        m = checkTime( m );
        s = checkTime( s );

        var _temp = "<i class='material-icons'>watch_later</i><span class='hours'>" + h + "</span><span class='separator'>:</span><span class='minutes'>" + m + "</span>" + "<span class='separator'>:</span><span class='seconds'>" + s + "</span>";
        document.getElementById( 'clock' ).innerHTML = _temp;

        var t = setTimeout( startTime, 500 );
    }

    function checkTime( i ) {
        if ( i < 10 ) { i = "0" + i };  // add zero in front of numbers < 10
        return i;
    }   

    function ajax_json_post( url, data, beforeSend_callback, success_callback, error_callback, complete_callback ) {
        $.ajax ({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            beforeSend: function() {  
                if( typeof beforeSend_callback === "function" ) {
                    beforeSend_callback();
                }
                return true;
            },
            success: function( response ) {                               
                if( typeof success_callback === "function" ) {
                    success_callback( response );
                }
                return true;
            },
            error: function( jqXHR, textStatus, errorThrown ) {
                if( typeof error_callback === "function" ) {
                    error_callback( jqXHR, textStatus, errorThrown );
                }
                return false;
            },
            complete: function() {
                if( typeof complete_callback === "function" ) {
                    complete_callback();
                }
                return true;
            }  
        });
    }    

    function loadView ( view ) {
        var _view = view;

        var options = {
            'className': "",
            'template': "",
            'ajaxUrl': dashoard_ajax.url,
            'ajaxAction': 'load_view'
        };

        $.ajax({
            url: options.ajaxUrl,
            type: 'POST',
            data: {
                'action': options.ajaxAction,
                'view': view
            },
            beforeSend: function() {
                $( '#dashboard' ).fadeOut( 800 );
                $( 'loading' ).fadeIn( 1000 );
            },
            success: function( response ) {
                if ( 0 != response ) {
                    view = $.parseHTML( response );
                    /* Add the modal to the body. */
                    $( '#dashboard' ).html( view ).fadeIn( "slow" );

                    switch ( _view ) {
                        default: 
                            app.overview.run();
                        break;
                        case 'overview':
                            app.overview.run();
                            break;
                        case 'users':
                            app.users.run();
                            break;
                        case 'leads':
                            app.leads.run();
                            break;
                        case 'clients':
                            app.clients.run();
                            break;
                        case 'evangelizers':
                            app.evangelizers.run();
                            break;
                    }
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {  
                console.log( 'Houve um erro inesperado.' ); 
            },
            complete: function(){
                $( '#dashboard' ).fadeIn( 1000 );
                $( 'loading' ).fadeOut( 800 );
            }
        });
    }

    function load_events() {
        $( '[data-animate="count"]' ).each( function () {
            $( this ).prop( 'Counter', 0 ).animate({
                Counter: $( this ).text()
            }, {
                duration: 4000,
                easing: 'swing',
                step: function ( now ) {
                    $( this ).text( Math.ceil( now ) );
                }
            });
        });
        $('.selectpicker').selectpicker({
            selectpicker: true,
            liveSearchNormalize: true,
            liveSearchPlaceholder: 'Pesquisar',
            size: 10
        });

        $('.selectpicker').on( 'show.bs.select', function ( e ) {
            e.preventDefault();
            
            $( '.bootstrap-select' ).removeClass( 'open' );
            return false;
        });

        $('.selectpicker').on( 'changed.bs.select', function ( clickedIndex, newValue, oldValue ) {
            $( '.bootstrap-select' ).removeClass( 'open' );
        });

        $(document).click(function(){
            $( '.dropdown' ).removeClass( 'show' );
            $( '.bootstrap-select' ).removeClass( 'open' );
        });

        $( '[data-toggle="dropdown"]' ).click(function(e) {
            e.preventDefault();

            if( $(this).hasClass('btn') ){
                $( this ).parent().toggleClass( 'open' ).next().toggleClass( 'open' ); 
            } else{
                $( this ).parent().toggleClass( 'show' ).next().toggleClass( 'show' );
            }
            return false;
        });

        $('[data-toggle="tooltip"]').tooltip();
    }

    $( document ).on( 'ready' ,function() {
        startTime();
        loadView();
    });

    $( window ).on( 'load', function() { 
        /* Load a modal when any element with a modal specified is clicked */
        $( '[data-target="view"]' ).on( 'click', function( e ) {                
            e.preventDefault(); 
            $( '.navigation--item' ).removeClass('active');
            $( this ).parent().addClass('active');

            var view = $( this ).data( 'view' );               
            loadView( view );
        }); 
    });

    // Define o objeto global do projeto, igual a um já existente ou cria um novo objeto.
    var app = app || {View:{}, Model:{}, Collection:{}};

    app.overview = {
        options:{
            className: "",
            template: "",
            ajaxUrl: overview_ajax.url,
            ajaxAction: overview_ajax.action,
            ajaxNonce: overview_ajax.nonce
        },
        
        run: function() {
            this.init();
        },

        init: function() {
            var self = this;

            self.bindEvents();

            self.postMostPopular();
            self.productsPopular();
            self.categoriesPopular();
            self.loadUsersRecents();
            self.loadLeads();
            self.loadClients();
            self.loadEvangelizers();
            self.loadPostsMostAccessed();
        },

        bindEvents: function(){
            var self = this;

            load_events();

            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data,
                function() { //beforeSend
                    $( '.card-stats' ).find( '.loading' ).show();
                    $( '.card-social' ).find( '.loading' ).show();
                    $( '.card-featured' ).find( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.overview;
                        console.log( data );

                        $( '#total_users' ).html( self.kFormatter( data.total_users ) ).fadeIn( 'slow' );
                        $( '#total_posts' ).html( self.kFormatter( data.total_posts ) ).fadeIn( 'slow' );
                        $( '#total_visitors' ).html( self.kFormatter( data.total_visitors ) ).fadeIn( 'slow' );
                        $( '#unique_accesses' ).html( self.kFormatter( data.unique_accesses ) ).fadeIn( 'slow' );

                        $( '#instagram_followers_count' ).html( self.kFormatter( data.instagram_followers_count ) ).fadeIn( 'slow' );
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //complete
                    $( '.card-stats' ).find( '.loading' ).hide();
                    $( '.card-social' ).find( '.loading' ).hide();
                    $( '.card-featured' ).find( '.loading' ).hide();
                }
            );
        },

        postMostPopular: function() {
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '#card-post_mostread' ).find( '.loading' ).show();
                }, 
                function( response ) { //success                    
                    if( response.success === true ){
                        var data = response.data.overview.postMostPopular;

                        var _list = '<li>' +
                            '<a href="' + data.post_link  + '" target="_blank">' +
                            '<h4 id="post_title">' + data.post_title  + '</h4>' +
                            '<span id="post_count">' +  data.post_count + '</span>' +
                            '</a>' +
                        '</li>';

                        $( '#popularpost' ).hide().html( _list ).fadeIn( 'slow' )
                    }

                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '#card-post_mostread' ).find( '.loading' ).hide();
                }
            );
        },

        productsPopular: function() {
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '#card-products_clicks' ).find( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.overview.productsClicks;
                        if( data ){
                            var _list = '';
                            
                            for ( var i in data ) {              
                                _list += '<li><a href="' + data[i].product_link + '" target="_blank">' + data[i].product_name + '</a> <div class="badge bg-blue">' + data[i].product_count + '</div></li>';
                            }

                            $( '#products_clicks' ).fadeIn( 'slow' ).find('ul').hide().html( _list ).fadeIn( 'slow' );
                        }
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '#card-products_clicks' ).find( '.loading' ).hide();
                }
            );
        },

        categoriesPopular: function() {
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '#card-categories_mostread' ).find( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.overview.categoriesMostAccessed;
                        if( data ){
                            var _list = '';
                            
                            for ( var i in data ) {              
                                _list += '<li><a href="' + data[i].category_link + '" target="_blank">' + data[i].category_name + '</a> <div class="badge bg-blue">' + data[i].category_count + '</div></li>';
                            }

                            $( '#categories_mostread' ).fadeIn( 'slow' ).find('ul').hide().html( _list ).fadeIn( 'slow' );
                        }
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '#card-categories_mostread' ).find( '.loading' ).hide();
                }
            );
        },

        loadUsersRecents: function(){
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '#card-users_recents' ).find( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.overview.recentsUsers.total;

                        if( 0 != data ){
                            var total = data;
                        }else{
                            var total = 0;
                        }
                        
                        var _html = '<a href="javascript:void(0)" class="got_to_view" data-view="users">' +
                            '<div class="box-label">Total</div>' +
                            '<div class="box-total badge bg-blue">' + total + '</div>' +
                        '</a>';
                        $( '#users_recents' ).fadeIn( 'slow' ).html( _html ).fadeIn( 'slow' );

                        $( '.got_to_view' ).on( 'click', function() {
                            var view = $( this ).data( 'view' );

                            $( '.navigation--item' ).removeClass('active');
                            $( '#' + view ).parent().addClass('active');
               
                            loadView( view );
                        });
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '#card-users_recents' ).find( '.loading' ).hide();
                }
            );
        },

        loadLeads: function(){
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '#card-leads_recents' ).find( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.overview.recentsLeads.total;

                        if( 0 != data ){
                            var total = data;
                        }else{
                            var total = 0;
                        }
                        
                        var _html = '<a href="javascript:void(0)" class="got_to_view" data-view="leads">'+
                            '<div class="box-label">Total</div>'+
                            '<div class="box-total badge bg-blue">' + total + '</div>'+
                        '</a>';
                        $( '#leads_recents' ).fadeIn( 'slow' ).html( _html ).fadeIn( 'slow' );

                        $( '.got_to_view' ).on( 'click', function() {
                            var view = $( this ).data( 'view' );

                            $( '.navigation--item' ).removeClass('active');
                            $( '#' + view ).parent().addClass('active');
               
                            loadView( view );
                        });
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '#card-leads_recents' ).find( '.loading' ).hide();
                }
            );
        },

        loadClients: function(){
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '#card-clients_recents' ).find( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.overview.recentsClients.total;

                        if( 0 != data ){
                            var total = data;
                        }else{
                            var total = 0;
                        }

                        var _html = '<a href="javascript:void(0)" class="got_to_view" data-view="clients">'+
                            '<div class="box-label">Total</div>'+
                            '<div class="box-total badge bg-blue">' + total + '</div>'+
                        '</a>';
                        $( '#clients_recents' ).fadeIn( 'slow' ).html( _html ).fadeIn( 'slow' );

                        $( '.got_to_view' ).on( 'click', function() {
                            var view = $( this ).data( 'view' );

                            $( '.navigation--item' ).removeClass('active');
                            $( '#' + view ).parent().addClass('active');
               
                            loadView( view );
                        });
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '#card-clients_recents' ).find( '.loading' ).hide();
                }
            );
        },

        loadEvangelizers: function(){
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '#card-evangelizers_recents' ).find( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.overview.recentsEvangelizers;

                        if( 0 != data ){

                            var _html = "";
                            $.each( data, function( index, value ) {

                                if( value.status === "canceled" ){
                                    var labelbg = "bg-red";
                                }
                                if( value.status === "invoiced" ){
                                    var labelbg = "bg-green";
                                }

                                _html += '<tr class="parent">'+
                                    '<td>' + index + '</td>'+
                                    '<td>' + value.username + '</td>'+
                                    '<td><span class="label ' + labelbg + '">' + value.statusDescription + '</span></td>'+
                                    '<td>' + value.creationDate + '</td>'+
                                    '<td>' + value.countItems + '</td>'+
                                '</tr>';
                            });

                            $( "#overview--evangelizers_recents" ).children('tbody').html( _html ).fadeIn( 'slow' );
                        } else{
                            var _html = '<tr>' +
                                '<td>Nenhum usuário Evangelizador encontrado.</td>' +
                            '</tr>';
                            $( '#overview--evangelizers_recents' ).html( _html ).fadeIn( 'slow' );
                        }
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '#card-evangelizers_recents' ).find( '.loading' ).hide();
                }
            );
        },

        loadPostsMostAccessed: function( period ){
            var self = this;

            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce,
                period: period
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.overview.postsMostAccessed;
                        if( data ){
                            var _list = '';
                            
                            for ( var i in data ) {              
                                _list += '<li><a href="' + data[i].post_href + '" target="_blank">' + data[i].post_title + '</a> <div class="badge bg-blue"></div></li>';
                            }

                            $( '#posts_most_accessed' ).fadeIn( 'slow' ).find('ul').hide().html( _list ).fadeIn( 'slow' );
                        }
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '.loading' ).hide();
                }
            );
        },

        changePeriod: function(){          
            var self = this;

            $( '.period' ).on( 'click', function( e ) {     
                e.preventDefault();
                $('.period').removeClass('active');
                $(this).addClass('active');

                var period = $( this ).data( 'period' );                
                self.loadPostsMostAccessed( period );
            });
        },

        kFormatter: function( num ) {
            if ( num >= 1000000000 ) {
                return (  num / 1000000000 ).toFixed( 1 ).replace(/\.0$/, '') + 'G';
            }
            if ( num > 999999 && num < 1000000000 ) {
                return (num / 1000000).toFixed( 1 ) + 'M';
            }
            if ( num > 999 && num < 1000000 ) {
                return ( num / 1000 ).toFixed( 1 ).replace(/\.0$/, '') + 'K';
            }

            return num;
        },

        formatMoneyBR: function( int ) {
            var tmp = int+'';
            var neg = false;

            if( tmp.indexOf( "-" ) == 0 ){
                neg = true;
                tmp = tmp.replace( "-","" );
            }
            
            if( tmp.length == 1 ) tmp = "0" + tmp
        
            tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
            if( tmp.length > 6 )
                tmp = tmp.replace( /([0-9]{3}),([0-9]{2}$)/g, ".$1,$2" );
            
            if( tmp.length > 9 )
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2,$3");
        
            if( tmp.length > 12 )
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2.$3,$4");
            
            if( tmp.indexOf( "." ) == 0 ) tmp = tmp.replace( ".", "" );
            if( tmp.indexOf( "," ) == 0 ) tmp = tmp.replace( ",", "0," );
            
            return ( neg ? '-' + tmp : tmp );
        }
    };

    app.users = {
        options:{
            className: "",
            template: "users",
            ajaxUrl: data_users_ajax.url,
            ajaxAction: data_users_ajax.action,
            ajaxNonce: data_users_ajax.nonce
        },

        run: function() {
            this.init();
        },

        init: function() {
            var self = this;

            self.bindEvents();          
        },

        bindEvents: function(){
            var self = this;

            load_events();
            self.load_recents_users();

            $( '#select_user_id' ).change( function() {

                var user_id = $( this ).val(); 
                self.selectUserById( user_id );

            });            

            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            

            ajax_json_post( 
                self.options.ajaxUrl, 
                data,
                function() { //beforeSend
                    $( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){ 
                        var data = response.data.users;
                        
                        if( data.total_users ){
                            $( '#users-total_users' ).html( self.kFormatter( data.total_users ) ).fadeIn( 'slow' );
                        }
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //complete
                    $( '.loading' ).hide();
                }
            ); 
        },

        load_recents_users: function(){
            var self = this;

            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.users.recents_users;
                        if( data ){
                            var _list = '';
                            
                            for ( var i in data ) {              
                                _list += '<li><a href="javascript:void(0)" data-user="' + data[i].userid + '" class="user--link">' + data[i].username + '</a></li>';
                            }

                            $( '#users--recents_users' ).fadeIn( 'slow' ).find('ul').hide().html( _list ).fadeIn( 'slow' );

                            $( '.user--link' ).on( 'click', function() {                
                                var user_id = $( this ).data( 'user' );
                                self.selectUserById( user_id );
                            }); 
                        }
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '.loading' ).hide();
                }
            );
        },

        selectUserById: function( user_id ){
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce,
                user_id: user_id
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.users;
                        if( data ){
                            if ( data.search_user.leads ) {
                                self.set_info_user( data.search_user.leads );
                            }

                            if ( data.posts_most_accessed && data.posts_most_accessed.length != 0 ) {
                                self.most_popular_posts( data.posts_most_accessed );
                                $( '#is_popular_posts' ).fadeIn( 'slow' );
                            } else{
                                $( '#is_popular_posts' ).fadeOut( 'slow' );                             
                            }

                            if ( data.search_user_leads.objects && data.search_user_leads.objects.length != 0 ) {
                                self.is_leads( data.search_user_leads.objects );
                                $( '#is_leads' ).fadeIn( 'slow' );
                            } else{
                                $( '#is_leads' ).fadeOut( 'slow' );                             
                            }                            
                        }
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '#users--row_list' ).fadeOut( 'slow', function() {
                        $( '.loading' ).hide();
                        $( '#users--row_search' ).fadeIn( 'slow' );
                    });                    
                }
            );
        },

        most_popular_posts: function( data ){
            var self = this;

            var _list = '';
                            
            for ( var i in data ) {              
                _list += '<li><a href="' + data[i].post_link + '" target="_blank">' + data[i].post_title + '</a> <div class="badge bg-blue">' + data[i].post_count + '</div></li>';
            }

            $(  '#users--most_popular_posts' ).fadeIn( 'slow' ).find('ul').hide().html( _list ).fadeIn( 'slow' );
        },

        set_info_user: function( data ){
            var self = this;

            var objects = [
                { "name" : data.profile.user_login, "label" : "Login", "icon": 'contact_mail' },
                { "name" : data.profile.user_registered, "label" : "Data de Registro", "icon": 'contact_mail' },
                { "name" : data.profile.user_email, "label" : "E-mail", "icon": 'contact_mail' },
                { "name" : data.profile.api_phone, "label" : "Telefone", "icon": 'contact_phone' },
                { "name" : data.profile.user_genero, "label" : "Sexo", "icon": 'contact_mail' },
                { "name" : data.profile.user_category, "label" : "Categoria", "icon": 'contact_mail' },
                { "name" : data.type, "label" : "Tipo de Usuário", "icon": 'contact_mail' }
            ];

            $( '#users--load_author' ).html( data.user_avatar + '<h5 class="title">' + data.display_name + '</h5>' ).fadeIn( 'slow' );

            var _list = '';

            for ( var i in objects ) {
                if( objects[i].name != null && objects[i].name != '' ){
                    _list += '<li><div class="label bg-blue">' + objects[i].label +  '</div><span>' + objects[i].name + '</span></li>';
                }
            }

            $( '#users--load_description' ).find('ul').hide().html( _list ).fadeIn( 'slow' );

        },

        is_leads: function( data ){
            var self = this;

            var _html     = "";
            $.each( data, function( index, value ) {

                if( value.status === "canceled" ){
                    var labelbg = "bg-red";
                }
                if( value.status === "invoiced" ){
                    var labelbg = "bg-green";
                }

                _html += '<tr class="parent">'+
                    '<td>' + index + '</td>'+
                    '<td>' + value.origin + '</td>'+
                    '<td><span class="label ' + labelbg + '">' + value.statusDescription + '</span></td>'+
                    '<td>' + value.creationDate + '</td>'+
                    '<td>' + value.countItems + '</td>'+
                    '<td>R$: ' + self.formatMoneyBR(value.totalValue) + '</td>'+
                '</tr>';
            });

            $( "#users--table_leads" ).children('tbody').html( _html ).fadeIn( 'slow' );
        },

        formatMoneyBR: function( int ) {
            var tmp = int+'';
            var neg = false;

            if( tmp.indexOf( "-" ) == 0 ){
                neg = true;
                tmp = tmp.replace( "-","" );
            }
            
            if( tmp.length == 1 ) tmp = "0" + tmp
        
            tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
            if( tmp.length > 6 )
                tmp = tmp.replace( /([0-9]{3}),([0-9]{2}$)/g, ".$1,$2" );
            
            if( tmp.length > 9 )
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2,$3");
        
            if( tmp.length > 12 )
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2.$3,$4");
            
            if( tmp.indexOf( "." ) == 0 ) tmp = tmp.replace( ".", "" );
            if( tmp.indexOf( "," ) == 0 ) tmp = tmp.replace( ",", "0," );
            
            return ( neg ? '-' + tmp : tmp );
        },

        kFormatter: function( num ) {
            if ( num >= 1000000000 ) {
                return (  num / 1000000000 ).toFixed( 1 ).replace(/\.0$/, '') + 'G';
            }
            if ( num > 999999 && num < 1000000000 ) {
                return (num / 1000000).toFixed( 1 ) + 'M';
            }
            if ( num > 999 && num < 1000000 ) {
                return ( num / 1000 ).toFixed( 1 ).replace(/\.0$/, '') + 'K';
            }

            return num;
        }
    };

    app.leads = {
        options:{
            className: "",
            template: "leads",
            ajaxUrl: data_leads_ajax.url,
            ajaxAction: data_leads_ajax.action,
            ajaxNonce: data_leads_ajax.nonce
        },

        run: function() {
            this.init();
        },

        init: function() {
            var self = this;

            self.bindEvents();          
        },

        bindEvents: function(){
            var self = this;

            load_events();
            self.leads_users_recents();

            $( '#user_leads_select' ).change( function() {
                var user_id = $( this ).val(); 
                self.get_data_user_in_select( user_id );
            });

            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data,
                function() { //beforeSend
                }, 
                function( response ) { //success
                    if( response.success === true ){ 
                        var data = response.data.leads;

                        if( data.leads_users_total > 0 ){
                            $( '#leads--total_users_leads' ).html( self.kFormatter( data.leads_users_total ) ).fadeIn( 'slow' );
                        } else{
                            $( '#leads--total_users_leads' ).html( '0' ).fadeIn( 'slow' );
                        }                        
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //complete
                }
            ); 
        },

        leads_users_recents: function(){
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.leads.leads_users_recents;
                        if( data ){
                            var _list = '';
                            
                            for ( var i in data ) {              
                                _list += '<li><a href="javascript:void(0)" data-value="' + data[i].user_id + '" class="user--link">' + data[i].username + '</a></li>';
                            }

                            $( '#leads--recents_users' ).fadeIn( 'slow' ).find('ul').hide().html( _list ).fadeIn( 'slow' );
                            
                            $( '.user--link' ).on( 'click', function() {
                                
                                var user_id = $( this ).data( 'value' );
                                self.get_data_user_in_select( user_id );
                            }); 
                        }
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '.loading' ).hide();
                }
            );
        },

        get_data_user_in_select: function( user_id ){
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce,
                user_id: user_id
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.leads;
                        console.log( data );

                        if( data ){

                            if ( data.search_user ) {
                                self.set_info_user( data.search_user );
                            }

                            // if ( data.leads_users_popupar_post && data.leads_users_popupar_post.length != 0 ) {
                            //     self.leads_users_popupar_post( data.leads_users_popupar_post );
                            //     $( '#is_popular_posts_leads' ).fadeIn( 'slow' );
                            // } else{
                            //     $( '#is_popular_posts_leads' ).fadeOut( 'slow' );                           
                            // }

                            if ( data.search_user_leads.objects && data.search_user_leads.objects.length != 0 ) {
                                self.is_leads( data.search_user_leads.objects );
                                $( '#is_leads' ).fadeIn( 'slow' );
                            } else{
                                $( '#is_leads' ).fadeOut( 'slow' );                             
                            }
                            
                        }
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '.loading' ).hide();
                    $( '#leads--row_list' ).fadeOut( 'slow', function() {
                        $( '#leads--row_search' ).fadeIn( 'slow' );
                    });                    
                }
            );
        },

        set_info_user: function( data ){
            var self = this;

            var objects = [
                { "name" : data.user_login, "label" : "Login", "icon": 'contact_mail' },
                { "name" : data.user_registered, "label" : "Data de Registro", "icon": 'contact_mail' },
                { "name" : data.user_email, "label" : "E-mail", "icon": 'contact_mail' },
                { "name" : data.api_phone, "label" : "Telefone", "icon": 'contact_phone' },
                { "name" : data.user_genero, "label" : "Sexo", "icon": 'contact_mail' },
                { "name" : data.user_category, "label" : "Categoria", "icon": 'contact_mail' }
            ];

            $( '#leads--load_author' ).html( data.user_avatar + '<h5 class="title">' + data.display_name + '</h5>' ).fadeIn( 'slow' );

            var _list = '';

            for ( var i in objects ) {
                if( objects[i].name != null && objects[i].name != '' ){
                    _list += '<li><div class="label bg-blue">' + objects[i].label +  '</div><span>' + objects[i].name + '</span></li>';
                }
            }

            $( '#leads--load_description' ).find('ul').hide().html( _list ).fadeIn( 'slow' );

        },

        leads_users_popupar_post: function( data ){
            var self = this;

            var _list = '';
                            
            for ( var i in data ) {              
                _list += '<li><a href="' + data[i].post_link + '" target="_blank">' + data[i].post_title + '</a> <div class="badge bg-blue">' + data[i].post_count + '</div></li>';
            }

            $(  '#leads--most_popular_posts' ).fadeIn( 'slow' ).find('ul').hide().html( _list ).fadeIn( 'slow' );
        },

        is_leads: function( data ){
            var self = this;

            var _html     = "";
            $.each( data, function( index, value ) {

                if( value.status === "canceled" ){
                    var labelbg = "bg-red";
                }
                if( value.status === "invoiced" ){
                    var labelbg = "bg-green";
                }

                _html += '<tr class="parent">'+
                    '<td>' + index + '</td>'+
                    '<td>' + value.origin + '</td>'+
                    '<td><span class="label ' + labelbg + '">' + value.statusDescription + '</span></td>'+
                    '<td>' + value.creationDate + '</td>'+
                    '<td>' + value.countItems + '</td>'+
                    '<td>R$: ' + self.formatMoneyBR(value.totalValue) + '</td>'+
                '</tr>';
            });

            $( "#leads--table_leads" ).children('tbody').html( _html ).fadeIn( 'slow' );
        },

        formatMoneyBR: function( int ) {
            var tmp = int+'';
            var neg = false;

            if( tmp.indexOf( "-" ) == 0 ){
                neg = true;
                tmp = tmp.replace( "-","" );
            }
            
            if( tmp.length == 1 ) tmp = "0" + tmp
        
            tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
            if( tmp.length > 6 )
                tmp = tmp.replace( /([0-9]{3}),([0-9]{2}$)/g, ".$1,$2" );
            
            if( tmp.length > 9 )
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2,$3");
        
            if( tmp.length > 12 )
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2.$3,$4");
            
            if( tmp.indexOf( "." ) == 0 ) tmp = tmp.replace( ".", "" );
            if( tmp.indexOf( "," ) == 0 ) tmp = tmp.replace( ",", "0," );
            
            return ( neg ? '-' + tmp : tmp );
        },

        kFormatter: function( num ) {
            if ( num >= 1000000000 ) {
                return (  num / 1000000000 ).toFixed( 1 ).replace(/\.0$/, '') + 'G';
            }
            if ( num > 999999 && num < 1000000000 ) {
                return (num / 1000000).toFixed( 1 ) + 'M';
            }
            if ( num > 999 && num < 1000000 ) {
                return ( num / 1000 ).toFixed( 1 ).replace(/\.0$/, '') + 'K';
            }

            return num;
        }
    };

    app.clients = {
        options:{
            className: "",
            template: "clients",
            ajaxUrl: data_clients_ajax.url,
            ajaxAction: data_clients_ajax.action,
            ajaxNonce: data_clients_ajax.nonce
        },

        run: function() {
            this.init();
        },

        init: function() {
            var self = this;

            self.bindEvents();          
        },

        bindEvents: function(){
            var self = this;

            load_events();
            self.users_recents();

            $( '#user_clients_select' ).change( function() {
                var user_id = $( this ).val(); 
                self.get_data_user_in_select( user_id );
            });

            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data,
                function() { //beforeSend
                }, 
                function( response ) { //success
                    if( response.success === true ){ 
                        var data = response.data.clients;

                        if( data.users_total.total > 0 ){
                            $( '#clients--users_total' ).html( self.kFormatter( data.users_total.total ) ).fadeIn( 'slow' );
                        } else{
                            $( '#clients--users_total' ).html( '0' ).fadeIn( 'slow' );
                        }                       
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //complete
                }
            ); 
        },

        users_recents: function(){
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.clients.users_recents;
                        if( data ){
                            var _list = '';
                            
                            for ( var i in data ) {              
                                _list += '<li><a href="javascript:void(0)" data-value="' + data[i].user_id + '" class="user--link">' + data[i].username + '</a></li>';
                            }

                            $( '#clients--users_recents' ).fadeIn( 'slow' ).find('ul').hide().html( _list ).fadeIn( 'slow' );

                            $( '.user--link' ).on( 'click', function() {                                
                                var user_id = $( this ).data( 'value' );
                                self.get_data_user_in_select( user_id );
                            }); 
                        }
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '.loading' ).hide();
                }
            );
        },

        get_data_user_in_select: function( user_id ){
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce,
                user_id: user_id
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.clients;
                        console.log( data );

                        if( data ){

                            if ( data.search_user ) {
                                self.set_info_user( data.search_user );
                            }

                            // if ( data.leads_users_popupar_post && data.leads_users_popupar_post.length != 0 ) {
                            //     self.leads_users_popupar_post( data.leads_users_popupar_post );
                            //     $( '#clients-is_popular_posts' ).fadeIn( 'slow' );
                            // } else{
                            //     $( '#clients-is_popular_posts' ).fadeOut( 'slow' );                           
                            // }

                            if ( data.search_user_leads.objects && data.search_user_leads.objects.length != 0 ) {
                                self.is_leads( data.search_user_leads.objects );
                                $( '#clients-is_leads' ).fadeIn( 'slow' );
                            } else{
                                $( '#clients-is_leads' ).fadeOut( 'slow' );                             
                            }
                            
                        }
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '.loading' ).hide();
                    $( '#clients--row_list' ).fadeOut( 'slow', function() {
                        $( '#clients--row_search' ).fadeIn( 'slow' );
                    });                    
                }
            );
        },

        set_info_user: function( data ){
            var self = this;

            var objects = [
                { "name" : data.user_login, "label" : "Login", "icon": 'contact_mail' },
                { "name" : data.user_registered, "label" : "Data de Registro", "icon": 'contact_mail' },
                { "name" : data.user_email, "label" : "E-mail", "icon": 'contact_mail' },
                { "name" : data.api_phone, "label" : "Telefone", "icon": 'contact_phone' },
                { "name" : data.user_genero, "label" : "Sexo", "icon": 'contact_mail' },
                { "name" : data.user_category, "label" : "Categoria", "icon": 'contact_mail' }
            ];

            $( '#clients--author' ).html( data.user_avatar + '<h5 class="title">' + data.display_name + '</h5>' ).fadeIn( 'slow' );

            var _list = '';

            for ( var i in objects ) {
                if( objects[i].name != null && objects[i].name != '' ){
                    _list += '<li><div class="label bg-blue">' + objects[i].label +  '</div><span>' + objects[i].name + '</span></li>';
                }
            }

            $( '#clients--description' ).find('ul').hide().html( _list ).fadeIn( 'slow' );

        },

        leads_users_popupar_post: function( data ){
            var self = this;

            var _list = '';
                            
            for ( var i in data ) {              
                _list += '<li><a href="' + data[i].post_link + '" target="_blank">' + data[i].post_title + '</a> <div class="badge bg-blue">' + data[i].post_count + '</div></li>';
            }

            $(  '#clients--most_popular_posts' ).fadeIn( 'slow' ).find('ul').hide().html( _list ).fadeIn( 'slow' );
        },

        is_leads: function( data ){
            var self = this;

            var _html     = "";
            $.each( data, function( index, value ) {

                if( value.status === "canceled" ){
                    var labelbg = "bg-red";
                }
                if( value.status === "invoiced" ){
                    var labelbg = "bg-green";
                }

                _html += '<tr class="parent">'+
                    '<td>' + index + '</td>'+
                    '<td>' + value.origin + '</td>'+
                    '<td><span class="label ' + labelbg + '">' + value.statusDescription + '</span></td>'+
                    '<td>' + value.creationDate + '</td>'+
                    '<td>' + value.countItems + '</td>'+
                    '<td>R$: ' + self.formatMoneyBR(value.totalValue) + '</td>'+
                '</tr>';
            });

            $( "#clients--table_orders" ).children('tbody').html( _html ).fadeIn( 'slow' );
        },

        formatMoneyBR: function( int ) {
            var tmp = int+'';
            var neg = false;

            if( tmp.indexOf( "-" ) == 0 ){
                neg = true;
                tmp = tmp.replace( "-","" );
            }
            
            if( tmp.length == 1 ) tmp = "0" + tmp
        
            tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
            if( tmp.length > 6 )
                tmp = tmp.replace( /([0-9]{3}),([0-9]{2}$)/g, ".$1,$2" );
            
            if( tmp.length > 9 )
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2,$3");
        
            if( tmp.length > 12 )
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2.$3,$4");
            
            if( tmp.indexOf( "." ) == 0 ) tmp = tmp.replace( ".", "" );
            if( tmp.indexOf( "," ) == 0 ) tmp = tmp.replace( ",", "0," );
            
            return ( neg ? '-' + tmp : tmp );
        },

        kFormatter: function( num ) {
            if ( num >= 1000000000 ) {
                return (  num / 1000000000 ).toFixed( 1 ).replace(/\.0$/, '') + 'G';
            }
            if ( num > 999999 && num < 1000000000 ) {
                return (num / 1000000).toFixed( 1 ) + 'M';
            }
            if ( num > 999 && num < 1000000 ) {
                return ( num / 1000 ).toFixed( 1 ).replace(/\.0$/, '') + 'K';
            }

            return num;
        }
    };

    app.evangelizers = {
        options:{
            className: "",
            template: "clients",
            ajaxUrl: data_evangelizer_ajax.url,
            ajaxAction: data_evangelizer_ajax.action,
            ajaxNonce: data_evangelizer_ajax.nonce
        },

        run: function() {
            this.init();
        },

        init: function() {
            var self = this;

            self.bindEvents();          
        },

        bindEvents: function(){
            var self = this;

            load_events();
            self.users_recents();

            $( '#user_evangelizers_select' ).change( function() {
                var user_id = $( this ).val(); 
                self.get_data_user_in_select( user_id );
            }); 

            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data,
                function() { //beforeSend
                }, 
                function( response ) { //success
                    if( response.success === true ){ 
                        var data = response.data.evangelizers;
                        $( '#evangelizers--users_total' ).html( self.kFormatter( data.users_total ) ).fadeIn( 'slow' );
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //complete
                }
            ); 
        },

        users_recents: function(){
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.evangelizers.users_recents;

                        if( 0 != data ){                            
                            var _list = '';
                            
                            for ( var i in data ) {              
                                _list += '<li><a href="javascript:void(0)" data-value="' + data[i].user_id + '" class="user--link">' + data[i].username + '</a></li>';
                            }

                            $( '#evangelizers--users_recents' ).fadeIn( 'slow' ).find('ul').hide().html( _list ).fadeIn( 'slow' );

                            $( '.user--link' ).on( 'click', function() {
                                
                                var user_id = $( this ).data( 'value' );
                                self.get_data_user_in_select( user_id );

                            });
                        } else{
                            $( '#evangelizers--users_recents' ).fadeIn( 'slow' ).find('ul').hide().html( 'Nenhum usuário Evangelizador encontrado.' ).fadeIn( 'slow' );
                        }
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '.loading' ).hide();
                }
            );
        },

        get_data_user_in_select: function( user_id ){
            var self = this;
            var data = {
                action: self.options.ajaxAction,
                nonce: self.options.ajaxNonce,
                user_id: user_id
            };            
            
            ajax_json_post( 
                self.options.ajaxUrl, 
                data, 
                function() { //beforeSend
                    $( '.loading' ).show();
                }, 
                function( response ) { //success
                    if( response.success === true ){
                        var data = response.data.evangelizers;
                        console.log( data );

                        if( data ){

                            if ( data.search_user ) {
                                self.set_info_user( data.search_user );
                            }

                            // if ( data.leads_users_popupar_post && data.leads_users_popupar_post.length != 0 ) {
                            //     self.leads_users_popupar_post( data.leads_users_popupar_post );
                            //     $( '#evangelizers-is_popular_posts' ).fadeIn( 'slow' );
                            // } else{
                            //     $( '#evangelizers-is_popular_posts' ).fadeOut( 'slow' );                           
                            // }

                            if ( data.search_user_leads.objects && data.search_user_leads.objects.length != 0 ) {
                                self.is_leads( data.search_user_leads.objects );
                                $( '#evangelizers-is_leads' ).fadeIn( 'slow' );
                            } else{
                                $( '#evangelizers-is_leads' ).fadeOut( 'slow' );                             
                            }
                            
                        }
                    }
                }, 
                function( error ) { //error
                    console.log( error.responseText );

                }, 
                function() { //done
                    $( '.loading' ).hide();
                    $( '#evangelizers--row_list' ).fadeOut( 'slow', function() {
                        $( '#evangelizers--row_search' ).fadeIn( 'slow' );
                    });                    
                }
            );
        },

        set_info_user: function( data ){
            var self = this;

            var objects = [
                { "name" : data.user_login, "label" : "Login", "icon": 'contact_mail' },
                { "name" : data.user_registered, "label" : "Data de Registro", "icon": 'contact_mail' },
                { "name" : data.user_email, "label" : "E-mail", "icon": 'contact_mail' },
                { "name" : data.api_phone, "label" : "Telefone", "icon": 'contact_phone' },
                { "name" : data.user_genero, "label" : "Sexo", "icon": 'contact_mail' },
                { "name" : data.user_category, "label" : "Categoria", "icon": 'contact_mail' }
            ];

            $( '#evangelizers--author' ).html( data.user_avatar + '<h5 class="title">' + data.display_name + '</h5>' ).fadeIn( 'slow' );

            var _list = '';

            for ( var i in objects ) {
                if( objects[i].name != null && objects[i].name != '' ){
                    _list += '<li><div class="label bg-blue">' + objects[i].label +  '</div><span>' + objects[i].name + '</span></li>';
                }
            }

            $( '#evangelizers--description' ).find('ul').hide().html( _list ).fadeIn( 'slow' );

        },

        leads_users_popupar_post: function( data ){
            var self = this;

            var _list = '';
                            
            for ( var i in data ) {              
                _list += '<li><a href="' + data[i].post_link + '" target="_blank">' + data[i].post_title + '</a> <div class="badge bg-blue">' + data[i].post_count + '</div></li>';
            }

            $(  '#evangelizers--most_popular_posts' ).fadeIn( 'slow' ).find('ul').hide().html( _list ).fadeIn( 'slow' );
        },

        is_leads: function( data ){
            var self = this;

            var _html     = "";
            $.each( data, function( index, value ) {

                if( value.status === "canceled" ){
                    var labelbg = "bg-red";
                }
                if( value.status === "invoiced" ){
                    var labelbg = "bg-green";
                }

                _html += '<tr class="parent">'+
                    '<td>' + index + '</td>'+
                    '<td>' + value.origin + '</td>'+
                    '<td><span class="label ' + labelbg + '">' + value.statusDescription + '</span></td>'+
                    '<td>' + value.creationDate + '</td>'+
                    '<td>' + value.countItems + '</td>'+
                    '<td>R$: ' + self.formatMoneyBR(value.totalValue) + '</td>'+
                '</tr>';
            });

            $( "#evangelizers--table_leads" ).children('tbody').html( _html ).fadeIn( 'slow' );
        },

        formatMoneyBR: function( int ) {
            var tmp = int+'';
            var neg = false;

            if( tmp.indexOf( "-" ) == 0 ){
                neg = true;
                tmp = tmp.replace( "-","" );
            }
            
            if( tmp.length == 1 ) tmp = "0" + tmp
        
            tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
            if( tmp.length > 6 )
                tmp = tmp.replace( /([0-9]{3}),([0-9]{2}$)/g, ".$1,$2" );
            
            if( tmp.length > 9 )
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2,$3");
        
            if( tmp.length > 12 )
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2.$3,$4");
            
            if( tmp.indexOf( "." ) == 0 ) tmp = tmp.replace( ".", "" );
            if( tmp.indexOf( "," ) == 0 ) tmp = tmp.replace( ",", "0," );
            
            return ( neg ? '-' + tmp : tmp );
        },

        kFormatter: function( num ) {
            if ( num >= 1000000000 ) {
                return (  num / 1000000000 ).toFixed( 1 ).replace(/\.0$/, '') + 'G';
            }
            if ( num > 999999 && num < 1000000000 ) {
                return (num / 1000000).toFixed( 1 ) + 'M';
            }
            if ( num > 999 && num < 1000000 ) {
                return ( num / 1000 ).toFixed( 1 ).replace(/\.0$/, '') + 'K';
            }

            return num;
        }
    };

})(jQuery);