(function($) {
    'use strict';
    
    function FB_init() {
        window.fbAsyncInit = function() {
            FB.init({
                appId: '1651753734937810',
                status: true, 
                cookie: true, 
                xfbml: true,
                version: 'v2.12'
            });

            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    FB_DataUser();
                } else if (response.status === 'not_authorized') {
                    FB_Login();
                } else {
                    FB_Login();
                }
            });
        };

        (function(d){
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        } (document));
    }
    //FB_init();

    // Facebook login with JavaScript SDK
    function FB_Login() {
        FB.login(function(response) {
            if (response.authResponse) {
                // Get and display the user profile data
                FB_DataUser();
            } else {
                $('#login').find('socialstatus').show().html('O usuário cancelou o login ou não autorizou totalmente.');
            }
        }, {
            scope: 'email, public_profile',
            auth_type: 'rerequest'
        });
    }

    // Fetch the user profile data from facebook
    function FB_DataUser(){ 
        FB.api('/me', {
            locale: 'en_US', 
            fields: 'id, name, first_name, last_name, email, link, address, gender, website, birthday, locale, picture'
        },
        function (response) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: wpajax.ajaxurl,
                data: { 
                    'action': 'social_connect',
                    'id': response.id, 
                    'first_name': response.first_name, 
                    'last_name': response.last_name,
                    'email': response.email,
                    'gender': response.gender,
                    'locale': response.locale,
                    'avatar': response.picture.data.url,
                    'link': response.link
                },
                beforeSend: function(data) {
                    console.log('Conectando com o Facebook, aguarde...');
                },
                success: function(data){
                    console.log(data);
                    if (data.loggedin === true) {
                        $('#login').find('socialstatus').show().html(data.message);
                    } else {
                        $('#login').find('socialstatus').show().html(data.message);
                    }
                }
            });
        });
    }

    // Logout from facebook
    function FB_Logout() {
        FB.logout(function() {
            console.log('Saiu do facebook');
        });
    }

    // Share post in facebook
    function FB_PostToFeed(title, desc, url, image){
        var obj = {
            method: 'feed',
            link: url, 
            picture: image,
            name: title,
            description: desc
        };
        function callback(response){
            
        }
        FB.ui(obj, callback);
    };

    $('.facebook').click(function(){
        var elem = $(this);
        FB_PostToFeed(elem.data('title'), elem.data('desc'), elem.prop('href'), elem.data('image'));

        return false;
    });

})(jQuery);