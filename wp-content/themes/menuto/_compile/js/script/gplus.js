(function() {
    var po = document.createElement('script');
	po.type  = 'text/javascript'; 
	po.async = true;
	po.defer = true;
	po.src   = '//apis.google.com/js/client:platform.js?onload=HandleGoogleApiLibrary';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(po, s);
})();
//<script async defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};HandleGoogleApiLibrary()" onreadystatechange="if (this.readyState === 'complete') this.onload()"></script>

// Called when Google Javascript API Javascript is loaded
function HandleGoogleApiLibrary() {
	// Load "client" & "auth2" libraries
	gapi.load('client:auth2', {
		callback: function() {
			// Initialize client library
			// clientId & scope is provided => automatically initializes auth2 library
			gapi.client.init({
		    	apiKey: 'AIzaSyBfLmavN46z5cbE7K7AoVe9DAwo91YIb8M',
		    	clientId: '247185214988-atthp3d26vpb2jv5uscc1f30gu37ate4.apps.googleusercontent.com',
		    	scope: 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me'
			}).then(
				// On success
				function(success) {
			  		// After library is successfully loaded then enable the login button
			  		renderButton();
				}, 
				// On error
				function(error) {
					document.getElementById('status').innerHTML = 'Error : Failed to Load Library';
			  	}
			);
		},
		onerror: function() {
			// Failed to load libraries
		}
	});
}

function onSuccess(googleUser) {
    var profile = googleUser.getBasicProfile();
    gapi.client.load('plus', 'v1', function () {
        var request = gapi.client.plus.people.get({
            'userId': 'me'
        });
        //Display the user details
        request.execute(function (resp) {
        	console.log(resp);
            var profileHTML = '<div class="profile">'+
        		'<div class="proDetails">'+
	            	'<p><b>ID:</b> '+resp.id+'</p>'+
	            	'<p><b>Locale:</b> '+resp.language+'</p>'+
        		'</div>'+
            	'<div class="head">Welcome '+resp.displayName+'! <a href="javascript:void(0);" onclick="signOut();">Sign out</a></div>'+
	            '<img src="'+resp.image.url+'" />'+
	            '<div class="proDetails">'+
	            	'<p><b>Nome:</b> '+resp.name.givenName+'</p>'+
	            	'<p><b>Sobreome:</b> '+resp.name.familyName+'</p>'+
	            	'<p><b>E-mail:</b> '+resp.emails[0].value+'</p>'+
	            	'<p><b>Sexo:</b> '+resp.gender+'</p>'+
	            	'<p><a href="'+resp.url+'">View Google+ Profile</a></p>'+
	            '</div>'+
            '</div>';
            $('.userContent').html(profileHTML);
            $('#gSignIn').slideUp('slow');

            // Save user data
        	saveUserData(resp);
        });
    });
}

function onFailure(error) {
    document.getElementById('status').innerHTML = error;
}
function renderButton() {
    gapi.signin2.render('gSignIn', {
        'scope': 'profile email',
        'width': 240,
        'height': 50,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });
}
function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        $('.userContent').html('');
        $('#gSignIn').slideDown('slow');
    });
}

// Save user data to the database
function saveUserData(userData){
    $.post('userData.php', {
    	oauth_provider:'google',
    	userData: JSON.stringify(userData)
    }, function(data){ 
    	return true; 
    });
}