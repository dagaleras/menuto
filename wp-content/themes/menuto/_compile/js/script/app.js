/*!
 * @Author: Silvio Monnerat
 * Version: 3.3.7
 * Copyright 2017-2018 Cabana Criação, Inc.
 * Licensed under the MIT license
 */

(function($) {
    'use strict';

    var App = {
        cache: {
            $document: $(document),
            $window: $(window)
        },

        init: function() {
            this.bindEvents();
        },

        bindEvents: function() {
            var self = this;

            this.cache.$window.on( 'load', function() {
                // Page loader
                $(window).trigger("scroll");
                $(window).trigger("resize");

                self.onReadyPage(function() {
                    self.show('site', true);
                    self.show('preloader', false);
                });

            });

            this.cache.$window.on( 'resize', function() {
                self.initOwl();
            });

            this.cache.$document.on( 'ready', function() {
                $(window).trigger("resize");
                
                self.initWOW();
                self.initOwl();
                self.initCF7();   
                self.initMaskFieldForm();   
                self.getPhotosFromInstagram();
                self.myaccount();
                self.login();
                self.lostpassword();
                self.register();
                self.quiz();
                self.TW();
                self.EM();
                self.initModal();
                self.urlOpenModal();
                self.initApply();
            });

            this.cache.$window.on( 'scroll', function() {
                
                var scroll = $(window).scrollTop();
                var wh = $(window).height();  

                // if ( scroll > 100 ) {
                //     $('#header').addClass('sticky');
                // } else {            
                //     $('#header').removeClass('sticky');
                // } 

            });

        },

        onReadyPage: function(callback) {
            var intervalID = window.setInterval(checkReady, 1000);

            function checkReady() {
                if (document.getElementsByTagName('body')[0] !== undefined) {
                    window.clearInterval(intervalID);
                    callback.call(this);
                }
            }
        },

        setNavigation: function(element) {
            // Menu active
            $(element).each(function(index) {
                if(this.href.trim() == window.location)
                $(this).parent().addClass("active");
            });
        }, 
        
        show: function(id, value) {
            document.getElementById(id).style.display = value ? 'block' : 'none';
        },

        nextPost: function(){
            var $container = $(".section--list");

            var element = $('#reading > ul > li');

            var pageurl = element.find('a').attr('href');            
            var postid  = $('#nextID').text();
            var title   = $('#nextTitle').text();

            $.ajax({
                url: wpajax.ajaxurl,
                data: { 
                    'action': 'nextpost',
                    'postid': postid
                },
                method   : "POST",
                beforeSend:function(xhr){
                    $container.find('#loader').show();
                },
                success:function(data){
                    $container.find('#loader').hide();

                    
                    $container.find('.single--next').hide();
                    $('.single--next').data('id', postid ).show();

                    $container.html(data).fadeIn( 1000 );
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }

            });
            return false;
        },

        initWOW: function() {

            var wow = new WOW({
                boxClass: 'wow',
                animateClass: 'animated',
                offset: 90,
                mobile: false,
                live: true
            });

            if ($("body").hasClass("appear-animate")) {
                wow.init();
            }
        }, 

        initOwl: function (){
            var _featured = $("#owl-featured");
            _featured.owlCarousel({
                singleItem         : true,
                navigation         : false,
                pagination         : false,
                autoPlay           : true,
                slideSpeed         : 800,
                autoPlayTimeout    : 1000,
                autoPlayHoverPause : true   
            });

            var _homeProduct = $("#owl-products-home");
            _homeProduct.owlCarousel({
                loop               : true,
                navigation         : true,
                pagination         : false,
                nav                : true,
                items              : 3,
                responsive         : {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    767: {
                        items: 3
                    },
                    991: {
                        items: 3
                    }
                },
                autoPlay           : false,
                slideSpeed         : 800,
                autoPlayTimeout    : 1000,
                autoPlayHoverPause : true   
            });

            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                var _Product = $("#owl-products");
                _Product.owlCarousel({
                    loop               : true,
                    navigation         : true,
                    pagination         : false,
                    nav                : true,
                    items              : 3,
                    responsive         : {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 2
                        },
                        767: {
                            items: 3
                        },
                        991: {
                            items: 3
                        }
                    },
                    autoPlay           : false,
                    slideSpeed         : 800,
                    autoPlayTimeout    : 1000,
                    autoPlayHoverPause : true   
                });
            }
        },

        initMaskFieldForm: function() {
            $(".cpf").mask("999.999.999-99");
            $(".rg").mask("99.999.999-99");
            $(".nascimento").mask("99/99/9999");
            $(".cep").mask("99999-999");
            $(".cnpj").mask("99.999.999/9999-99");

            $(".tel").mask("(99) 9999?9-9999");        
            $('.tel').blur(function() {
                var phone    = $(this).val();
                var intRegex = /[0-9 -()+]+$/;
                var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );    
                if( last.length == 3 ) {
                    var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
                    var lastfour = move + last;                 
                    var first = $(this).val().substr( 0, 9 );                   
                    $(this).val( first + '-' + lastfour );
                }
            });
        },

        getPhotosFromInstagram: function(){
            var access_token = wpajax.access_token,
                hashtag      = wpajax.hashtag, // hashtag without # symbol
                count        = wpajax.count,
                clientID     = wpajax.clientID;

            var API_URL = 'https://api.instagram.com/v1';

            var URLTagsRecent = API_URL + '/tags/' + hashtag + '/media/recent?access_token=' + access_token + '&count=' + count;
            
            $.ajax({
                url      : URLTagsRecent,
                dataType : 'jsonp',
                type     : 'GET',
                cache    : false,
                success: function(res){
                    if(res.data){
                        var medias = res.data;

                        for(var i = 0; i < medias.length; i++) { 
                            var lead  = medias[i]; 
                            var tag   = lead.tags;

                            var fullname = lead.caption.from.full_name;
                            var text     = lead.caption.text;

                            // output media
                            if (lead.type === 'video') {
                                // video
                                var image = lead.images.standard_resolution.url;
                                var video = lead.videos.standard_resolution.url;
                                var template = '<video class="media video-js vjs-default-skin" poster="'+image+'" data-setup="{"controls": true, "autoplay": false, "preload": "auto"}"><source src="'+video+'" type="video/mp4" /></video>';
                            } else {
                                // image
                                var image    = lead.images.standard_resolution.url;
                                var template = "<img class='media' src='"+image+"' />";
                            }

                            $("ul#instagramFeed").append('<li class="media-'+i+'"><div class="overlay">' + template + '<a href="'+lead.link+'" title="'+fullname+'" target="_blank"><i class="fa fa-instagram"></i></div></a></li>');
                        }
                    }
                },
                error: function(data){
                    console.log(data);
                }
            });
        }, 

        login: function(){
            // Perform AJAX login on form submit
            var _form = $('#form-login');
            _form.on('submit', function(e){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: wpajax.ajaxurl,
                    data: { 
                        'action': 'ajaxlogin',
                        'username': _form.find('#username').val(), 
                        'password': _form.find('#password').val(), 
                        'security': _form.find('#loginsecurity').val() 
                    },
                    beforeSend: function() {
                        _form.find('.status').show().html(wpajax.loadingmessage);
                    },
                    success: function(data){
                        if (data.loggedin == true){
                            _form.find('.status').show().html(data.message);
                            setTimeout(function(){
                                document.location.href = wpajax.redirecturl;
                            }, 500);            
                        } else{
                            _form.find('.status').show().html(data.message);

                            _form.find('input').addClass('error');
                            _form.find('label').addClass('error');
                        }
                    }
                });
                e.preventDefault();
            });
        },

        myaccount: function(){
            $(document).on( 'click', '.myaccount--delete-confirm', function() {
                var id    = $(this).data('id');
                var nonce = $(this).data('nonce');
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: wpajax.ajaxurl,
                    data: {
                        action: 'user_deletemyaccount',
                        nonce: nonce,
                        id: id
                    },
                    beforeSend: function() {
                        $('#deleteaccount').find('.status').show().html('Aguarde por favor...');
                    },
                    success: function(data){
                        if (data.status == true){
                            $('#deleteaccount').find('.status').hide();
                            $('#deleteaccount').find('.modal-body').hide();
                            $('#deleteaccount').find('.modal-footer').hide();
                            
                            $('#deleteaccount').find('.wrapper').addClass('large');
                            $('#deleteaccount').find('.modal-title').fadeOut('slow', function() {
                                $('#deleteaccount').find('.modal-title').text(data.message).fadeIn();
                            });

                            setTimeout(function(){
                                window.location.href = data.urlhome
                            }, 3000);
                        } else{
                            $('#deleteaccount').find('.status').show().html(data.message);
                        }
                    }
                })
                return false;
            });
                        

            $('#youremail').focusin(function(){
               $(this).data('oldValue', $(this).val());
            });
            $('#youremail').change(function(){
               var oldValue = ($(this).data('oldValue')); //old value
               var newValue = $(this).val(); //new value
               $('#editEmailUserConfirm .modal-title').find('span').text(newValue);
            });            
            $(document).on( 'click', '.editemail', function(e) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: wpajax.ajaxurl,
                    data: { 
                        'action': 'checkemail',
                        'youremail': $('#form-editemail').find('#youremail').val(),
                        'edit_user_id': $('#form-editemail').find('#edit_user_id').val()
                    },
                    beforeSend: function() {
                        $('#form-editemail').find('.res').show().html('Aguarde por favor...');
                    },
                    success: function(data){
                        $('#form-editemail').find('.res').hide();
                        if (data.status == true){
                            $('#editEmailUserConfirm').addClass('fade');
                            $('#editEmailUserConfirm').find('.modal-content').addClass('in');                            
                        } else{
                            $('#form-editemail').find('#youremail').addClass('error');
                            $('#form-editemail').find('#youremail').parent().children('label').addClass('error').removeClass('valid');
                            $('#form-editemail').find('#youremail').parent().children('.msg').addClass('error').text(data.message);
                        }
                    }
                });
                e.preventDefault();
                return false;
            });            
            $('#form-editemail').on('submit', function(e){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: wpajax.ajaxurl,
                    data: { 
                        'action': 'editEmailUser',
                        'youremail': $('#form-editemail').find('#youremail').val(),
                        'edit_user_id': $('#form-editemail').find('#edit_user_id').val(),
                        'security': $('#form-editemail').find('#editemail').val()
                    },
                    beforeSend: function() {
                        $('#form-editemail').find('.status').show().html('Aguarde por favor...');
                    },
                    success: function(data){
                        if (data.status == true){
                            $('#form-editemail').find('.status').show().html(data.message);
                            setTimeout(function(){
                                window.location.href = data.redirect;
                            }, 3000);
                        } else{
                            $('#form-editemail').find('.status').show().html(data.message);
                        }
                    }
                });
                e.preventDefault();
                return false;
            });

            $(document).on( 'click', '.myaccount--changepassword', function() {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: wpajax.ajaxurl,
                    data: { 
                        'action': 'check_password',
                        'userid': $("#form-changepassword").find('#changepassword_userid').val(),
                        'currentpassword': $("#form-changepassword").find('#currentpassword').val(),
                        'newpassword': $("#form-changepassword").find('#newpassword').val(),
                        'security': $("#form-changepassword").find('#changepassword').val()
                    },
                    beforeSend: function() {
                        $('#form-changepassword').find('.statususer').show().html('Aguarde por favor...');
                    },
                    success: function(data){
                        if (data.status == true){
                            $('#form-changepassword').find('.statususer').hide();
                            $('#moda_changepassword').addClass('fade');
                            $('#moda_changepassword').find('.modal-content').addClass('in');
                        } else{
                            $('#form-changepassword').find('.statususer').show().html(data.message);
                        }
                    }
                })
                return false;
            });

            $("#form-changepassword").on('submit', function(e){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: wpajax.ajaxurl,
                    data: { 
                        'action': 'change_password',
                        'userid': $("#form-changepassword").find('#changepassword_userid').val(),
                        'currentpassword': $("#form-changepassword").find('#currentpassword').val(),
                        'newpassword': $("#form-changepassword").find('#newpassword').val(),
                        'c_newpassword': $("#form-changepassword").find('#c_newpassword').val(),
                        'security': $("#form-changepassword").find('#changepassword').val()
                    },
                    beforeSend: function() {
                        $("#form-changepassword")[0].reset();
                        $('#moda_changepassword').find('.status').show().html('Aguarde por favor...');
                    },
                    success: function(data){
                        if (data.status == true){
                            $('#moda_changepassword').find('.status').show().html(data.message);
                        } else{
                            $('#moda_changepassword').find('.status').show().html(data.message);
                        }
                    }
                });
                e.preventDefault();
                return false;
            });
        },

        lostpassword: function(){
            var _form = $('#form-lostpassword');
            _form.on('submit', function(e){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: wpajax.ajaxurl,
                    data: { 
                        'action': 'lostpassword',
                        'user_login': _form.find('#lost_userlogin').val(),
                        'security': _form.find('#forgotsecurity').val(),
                    },
                    beforeSend: function() {
                        _form.find('.status').show().html('Aguarde por favor...');
                    },
                    success: function(data){
                        if (data.loggedin == true){
                            _form.find('.status').show().html(data.message);
                            _form.find('#lost_userlogin').addClass('valid');
                            _form.find('#lost_userlogin').parent().children('label').addClass('valid');
                            _form.find('#lost_userlogin').parent().children('.msg').removeClass('error').text('');
                            setTimeout(function(){
                                $('#lostpassword').find('.ajax_login').fadeOut('slow', function() {
                                    $('#lostpassword').find('.ajax-response').fadeIn('slow');
                                });
                            }, 150);            
                        } else{
                            _form.find('.status').show().html(data.message);
                            _form.find('#lost_userlogin').addClass('error');
                            _form.find('#lost_userlogin').parent().children('label').addClass('error').removeClass('valid');
                            _form.find('#lost_userlogin').parent().children('.msg').addClass('error').text('Campo requerido');
                        }
                    }
                });
                e.preventDefault();
                return false;
            });
        },

        register: function(){
            var _form = $('#form-register');

            $(".skip-gender").click(function(e) {
                var status = 0;

                if( _form.find('#reg_username').val() != '' ){
                    status = 1;
                    _form.find('#reg_username').addClass('valid');
                    _form.find('#reg_username').parent().children('label').addClass('valid');
                    _form.find('#reg_username').parent().children('.msg').removeClass('error').text('');
                } else{
                    status = 0;
                    _form.find('#reg_username').addClass('error');
                    _form.find('#reg_username').parent().children('label').addClass('error').removeClass('valid');
                    _form.find('#reg_username').parent().children('.msg').addClass('error').text('Campo requerido');
                }

                if( _form.find('#reg_email').val() != '' ){
                    status = 1;
                    _form.find('#reg_email').addClass('valid');
                    _form.find('#reg_email').parent().children('label').addClass('valid');
                    _form.find('#reg_email').parent().children('.msg').removeClass('error').text('');
                } else{
                    status = 0;
                    _form.find('#reg_email').addClass('error');
                    _form.find('#reg_email').parent().children('label').addClass('error').removeClass('valid');
                    _form.find('#reg_email').parent().children('.msg').addClass('error').text('Campo requerido');
                }

                if( _form.find('#reg_c_email').val() != '' ){
                    status = 1;
                    _form.find('#reg_c_email').addClass('valid');
                    _form.find('#reg_c_email').parent().children('label').addClass('valid');
                    _form.find('#reg_c_email').parent().children('.msg').removeClass('error').text('');
                } else{
                    status = 0;
                    _form.find('#reg_c_email').addClass('error');
                    _form.find('#reg_c_email').parent().children('label').addClass('error').removeClass('valid');
                    _form.find('#reg_c_email').parent().children('.msg').addClass('error').text('Campo requerido');
                }

                if( _form.find('#reg_email').val() == _form.find('#reg_c_email').val() ){
                    status = 1;
                    _form.find('#reg_email').addClass('valid');
                    _form.find('#reg_email').parent().children('label').addClass('valid');
                    _form.find('#reg_email').parent().children('.msg').removeClass('error').text('');

                    _form.find('#reg_c_email').addClass('valid');
                    _form.find('#reg_c_email').parent().children('label').addClass('valid');
                    _form.find('#reg_c_email').parent().children('.msg').removeClass('error').text('');
                } else{
                    status = 0;
                    _form.find('#reg_email').addClass('error');
                    _form.find('#reg_email').parent().children('label').addClass('error').removeClass('valid');
                    _form.find('#reg_email').parent().children('.msg').addClass('error').text('E-mail não confere.');

                    _form.find('#reg_c_email').addClass('error');
                    _form.find('#reg_c_email').parent().children('label').addClass('error').removeClass('valid');
                    _form.find('#reg_c_email').parent().children('.msg').addClass('error').text('E-mail não confere.');
                }

                if( _form.find('#reg_password').val() != '' ){
                    status = 1;
                    _form.find('#reg_password').addClass('valid');
                    _form.find('#reg_password').parent().children('label').addClass('valid');
                    _form.find('#reg_password').parent().children('.msg').removeClass('error').text('');
                } else{
                    status = 0;
                    _form.find('#reg_password').addClass('error');
                    _form.find('#reg_password').parent().children('label').addClass('error').removeClass('valid');
                    _form.find('#reg_password').parent().children('.msg').addClass('error').text('Campo requerido');
                }

                if(status == 1){
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: wpajax.ajaxurl,
                        data: { 
                            'action': 'checkdatauser',
                            'name': _form.find('#reg_username').val(), 
                            'email': _form.find('#reg_email').val(), 
                            'c_email': _form.find('#reg_c_email').val(), 
                            'password': _form.find('#reg_password').val()
                        },
                        beforeSend: function() {
                            _form.find('.statususer').show().html('Aguarde...');
                        },
                        success: function(data){
                            if (data.userexists == true){
                                _form.find('.statususer').show().html(data.message);
                            } else{
                                $('#register').find('.group-user').fadeOut('slow', function() {
                                    $('#register').find('.group-gender').fadeIn('slow');
                                });
                            }
                        }
                    });
                    e.preventDefault();
                    return false;
                }
                
            });

            _form.find('input[type=radio]').on('change', function(e){              
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: wpajax.ajaxurl,
                    data: { 
                        'action': 'ajaxregister',
                        'username': _form.find('#reg_username').val(), 
                        'email': _form.find('#reg_email').val(), 
                        'password': _form.find('#reg_password').val(),
                        'gender': _form.find("input[name='gender']:checked").val(),
                        'security': _form.find('#registersecurity').val() 
                    },
                    beforeSend: function() {
                        _form.find('.status').show().html('Aguarde, estamos finalizando seu cadastro.');
                    },
                    success: function(data){
                        if (data.loggedin == true){
                            _form.find('.status').show().html(data.redirectmsg);
                            $('#form-quiz').find('#user_id').val(data.user_id);
                            setTimeout(function(){
                                window.location.href = data.redirecturl
                            }, 600);               
                        }
                    }
                });
                e.preventDefault();
                return false;
            });
        },

        quiz: function(){
            var _form = $('#form-quiz');

            $(".start-quiz").click(function(e) {                
                $('#quiz').find('.quiz-start').fadeOut('slow', function() {
                    $('#quiz').find('.quiz-questions').fadeIn('slow');
                });
            });

            _form.find('input[type=radio]').on('change', function(e){
                var step = $(this).data('next');
                if( step != 0){
                    $('.wizard-step').removeClass('active');
                    setTimeout(function(){
                        $('#step_'+step).addClass('active');
                    }, 620);
                }              
            });

            _form.find("input[name='user__question_7']").on('change', function(e){
                var question_1 = $('input:radio[name=user__question_1]:checked').val();
                var question_2 = $('input:radio[name=user__question_2]:checked').val();
                var question_3 = $('input:radio[name=user__question_3]:checked').val();
                var question_4 = $('input:radio[name=user__question_4]:checked').val();
                var question_5 = $('input:radio[name=user__question_5]:checked').val();
                var question_6 = $('input:radio[name=user__question_6]:checked').val();
                var question_7 = $('input:radio[name=user__question_7]:checked').val();
                var user_id    = $('#user_id').val();

                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: wpajax.ajaxurl,
                    data: { 
                        'action': 'ajaxquiz',
                        'user__question_1': question_1,
                        'user__question_2': question_2,
                        'user__question_3': question_3,
                        'user__question_4': question_4,
                        'user__question_5': question_5,
                        'user__question_6': question_6,
                        'user__question_7': question_7,
                        'user_id': user_id,
                        'security': _form.find('#quizsecurity').val() 
                    },
                    beforeSend: function() {
                        _form.find('.status').show().html(wpajax.msgbeforeregister);
                    },
                    success: function(data){
                        if (data.loggedin == true){
                            _form.find('.status').show().html(data.message);
                            setTimeout(function(){
                                $('.quiz-questions').fadeOut('slow', function() {
                                    $('.quiz-results').fadeIn('slow');
                                    $('#cat_'+data.user__categoria).fadeIn('slow');
                                });
                            }, 300);            
                        } else{
                            _form.find('.status').show().html(data.message);
                        }
                    },
                    error: function(err){
                        _form.find('.status').show().html(err.statusTexte);
                    }
                });
                e.preventDefault();
                return false;
            });
        },

        initCF7: function(){

            var wpcf7Elm = document.querySelector( '.wpcf7' );
 
            document.addEventListener( 'wpcf7invalid', function( event ) {                
                $('.wpcf7 div.wpcf7-response-output').delay(3000).fadeOut();
                $("input[type=text].wpcf7-not-valid, input[type=email].wpcf7-not-valid").attr("placeholder", "O campo é obrigatório.");
            }, false );

            document.addEventListener( 'wpcf7spam', function( event ) {
                
            }, false );

            document.addEventListener( 'wpcf7mailsent', function( event ) {
                $('.wpcf7 div.wpcf7-response-output').delay(3000).fadeOut();
            }, false );

            document.addEventListener( 'wpcf7mailfailed', function( event ) {
               
            }, false );

            document.addEventListener( 'wpcf7submit', function( event ) {
                
            }, false );

        },

        closeModal: function(){
            $("[data-dismiss='modal']").on("click", function(e) {
                $(this).closest(".modal").removeClass('fade');
                $(this).closest(".modal").find('.modal-content').removeClass('in');
                $(this).closest(".modal").find('form').trigger('reset');
            });
        },

        urlOpenModal: function(){
            var url = new URL(window.location.href);
            setTimeout(function(){
                var modal = url.searchParams.get("modal");
                $('#'+modal).addClass('fade');
                $('#'+modal).find('.modal-content').addClass('in');
            }, 600);
        },

        initModal: function() {
            $("[data-modal='modal']").on("click", function() {
                var modal = $(this).data("target");
                $(modal).addClass('fade');
                $(modal).find('.modal-content').addClass('in');
            });

            // $(".modal").on("click", function(e) {
            //     var className = e.target.className;
            //     if(className === "modal fade" || className === "modal-close"){
            //         $(this).closest(".modal").removeClass('fade');
            //         $(this).closest(".modal").find('.modal-content').removeClass('in');
            //     }
            // });
            $("[data-dismiss='modal']").on("click", function(e) {
                $(this).closest(".modal").removeClass('fade');
                $(this).closest(".modal").find('.modal-content').removeClass('in');
            });
        },

        initApply: function() {
            var self = this;

            window.onpopstate = function() {
                window.onpopstate = function(e) {
                    e.preventDefault(), window.history.back()
                }
            }

            $('.form-control').on('focus blur', function (e) {
                $(this).parents('.inputText').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
            }).trigger('blur');

            //self.setNavigation('.header--brand a');
            self.setNavigation('.header--categoies ul li a');
            self.setNavigation('.menu--nav-links ul li a');
           
            if( $('body').hasClass('search') ){
                $('#drop-search').addClass('active');
                $('#button-search').addClass('active');
                $('#button-search').find('i').removeClass('i-search').addClass('i-close');
                self.show('menu_category', false);
                self.show('menu_newsletter', false);
            }
            
            $('#button-menu').click(function () {

                if( $('#drop-search').hasClass('active') ){
                    $('#drop-search').toggleClass('active');
                    $('#button-search').toggleClass('active');
                    $('#button-search').find('i').removeClass('i-close').addClass('i-search');

                    setTimeout(function(){
                    $(this).toggleClass('active');
                        $('#drop-menu').toggleClass('active');

                        if( $('#drop-menu').hasClass('active') ){

                            $('#menu_category').fadeOut();
                            $('#menu_newsletter').fadeOut();
                            
                            if( ! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                                $('.is-menu-close').fadeOut(100, function(){
                                    $('.is-menu-open').fadeIn(100);
                                });
                            }

                            $('.header--login').addClass('border');

                            $(this).find('i').removeClass('i-hamburguer').addClass('i-close'); 
                        } else{
                            $('#menu_category').fadeIn();
                            $('#menu_newsletter').fadeIn();
                            
                            if( ! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                                $('.is-menu-open').fadeOut(100, function(){
                                    $('.is-menu-close').fadeIn(100);
                                });
                            }

                            $('.header--login').removeClass('border');

                            $(this).find('i').removeClass('i-close').addClass('i-hamburguer');           
                        }
                    }, 100);           

                } else{
                    $(this).toggleClass('active');
                    $('#drop-menu').toggleClass('active');

                    if( $('#drop-menu').hasClass('active') ){

                        $('#menu_category').fadeOut();
                        $('#menu_newsletter').fadeOut();
                        
                        if( ! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            $('.is-menu-close').fadeOut(100, function(){
                                $('.is-menu-open').fadeIn(100);
                            });
                        }

                        $('.header--login').addClass('border');

                        $(this).find('i').removeClass('i-hamburguer').addClass('i-close'); 
                    } else{
                        $('#menu_category').fadeIn();
                        $('#menu_newsletter').fadeIn();
                        
                        if( ! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            $('.is-menu-open').fadeOut(100, function(){
                                $('.is-menu-close').fadeIn(100);
                            });
                        }

                        $('.header--login').removeClass('border');

                        $(this).find('i').removeClass('i-close').addClass('i-hamburguer');           
                    }
                }

                

                return false;
            });

            $('body').click(function () {

            });

            $('#button-search').click(function () {

                if($('.searchwp-live-search-results').hasClass('searchwp-live-search-results-showing')){
                    $('.searchwp-live-search-results').removeClass('searchwp-live-search-results-showing');
                }

                if( $('#drop-menu').hasClass('active') ){
                    $('#drop-menu').toggleClass('active');
                    $('#button-menu').toggleClass('active');
                    $(this).find('i').removeClass('i-close').addClass('i-hamburguer'); 

                    setTimeout(function(){

                        $(this).toggleClass('active');
                        $('#drop-search').toggleClass('active');

                        if( $('#drop-search').hasClass('active') ){
                            $('#menu_category').fadeOut();
                            $('#menu_newsletter').fadeOut();

                            $(this).find('i').removeClass('i-search').addClass('i-close');          

                        } else{
                            $('#menu_category').fadeIn();
                            $('#menu_newsletter').fadeIn();

                            $(this).find('i').removeClass('i-close').addClass('i-search');      
                        }
                    }, 100);
                }else{
                    $(this).toggleClass('active');
                    $('#drop-search').toggleClass('active');

                    if( $('#drop-search').hasClass('active') ){
                        $('#menu_category').fadeOut();
                        $('#menu_newsletter').fadeOut();

                        $(this).find('i').removeClass('i-search').addClass('i-close');          

                    } else{
                        $('#menu_category').fadeIn();
                        $('#menu_newsletter').fadeIn();

                        $(this).find('i').removeClass('i-close').addClass('i-search');      
                    }
                }

                return false;
            });

            $('#button-newsletter').click(function () {

                $(this).parent().toggleClass('active');
                $('#drop-newsletter').toggleClass('active');

                if( $('#drop-newsletter').hasClass('active') ){
                    self.show('menu_category', false);
                    $('#button-newsletter').find('span').fadeOut(300, function() {
                        $('#button-newsletter').find('i').fadeIn();
                    });

                } else{
                    self.show('menu_category', true);
                    $('#button-newsletter').find('i').fadeOut(300, function() {
                        $('#button-newsletter').find('span').fadeIn();
                    });
   
                }

                return false;
            });     

            $('.count').each(function () {
                $(this).prop('Counter',0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 4000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });     
            
            // Scroll Âncora
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({ scrollTop:$(this.hash).offset().top}, 800 );
            });


            if( ! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

                $('#header').find('ul.menu').hoverSlider({
                    activeElemClass: 'active',
                    top: '64px',
                    left: '76px',
                    height: '3px',
                    onInit: function () {
                        $('.hover-slider').css({
                            top: '64px',
                            left: '76px',
                            height: '3px'
                        });
                    }
                });

                
                $('#header').find('ul.menu-drop').hoverSlider({
                    activeElemClass: 'active',
                    top: '53px',
                    left: '0',
                    height: '3px',
                    onInit: function () {
                        $('.hover-slider').css({
                            top: '53px',
                            left: '0',
                            height: '3px'
                        });
                    }
                });
            }

            if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                $('.dropmenu--nav-links').css({
                    height: ($(window).height() - $('.header').height() )
                });
            }

            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                $('.is-mobile').show();
            }

            $(".open-menubox").click(function(e){                
                e.preventDefault();
                $(this).toggleClass('is-open').next().toggleClass('is-open');
                return false;
            });

        },

        TW: function(){
            $('.twitter').onclick = function(e) {
                e.preventDefault();
                var elem = $(this);
                var twitterWindow = window.open('https://twitter.com/share?url=' + elem.prop('href')+'&text='+elem.data('desc'), 'twitter-popup', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
                if(twitterWindow.focus) { 
                    twitterWindow.focus(); 
                }
                return false;
            }
        },

        WT: function(){
            $('.whatsapp').onclick = function(e) {
                e.preventDefault();
                var elem = $(this);
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    var whatsappWindow = window.open('whatsapp://send?text='+elem.data('title')+','+elem.prop('href'), 'whatsapp-popup', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
                    if(whatsappWindow.focus) { 
                        whatsappWindow.focus(); 
                    }
                }else{
                    alert('You Are Not On A Mobile Device. Please Use This Button To Share On Mobile');
                }
                return false;
            }
        },

        EM: function(){
            $('.envelope').onclick = function(e) {
                e.preventDefault();
                var elem = $(this);
                var whatsappWindow = window.open('mailto:?Subject='+elem.data('title')+'&Body='+elem.prop('href'), 'email-popup', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
                if(whatsappWindow.focus) { 
                    whatsappWindow.focus(); 
                }                
                return false;
            }
        },

    };

    App.init();

    $( '.product--link' ).on( 'click', function() {
        var data = {
            'action': 'click_product',
            'user_id': $( this ).data( 'user' ),
            'product_id': $( this ).data( 'prod' ),
            'post_id': $( this ).data( 'post' ),
            'sku': $( this ).data( 'sku' )
        };
        $.ajax({
            url: wpajax.ajaxurl,
            type: 'POST',
            data: data
        });
    });

    $(".change-modal").click(function(e) {

        var modal = $($(this).data("target"));

        $(".modal").removeClass('fade');
        $(".modal").find('.modal-content').removeClass('in');
        
        setTimeout(function(){
            modal.addClass('fade');
            modal.find('.modal-content').addClass('in');
        }, 500); 

    });

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    //$('input.required').blur(function() {
    $("form").submit(function (e) {
        e.preventDefault();
        if( $('input').hasClass('required') ){
            if ($(this).val().trim() != '') {
                if ( $(this).is('#email') || $(this).is('#c_email') ) {
                    if (validateEmail($(this).val())) {
                        $(this).addClass('valid');
                        $(this).parent().children('label').addClass('valid');
                        $(this).parent().children('.msg').removeClass('error').text('');
                    } else {
                        $(this).addClass('error');
                        $(this).parent().children('label').addClass('error');
                        $(this).parent().children('.msg').addClass('error').text('E-mail inválido');
                    }
                } else {
                    $(this).addClass('valid');
                    $(this).parent().children('label').addClass('valid');
                    $(this).parent().children('.msg').removeClass('error').text('');
                }

            } else {
                $(this).addClass('error');
                $(this).parent().children('label').addClass('error').removeClass('valid');
                $(this).parent().children('.msg').addClass('error').text('Campo requerido');
            }
        }
    });

    $(".toggle-password").click(function() {
        $(this).toggleClass("show");
        var parent = $(this).parent();
        var input = parent.find('input');
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

})(jQuery);