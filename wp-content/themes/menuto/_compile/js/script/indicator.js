(function($) {
    'use strict';
	
	function last_scroll(){
        var last_scroll = 0;
        $( window ).scroll( function() {
            var scroll_pos = $( window ).scrollTop();

            if ( Math.abs( scroll_pos - last_scroll ) > $( window ).height() * 0.1 ) {

                last_scroll = scroll_pos;

                $( '.article').each( function(i) {
                    var i = i + 1;

                    var scroll_pos    = $( window ).scrollTop();
                    var window_height = $( window ).height();
                    var el_top        = $( this ).offset().top;
                    var el_height     = $( this ).outerHeight();
                    var el_sidebar    = $('.article--body-sidebar').outerHeight();
                    var el_bottom     = el_top + el_height;

                    if ( ( el_bottom > scroll_pos ) ) {
                        if ( window.location.href !== $( this ).attr( "data-permalink" ) ) {
                            history.replaceState( null, null, $( this ).attr( "data-permalink" ) );

                            $('article').removeClass('active');
                            $(this).addClass('active');

                            $( "meta[property='og:title']" ).attr( 'content', $( this ).attr( "data-title" ) );
                            $( "title" ).html( $( this ).attr( "data-title" ) );
                            $( "meta[property='og:url']" ).attr( 'content', $( this ).attr( "data-permalink" ) );
                            $( "meta[property='og:image']" ).attr( 'content', $( this ).attr( "data-image" ) );
                        
                        }
                        return( false );
                    }

                });
            }
        });
    }

	$('.indicator ul li').find('a').removeClass('read');
    $(document).ready(function(){

    	var articlesWrapper = $('.cd-articles');

		if( articlesWrapper.length > 0 ) {
			// cache jQuery objects
			var windowHeight    = $(window).height(),
				articles            = articlesWrapper.find('article'),
                sidebars            = articles.find('aside'),
				aside               = $('.cd-read-more'),
				articleSidebarLinks = aside.find('li');
			// initialize variables
			var	scrolling    = false,
				sidebarAnimation = false,
				resizing         = false,
				mq               = checkMQ(),
				svgCircleLength  = parseInt(Math.PI*(articleSidebarLinks.eq(0).find('circle').attr('r')*2));


            $('body').append('<div class="progress"></div>');
            for (var i = 0; i < articles.length; i++) {
                $('.progress').append('<div id="bar_' + i + '" class="progress-bar"></div>');
            }

            // check media query and bind corresponding events
			$(window).on('scroll', checkRead);
			$(window).on('scroll', checkSidebar);

			$(window).on('resize', resetScroll);

			updateArticle();
			updateSidebarPosition();
            updateArticleMobile();

            aside.on('click', 'a', function(event){
				event.preventDefault();
				var selectedArticle = articles.eq($(this).parent('li').index()),
					selectedArticleTop = selectedArticle.offset().top;

				$(window).off('scroll', checkRead);

				$('body,html').animate(
					{'scrollTop': selectedArticleTop + 2}, 
					300, function(){
						checkRead();
						$(window).on('scroll', checkRead);
					}
				); 
		    });

            $('.button').on('click', function(){
                $('.sidebar').trigger('detach.ScrollToFixed');
                $(this).toggleClass('retract');
                $('.sidebar').toggleClass('retract');

                if ( $('.sidebar').hasClass('retract') ){
                    $('.button').find('i').removeClass('open').addClass('close');

                    updateArticle();
                    updateSidebarPosition();

                    articles.find('.sidebar').addClass('retract');
                    articles.find('.body').addClass('retract');                
                } else{
                    $('.button').find('i').removeClass('close').addClass('open');

                    updateArticle();
                    updateSidebarPosition();

                    articles.find('.sidebar').removeClass('retract');                    
                    articles.find('.body').removeClass('retract');
                }
            });

        }       

        function checkRead() {
			if( !scrolling ) {
				scrolling = true;				
                (!window.requestAnimationFrame) ? setTimeout(updateArticle, 300) : window.requestAnimationFrame(updateArticle);                
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    (!window.requestAnimationFrame) ? setTimeout(updateArticleMobile, 300) : window.requestAnimationFrame(updateArticleMobile);
                }
			}
		}

		function checkSidebar() {
			if( !sidebarAnimation ) {
				sidebarAnimation = true;
				(!window.requestAnimationFrame) ? setTimeout(updateSidebarPosition, 300) : window.requestAnimationFrame(updateSidebarPosition);
			}
		}

		function resetScroll() {
			if( !resizing ) {
				resizing = true;
				(!window.requestAnimationFrame) ? setTimeout(updateParams, 300) : window.requestAnimationFrame(updateParams);
			}
		}

		function updateParams() {
			windowHeight = $(window).height();
			mq = checkMQ();
			$(window).off('scroll', checkRead);
			$(window).off('scroll', checkSidebar);
			
			$(window).on('scroll', checkRead);
			$(window).on('scroll', checkSidebar);

			resizing = false;
		}

        function updateArticleMobile(){
            var scrollTop = $(window).scrollTop();
            var j = 0;
            articles.each(function(){
                var article            = $(this),
                    articleTop         = article.offset().top,
                    articleHeight      = article.outerHeight(),
                    scrollPercent;

                if( articleTop > scrollTop) {
                    $('#bar_' + j).removeClass('fill');
                } else if( scrollTop >= articleTop && articleTop + articleHeight > scrollTop) {
                    scrollPercent = ( (scrollTop - articleTop) / articleHeight ) * 100;
                    $('.progress-bar').parent().show();
                    $('.progress-bar').hide();
                    $('#bar_' + j).show().addClass('fill').css('width', scrollPercent + '%');
                } else{
                    $('#bar_' + j).removeClass('fill');
                }
                j++;
            });
            scrolling = false;
        }

        function updateArticle() {
            var scrollTop = $(window).scrollTop();

			articles.each(function(){
				var article = $(this),
					articleTop = article.offset().top,
					articleHeight = article.outerHeight(),
					articleSidebarLink = articleSidebarLinks.eq(article.index()).children('a');

				if( article.is(':last-of-type') ) articleHeight = articleHeight - windowHeight;

                if( articleTop > scrollTop) {
					articleSidebarLink.removeClass('read reading');
				} else if( scrollTop >= articleTop && articleTop + articleHeight > scrollTop) {
                    var dashoffsetValue = svgCircleLength*( 1 - (scrollTop - articleTop)/articleHeight);
					articleSidebarLink.addClass('reading').removeClass('read').find('circle').attr({ 'stroke-dashoffset': dashoffsetValue });
                    if ( window.location.href !== $( this ).attr( "data-permalink" ) ) {
                        changeUrl(articleSidebarLink.attr('href'));
                        // change title page
                        $( "title" ).html( $( this ).attr( "data-title" ) );
                        // change content meta tags page
                        $( "meta[property='og:title']" ).attr( 'content', $( this ).attr( "data-title" ) );
                        $( "meta[property='og:url']" ).attr( 'content', $( this ).attr( "data-permalink" ) );
                        $( "meta[property='og:image']" ).attr( 'content', $( this ).attr( "data-image" ) );
                        $( ".shares-icons a" ).attr( 'href', $( this ).attr( "data-permalink" ) );
                        $( ".shares-icons a" ).attr( 'data-title', $( this ).attr( "data-title" ) );
                        $( ".shares-icons a" ).attr( 'data-desc', $( this ).attr( "data-summary" ) );
                        $( ".shares-icons a" ).attr( 'data-image', $( this ).attr( "data-image" ) );
                        $( ".cd-read-more" ).appendTo( '#sidebar_'+$(this).data( "id" ) );
                        $('article').removeClass('active');
                        $(this).addClass('active');                    
                    }
                    return( false );

                } else {
                    articleSidebarLink.removeClass('reading').addClass('read');
                }

            });
            scrolling = false;
        }

        function updateSidebarPosition() {
            if(!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                var sidebars = $('.sidebar');
                sidebars.each(function(index, el) {
                    var sidebar = $(sidebars[index]);
                    var next = sidebars[index + 1];

                    sidebar.scrollToFixed({
                        marginTop: $('.header').outerHeight(),
                        limit: function() {
                            var limit = 0;
                            if (next) {
                                limit = $(next).parent().offset().top - $(this).outerHeight(true) - 180;
                            } else {
                                limit = $('article').offset().top - $(this).outerHeight(true) - 180;
                            }
                            return limit;
                        }
                    });

                });
                //sidebarAnimation = false;
            }
        }

        function changeUrl(link) {
			var pageArray = location.pathname.split('/'),
	        	actualPage = pageArray[pageArray.length - 1] ;
	        if( actualPage != link && history.pushState ) window.history.pushState({path: link},'',link);
		}

		function checkMQ() {
			return window.getComputedStyle(articlesWrapper.get(0), '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, "");
		}
    });

})(jQuery);