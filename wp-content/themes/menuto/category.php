<?php 
global $show_content, $show_category;
$category = get_category( get_query_var( 'cat' ) );

get_header(); ?>
	
	<main role="main" id="main" class="site-main page">
		
		<section class="container-fluid">
			<div class="category--title">
				<h1><?php single_cat_title(); ?></h1>
			</div>
		</section>

		<?php
			$args = array(
		        'post_type'           => 'post',
		        'no_found_rows'       => true,
		        'post_status'         => 'publish',
		        'ignore_sticky_posts' => true,
		        //'orderby'             => 'meta_value_num',
		        //'meta_key'            => 'data_most_read_post',
		        'order'               => 'DESC',
				'cat'                 => array($category->term_id)
			);

			$featured = new WP_Query( apply_filters( 'category_most_read_posts_args', $args ) );		 	
		?>	
		<!-- Posts Destaque -->
		<section class="bannerCat">
			<div class="bannerCat--area">
				<?php
					$args['posts_per_page'] = 1;
					$banner = new WP_Query( $args );					
				?>
				<?php while($banner->have_posts()): $banner->the_post();  ?>
					<div class="bannerCat--area-loop">
			            <a href="<?php echo esc_url(get_permalink()); ?>">
			                <div class="mask"></div>
			                <?php if( get_field('banner__image') ) : ?>
								<figure class="figure">
									<img class="figure-img" src="<?php the_field('banner__image') ; ?>" alt="" />
								</figure>
							<?php else: ?>
								<?php if( has_post_thumbnail() ) : ?>
									<figure class="figure">
										<img class="figure-img" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
									</figure>
								<?php endif; ?>
							<?php endif; ?>	
							<div class="content">
								<div class="arrow-up"></div>
								<h2 class="content--title"><?php the_title(); ?></h2>
								<div class="content--paragraph">													
									<div class="content--paragraph-summary">
										<?php echo wp_trim_words( get_the_excerpt(), 18, '...' ); ?>
									</div>
								</div>										
							</div>                                          
			            </a>
			        </div>
		        <?php endwhile; wp_reset_postdata(); ?>

			</div>
		</section>


		<section class="container-fluid">					
			<div class="posts big">
				<div class="posts--area pad">
					<?php
						$args['posts_per_page'] = 2;
						$args['offset']         = 1;

						$big = new WP_Query($args);
					?>
					<?php if ($big->have_posts()) : ?>
						<div class="posts--area-loop">
							<div class="row">
								<?php while( $big->have_posts() ) : $big->the_post(); ?>
									<div class="col col-2">
										<div class="post">
											<div class="post--card">
												<a href="<?php echo esc_url(get_permalink()); ?>" class="post--card-link">
													<?php if( has_post_thumbnail() ) : ?>
														<figure class="post--card-figure">
															<img class="post--card-image" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
														</figure>
													<?php endif; ?>
													<div class="post--card-content">
														<h3 class="post--card-title"><?php the_title(); ?></h3>
														<div class="post--card-text"><?php echo wp_trim_words( do_shortcode($post->post_content), 25, '...' ); ?></div>
													</div>
												</a>
											</div>
										</div>
									</div>
								<?php endwhile; wp_reset_postdata(); ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</section>

		<section class="newsletter">
			<?php if( get_field('newsletter__title', 'option') ) : ?>
				<h3 class="newsletter--title"><?php the_field('newsletter__title', 'option'); ?></h3>
			<?php endif; ?>
			<div class="newsletter--form">
				<div class="form">
					<?php get_template_part( 'content/newsletter' ); ?>
				</div>
			</div>
		</section>

		<?php if (have_posts()) : ?>
			<section class="posts">
				<div class="posts--area listing">						
					<div class="posts--area-loop">											
						<div class="row">
							<?php while( have_posts() ) : the_post(); ?>
								<div class="col col-3">
									<div class="post">
										<div class="post--card">
											<a href="<?php echo esc_url(get_permalink()); ?>" class="post--card-link">
												<?php if( has_post_thumbnail() ) : ?>
													<figure class="post--card-figure">
														<img class="post--card-image" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
													</figure>
												<?php endif; ?>
												<div class="post--card-content">
													<h3 class="post--card-title"><?php the_title(); ?></h3>
												</div>
											</a>
										</div>
									</div>
								</div>
							<?php endwhile; ?>
						</div>
						<div class="row">
							<?php echo get_pagination_links(); ?>
						</div>
					</div>						
				</div>
			</section>
		<?php
			else :
				get_template_part( 'content', 'none' );
			endif;
		?>		

	</main><!-- .site-main -->

<?php get_footer(); ?>