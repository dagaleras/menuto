<?php


function init_setup()  {

    add_filter('sanitize_file_name', 'remove_accents' );

    // Add theme support for Automatic Feed Links
    add_theme_support( 'automatic-feed-links' );

    // Add theme support for Post Formats
    add_theme_support( 'post-formats', array( 'status', 'quote', 'gallery', 'image', 'video', 'audio', 'link', 'aside', 'chat' ) );

    // Add theme support for Featured Images
    add_theme_support( 'post-thumbnails' );

    // Set custom thumbnail dimensions
    set_post_thumbnail_size( 260, 146, true );

    // Add theme support for HTML5 Semantic Markup
    add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

    // Add theme support for document Title tag
    add_theme_support( 'title-tag' );

    // Add theme support for custom CSS in the TinyMCE visual editor
    add_editor_style();

    // Add theme support for Translation
    load_theme_textdomain( 'menuto', get_template_directory() . '/languages/' );

    // Add theme support for woocommerce plugin
    add_theme_support( 'woocommerce' );

    /** Shortcodes Filters */
    add_filter( 'widget_text', 'do_shortcode' );
    add_filter( 'category_description', 'do_shortcode' );

    // Custom Post Format - List Thumbnails
    add_image_size( 'post_type', 80, 50,   true );
    add_image_size( 'admin-full', 1920, 500, true );
    add_image_size( 'slider', 1920, 300, true );
    add_image_size( 'facebook', 1200, 630, true );

    // Remove a barra do painel adminitrativo caso o usuário logado não seja o admin do site.
    if ( !current_user_can('administrator') && !is_admin() ) {
        show_admin_bar(false);
        add_filter('show_admin_bar', '__return_false');
    } else{
        show_admin_bar(false);
        add_filter('show_admin_bar', '__return_false');
    }

}
add_action( 'after_setup_theme', 'init_setup' );

function the_page_title( $echo = false ){           
        
    if( is_front_page() ){
        
        // Front page ---------------------------------------
        $title = get_bloginfo('name');
        
    } elseif( is_home() ){
        
        // Blog ---------------------------------
        $title = wp_title() . ' - ' . get_bloginfo('name');
        
    } elseif( is_tag() ){
        
        // Blog | Tag ---------------------------------
        $title = single_tag_title('', false) . ' - ' . get_bloginfo('name');
        
    } elseif( is_404() ){
        
        // Blog | Tag ---------------------------------
        $title = __( '404' ) . ' - ' . get_bloginfo('name');
        
    } elseif( is_category() ){
        
        // Blog | Category ----------------------------
        $title = single_cat_title('', false) . ' - ' . get_bloginfo('name');
        
    } elseif( is_search() ){
        
        // Blog | Category ----------------------------
        $title = get_search_query() . ' - ' . get_bloginfo('name');
        
    } elseif( is_author() ){
        
        // Blog | Author ------------------------------
        $title = get_the_author() . ' - ' . get_bloginfo('name');
    
    } elseif( is_day() ){
    
        // Blog | Day ---------------------------------
        $title = get_the_time('d') . ' - ' . get_bloginfo('name');
    
    } elseif( is_month() ){
    
        // Blog | Month -------------------------------
        $title = get_the_time('F') . ' - ' . get_bloginfo('name');

    } elseif( is_year() ){
    
        // Blog | Year --------------------------------
        $title = get_the_time('Y') . ' - ' . get_bloginfo('name');
        
    } elseif( is_single() || is_page() ){
        
        // Single -------------------------------------
        $title = get_the_title( get_the_ID() ) . ' - ' . get_bloginfo('name');
        
    } elseif( get_post_taxonomies() ){
        
        // Taxonomy -----------------------------------
        $title = single_cat_title('', false) . ' - ' . get_bloginfo('name');
        
    } elseif( is_tax() ){
        
        // Taxonomy -----------------------------------
        $title = single_cat_title('', false) . ' - ' . get_bloginfo('name');
        
    } else {
        
        // Default ------------------------------------
        $title = get_the_title( get_the_ID() ) . ' - ' . get_bloginfo('name');     
    }
    
    if( $echo ) echo $title;
    return $title;
}

// Register Navigation Menus
if( ! function_exists( 'register_nav_init' ) ) {
    function register_nav_init() {

        $locations = array(
            'category-header' => __( 'Menu Categoria Header' ),
            'dropdow-header'  => __( 'Menu Dropdown Header' ),
            'category-footer' => __( 'Menu Categoria Footer' ),
            'pages-footer'    => __( 'Menu Páginas Footer' )
        );
        register_nav_menus( $locations );

    }
    add_action( 'init', 'register_nav_init' );
}

/**
 * Criando uma area de widgets
 *
 */
function widgets_novos_widgets_init() {

    register_sidebar( array(
        'name'          => 'Rodape',
        'id'            => 'rodape_widgets',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'widgets_novos_widgets_init' );

function remove_dashboard_meta() {
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
}
add_action( 'admin_init', 'remove_dashboard_meta' );

add_role( 'analyst', __( 'Analista' ), array(
    'level_0'      => true, 
    'read'         => true,  // true allows this capability
    'edit_posts'   => false,
    'delete_posts' => false, // Use false to explicitly deny
    'upload_files' => false
));

// Adiciona estilo para elementos no Login
// ==========================================
function load_enqueue_login() {
    wp_deregister_style( 'styles' );
    wp_register_style( 'styles', get_template_directory_uri().'/css/styles.min.css', false, '1.0' );
    wp_enqueue_style( 'styles' );
}
add_action( 'login_enqueue_scripts', 'load_enqueue_login', 10 );


function load_enqueue_admin(){
    // Styles
    wp_deregister_style( 'font-awesome' );
    wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), '4.7.0' );
    wp_enqueue_style( 'font-awesome' );

    wp_deregister_style( 'admin' );
    wp_register_style( 'admin', get_template_directory_uri().'/css/admin.css', false, '1.0' );
    wp_enqueue_style( 'admin' );
}
add_action('admin_enqueue_scripts', 'load_enqueue_admin' );

// Carrega arquivos CSS e JS para o front
// ==========================================
function load_enqueue_front() {
    global $data;

    $user = wp_get_current_user();

    wp_enqueue_style( 'dashicons' );

    // Styles
    wp_deregister_style( 'styles' );
    wp_register_style( 'styles', get_template_directory_uri().'/css/styles.min.css', false, '1.0' );
    wp_enqueue_style( 'styles' );

    wp_deregister_style( 'modernizr' );
    wp_register_script( 'modernizr', get_template_directory_uri().'/js/modernizr.js', false, '2.8.3', false );
    wp_enqueue_script( 'modernizr' );

    
    if( is_page_template('templates/login.php') ){
        
    }

    wp_deregister_script( 'scripts' );
    wp_register_script( 'scripts', get_template_directory_uri().'/js/scripts.min.js', false, '1.0', true );
    wp_enqueue_script( 'scripts' );

    wp_enqueue_script( 'html5shiv', '//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js' );
    wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

    wp_enqueue_script( 'respond', '//oss.maxcdn.com/respond/1.4.2/respond.min.js' );
    wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

    // Habilitando Ajax WordPressx   
    wp_localize_script( 'scripts', 'wpajax', array( 
        'directory_uri' => get_template_directory_uri(),
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        //// Login
        'redirecturl'    => home_url('/'),
        'loadingmessage' => __( 'Fazendo login, aguarde...' ),
        //// Register user
        'msgbeforeregister' => __( 'Estamos verificando seus dados, aguarde...' ),
        'urlQuiz'           => get_permalink(363),
        'urlHome'           => home_url('/'),
        //// Instagram
        'clientID'     => get_field('instagram__client_id','option') != '' ? get_field('instagram__client_id','option') : '',
        'clientSecret' => get_field('instagram__secret','option') != '' ? get_field('instagram__secret','option') : '',
        'access_token' => get_field('instagram__accesstoken','option') != '' ? get_field('instagram__accesstoken','option') : '',
        'code'         => get_field('instagram__code','option') != '' ? get_field('instagram__code','option') : '',
        'hashtag'      => get_field('instagram__hashtag','option') != '' ? get_field('instagram__hashtag','option') : '',
        'count'        => get_field('instagram__count','option') != '' ? get_field('instagram__count','option') : '',
    ));

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

}
add_action( 'wp_enqueue_scripts', 'load_enqueue_front', 20, 1 );


// Identify browser and os
// ==================================
function add_defer_parsing_of_js( $url ) {        
    if ( FALSE === strpos( $url, '.js' ) ) return $url;
    if ( strpos( $url, 'jquery.js' ) ) return $url;
    // return "$url' defer ";
    return "$url' defer='defer' onload='";    
}
if ( !is_admin() ) {
  add_filter( 'clean_url', 'add_defer_parsing_of_js', 11, 1 );
}

// Page Slug Body Class
// ==================================
function add_slug_body_class( $classes ) {
    global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_name;
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

// Identify browser and os
// ==================================
function browser_body_class($classes) {
    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
    if($is_lynx) $classes[]       = 'lynx';
    elseif($is_gecko) $classes[]  = 'gecko';
    elseif($is_opera) $classes[]  = 'opera';
    elseif($is_NS4) $classes[]    = 'ns4';
    elseif($is_safari) $classes[] = 'safari';
    elseif($is_chrome) $classes[] = 'chrome';
    elseif($is_IE) {
        $classes[] = 'ie';
        if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
        $classes[] = 'ie'.$browser_version[1];
    } else $classes[] = 'unknown';
    if($is_iphone) $classes[] = 'iphone';
    if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
        $classes[] = 'osx';
    } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
        $classes[] = 'linux';
    } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
        $classes[] = 'windows';
    }
    return $classes;
}
add_filter('body_class','browser_body_class');

function get_pagination_links() {
    ob_start();
    ?>
        <nav class="pagination">
            <?php
                global $wp_query;
                $current = max( 1, absint( get_query_var( 'paged' ) ) );
                $pagination = paginate_links( array(
                    'base'      => str_replace( PHP_INT_MAX, '%#%', esc_url( get_pagenum_link( PHP_INT_MAX ) ) ),
                    'format'    => '?paged=%#%',
                    'current'   => $current,
                    'total'     => $wp_query->max_num_pages,
                    'type'      => 'array',
                    'prev_text' => '<',
                    'next_text' => '>',
                ) ); ?>
            <?php if ( ! empty( $pagination ) ) : ?>
                <ul class="pagination--list">
                    <?php foreach ( $pagination as $key => $page_link ) : ?>
                        <li class="pagination--list-link<?php if ( strpos( $page_link, 'current' ) !== false ) { echo ' active'; } ?>"><?php echo $page_link ?></li>
                    <?php endforeach ?>
                </ul>
            <?php endif ?>
        </nav>
    <?php
    $links = ob_get_clean();
    return apply_filters( 'get_pagination_links', $links );
}

// Change search function globally to search only post, page and portfolio post types
function prefix_limit_post_types_in_search( $query ) {
    if ( $query->is_search ) {
        $query->set( 'post_type', array( 'post' ) );
    }
    return $query;
}
add_filter( 'pre_get_posts', 'prefix_limit_post_types_in_search' );

/**
* POST: Remove attribute height and width image post.
**/
if( ! function_exists( 'theme_remove_width_attribute' ) ) {

    function theme_remove_width_attribute( $html ) {
        $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
        return $html;
    }
    add_filter( 'post_thumbnail_html', 'theme_remove_width_attribute', 10 );  
    add_filter( 'image_send_to_editor', 'theme_remove_width_attribute', 10 );
}

// Includes files supports theme
// ==================================
function get_social_network_sherad_id($url){
    if(strpos($url, "twitter.com") !== false){
        return "twitter";
    }
    else if(strpos($url, "facebook.com") !== false){
        return "facebook";
    }
    else if(strpos($url, "plus.google.com") !== false){
        return "google-plus";
    }
    else if(strpos($url, "pinterest.com") !== false){
        return "pinterest";
    }
    else if(strpos($url, "tumblr.com") !== false){
        return "tumblr";
    }
    else if(strpos($url, "linkedin.com") !== false){
        return "linkedin";
    }
    else if(strpos($url, "instagram.com") !== false){
        return "instagram";
    }
    else if(strpos($url, "youtube.com") !== false){
        return "youtube";
    }else{
        return "unknown";
    }

    return $url;
}

function get_post_siblings( $limit = 3, $date = '' ) {
    global $wpdb, $post;

    if( empty( $date ) )
        $date = $post->post_date;

    //$date = '2009-06-20 12:00:00'; // test data

    $limit = absint( $limit );
    if( !$limit )
        return;

    $p = $wpdb->get_results( "
    (
        SELECT 
            p1.post_title, 
            p1.post_content, 
            p1.post_date,
            p1.ID
        FROM 
            $wpdb->posts p1 
        WHERE 
            p1.post_date < '$date' AND 
            p1.post_type = 'post' AND 
            p1.post_status = 'publish' 
        ORDER by 
            p1.post_date DESC
        LIMIT 
            $limit
    )
    UNION 
    (
        SELECT 
            p2.post_title, 
            p2.post_content,
            p2.post_date,
            p2.ID 
        FROM 
            $wpdb->posts p2 
        WHERE 
            p2.post_date > '$date' AND 
            p2.post_type = 'post' AND 
            p2.post_status = 'publish' 
        ORDER by
            p2.post_date DESC
        LIMIT 
            $limit
    ) 
    ORDER by post_date DESC
    " );
    $i = 0;
    $adjacents = array();
    for( $c = count($p); $i < $c; $i++ )
        if( $i < $limit )
            $adjacents['prev'][] = $p[$i];
        else
            $adjacents['next'][] = $p[$i];

    return $adjacents;
}

function trunc($phrase, $max_words) {
    $phrase_array = explode(' ',$phrase);
    if(count($phrase_array) > $max_words && $max_words > 0)
        $phrase = implode(' ',array_slice($phrase_array, 0, $max_words));

   return $phrase;
}

// Includes files supports theme
// ==================================
$integrations = array(
    'debug' => 'super-dump',
    'functions' => array(
        'acf',
        'cf7',
        'admin',
        'login',
        'ajax',
        'share',
        'post',
        'leads'
    ),
    'articles' => array(
        'artigo-destaque'
    ),
    'tinymce' => array(
        'taxonomies',
        'shortcodes',
    ),
    'post-type' => array(
        'post',
        'product'
    ),
    'shortcodes' => array(
        'products',
        'columns'
    ),
    'widgets' => array()
);

foreach ( $integrations as $folders => $files ) {
    if ( $files ) {
        if( is_array($files) ){
            foreach ( $files as $file ) {
                require_once( get_template_directory() . '/inc/' . $folders . '/'. $file . '.php' );
            }
        } else{
            require_once( get_template_directory() . '/inc/' . $folders . '/'. $files . '.php' );
        }
    }
}

require_once( get_template_directory() . '/dashboard/init.php' );