<?php

if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>
		<h2 class="comments-area--title">
			<?php printf( _nx( '1 Comentário', '%1$s Comentários', get_comments_number(), 'comments title' ),	number_format_i18n( get_comments_number() ) ); ?>
		</h2>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<nav class="comments-area--navigation navigation" role="navigation">
				<h2 class="screen-reader-text"><?php _e( 'Navegar nos comentários' ); ?></h2>
				<div class="nav-links">
					<?php
						if ( $prev_link = get_previous_comments_link( __( 'Comentários mais antigos' ) ) ) :
							printf( '<div class="nav-previous">%s</div>', $prev_link );
						endif;

						if ( $next_link = get_next_comments_link( __( 'Comentários mais recentes' ) ) ) :
							printf( '<div class="nav-next">%s</div>', $next_link );
						endif;
					?>
				</div><!-- .nav-links -->
			</nav><!-- .comment-navigation -->
		<?php endif; ?>

		<ol class="comments-area--list">
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 56,
				) );
			?>
		</ol><!-- .comment-list -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<nav class="comments-area--navigation navigation" role="navigation">
				<h2 class="screen-reader-text"><?php _e( 'Navegar nos comentários' ); ?></h2>
				<div class="nav-links">
					<?php
						if ( $prev_link = get_previous_comments_link( __( 'Comentários mais antigos' ) ) ) :
							printf( '<div class="nav-previous">%s</div>', $prev_link );
						endif;

						if ( $next_link = get_next_comments_link( __( 'Comentários mais recentes' ) ) ) :
							printf( '<div class="nav-next">%s</div>', $next_link );
						endif;
					?>
				</div><!-- .nav-links -->
			</nav><!-- .comment-navigation -->
		<?php endif; ?>

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comentários está fechados.' ); ?></p>
	<?php endif; ?>

	<?php
		$comments_args = array(
			// change the title of send button 
			'label_submit'         => 'Enviar',
			// change the title of the reply section
			'title_reply'          => 'Deixe um comentário',
			// remove "Text or HTML to be displayed after the set of comment fields"
			'comment_notes_before' => '<span>Seu email não será publicado</span>',
			'comment_notes_after'  => ''
		);

		comment_form($comments_args);
	?>

</div><!-- .comments-area -->