<?php get_header(); ?>
	
	<main role="main" id="main" class="site-main home">
		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					global $wpdb, $post;

					$postID = get_the_ID();

					$args = array(
						'post_type'           => 'post',
				        'post_status'         => 'publish',
				        'no_found_rows'       => true,
				        'ignore_sticky_posts' => true
					);
					 
					if( is_user_logged_in() ) :
						$user_id    = get_current_user_id();

			            $key   = 'post_views_user_'.$user_id;
			            $views = get_post_meta( $post->ID, $key, true );

			            if ( metadata_exists( 'post', $post->ID, $key ) ) {
							$args['orderby']  = 'meta_value_num';
							$args['meta_key'] = $key;
							$args['order']    = 'ASC';

			            } else{
			            	$args['orderby']  = 'date';
							$args['order']    = 'DESC';
			            }

					else :
						$key   = 'post_views_general';
						if ( metadata_exists( 'post', $post->ID, $key ) ) {
							$args['orderby']  = 'meta_value_num';
							$args['meta_key'] = $key;
							$args['order']    = 'ASC';
						} else{
			            	$args['orderby']  = 'date';
							$args['order']    = 'DESC';
			            }
					endif; 
				?>

				<!-- Posts Destaque -->
				<section class="banner">
					<div class="banner--area">
						<?php do_action('machine_learn_loop_featured'); ?>					
					</div>
				</section>
				
				<!-- Posts mosaicos -->
				<section class="posts">					
					<div class="posts--area">
						<?php 
							$args['posts_per_page'] = 1;
							$args['offset']         = 1;

							$big = new WP_Query($args);
						?>
						<div class="posts--area-loop">
							<div class="row">
								<div class="post-left col">
									<?php while( $big->have_posts() ) : $big->the_post(); ?>
										<?php get_template_part( 'views/card', 'featured' ); ?>
									<?php endwhile; wp_reset_postdata(); ?>
								</div>

								<div class="post-right col">
									<?php
										$rowCount  = 0;
										$numOfCols = 2;
										$args['posts_per_page'] = 4;
										$args['offset']         = 2;

										$medium = new WP_Query($args);
									?>													
									<div class="row">
										<?php while( $medium->have_posts() ) : $medium->the_post(); ?>
											<div class="col col-2">
												<?php get_template_part( 'views/card', 'post' ); ?>
											</div>
											<?php
											    $rowCount++;
											    if($rowCount % $numOfCols == 0) echo '</div><div class="row last">';
											?>
										<?php endwhile; wp_reset_postdata(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				
				<!-- Opt-in -->
				<section class="newsletter">
					<?php if( get_field('newsletter__title', 'option') ) : ?>
						<h3 class="newsletter--title"><?php the_field('newsletter__title', 'option'); ?></h3>
					<?php endif; ?>
					<div class="newsletter--form">
						<div class="form">
							<?php get_template_part( 'content/newsletter' ); ?>
						</div>
					</div>
				</section>

				<!-- Produtos -->
				<section class="container-fluid">
					<div class="products">
						<div class="products--area">
							<?php if( get_field('home__produtct_title') ) : ?>
								<h3 class="products--area-title"> <?php the_field('home__produtct_title'); ?></h3>
							<?php endif ?>

							<?php if( get_field('home__produtct_text') ) : ?>
								<p class="products--area-text"><?php the_field('home__produtct_text'); ?></p>
							<?php endif ?>
							<?php
								$args  = array(
									'post_status'    => 'publish',
									'post_type'      => 'product',
									'posts_per_page' => -1,
									'orderby'        => 'date',
									'order'          => 'DESC'
					            );

					            if( get_field('home__produtct_categoria') ){
					            	$product_cat = get_field('home__produtct_categoria');
					            	$args['tax_query'][] = array(
									    'taxonomy' => 'product_cat',
									    'field'    => 'term_id',
									    'terms'    => array($product_cat[0])
									);
					            }

								$products = new WP_Query($args);				
							?>
							<?php if ($products->have_posts()) : ?>
								<div class="products--area-loop">
									<div id="owl-products-home">
										<?php while( $products->have_posts() ) : $products->the_post(); ?>
											<?php 
												if( is_user_logged_in() ){
													$userID = get_current_user_id();
												}else{
													$userID = '';
												}

												if( get_field('produtct__sku') ) :
													$sku = get_field('produtct__sku');
												endif;

												$prodID = $post->ID;
												$postID = $postID;
												$userID = $userID;
											?>									
											<div class="product">
												<a href="<?php echo get_field('product__url' ); ?>" data-prod="<?php echo $prodID; ?>"  data-post="<?php echo $postID; ?>" data-user="<?php echo $userID; ?>" data-sku="<?php echo $sku; ?>" class="product--link" target="_blank">
													
													<figure class="product--figure">
														
														<?php if( has_post_thumbnail() ) : ?>
															<img class="image-hover" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
														<?php endif; ?>														
														
														<?php if( get_field('product__image_hover') ) : ?>
															<img class="image-effect" src="<?php the_field('product__image_hover'); ?>" alt="" />
														<?php else : ?>
															<img class="image-effect" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
														<?php endif; ?>	

														<button class="product--button desktop"><?php echo esc_html( __( 'Ver na loja', 'menuto' ) ); ?></button>

														<?php if( get_field('produtct__sku') ) : ?>
															<em class="product--sku"><?php the_field('produtct__sku'); ?></em>
														<?php endif; ?>

													</figure>
													
													<div class="product--title"><?php the_title(); ?></div>
													<button class="product--button mobile"><?php echo esc_html( __( 'Ver na loja', 'menuto' ) ); ?></button>

												</a>
											</div>
										<?php endwhile; wp_reset_postdata(); ?>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</section>				
				
				<!-- Posts finais -->
				<section class="container-fluid">					
					<div class="posts big">
						<div class="posts--area pad">
							<?php
								$args  = array(
									'post_status'    => 'publish',
									'post_type'      => 'post',
									'orderby'        => 'date',
									'order'          => 'DESC',
									'posts_per_page' => 2,
					            );

								$othersPosts = new WP_Query($args);
							?>
							<?php if ($othersPosts->have_posts()) : ?>
								<div class="posts--area-loop">
									<div class="row">
										<?php while( $othersPosts->have_posts() ) : $othersPosts->the_post(); ?>
											<div class="col col-2">
												<div class="post">
													<div class="post--card">
														<a href="<?php echo esc_url(get_permalink()); ?>" class="post--card-link">
															<?php if( has_post_thumbnail() ) : ?>
																<figure class="post--card-figure">
																	<div class="mask"></div>
																	<img class="post--card-image" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />																	
																	<?php if( $category = get_the_terms( $post->ID, 'category' ) ): ?>
																		<span class="post--card-category">
																			<?php foreach ( $category as $cat ): ?>
																				<?php echo $cat->name; ?>
																			<?php endforeach; ?>
																		</span>
																	<?php endif; ?>
																</figure>
															<?php else : ?>
																<figure class="post--card-figure">
																	<div class="mask"></div>
																	<?php if( get_field('banner__image') ) : ?>
																		<img class="post--card-image" src="<?php the_field('banner__image') ; ?>" alt="" />
																	<?php endif; ?>																	
																	<?php if( $category = get_the_terms( $post->ID, 'category' ) ): ?>
																		<span class="post--card-category">
																			<?php foreach ( $category as $cat ): ?>
																				<?php echo $cat->name; ?>
																			<?php endforeach; ?>
																		</span>
																	<?php endif; ?>
																</figure>
															<?php endif; ?>
															<div class="post--card-content">
																<h3 class="post--card-title"><?php the_title(); ?></h3>
																<div class="post--card-text"><?php echo wp_trim_words( do_shortcode($post->post_content), 25, '...' ); ?></div>
															</div>
														</a>
													</div>
												</div>
											</div>
										<?php endwhile; wp_reset_postdata(); ?>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</section>

				<!-- Instagram -->
				<section class="container-fluid">
					<?php get_template_part( 'content/instagram' ); ?>
				</section>

			<?php endwhile; ?>	

		<?php
			else :
				get_template_part( 'content', 'none' );
			endif;
		?>

	</main><!-- .site-main -->

<?php get_footer(); ?>