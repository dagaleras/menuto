		<?php //if( !is_singular( 'post' ) ) : ?>		
			<footer id="footer" class="footer">
				<div class="container-fluid">
					<div class="inner">

						<div class="footer--brand col-5">
							<div class="space">				
								<a href="<?php echo esc_url(home_url('/')) ?>">
			                        <img src="<?php bloginfo( 'template_url' ); ?>/images/logo/menuto_logo_novatagline.svg" alt="<?php bloginfo( 'name' ); ?>" />
			                    </a>
		                    </div>
						</div>

						<div class="footer--pages col-6">
							<div class="space">
								<?php 
									wp_nav_menu( array(
										'theme_location'  => 'pages-footer',
										'menu'            => 'pages-footer',
										'container'       => 'nav',
										'container_class' => false,
										'menu_class'      => 'menu',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 0,
										'walker'          => '',
									) );
								?>
							</div>
						</div>

						<div class="footer--categories col-7">
							<div class="space">
								<?php 
									wp_nav_menu( array(
										'theme_location'  => 'category-footer',
										'menu'            => 'category-footer',
										'container'       => 'nav',
										'container_class' => false,
										'menu_class'      => 'menu',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 0,
										'walker'          => '',
									) );
								?>
							</div>
						</div>

						<div class="footer--followus col-5">
							<div class="space">
								<h3><?php echo esc_html( __( 'Siga nos', 'menuto' ) ); ?></h3>
								<ul>
									<?php
			                            $html = "";
			                            $links = get_field('footer__redes_sociais', 'option');

			                            if(isset($links) && strlen($links) > 0) {
			                                $links_array = explode("\n", $links);
			                                foreach($links_array as $link){
			                                    $html .= '
			                                        <li>
			                                            <a href="'.trim($link).'" target="_blank" title="'.get_social_network_sherad_id($link).'" id="'.get_social_network_sherad_id($link).'">
			                                                <i class="fa fa-'.get_social_network_sherad_id($link).'"></i>
			                                            </a>
			                                        </li>
			                                    ';
			                                }
			                            }
			                            echo $html;
			                        ?>
								</ul>
							</div>
						</div>

						<div class="footer--newsletter col-5">
							<div class="space">
								<?php if( get_field('footer__newsletter_title', 'option') ) : ?>
									<h3 class="footer--newsletter-title"><?php the_field('footer__newsletter_title', 'option'); ?></h3>
								<?php endif; ?>

								<div class="footer--newsletter-form">
									<?php get_template_part( 'content/newsletter' ); ?>
								</div>

							</div>
						</div>

						<?php if( get_field('foot__copyright', 'option') ) : ?>					
							<div class="footer--copyright">
								<p><?php the_field('foot__copyright', 'option'); ?></p>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</footer>
		<?php //endif; ?>

		</div>
		<?php wp_footer(); ?>
	</body>

</html>