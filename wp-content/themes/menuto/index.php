<?php get_header(); ?>
	
	<main role="main" id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

			<?php endwhile; ?>	

		<?php
			else :
				get_template_part( 'content', 'none' );
			endif;
		?>

	</main><!-- .site-main -->

<?php get_footer(); ?>