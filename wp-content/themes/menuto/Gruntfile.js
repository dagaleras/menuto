'use strict';
module.exports = function(grunt) {

	grunt.initConfig({

		dirs: {
			js: 'js',
			css: 'css',
		},

		watch: {
			options: {
				livereload: 12345,
			},
			js: {
				files: [
					'Gruntfile.js',
					'_compile/js/vendors/*.js',
					'_compile/js/libs/*.js',
					'_compile/js/script/*.js',
					'_compile/dashboard/scripts/*/*.js',
					'_compile/dashboard/scripts/*.js'
				],
				tasks: ['uglify', 'clean' ]
			},
			css: {
				files: [
					'_compile/css/sass/*/*.scss',
					'_compile/css/sass/*.scss',
					'_compile/dashboard/sass/*/*.scss',
					'_compile/dashboard/sass/*.scss'
				],
				tasks: ['sass', 'concat', 'cssmin', 'clean']
			}
		},

		// uglify to concat, minify, and make source maps
		uglify: {
			dist: {
				options: {
					sourceMap: true
				},
				files: {
					'js/scripts.min.js': [
						'_compile/js/jquery/*.js',
						'_compile/js/vendors/*.js',
						'_compile/js/libs/*.js',
						'_compile/js/script/*.js'
					],
					'dashboard/assets/js/dashboard.min.js': [
						'_compile/dashboard/scripts/plugins/*.js',
						'_compile/dashboard/scripts/plugins/i18n/*.js',
						'_compile/dashboard/scripts/*.js',
					]
				}
			}
		},

		sass: {
			dist: {
				files: {
					'_compile/css/styles.css' : '_compile/css/sass/style.scss',
					'_compile/dashboard/css/dashboard.css' : '_compile/dashboard/sass/dashboard.scss'
				}
			}
		},

		concat: {
			dist: {
				files: {
					'_compile/css/styles.css': [ '_compile/css/libs/*.css', '_compile/css/styles.css'],
					'_compile/dashboard/css/dashboard.css': [ '_compile/dashboard/css/dashboard.css']
				}
			}
		},

		cssmin: {
			dist: {
				files: {
					'css/styles.min.css': [ '_compile/css/styles.css' ],
					'dashboard/assets/css/dashboard.min.css': [ '_compile/dashboard/css/dashboard.css' ]
				}
			}
		},

		clean: {
			dist: [
				'_compile/js/vendors.min.js',
				'_compile/js/vendors.min.js.map',
				'_compile/js/libs.min.js',
				'_compile/js/libs.min.js.map',
				'_compile/css/libs.css',
				'_compile/css/styles.css.map'
			]
		},

		cssjanus: {
			theme: {
				options: {
					swapLtrRtlInUrl: false
				},
				files: [
					{
						src: 'css/style.min.css',
						dest: 'css/style.min-rtl.css'
					}
				]
			}
		},

		makepot: {
			theme: {
				options: {
					type: 'wp-theme'
				}
			}
		},

		exec: {
			txpull: {
				cmd: 'tx pull -a --minimum-perc=75'
			},
			txpush_s: {
				cmd: 'tx push -s'
			},
		},

		potomo: {
			dist: {
				options: {
					poDel: false // Set to true if you want to erase the .po
				},
				files: [{
					expand: true,
					cwd: 'languages',
					src: ['*.po'],
					dest: 'languages',
					ext: '.mo',
					nonull: true
				}]
			}
		}

	});

	grunt.loadNpmTasks( 'grunt-contrib-watch' );
	grunt.loadNpmTasks( 'grunt-contrib-concat' );
	grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
	grunt.loadNpmTasks( 'grunt-contrib-uglify' );
	grunt.loadNpmTasks( 'grunt-contrib-clean' );
	grunt.loadNpmTasks( 'grunt-contrib-sass' );
	grunt.loadNpmTasks( 'grunt-wp-i18n' );
	grunt.loadNpmTasks( 'grunt-cssjanus' );
	grunt.loadNpmTasks( 'grunt-exec' );
	grunt.loadNpmTasks( 'grunt-potomo' );

	// register task
	grunt.registerTask('default', ['watch']);

	grunt.registerTask( 'tx', ['exec:txpull', 'potomo']);
	grunt.registerTask( 'makeandpush', ['makepot', 'exec:txpush_s']);

	grunt.registerTask('build', ['uglify', 'sass', 'concat', 'cssmin', 'clean', 'makepot', 'tx', 'makeandpush','cssjanus' ]);
};
