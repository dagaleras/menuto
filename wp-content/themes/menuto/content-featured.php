<div class="banner--area-loop">
    <a href="<?php echo esc_url(get_permalink(get_the_ID())); ?>">
        <div class="mask"></div>
        <?php if( get_field('banner__image', get_the_ID()) ) : ?>
            <figure class="figure">
                <img class="figure-img" src="<?php the_field('banner__image', get_the_ID()) ; ?>" alt="" />
            </figure>
        <?php else: ?>
            <?php if( has_post_thumbnail() ) : ?>
                <figure class="figure">
                    <?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ), 'full'); ?>
                    <img class="figure-img" src="<?php echo $thumbnail[0]; ?>" alt="" />
                </figure>
            <?php endif; ?>
        <?php endif; ?> 
        <div class="content">                                                       
            <h2 class="content--title"><?php the_title(); ?></h2>
            <div class="content--paragraph">
                <div class="arrow-up"></div>
                <div class="content--paragraph-summary">
                    <?php echo wp_trim_words( do_shortcode($post->post_content), 17, '...' ); ?>
                </div>
                <button class="content--paragraph-button"><?php _e('leia mais'); ?></button>
            </div>                                                      
        </div>
        <?php if( $category = get_the_terms( get_the_ID(), 'category' ) ): ?>
            <div class="category">
                <?php foreach ( $category as $cat ): ?>
                    <label for="category"><?php echo $cat->name; ?></label>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>                                             
    </a>
</div>