<?php 
	global $post;
	$category = get_the_terms( $post->ID, 'category' );
?>
<div class="post">
	<div class="post--card">
		<a href="<?php echo esc_url(get_permalink()); ?>" class="post--card-link">
			<?php if( has_post_thumbnail() ) : ?>
				<figure class="post--card-figure">
					<div class="mask"></div>
					<img class="post--card-image" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />																	
					<?php if( $category ): ?>
						<span class="post--card-category">
							<?php foreach ( $category as $cat ): ?>
								<?php echo $cat->name; ?>
							<?php endforeach; ?>
						</span>
					<?php endif; ?>
				</figure>
			<?php else : ?>
				<figure class="post--card-figure">
					<div class="mask"></div>
					<?php if( get_field('banner__image') ) : ?>
						<img class="post--card-image" src="<?php the_field('banner__image') ; ?>" alt="" />
					<?php endif; ?>																	
					<?php if( $category ): ?>
						<span class="post--card-category">
							<?php foreach ( $category as $cat ): ?>
								<?php echo $cat->name; ?>
							<?php endforeach; ?>
						</span>
					<?php endif; ?>
				</figure>
			<?php endif; ?>
			<div class="post--card-content">
				<h3 class="post--card-title"><?php the_title(); ?></h3>
			</div>
		</a>
	</div>
</div>