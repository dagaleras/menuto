<div id="lostpassword" class="modal" tabindex="-1" role="dialog">
  	<div class="modal-content">
  		<div class="wrapper">
			<div class="modal-body">
				<div class="ajax_login">
		  			<div class="modal-header">
						<h5 class="modal-title">Digite seu e-mail<br />para recuperar senha</h5>				
					</div>
		            <form id="form-lostpassword" method="post">
		                <div class="form-group">
		                	<?php $user_login = isset( $_POST['lost_userlogin'] ) ? $_POST['lost_userlogin'] : ''; ?>
			                <input type="text" name="lost_userlogin" id="lost_userlogin" class="form-control required" value="<?php echo $user_login; ?>" required />
			                <label class="form-control-placeholder" for="lost_userlogin"><?php esc_attr_e('Seu e-mail*', 'menuto') ?></label>
			                <div class="msg"></div>
			            </div>

		                <?php wp_nonce_field('ajax-forgot-nonce', 'forgotsecurity'); ?>

		                <input name="submit" class="submit_button" type="submit" value="<?php esc_attr_e('Continuar','menuto') ?>" />
		                <input type="hidden" name="reset_pass" value="1" />
		    			<input type="hidden" name="user-cookie" value="1" />

		                <div class="status"></div>
		            </form>
		        </div>
		        <div class="ajax-response">
		        	<h5 class="modal-title">Enviamos sua senha <br /> por e-mail!</h5>
		        </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close"></button>
			</div>
		</div>
  	</div>
</div>