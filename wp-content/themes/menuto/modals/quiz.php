<?php 
	$current_user = wp_get_current_user();

	$acf_question_1 = "field_5a5ce32b14683";
	$question_1     = get_field_object($acf_question_1);

	$acf_question_2 = "field_5a5ce36d14684";
	$question_2     = get_field_object($acf_question_2);

	$acf_question_3 = "field_5a5ce39a14685";
	$question_3     = get_field_object($acf_question_3);


	$acf_question_4 = "field_5aecaee6bb29c";
	$question_4     = get_field_object($acf_question_4);


	$acf_question_5 = "field_5aecaf06bb29d";
	$question_5     = get_field_object($acf_question_5);


	$acf_question_6 = "field_5aecaf54bb29e";
	$question_6     = get_field_object($acf_question_6);


	$acf_question_7 = "field_5aecaf79bb29f";
	$question_7     = get_field_object($acf_question_7);

	$result_quiz = get_field('group__result_quiz', 'option');
?>

<div id="quiz" class="modal" tabindex="-1" role="dialog">
	
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="quiz">

				<div class="quiz-start">
					<div class="modal-header">
						<h5 class="modal-title">Quiz</h5>						
					</div>
					<div class="modal-body">
						<p class="text">As perguntas a seguir ajudarão<br />a definir o conteúdo que mais<br />combina com o seu perfil. </p>
						<a href="javascript:void(0);" class="btn-cta start-quiz"><?php esc_attr_e('VAMOS LÁ','menuto') ?></a>
					</div>
					<div class="modal-footer">
						<a href="javascript:void(0);" class="modal-skip" data-dismiss="modal" aria-label="Close">pular quiz</a>
					</div>
				</div>

				<div class="quiz-questions">						
					<div class="modal-body">
						<div class="wizard">

							<form id="form-quiz" method="post">

								<div class="wizard-step active" id="step_1">
									<?php if($question_1['label']) : ?>
										<header class="wizard-header">
											<h5 class="wizard-title"><?php echo $question_1['label']; ?></h5>
											<div class="index-step">1 de 7</div>
										</header>
									<?php endif; ?>
									<div class="row">
										<?php if($question_1['choices']) : ?>
											<?php foreach ($question_1['choices'] as $key => $value) : ?>
												<div class="col col-3">
													<div class="question">
														<figure>
															<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question1/<?php echo $key; ?>.jpeg" alt="" />
														</figure>
														<div class="radio">
															<input type="radio" name="<?php echo $question_1['name']; ?>" id="step_1_<?php echo $key; ?>" value="<?php echo $key; ?>" data-next="2" />
															<label for="step_1_<?php echo $key; ?>">
																<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question1/<?php echo $key; ?>.jpeg" alt="" />
																<span><?php echo $value; ?></span>
															</label>
														</div>
													</div>
												</div>
											<?php endforeach; ?>
										<?php endif; ?>
									</div>
								</div>
								<div class="wizard-step" id="step_2">
									<?php if($question_2['label']) : ?>
										<header class="wizard-header">
											<h5 class="wizard-title"><?php echo $question_2['label']; ?></h5>
											<div class="index-step">2 de 7</div>
										</header>
									<?php endif; ?>
									<div class="row">
										<?php if($question_2['choices']) : ?>
											<?php foreach ($question_2['choices'] as $key => $value) : ?>
												<div class="col col-3">
													<div class="question">
														<figure>
															<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question2/<?php echo $key; ?>.jpeg" alt="" />
														</figure>
														<div class="radio">
															<input type="radio" name="<?php echo $question_2['name']; ?>" id="step_2_<?php echo $key; ?>" value="<?php echo $key; ?>" data-next="3" />
															<label for="step_2_<?php echo $key; ?>">
																<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question2/<?php echo $key; ?>.jpeg" alt="" />
																<span><?php echo $value; ?></span>
															</label>
														</div>
													</div>
												</div>
											<?php endforeach; ?>
										<?php endif; ?>
									</div>
								</div>
								<div class="wizard-step" id="step_3">
									<?php if($question_3['label']) : ?>
										<header class="wizard-header">
											<h5 class="wizard-title"><?php echo $question_3['label']; ?></h5>
											<div class="index-step">3 de 7</div>
										</header>
									<?php endif; ?>
									<div class="row">
										<?php if($question_3['choices']) : ?>
											<?php foreach ($question_3['choices'] as $key => $value) : ?>
												<div class="col col-3">
													<div class="question">
														<figure>
															<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question3/<?php echo $key; ?>.jpeg" alt="" />
														</figure>
														<div class="radio">
															<input type="radio" name="<?php echo $question_3['name']; ?>" id="step_3_<?php echo $key; ?>" value="<?php echo $key; ?>" data-next="4" />
															<label for="step_3_<?php echo $key; ?>">
																<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question3/<?php echo $key; ?>.jpeg" alt="" />
																<span><?php echo $value; ?></span>
															</label>
														</div>
													</div>
												</div>
											<?php endforeach; ?>
										<?php endif; ?>
									</div>
								</div>
								<div class="wizard-step" id="step_4">
									<?php if($question_4['label']) : ?>
										<header class="wizard-header">
											<h5 class="wizard-title"><?php echo $question_4['label']; ?></h5>
											<div class="index-step">4 de 7</div>
										</header>
									<?php endif; ?>
									<div class="row">
										<?php if($question_4['choices']) : ?>
											<?php foreach ($question_4['choices'] as $key => $value) : ?>
												<div class="col col-3">
													<div class="question">
														<figure>
															<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question4/<?php echo $key; ?>.jpeg" alt="" />
														</figure>
														<div class="radio">
															<input type="radio" name="<?php echo $question_4['name']; ?>" id="step_4_<?php echo $key; ?>" value="<?php echo $key; ?>" data-next="5" />
															<label for="step_4_<?php echo $key; ?>">
																<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question4/<?php echo $key; ?>.jpeg" alt="" />
																<span><?php echo $value; ?></span>
															</label>
														</div>
													</div>
												</div>
											<?php endforeach; ?>
										<?php endif; ?>
									</div>
								</div>
								<div class="wizard-step" id="step_5">
									<?php if($question_5['label']) : ?>
										<header class="wizard-header">
											<h5 class="wizard-title"><?php echo $question_5['label']; ?></h5>
											<div class="index-step">5 de 7</div>
										</header>
									<?php endif; ?>
									<div class="row">
										<?php if($question_5['choices']) : ?>
											<?php foreach ($question_5['choices'] as $key => $value) : ?>
												<div class="col col-3">
													<div class="question">
														<figure>
															<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question5/<?php echo $key; ?>.jpeg" alt="" />
														</figure>
														<div class="radio">
															<input type="radio" name="<?php echo $question_5['name']; ?>" id="step_5_<?php echo $key; ?>" value="<?php echo $key; ?>" data-next="6" />
															<label for="step_5_<?php echo $key; ?>">
																<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question5/<?php echo $key; ?>.jpeg" alt="" />
																<span><?php echo $value; ?></span>
															</label>
														</div>
													</div>
												</div>
											<?php endforeach; ?>
										<?php endif; ?>
									</div>
								</div>
								<div class="wizard-step" id="step_6">
									<?php if($question_6['label']) : ?>
										<header class="wizard-header">
											<h5 class="wizard-title"><?php echo $question_6['label']; ?></h5>
											<div class="index-step">6 de 7</div>
										</header>
									<?php endif; ?>
									<div class="row">
										<?php if($question_6['choices']) : ?>
											<?php foreach ($question_6['choices'] as $key => $value) : ?>
												<div class="col col-3">
													<div class="question">
														<figure>
															<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question6/<?php echo $key; ?>.jpeg" alt="" />
														</figure>
														<div class="radio">
															<input type="radio" name="<?php echo $question_6['name']; ?>" id="step_6_<?php echo $key; ?>" value="<?php echo $key; ?>" data-next="7" />
															<label for="step_6_<?php echo $key; ?>">
																<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question6/<?php echo $key; ?>.jpeg" alt="" />
																<span><?php echo $value; ?></span>
															</label>
														</div>
													</div>
												</div>
											<?php endforeach; ?>
										<?php endif; ?>
									</div>
								</div>
								<div class="wizard-step" id="step_7">
									<?php if($question_7['label']) : ?>
										<header class="wizard-header">
											<h5 class="wizard-title"><?php echo $question_7['label']; ?></h5>
											<div class="index-step">7 de 7</div>
										</header>
									<?php endif; ?>
									<div class="row">
										<?php if($question_7['choices']) : ?>
											<?php foreach ($question_7['choices'] as $key => $value) : ?>
												<div class="col col-3">
													<div class="question">
														<figure>
															<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question7/<?php echo $key; ?>.jpg" alt="" />
														</figure>
														<div class="radio">
															<input type="radio" name="<?php echo $question_7['name']; ?>" id="step_7_<?php echo $key; ?>" value="<?php echo $key; ?>" data-next="0" />
															<label for="step_7_<?php echo $key; ?>">
																<img src="<?php bloginfo( 'template_url' ); ?>/images/quiz/question7/<?php echo $key; ?>.jpg" alt="" />
																<span><?php echo $value; ?></span>
															</label>
														</div>
													</div>
												</div>
											<?php endforeach; ?>
										<?php endif; ?>
									</div>
									<div class="status"></div>
								</div>

								<input type="hidden" name="user_id" id="user_id" value="<?php echo $current_user->ID; ?>" />
								<?php wp_nonce_field( 'ajax-quiz-nonce', 'quizsecurity' ); ?>				                
							</form>

						</div>
					</div>
					<div class="modal-footer">
						<a href="<?php echo esc_url(home_url('/')) ?>" class="modal-skip" data-dismiss="modal" aria-label="Close">pular quiz</a>
					</div>
				</div>

				<div class="quiz-results">
					<?php if($result_quiz['resultados']) : ?>
						<div class="modal-header">
							<h5 class="modal-title">Seu perfil é:</h5>								
						</div>
						<div class="modal-body">
							<?php foreach ($result_quiz['resultados'] as $key => $value) : ?>
								<?php if( $value['categoria'] ) : ?><div class="gruporesult" id="cat_<?php echo $value['categoria']; ?>"><?php endif; ?>
									<?php if($value['imagem']) : ?>
										<figure>
											<img src="<?php echo $value['imagem']; ?>" alt="" />
										</figure>
									<?php endif; ?>
									<?php if($value['titulo']) : ?>
										<h2><?php echo $value['titulo']; ?></h2>
									<?php endif; ?>
									<?php if($value['texto']) : ?>
										<p class="text"><?php echo $value['texto']; ?></p>
									<?php endif; ?>
									<a href="<?php echo esc_url(home_url('/')) ?>" class="btn-cta">VEJA O CONTEÚDO SELECIONADO PARA VOCÊ</a>
								<?php if( $value['categoria'] ) : ?></div><?php endif; ?>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>

			</div>
		</div>
	</div>
</div>