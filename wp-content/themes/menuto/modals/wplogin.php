<div id="loginemail" class="modal" tabindex="-1" role="dialog">
  	<div class="modal-content">
  		<div class="wrapper">
	  		<div class="modal-header">
				<h5 class="modal-title">Acesse sua conta<br />ou crie uma nova</h5>				
			</div>
			<div class="modal-body">
				<div class="ajax_login">
		            <form id="form-login" method="post">					                
		                <div class="form-group">
			                <input type="text" name="username" id="username" class="form-control required" required />
			                <label class="form-control-placeholder" for="name"><?php esc_attr_e('Seu e-mail*', 'menuto') ?></label>
			                <div class="msg"></div>
			            </div>
			            <div class="form-group">
			                <input type="password" name="password" id="password" class="form-control password-toggle required" required />
			                <label class="form-control-placeholder" for="name"><?php esc_attr_e('Sua senha*', 'menuto') ?></label>
			                <span toggle=".password-toggle" class="field-icon toggle-password"></span>
			                <em>Mínimo de 6 caracteres</em>
			                <div class="msg"></div>
			            </div>

		                <input name="submit" data-target="#loginemail" class="submit_button" type="submit" value="<?php esc_attr_e('Continuar','menuto') ?>" />

		                <?php wp_nonce_field( 'ajax-login-nonce', 'loginsecurity' ); ?>

		                <div class="status"></div>
		            </form>				            
		            <p class="register">Ainda não é cadastrado? <a href="javascript:void(0);" class="change-modal" data-target="#register"><?php esc_attr_e('Crie uma conta.','menuto') ?></a></p>
		            <p class="lostpassword">Não lembra sua senha? <a href="javascript:void(0);" class="lost change-modal" data-target="#lostpassword"><?php esc_attr_e('Clique aqui.','menuto') ?></a></p>
		        </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close"></button>
			</div>
		</div>
  	</div>
</div>