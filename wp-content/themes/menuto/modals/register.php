<div id="register" class="modal" tabindex="-1" role="dialog">
	<div class="modal-content">
		<div class="wrapper">	  		
			<div class="modal-body">
				<div class="ajax_register">
		            <form id="form-register" method="post">
						
						<div class="group-user">
							<div class="modal-header">
								<h5 class="modal-title">Olá! Digite seus dados<br />de acesso abaixo</h5>
							</div>

			                <div class="form-group">
				                <input type="text" name="reg_username" id="reg_username" class="form-control required" required />
				                <label class="form-control-placeholder" for="reg_username"><?php esc_attr_e('Seu nome*', 'menuto') ?></label>
				                <div class="msg"></div>
				            </div>					            
				            <div class="form-group">
				                <input type="text" name="reg_email" id="reg_email" class="form-control required" required />
				                <label class="form-control-placeholder" for="reg_email"><?php esc_attr_e('Seu e-mail*', 'menuto') ?></label>
				                <div class="msg"></div>
				            </div>
				            <div class="form-group">
				                <input type="text" name="reg_c_email" id="reg_c_email" class="form-control required" required />
				                <label class="form-control-placeholder" for="reg_c_email"><?php esc_attr_e('Confirme seu e-mail*', 'menuto') ?></label>
				                <div class="msg"></div>
				            </div>
				            <div class="form-group">
				                <input type="password" name="reg_password" id="reg_password" class="form-control password-toggle required" required />
				                <label class="form-control-placeholder" for="reg_password"><?php esc_attr_e('Sua senha*', 'menuto') ?></label>
			                	<span toggle=".password-toggle" class="field-icon toggle-password"></span>
				                <em>Mínimo de 6 caracteres</em>
				                <div class="msg"></div>
				            </div>		                

			               	<a href="javascript:void(0);" class="btn-cta skip-gender"><?php esc_attr_e('Continuar','menuto') ?></a>

			               	<div class="statususer"></div>
		               	</div>

		               	<div class="group-gender">
		               		<div class="modal-header">
								<h5 class="modal-title">Qual o seu sexo?</h5>
							</div>

		               		<div class="radio-group">
						        <input type="radio" name="gender" id="masculino" value="masculino" />
						        <label class="gender masculino" for="masculino"></label>

						        <input type="radio" name="gender" id="feminino" value="feminino" />
						        <label class="gender feminino" for="feminino"></label>
						    </div>

						    <div class="radio-group">
						        <input type="radio" name="gender" id="notanswer" value="Prefiro não responder" />
						        <label class="gender notanswer" for="notanswer">Prefiro não responder</label>
						    </div>
		               	</div>

		               	<input type="hidden" name="quiz" value="#quiz" />

		                <?php wp_nonce_field( 'ajax-register-nonce', 'registersecurity' ); ?>
		                <div class="status"></div>
		            </form>
		        </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close"></button>
			</div>
		</div>
  	</div>
</div>