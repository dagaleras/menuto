(function() {
    tinymce.PluginManager.add('mce_button_taxonomies', function( editor, url ) {
        editor.addButton('mce_button_taxonomies', {
            text: '{ Produto por Categoria }',
            icon: 'icon dashicons-tag',
            tooltip: 'TAX List',
            onclick: function() {
                editor.windowManager.open( {
                    title: 'Inserir Produto por categoria',
                    width: 400,
                    height: 250,
                    body: [
                        {
                            type  : 'textbox',
                            name  : 'textboxCount',
                            label : 'Quantidade',
                            value : ''
                        },
                        {
                            type  : 'textbox',
                            name  : 'textboxColumn',
                            label : 'Coluna',
                            value : ''
                        },
                        {
                            type     : 'listbox',
                            name     : 'listboxTaxonomy',
                            label    : 'Categoria',
                            'values' : editor.settings.cptTaxonomyList
                        }
                    ],
                    onsubmit: function( e ) {
                        editor.insertContent( '[products perpage="' + e.data.textboxCount + '" col="' + e.data.textboxColumn + '" cat="' + e.data.listboxTaxonomy + '"]');
                    }
                });
            }
        });
    });
})();

// (function() {
//     tinymce.PluginManager.add( 'columns', function( editor, url ) {
//         // Add Button to Visual Editor Toolbar
//         editor.addButton('columns', {
//             title: 'Insert Column',
//             cmd: 'columns',
//             text: '{ Colunas }',
//             icon: 'icon dashicons-layout',
//         });
 
//         editor.addCommand('columns', function() {
//             var selected_text = editor.selection.getContent({
//                 'format': 'html'
//             });
//             if ( selected_text.length === 0 ) {
//                 alert( 'Please select some text.' );
//                 return;
//             }
//             var open_column  = '<div class="column">';
//             var close_column = '</div>';
//             var return_text  = '';
//             return_text      = open_column + selected_text + close_column;
//             editor.execCommand('mceReplaceContent', false, return_text);
//             return;
//         });
 
//     });
// })();


(function () {
    "use strict";

    var wcShortcodeManager = function(editor, url) {
        var wcDummyContent = 'Sample Content';
        var wcParagraphContent = '<p>Sample Content</p>';


        editor.addButton('wc_shortcodes_button', function() {
            return {
                title: "",
                text: "Shortcodes",
                image: url + "/images/shortcodes.png",
                type: 'menubutton',
                icons: false,
                menu: [
                    {
                        text: 'Columns',
                        menu: [
                            {
                                text: "1/2 + 1/2",
                                onclick: function(){
                                    editor.insertContent('[wc_row][wc_column size="one-half" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-half" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                                }
                            },
                            {
                                text: "1/3 + 1/3 + 1/3",
                                onclick: function(){
                                    editor.insertContent('[wc_row][wc_column size="one-third" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-third"]</p>' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-third" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                                }
                            },
                            {
                                text: "1/3 + 2/3",
                                onclick: function(){
                                    editor.insertContent('[wc_row][wc_column size="one-third" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="two-third" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                                }
                            },
                            {
                                text: "2/3 + 1/3",
                                onclick: function(){
                                    editor.insertContent('[wc_row][wc_column size="two-third" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-third" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                                }
                            },
                            {
                                text: "1/4 + 1/4 + 1/4 + 1/4",
                                onclick: function(){
                                    editor.insertContent('[wc_row][wc_column size="one-fourth" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth"]</p>' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth"]</p>' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                                }
                            },
                            {
                                text: "1/4 + 1/2 + 1/4",
                                onclick: function(){
                                    editor.insertContent('[wc_row][wc_column size="one-fourth" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-half"]</p>' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                                }
                            },
                            {
                                text: "1/2 + 1/4 + 1/4",
                                onclick: function(){
                                    editor.insertContent('[wc_row][wc_column size="one-half" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth"]</p>' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                                }
                            },
                            {
                                text: "1/4 + 1/4 + 1/2",
                                onclick: function(){
                                    editor.insertContent('[wc_row][wc_column size="one-fourth" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth"]</p>' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-half" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                                }
                            },
                            {
                                text: "1/4 + 3/4",
                                onclick: function(){
                                    editor.insertContent('[wc_row][wc_column size="one-fourth" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="three-fourth" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                                }
                            },
                            {
                                text: "3/4 + 1/4",
                                onclick: function(){
                                    editor.insertContent('[wc_row][wc_column size="three-fourth" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                                }
                            }
                        ]
                    },
                    {
                        text: 'Elements',
                        menu: [
                            {
                                text: "Posts",
                                onclick: function(){
                                    editor.insertContent('[wc_posts author="" author_name="" p="" post__in="" order="DESC" orderby="date" post_status="publish" post_type="post" posts_per_page="10" taxonomy="" field="slug" terms="" title="yes" meta_all="yes" meta_author="yes" meta_date="yes" meta_comments="yes" thumbnail="yes" content="yes" paging="yes" size="large" filtering="yes" columns="3" gutter_space="0.020" heading_type="h2" layout="isotope"][/wc_posts]');
                                }
                            },
                            {
                                text: "Button",
                                onclick: function(){
                                    editor.insertContent('[wc_button type="primary" url="http://webplantmedia.com" title="Visit Site" target="self" position="float"]' + wcDummyContent + '[/wc_button]');
                                }
                            },
                            {
                                text: "Google Map",
                                onclick: function(){
                                    editor.insertContent('[wc_googlemap title="St. Paul\'s Chapel" location="209 Broadway, New York, NY 10007" zoom="10" height="250" title_on_load="no" class=""]');
                                }
                            },
                            {
                                text: "Heading",
                                onclick: function(){
                                    editor.insertContent('[wc_heading type="h1" title="' + wcDummyContent + '" text_align="left"]');
                                }
                            },
                            {
                                text: "Pricing Table",
                                onclick: function(){
                                    editor.insertContent('[wc_pricing type="primary" featured="yes" plan="Basic" cost="$19.99" per="per month" button_url="#" button_text="Sign Up" button_target="self" button_rel="nofollow"]<ul><li>30GB Storage</li><li>512MB Ram</li><li>10 databases</li><li>1,000 Emails</li><li>25GB Bandwidth</li></ul>[/wc_pricing]');
                                }
                            },
                            {
                                text: "Skillbar",
                                onclick: function(){
                                    editor.insertContent('[wc_skillbar title="' + wcDummyContent + '" percentage="100" color="#6adcfa"]');
                                }
                            },
                            {
                                text: "Social Icon",
                                onclick: function(){
                                    editor.insertContent('[wc_social_icons align="left" size="large" display="facebook,google,twitter,pinterest,instagram,bloglovin,flickr,rss,email,custom1,custom2,custom3,custom4,custom5"]');
                                }
                            },
                            {
                                text: "Testimonial",
                                onclick: function(){
                                    editor.insertContent('[wc_testimonial by="Wordpress Canvas" url="" position="left"]' + wcDummyContent + '[/wc_testimonial]');
                                }
                            },
                            {
                                text: "Image",
                                onclick: function(){
                                    editor.insertContent('[wc_image attachment_id="" size="" title="" alt="" caption="" link_to="post" url="" align="none" flag="For Sale" left="" top="" right="0" bottom="20" text_color="" background_color="" font_size=""][/wc_image]');
                                }
                            },
                            {
                                text: "Countdown",
                                onclick: function(){
                                    var d = new Date();
                                    var year = d.getFullYear() + 1;
                                    editor.insertContent('[wc_countdown date="July 23, '+year+', 6:00:00 PM" format="wdHMs" message="Your Message Here!" labels="Years,Months,Weeks,Days,Hours,Minutes,Seconds" labels1="Year,Month,Week,Day,Hour,Minute,Second"]');
                                }
                            },
                            {
                                text: "RSVP",
                                onclick: function(){
                                    editor.insertContent('[wc_rsvp columns="3" align="left" button_align="center"]');
                                }
                            },
                            {
                                text: "HTML",
                                onclick: function(){
                                    editor.insertContent('[wc_html name="Custom Field Name"]');
                                }
                            }
                        ]
                    },
                    {
                        text: 'Boxes',
                        menu: [
                            {
                                text: "Primary",
                                onclick: function(){
                                    editor.insertContent('[wc_box color="primary" text_align="left"]' + wcDummyContent + '[/wc_box]');
                                }
                            },
                            {
                                text: "Secondary",
                                onclick: function(){
                                    editor.insertContent('[wc_box color="secondary" text_align="left"]' + wcDummyContent + '[/wc_box]');
                                }
                            },
                            {
                                text: "Inverse",
                                onclick: function(){
                                    editor.insertContent('[wc_box color="inverse" text_align="left"]' + wcDummyContent + '[/wc_box]');
                                }
                            },
                            {
                                text: "Success",
                                onclick: function(){
                                    editor.insertContent('[wc_box color="success" text_align="left"]' + wcDummyContent + '[/wc_box]');
                                }
                            },
                            {
                                text: "Warning",
                                onclick: function(){
                                    editor.insertContent('[wc_box color="warning" text_align="left"]' + wcDummyContent + '[/wc_box]');
                                }
                            },
                            {
                                text: "Danger",
                                onclick: function(){
                                    editor.insertContent('[wc_box color="danger" text_align="left"]' + wcDummyContent + '[/wc_box]');
                                }
                            },
                            {
                                text: "Info",
                                onclick: function(){
                                    editor.insertContent('[wc_box color="info" text_align="left"]' + wcDummyContent + '[/wc_box]');
                                }
                            }
                        ]
                    },
                    {
                        text: 'Highlights',
                        menu: [
                            {
                                text: "Blue",
                                onclick: function(){
                                    editor.insertContent('[wc_highlight color="blue"]' + wcDummyContent + '[/wc_highlight]');
                                }
                            },
                            {
                                text: "Gray",
                                onclick: function(){
                                    editor.insertContent('[wc_highlight color="gray"]' + wcDummyContent + '[/wc_highlight]');
                                }
                            },
                            {
                                text: "Green",
                                onclick: function(){
                                    editor.insertContent('[wc_highlight color="green"]' + wcDummyContent + '[/wc_highlight]');
                                }
                            },
                            {
                                text: "Red",
                                onclick: function(){
                                    editor.insertContent('[wc_highlight color="red"]' + wcDummyContent + '[/wc_highlight]');
                                }
                            },
                            {
                                text: "Yellow",
                                onclick: function(){
                                    editor.insertContent('[wc_highlight color="yellow"]' + wcDummyContent + '[/wc_highlight]');
                                }
                            }
                        ]
                    },
                    {
                        text: 'Dividers',
                        menu: [
                            {
                                text: "Solid",
                                onclick: function(){
                                    editor.insertContent('[wc_divider style="solid" line="single" margin_top="" margin_bottom=""]');
                                }
                            },
                            {
                                text: "Dashed",
                                onclick: function(){
                                    editor.insertContent('[wc_divider style="dashed" line="single" margin_top="" margin_bottom=""]');
                                }
                            },
                            {
                                text: "Dotted",
                                onclick: function(){
                                    editor.insertContent('[wc_divider style="dotted" line="single" margin_top="" margin_bottom=""]');
                                }
                            },
                            {
                                text: "Double",
                                onclick: function(){
                                    editor.insertContent('[wc_divider style="solid" line="double" margin_top="" margin_bottom=""]');
                                }
                            },
                            {
                                text: "Image1",
                                onclick: function(){
                                    editor.insertContent('[wc_divider style="image" margin_top="" margin_bottom=""]');
                                }
                            },
                            {
                                text: "Image2",
                                onclick: function(){
                                    editor.insertContent('[wc_divider style="image2" margin_top="" margin_bottom=""]');
                                }
                            },
                            {
                                text: "Image3",
                                onclick: function(){
                                    editor.insertContent('[wc_divider style="image3" margin_top="" margin_bottom=""]');
                                }
                            }
                        ]
                    },
                    {
                        text: 'jQuery',
                        menu: [
                            {
                                text: "Accordion",
                                onclick: function(){
                                    editor.insertContent('[wc_accordion collapse="0"]<p>[wc_accordion_section title="Section 1"]</p>' + wcParagraphContent + '<p>[/wc_accordion_section]</p><p>[wc_accordion_section title="Section 2"]</p>' + wcParagraphContent + '<p>[/wc_accordion_section]</p>[/wc_accordion]');
                                }
                            },
                            {
                                text: "Tabs",
                                onclick: function(){
                                    editor.insertContent('[wc_tabgroup][wc_tab title="First Tab"]'+wcParagraphContent+'<p>[/wc_tab][wc_tab title="Second Tab"]</p>'+wcParagraphContent+'[/wc_tab][/wc_tabgroup]');
                                }
                            },
                            {
                                text: "Toggle",
                                onclick: function(){
                                    editor.insertContent('[wc_toggle title="This Is Your Toggle Title" padding="" border_width=""]' + wcParagraphContent + '[/wc_toggle]');
                                }
                            }
                        ]
                    },
                    {
                        text: 'Other',
                        menu: [
                            {
                                text: "Spacing",
                                onclick: function(){
                                    editor.insertContent('[wc_spacing size="40px"]');
                                }
                            },
                            {
                                text: "Clear Floats",
                                onclick: function(){
                                    editor.insertContent('[wc_clear_floats]');
                                }
                            },
                            {
                                text: "Center Content",
                                onclick: function(){
                                    editor.insertContent('[wc_center max_width="500px" text_align="left"]' + wcParagraphContent + '[/wc_center]');
                                }
                            },
                            {
                                text: "Full Width",
                                onclick: function(){
                                    editor.insertContent('[wc_fullwidth selector="#main"]' + wcDummyContent + '[/wc_fullwidth]');
                                }
                            },
                            {
                                text: "Code",
                                onclick: function(){
                                    editor.insertContent('[wc_code]' + wcDummyContent + '[/wc_code]');
                                }
                            },
                            {
                                text: "Pre",
                                onclick: function(){
                                    editor.insertContent('[wc_pre color="1" wrap="0" scrollable="1" linenums="0" name="Custom Field Name"]');
                                }
                            }
                        ]
                    },
                ]
            }
        });
    };
    
    tinymce.PluginManager.add( "wc_shortcodes", wcShortcodeManager );
})();

// ( function() {
//     "use strict";

//     var ICONS;

//     var setIconShortcode = function(id) {
//         return '[wc_fa icon="' + id + '" margin_left="" margin_right=""][/wc_fa]';
//     }

//     var icon = function(id) {
//         return '<i class="fa fa-' + id + '"></i>';
//     }

//     var wcShortcodeFA = function( editor, url ) {
//         editor.addButton('wcfontAwesomeGlyphSelect', function() {
//             var values = [];

//             for (var i = 0; i < ICONS.length; i++) {
//                 var _id = ICONS[i];
//                 values.push({text: _id, value: _id});
//             }
     
//             return {
//                 type: 'listbox',
//                 //name: 'align',
//                 text: 'FontAwesome',
//                 label: 'Select :',
//                 fixedWidth: true,
//                 onselect: function(e) {
//                     if (e) {
//                         editor.insertContent(setIconShortcode(e.control.settings.value));
//                     }       
//                     return false;
//                 },
//                 values: values,
//             };
//         });
//     };
//     tinymce.PluginManager.add( 'wc_font_awesome', wcShortcodeFA );

//     ICONS = ["adjust", "adn", "align-center", "align-justify", "align-left", "align-right", "ambulance", "anchor", "android", "angle-double-down", "angle-double-left", "angle-double-right", "angle-double-up", "angle-down", "angle-left", "angle-right", "angle-up", "apple", "archive", "arrow-circle-down", "arrow-circle-left", "arrow-circle-o-down", "arrow-circle-o-left", "arrow-circle-o-right", "arrow-circle-o-up", "arrow-circle-right", "arrow-circle-up", "arrow-down", "arrow-left", "arrow-right", "arrow-up", "arrows", "arrows-alt", "arrows-h", "arrows-v", "asterisk", "backward", "ban", "bar-chart-o", "barcode", "bars", "beer", "bell", "bell-o", "bitbucket", "bitbucket-square", "bitcoin", "bold", "book", "bookmark", "bookmark-o", "briefcase", "bug", "building-o", "bullhorn", "bullseye", "calendar", "calendar-o", "camera", "camera-retro", "caret-down", "caret-left", "caret-right", "caret-up", "certificate", "chain", "check", "check-circle", "check-circle-o", "check-square", "check-square-o", "chevron-circle-down", "chevron-circle-left", "chevron-circle-right", "chevron-circle-up", "chevron-down", "chevron-left", "chevron-right", "chevron-up", "circle", "circle-o", "clock-o", "cloud", "cloud-download", "cloud-upload", "cny", "code", "code-fork", "coffee", "columns", "comment", "comment-o", "comments", "comments-o", "compass", "compress", "copy", "credit-card", "crop", "crosshairs", "css3", "cut", "cutlery", "dashboard", "dedent", "desktop", "dollar", "dot-circle-o", "download", "dribbble", "dropbox", "edit", "eject", "ellipsis-h", "ellipsis-v", "envelope", "envelope-o", "eraser", "euro", "exchange", "exclamation", "exclamation-circle", "expand", "external-link", "external-link-square", "eye", "eye-slash", "facebook", "facebook-square", "fast-backward", "fast-forward", "female", "fighter-jet", "file", "file-o", "file-text", "file-text-o", "film", "filter", "fire", "fire-extinguisher", "flag", "flag-checkered", "flag-o", "flash", "flask", "flickr", "folder", "folder-o", "folder-open", "folder-open-o", "font", "forward", "foursquare", "frown-o", "gamepad", "gbp", "gear", "gears", "gift", "github", "github-alt", "github-square", "gittip", "glass", "globe", "google-plus", "google-plus-square", "group", "h-square", "hand-o-down", "hand-o-left", "hand-o-right", "hand-o-up", "hdd-o", "headphones", "heart", "heart-o", "home", "hospital-o", "html5", "inbox", "indent", "info", "info-circle", "instagram", "italic", "key", "keyboard-o", "laptop", "leaf", "legal", "lemon-o", "level-down", "level-up", "lightbulb-o", "linkedin", "linkedin-square", "linux", "list", "list-alt", "list-ol", "list-ul", "location-arrow", "lock", "long-arrow-down", "long-arrow-left", "long-arrow-right", "long-arrow-up", "magic", "magnet", "mail-forward", "mail-reply", "mail-reply-all", "male", "map-marker", "maxcdn", "medkit", "meh-o", "microphone", "microphone-slash", "minus", "minus-circle", "minus-square", "minus-square-o", "mobile-phone", "money", "moon-o", "music", "pagelines", "paperclip", "paste", "pause", "pencil", "pencil-square", "phone", "phone-square", "picture-o", "pinterest", "pinterest-square", "plane", "play", "play-circle", "play-circle-o", "plus", "plus-circle", "plus-square", "plus-square-o", "power-off", "print", "puzzle-piece", "qrcode", "question", "question-circle", "quote-left", "quote-right", "random", "refresh", "renren", "reply-all", "retweet", "road", "rocket", "rotate-left", "rotate-right", "rss", "rss-square", "ruble", "rupee", "save", "search", "search-minus", "search-plus", "share-square", "share-square-o", "shield", "shopping-cart", "sign-in", "sign-out", "signal", "sitemap", "skype", "smile-o", "sort-alpha-asc", "sort-alpha-desc", "sort-amount-asc", "sort-amount-desc", "sort-down", "sort-numeric-asc", "sort-numeric-desc", "sort-up", "spinner", "square", "square-o", "stack-exchange", "stack-overflow", "star", "star-half", "star-half-empty", "star-o", "step-backward", "step-forward", "stethoscope", "stop", "strikethrough", "subscript", "suitcase", "sun-o", "superscript", "table", "tablet", "tag", "tags", "tasks", "terminal", "text-height", "text-width", "th", "th-large", "th-list", "thumb-tack", "thumbs-down", "thumbs-o-down", "thumbs-o-up", "thumbs-up", "ticket", "times", "times-circle", "times-circle-o", "tint", "toggle-down", "toggle-left", "toggle-right", "toggle-up", "trash-o", "trello", "trophy", "truck", "tumblr", "tumblr-square", "turkish-lira", "twitter", "twitter-square", "umbrella", "underline", "unlink", "unlock", "unlock-alt", "unsorted", "upload", "user", "user-md", "video-camera", "vimeo-square", "vk", "volume-down", "volume-off", "volume-up", "warning", "weibo", "wheelchair", "windows", "won", "wrench", "xing", "xing-square", "youtube", "youtube-play", "youtube-square"];
    
// } )();