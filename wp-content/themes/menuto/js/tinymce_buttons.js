(function () {
    "use strict";

    tinymce.PluginManager.add('mce_button_taxonomies', function( editor, url ) {
        editor.addButton('mce_button_taxonomies', {
            text: '{ Produto por Categoria }',
            icon: 'icon dashicons-tag',
            tooltip: 'TAX List',
            onclick: function() {
                editor.windowManager.open( {
                    title: 'Inserir Produto por categoria',
                    width: 400,
                    height: 250,
                    body: [
                        {
                            type  : 'textbox',
                            name  : 'textboxCount',
                            label : 'Quantidade',
                            value : ''
                        },
                        {
                            type  : 'textbox',
                            name  : 'textboxColumn',
                            label : 'Coluna',
                            value : ''
                        },
                        {
                            type     : 'listbox',
                            name     : 'listboxTaxonomy',
                            label    : 'Categoria',
                            'values' : editor.settings.cptTaxonomyList
                        }
                    ],
                    onsubmit: function( e ) {
                        editor.insertContent( '[products perpage="' + e.data.textboxCount + '" col="' + e.data.textboxColumn + '" cat="' + e.data.listboxTaxonomy + '"]');
                    }
                });
            }
        });
    });

    var wcShortcodeManager = function(editor, url) {
        var wcDummyContent = 'Conteúdo aqui!';
        var wcParagraphContent = '<p>Insira o conteúdo aqui!</p>';


        editor.addButton('wc_shortcodes_button', function() {
            return {
                title: "",
                text: "Adicionar Colunas",
                //image: url + "/images/shortcodes.png",
                type: 'menubutton',
                icons: false,
                menu: [{
                    text: "1/2 + 1/2",
                    onclick: function(){
                        editor.insertContent('[wc_row][wc_column size="one-half" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-half" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                    }
                },
                {
                    text: "1/3 + 1/3 + 1/3",
                    onclick: function(){
                        editor.insertContent('[wc_row][wc_column size="one-third" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-third"]</p>' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-third" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                    }
                },
                {
                    text: "1/3 + 2/3",
                    onclick: function(){
                        editor.insertContent('[wc_row][wc_column size="one-third" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="two-third" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                    }
                },
                {
                    text: "2/3 + 1/3",
                    onclick: function(){
                        editor.insertContent('[wc_row][wc_column size="two-third" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-third" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                    }
                },
                {
                    text: "1/4 + 1/4 + 1/4 + 1/4",
                    onclick: function(){
                        editor.insertContent('[wc_row][wc_column size="one-fourth" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth"]</p>' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth"]</p>' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                    }
                },
                {
                    text: "1/4 + 1/2 + 1/4",
                    onclick: function(){
                        editor.insertContent('[wc_row][wc_column size="one-fourth" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-half"]</p>' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                    }
                },
                {
                    text: "1/2 + 1/4 + 1/4",
                    onclick: function(){
                        editor.insertContent('[wc_row][wc_column size="one-half" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth"]</p>' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                    }
                },
                {
                    text: "1/4 + 1/4 + 1/2",
                    onclick: function(){
                        editor.insertContent('[wc_row][wc_column size="one-fourth" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth"]</p>' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-half" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                    }
                },
                {
                    text: "1/4 + 3/4",
                    onclick: function(){
                        editor.insertContent('[wc_row][wc_column size="one-fourth" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="three-fourth" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                    }
                },
                {
                    text: "3/4 + 1/4",
                    onclick: function(){
                        editor.insertContent('[wc_row][wc_column size="three-fourth" position="first"]' + wcParagraphContent + '<p>[/wc_column][wc_column size="one-fourth" position="last"]</p>' + wcParagraphContent + '[/wc_column][/wc_row]');
                    }
                }]
            }
        });
    };
    
    tinymce.PluginManager.add( "wc_shortcodes", wcShortcodeManager );
})();