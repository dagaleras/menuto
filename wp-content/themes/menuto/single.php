<?php get_header(); ?>
	<?php
		global $post;			
		$siblings = get_post_siblings( 4, $post->post_date );
		$prev = $siblings['prev'];
		// $next = $siblings['next'];
	?>
	<main role="main" id="main" class="site-main page single">

		<div class="cd-articles">
			<?php while ( have_posts() ) : the_post(); ?>
				<article class="active" data-id="<?php echo $post->ID; ?>" data-title="<?php echo get_the_title(); ?>" data-permalink="<?php echo get_permalink(); ?>" data-image="<?php the_post_thumbnail_url('full'); ?>" data-summary="<?php echo wp_trim_words( do_shortcode(get_the_content()), 25, '...' ); ?>">
					<header>
						<?php if( get_field('banner__image') ) : ?>
							<figure class="figure">
								<img class="figure-img" src="<?php the_field('banner__image') ; ?>" alt="" />
							</figure>
						<?php else: ?>
							<?php if( has_post_thumbnail() ) : ?>
								<figure class="figure">
									<img class="figure-img" src="<?php the_post_thumbnail_url('full'); ?>" alt="" />
								</figure>
							<?php endif; ?>
						<?php endif; ?>
						<?php if( $category = get_the_terms( $post->ID, 'category' ) ): ?>
							<div class="category">
								<?php foreach ( $category as $cat ): ?>
									<label for="category"><?php echo $cat->name; ?></label>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</header>
					<section>
						<div class="container">
							<div class="body">
								<h1 class="body--title"><?php the_title(); ?></h1>						
								<div class="body--content"><?php the_content(); ?></div>
								<div class="tags">
									<?php if( have_rows('tags__link') ) : ?>
										<h2><?php _e( 'Para saber mais:', 'menuto' ) ?></h2>
										<div class="tags--item">
											<?php 
												while ( have_rows('tags__link') ) {
													the_row();
													$tags = get_sub_field('tags__link_item');
													if($tags){
														echo '<a href="'.$tags['url'].'" target="'.$tags['target'].'">'.$tags['title'].'</a>';
													}
												}
											?>									
										</div>
									<?php endif; ?>
								</div>
							</div>
							<aside id="sidebar_<?php echo $post->ID; ?>" class="sidebar">							
								<div id="side_<?php echo $post->ID; ?>" class="cd-read-more">
									<div class="shares">
										<?php if( get_field('show_count', 'option') ) : ?>
										<?php endif; ?>
										<div id="counter" class="shares-count">
											<i class="fa fa-share-alt"></i>
											<div class="count">987</div>
											<?php //echo facebook_share( get_permalink(get_the_ID()) ); ?>	
										</div>
										<div class="shares-icons">
											<?php $shareImage = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ), 'facebook'); ?>

											<a href="<?php echo get_permalink(); ?>" data-image="<?php echo $shareImage[0]; ?>" data-title="<?php echo get_the_title();?>" data-desc="<?php echo wp_trim_words( do_shortcode(get_the_content()), 25, '...' ); ?>" class="facebook"></a>
											<a href="<?php echo get_permalink(); ?>" data-image="<?php echo $shareImage[0]; ?>" data-title="<?php echo get_the_title();?>" data-desc="<?php echo wp_trim_words( do_shortcode(get_the_content()), 25, '...' ); ?>" class="twitter"></a>
											<a href="<?php echo get_permalink(); ?>" data-image="<?php echo $shareImage[0]; ?>" data-title="<?php echo get_the_title();?>" data-desc="<?php echo wp_trim_words( do_shortcode(get_the_content()), 25, '...' ); ?>" class="whatsapp is-mobile"></a>
											<a href="<?php echo get_permalink(); ?>" data-image="<?php echo $shareImage[0]; ?>" data-title="<?php echo get_the_title();?>" data-desc="<?php echo wp_trim_words( do_shortcode(get_the_content()), 25, '...' ); ?>" class="envelope"></a>
										</div>
									</div>
									<div class="indicator">
										<header class="aside--reading--header">
											<h3 class="header__title"><?php echo esc_html( __( 'Lista de leitura', 'menuto' ) ); ?></h3>
										</header>
										<ul>
											<li>
												<a href="<?php echo esc_url( get_permalink() ); ?>">
													<em><?php the_title(); ?></em>
													<svg x="0px" y="0px" width="40px" height="40px" viewBox="0 0 36 36"><circle fill="none" stroke-width="2" cx="18" cy="18" r="16" stroke-dasharray="100 100" stroke-dashoffset="100" transform="rotate(-90 18 18)"></circle></svg>											
												</a>
											</li>
											<?php foreach( $prev as $p ) : ?>
												<li>
													<a href="<?php echo esc_url( get_permalink($p->ID) ); ?>">
														<em><?php echo apply_filters( 'the_title', $p->post_title ); ?></em>
														<svg x="0px" y="0px" width="40px" height="40px" viewBox="0 0 36 36"><circle fill="none" stroke-width="2" cx="18" cy="18" r="16" stroke-dasharray="100 100" stroke-dashoffset="100" transform="rotate(-90 18 18)"></circle></svg>
													</a>
												</li>						    
											<?php endforeach;?>
										</ul>
									</div>
									<?php if( get_field('show_opt-in', 'option') ) : ?>
										<div class="newsletter">
											<header class="newsletter--header">
												<?php if( get_field('sidebar__newsletter_titulo', 'option') ): ?>
													<h3 class="header__title"><?php the_field('sidebar__newsletter_titulo', 'option'); ?></h3>
												<?php endif; ?>
												<?php if( get_field('sidebar__newsletter_texto', 'option') ): ?>
													<p class="header__text"><?php the_field('sidebar__newsletter_texto', 'option'); ?></p>
												<?php endif; ?>
											</header>		
											<div class="newsletter-form dark">
												<div class="form">
													<?php 
														$form = get_field('newsletter__form', 'option');
														if(get_field('newsletter__form', 'option')){
															echo do_shortcode('[contact-form-7 id="'.$form->ID.'" title="'.$form->ID.'" html_name="'.$form->post_name.'" html_id="contact-form-'.$form->ID.'"]');
														}				
													?>
												</div>
											</div>
										</div>
									<?php endif; ?>

									<button id="open-sidebar" class="button"><span><?php _e( 'recolher' ); ?></span><i class="arrow open"></i></button>
								</div> <!-- .cd-read-more -->
							</aside>
						</div>
					</section>
				</article>
			<?php endwhile; ?>
			
			<?php foreach( $prev as $p ) : ?>
				<?php 
	                $post_thumbnail_id     = get_post_thumbnail_id( $p->ID );  
	                $m_img                 = wp_get_attachment_image_src(  $post_thumbnail_id, 'full' );
	                $image_path            = esc_url($m_img[0]);
				?>
				<article data-id="<?php echo $p->ID; ?>" data-title="<?php echo get_the_title($p->ID); ?>" data-permalink="<?php echo get_permalink($p->ID); ?>" data-image="<?php echo $image_path; ?>" data-summary="<?php echo wp_trim_words( do_shortcode(get_the_content($p->ID)), 25, '...' ); ?>">
					<div class="article--next">
						<div class="article-title">
						<?php
		                    $nextTitle = sprintf(
		                        '<strong>%1$s</strong><a href="%2$s">%3$s</a>',
		                        esc_html( __( 'PROXIMO ARTIGO: ', 'menuto' ) ),
		                        esc_url( get_permalink( $p->ID ) ),
		                        esc_html( $p->post_title )
		                    );
		                    echo $nextTitle;
		                ?>
		                </div>
					</div>
					<header>
						<?php if( get_field('banner__image', $p->ID) ) : ?>
							<figure class="figure">
								<img class="figure-img" src="<?php the_field('banner__image', $p->ID) ; ?>" alt="" />
							</figure>
						<?php else: ?>
							<?php if( has_post_thumbnail() ) : ?>
								<figure class="figure">
									<img class="figure-img" src="<?php echo $image_path;; ?>" alt="" />
								</figure>
							<?php endif; ?>
						<?php endif; ?>
						<?php if( $category = get_the_terms( $p->ID, 'category' ) ): ?>
							<div class="category">
								<?php foreach ( $category as $cat ): ?>
									<label for="category"><?php echo $cat->name; ?></label>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</header>
					<section>
						<div class="container">
							<div class="body">
								<h1 class="body--title"><?php echo apply_filters( 'the_title', $p->post_title ); ?></h1>						
								<div class="body--content"><?php echo apply_filters( 'the_content', $p->post_content ); ?></div>
								<div class="tags">
									<?php if( have_rows('tags__link', $p->ID) ) : ?>
										<h2><?php _e( 'Para saber mais:', 'menuto' ) ?></h2>
										<div class="tags--item">
											<?php 
												while ( have_rows('tags__link', $p->ID) ) {
													the_row();
													$tags = get_sub_field('tags__link_item', $p->ID);
													if($tags){
														echo '<a href="'.$tags['url'].'" target="'.$tags['target'].'">'.$tags['title'].'</a>';
													}
												}
											?>									
										</div>
									<?php endif; ?>
								</div>
							</div>
							<aside id="sidebar_<?php echo $p->ID; ?>" class="sidebar"></aside>
						</div>
					</section>
				</article>
			<?php endforeach; ?>
			<!-- <article><div class="container"><aside class="sidebar"></aside></div></article> -->
		</div> <!-- .cd-articles -->

	</main>
<?php get_footer(); ?>