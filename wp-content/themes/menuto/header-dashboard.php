<!DOCTYPE html>
<!-- add a class to the html tag if the site is being viewed in IE, to allow for any big fixes -->
<!--[if lt IE 8]><html class="ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 9]><html class="ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if gt IE 9]><html <?php language_attributes(); ?>><![endif]-->
<!--[if !IE]><html <?php language_attributes(); ?>><![endif]-->
<html <?php language_attributes(); ?> class="no-js">
    <head>        
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta http-equiv="Content-Type"                    content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
        <meta http-equiv="X-UA-Compatible"                 content="IE=edge" />
        <meta name="apple-mobile-web-app-capable"          content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="viewport"                              content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="icon" href="<?php bloginfo( 'template_url' ); ?>/images/logo/favico/favicon.ico" />
        <link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/images/logo/favico/favicon.ico" />
        <!--[if IE]><link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/images/logo/favico/favicon.ico" /><![endif]-->
        
        <?php wp_head(); ?>        
    </head>
    <body <?php body_class( 'appear-animate' ) ?>>
        
        <!-- Load Facebook SDK for JavaScript -->
        <div id="fb-root"></div>

        <div id="preloader">
            <div class="loadingGIF"></div>
        </div>

        <div id="site" class="main" tabindex="-1">

            <?php 
                if( !is_user_logged_in() ) 
                    return; 
            
                $user_id  = get_current_user_id();
                $user_obj = get_user_by('id', $user_id);
            ?>

            <nav class="navbar navbar-expand-md fixed-top navbar-shadow" role="navigation">
                <div class="navbar-wrapper">
                    <div class="navbar-header expanded">
                        <div class="navbar-brand">
                            <a href="<?php echo esc_url(home_url('/')) ?>">
                                <img src="<?php bloginfo( 'template_url' ); ?>/images/logo/Menuto_logo_RGB-02.svg" alt="<?php bloginfo( 'name' ); ?>" />
                            </a>
                        </div>
                    </div>
                    <div class="navbar-container content">
                        <h1 class="navbar--title"><i class="material-icons">home</i><span><?php the_title(); ?></span></h1>
                        <div class="navbar-nav">
                            <div id="clock" class="navbar--clock"></div>
                            <div class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown">
                                    <?php echo get_avatar( $user_id, 40 ); ?>
                                    <div class="user">
                                        <h2 class="username"><?php echo $user_obj->data->display_name; ?></h2>                                    
                                        <h6 class="userrole">Super Admin</h6>
                                    </div>                                
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href=""><i class=""></i> Minha Conta</a></li>
                                    <li><a href="<?php echo wp_logout_url( home_url('/') ); ?>"><i class=""></i> Logout</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>

            <aside class="dashboard--sidebar">
                <ul class="navigation">
                    <li class="navigation--item active">
                        <a href="javascript:void(0)" id="overview" class="navigation--link" data-target="view" data-view="overview"><i class="material-icons">dashboard</i> Overview</a>
                    </li>
                    <li class="navigation--item">
                        <a href="javascript:void(0)" id="users" class="navigation--link" data-target="view" data-view="users"><i class="material-icons">account_box</i> Usuários</a>
                    </li>
                    <li class="navigation--item">
                        <a href="javascript:void(0)" id="leads" class="navigation--link" data-target="view" data-view="leads"><i class="material-icons">account_box</i> Leads</a>
                    </li>
                    <li class="navigation--item">
                        <a href="javascript:void(0)" id="clients" class="navigation--link" data-target="view" data-view="clients"><i class="material-icons">account_box</i> Clientes</a>
                    </li>
                    <li class="navigation--item">
                        <a href="javascript:void(0)" id="evangelizers" class="navigation--link" data-target="view" data-view="evangelizers"><i class="material-icons">assignment_ind</i> Evangelizadores</a>
                    </li>
                </ul>
            </aside>