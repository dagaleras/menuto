<?php

/*
 * Enqueue scripts and styles
 */
function dashboard_widget_function_enqueue() {
    // wp_deregister_style( 'dashboard' );
    // wp_register_style( 'dashboard', get_template_directory_uri().'/inc/dashboard/dashboard.css', false, '1.0' );
    // wp_enqueue_style( 'dashboard' );

    wp_deregister_script( 'dashboard' );
    wp_register_script( 'dashboard', get_template_directory_uri().'/inc/dashboard/dashboard.js', false, '1.0', true );
    wp_enqueue_script( 'dashboard' );

    wp_localize_script( 'dashboard', 'wpajax', array( 
        'ajaxurl' => admin_url( 'admin-ajax.php' )
    ));
}
add_action( 'admin_enqueue_scripts', 'dashboard_widget_function_enqueue' );
/*
 * Add a widget to the dashboard.
 */
function add_dashboard_widget_leads() {
    wp_add_dashboard_widget(
        'dashboard_widget_leads',          // Widget slug.
        'Dashboard Leads',                 // Title.
        'dashboard_widget_leads_function'  // Display function.
    );
}
add_action( 'wp_dashboard_setup', 'add_dashboard_widget_leads' );
/*
 * The function that will display this widget.
 */
function dashboard_widget_leads_function() {
    global $wpdb;
    $usernames = $wpdb->get_results("SELECT * FROM $wpdb->users ORDER BY ID DESC LIMIT 6");
  
?>
    
    <table style="width: 100%; text-align: left;">
        <th>Nome</th>
        <th>E-mail</th>
        <th>Categoria</th>
        <?php 
        foreach ($usernames as $username) {
            $userid = $username->ID ;
        ?>
            <tr>
                <td>
                    <a href="<?php echo get_edit_user_link($userid); ?>"><?php echo $username->user_login; ?></a>
                </td>
                <td>
                    <a href="<?php echo get_edit_user_link($userid); ?>"><?php echo $username->user_email; ?></a>
                </td>
                <td>
                    <?php 
                        if( $category = get_field( 'user__categoria', 'user_'.$username->ID) ){
                            echo $category[0]->name;
                        }
                    ?>
                </td>
            </tr>
        <?php 
            }
        ?>
    </table>

    <form name="form-user" id="form-user">
        <select name="list_user" id="list-user">
            <option value="">Escolha um usuário</option>
            <?php
                foreach ($usernames as $username) {
                    echo '<option value="'.$username->ID.'">'.$username->user_login.'</option>';
                }
            ?>
        </select>
    </form>

    <div id="leads-user"> 
        
    </div>
<?php 
}

/*
 * The AJAX handler function
 */
function dashboard_widget_leads_ajax_handler() {
    global $wpdb;
    $id   = $_POST['list_user'];
    $table_leads = $wpdb->prefix.'machine_learn';
    $table_users = $wpdb->prefix.'users';

    $usernames = $wpdb->get_results( "SELECT * FROM {$table_user} WHERE ID = {$id}" );   
    $result    = $wpdb->get_results( "SELECT * FROM {$table_leads} WHERE user_id = {$id}" );
    foreach ( $result as $res ){
      // d(unserialize($res->post_category));
    ?>
        <?php d($res); ?>
    <?php
    }  

    wp_die(); // just to be safe
}
add_action( 'wp_ajax_dashboard_widget_leads_approval_action', 'dashboard_widget_leads_ajax_handler' );
add_action( 'wp_ajax_nopriv_dashboard_widget_leads_approval_action', 'dashboard_widget_leads_ajax_handler' );