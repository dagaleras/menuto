<?php 

class Column_Shortcodes {

	public function __construct() {
		add_action( 'wp_loaded', array( $this, 'add_shortcodes' ) );
	}

	public function add_shortcodes() {
		add_shortcode('wc_row', array( $this,  'rows') );

		foreach ( $this->get_shortcodes() as $shortcode ) {
			add_shortcode( 'wc_column', array( $this, 'columns' ) );
		}
	}

	private function get_prefix() {
		return apply_filters( 'column_prefix', '' );
	}

	public function rows($atts, $content = null) {
		return ' <div class="row">' . do_shortcode($content) . '</div>';
	}

	public function columns( $atts, $content = null, $name = '' ) {

		$atts = shortcode_atts( array(
			"id"       => '',
			"size"     => '',
			"position" => '',
		), $atts );

		$id       = sanitize_text_field( $atts['id'] );
		$class    = sanitize_text_field( $atts['size'] );
		$position = sanitize_text_field( $atts['position'] );

		$id = ( $id <> '' ) ? " id='" . esc_attr( $id ) . "'" : '';
		$class = ( $class <> '' ) ? esc_attr( ' ' . $class ) : '';

		// position generator
		if ( $position <> '' ) {
			
			// wraps the content in an extra div with padding applied
			$pos = esc_attr( $position );
		}

		$output = "<div{$id} class='{$class}'>".do_shortcode($content)."</div>";

		return $output;
	}

		/**
	 * get shortcodes
	 *
	 * @since 0.1
	 */
	private function get_shortcodes() {
		$shortcodes = array();

		$column_shortcodes = apply_filters( 'cpsh_column_shortcodes', array(
			'full_width'   => array( 'display_name' => __( 'full width', 'column-shortcodes' ) ),
			'one_half'     => array( 'display_name' => __( 'one half', 'column-shortcodes' ) ),
			'one_third'    => array( 'display_name' => __( 'one third', 'column-shortcodes' ) ),
			'one_fourth'   => array( 'display_name' => __( 'one fourth', 'column-shortcodes' ) ),
			'two_third'    => array( 'display_name' => __( 'two third', 'column-shortcodes' ) ),
			'three_fourth' => array( 'display_name' => __( 'three fourth', 'column-shortcodes' ) ),
			'one_fifth'    => array( 'display_name' => __( 'one fifth', 'column-shortcodes' ) ),
			'two_fifth'    => array( 'display_name' => __( 'two fifth', 'column-shortcodes' ) ),
			'three_fifth'  => array( 'display_name' => __( 'three fifth', 'column-shortcodes' ) ),
			'four_fifth'   => array( 'display_name' => __( 'four fifth', 'column-shortcodes' ) ),
			'one_sixth'    => array( 'display_name' => __( 'one sixth', 'column-shortcodes' ) ),
			'five_sixth'   => array( 'display_name' => __( 'five sixth', 'column-shortcodes' ) ),
		) );

		foreach ( $column_shortcodes as $short => $options ) {

			// add prefix
			$shortcode = $this->get_prefix() . $short;

			$shortcodes[] = array(
				'name'    => $shortcode,
				'class'   => $short,
				'options' => array(
					'display_name' => $options['display_name'],
					'open_tag'     => '\n' . "[{$shortcode}]",
					'close_tag'    => "[/{$shortcode}]" . '\n',
					'key'          => '',
				),
			);

			if ( 'full_width' === $short ) {
				continue;
			}

			$shortcodes[] = array(
				'name'    => "{$shortcode}_last",
				'class'   => "{$short}_last",
				'options' => array(
					'display_name' => $options['display_name'] . ' (' . __( 'last', 'column-shortcodes' ) . ')',
					'open_tag'     => '\n' . "[{$shortcode}_last]",
					'close_tag'    => "[/{$shortcode}_last]" . '\n',
					'key'          => '',
				),
			);
		}

		return $shortcodes;
	}
}
new Column_Shortcodes();