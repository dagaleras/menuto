<?php 

function products_loop_shortcode( $atts ) {
    extract( shortcode_atts( array(
		'cat'     => '',
		'perpage' => 2,
		'col'     => 1,
		'postID'  => get_the_ID()
    ), $atts ) );

    global $post;
    $args = array(
		'post_type'      => 'product',
		'post_status'    => 'publish',
		'posts_per_page' => $perpage
    );

    if( $cat ){        
        $args['tax_query'][] = array( 
            'taxonomy' => 'product_cat',
            'field'    => 'term_id',
            'terms'    => array($cat)
        );
	}

    $the_query = new  WP_Query( $args );

    $output = '<div class="single-product row">';
    	$mobile = '<div class="single-product row"><div id="owl-products">';
	    while ( $the_query->have_posts() ) : $the_query->the_post();
	    	if( has_post_thumbnail() ){
	    		$hover = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	    		$hover = $hover[0];
	    	}

			if( get_field('product__image_hover') ) :
				$effect = get_field('product__image_hover');
			else :
				$effect = $hover;
			endif;

	    	if( $link = get_field( 'product__url' ) ){
	    		$link = $link;
	    	} else{
	    		$link = '#';
	    	}

			if( get_field('produtct__sku') ) :
				$sku = get_field('produtct__sku');
			endif;

			if( is_user_logged_in() ){
				$userID = get_current_user_id();
			}else{
				$userID = '';
			}

			$prodID = $post->ID;
			$postID = $postID;
			$userID = $userID;

	        $output .= '
	        	<div class="col col-'.$col.'">
		        	<div class="product">'.
						'<a href="'.$link.'" data-user="'.$userID.'" data-prod="'.$prodID.'"  data-post="'.$postID.'" data-sku="'.$sku.'" class="product--link" target="_blank">'.
							'<figure class="product--figure">'.
	           					'<img class="image-hover" src="'.$hover.'" alt="" />'.
	           					'<img class="image-effect" src="'.$effect.'" alt="" />'.
	           					'<button class="product--button desktop">'.__( 'Ver na loja', 'menuto' ).'</button>'.
	           					'<em class="product--sku">'.$sku.'</em>'.
	       					'</figure>'.
           					'<div class="product--title">'.get_the_title().'</div>'.
						'</a>'.
					'</div>'.
				'</div>
			';
			
			$mobile .= '
	        	<div class="product">'.
					'<a href="'.$link.'" data-user="'.$userID.'" data-prod="'.$prodID.'"  data-post="'.$postID.'" data-sku="'.$sku.'" class="product--link" target="_blank">'.
						'<figure class="product--figure">'.
           					'<img class="image-hover" src="'.$hover.'" alt="" />'.
           					'<img class="image-effect" src="'.$effect.'" alt="" />'.
           					'<em class="product--sku">'.$sku.'</em>'.
       					'</figure>'.
       					'<div class="product--title">'.get_the_title().'</div>'.
       					'<button class="product--button mobile">'.__( 'Ver na loja', 'menuto' ).'</button>'.
					'</a>'.
				'</div>
			';

	    endwhile;
	    wp_reset_query();
	    $mobile .= '</div></div>';
    $output .= '</div>';
    if( ! wp_is_mobile() ) :
		return $output;
    else :
    	return $mobile;
    endif;
}
add_shortcode('products', 'products_loop_shortcode');
