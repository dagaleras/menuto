<?php 

//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
    return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
}
add_filter('language_attributes', 'add_opengraph_doctype');
 
//Lets add Open Graph Meta Info
 
function insert_fb_in_head() {
    global $post;
    if ( !is_singular()) //if it is not a post or a page
        return;
        $og = '<meta property="fb:admins" content="1588355647896704"/>';
        $og .= '<meta property="og:title" content="' . get_the_title($post->ID) . '"/>';
        $og .= '<meta property="og:type" content="article"/>';
        $og .= '<meta property="og:url" content="' . get_permalink($post->ID) . '"/>';
        $og .= '<meta property="og:site_name" content="'.get_bloginfo('name').'"/>';
        if(!has_post_thumbnail( $post->ID )) { 
            $default_image = "http://example.com/image.jpg";
            $og .=  '<meta property="og:image" content="' . $default_image . '"/>';
        }
        else{
            $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
            $og .= '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
        }
        echo $og;
    echo "";
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );

function get_response_body( $url ) {     
    $response = wp_remote_get( $url );
    $body     = wp_remote_retrieve_body( $response );
 
    return json_decode( $body );
}

function facebook_share( $url ) {
    $api_call = 'http://graph.facebook.com/?id=' . $url;

    $body = get_response_body( $api_call );
    
    $count = $body->share->share_count;

    return $count;
}