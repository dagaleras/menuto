<?php 

function goto_login_page() {
    global $page_id;
    $login_page = get_permalink(321);
    $page       = basename($_SERVER['REQUEST_URI']);

    if( $page == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
        wp_redirect($login_page);
    exit;
    }
}
//add_action('init','goto_login_page');

function login_failed() {
    global $page_id;
    $login_page = get_permalink(321);
    wp_redirect( $login_page . '?&login=failed' );
    exit;
}
//add_action( 'wp_login_failed', 'login_failed' );

function blank_username_password( $user, $username, $password ) {
    global $page_id;
    $login_page = get_permalink(321);
    if( $username == "" || $password == "" ) {
        wp_redirect( $login_page . "?&login=blank" );
        exit;
    }
}
//add_filter( 'authenticate', 'blank_username_password', 1, 3);

function logout_page() {
    global $page_id;
    $login_page = get_permalink(321);
    wp_redirect( $login_page . "?&login=logout" );
    exit;
}
//add_action('wp_logout', 'logout_page');