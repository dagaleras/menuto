<?php 
// Habilita no painel a opção de campos para o tema
// ================================================
if ( current_user_can('administrator') ){
    if ( class_exists( 'acf' ) ) {
        
        if( function_exists('acf_add_options_page') ) {  

            $parent = acf_add_options_page(array(
                'page_title'    => __(' Opções do Site'),
                'menu_title'    => __('Opções do Site'),
                'menu_slug'     => 'acf-options',
                'redirect'      => false
            )); 

            acf_add_options_sub_page(array(
                'page_title'    => __('Header'),
                'menu_title'    => __('Header'),
                'parent_slug'   => $parent['menu_slug'],
            ));

            acf_add_options_sub_page(array(
                'page_title'    => __('Newsletter'),
                'menu_title'    => __('Newsletter'),
                'parent_slug'   => $parent['menu_slug'],
            ));

            acf_add_options_sub_page(array(
                'page_title'    => __('Instagram'),
                'menu_title'    => __('Instagram'),
                'parent_slug'   => $parent['menu_slug'],
            ));

            acf_add_options_sub_page(array(
                'page_title'    => __('Sidebar'),
                'menu_title'    => __('Sidebar'),
                'parent_slug'   => $parent['menu_slug'],
            ));

            acf_add_options_sub_page(array(
                'page_title'    => __('Perguntas do Quiz'),
                'menu_title'    => __('Perguntas do Quiz'),
                'parent_slug'   => $parent['menu_slug'],
            ));

            acf_add_options_sub_page(array(
                'page_title'    => __('APIs'),
                'menu_title'    => __('APIs'),
                'parent_slug'   => $parent['menu_slug'],
            ));
        }

        add_filter( 'acf/settings/remove_wp_meta_box', '__return_false' );

    }
}

