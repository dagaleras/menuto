<?php 

/**
 * Callback ajax login
 *
 */
function handler_login(){
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-login-nonce', 'security' );

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login']    = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember']      = true;

    $user_signon = wp_signon( $info, false );

    if ( is_wp_error($user_signon) ){
        echo json_encode(
            array(
                'loggedin' => false, 
                'message'  => __( 'Nome de usuário ou senha incorretos.' )
            )
        );
    } else {
        //setting the user variables so the globals have content without an actual page load
        wp_set_current_user( $user_signon->ID );
        wp_set_auth_cookie( $user_signon->ID, true, false );
        do_action( 'wp_login', $user_signon->user_login );

        echo json_encode(
            array(
                'loggedin' => true, 
                'message'  => __( 'Login feito com sucesso' )
            )
        );             
    }

    die();

}
add_action( 'wp_ajax_ajaxlogin', 'handler_login' );
add_action( 'wp_ajax_nopriv_ajaxlogin', 'handler_login' );

/**
 * Callback ajax lost password
 *
 */
function handler_lostpassword(){
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-forgot-nonce', 'security' );

    global $wpdb;
    
    $account = $_POST['user_login'];
    
    if( empty( $account ) ) {
        $error = __('Digite um nome de usuário ou endereço de e-mail.','menuto');
    } else {
        if(is_email( $account )) {
            if( email_exists($account) ) 
                $get_by = 'email';
            else    
                $error = __('Não há usuário registrado com esse endereço de e-mail.','menuto');            
        }
        else if (validate_username( $account )) {
            if( username_exists($account) ) 
                $get_by = 'login';
            else    
                $error = __('Não há usuário registrado com esse nome de usuário.','menuto');             
        }
        else
            $error = __('Nome de usuário ou endereço de e-mail inválido.','menuto');     
    }

    if(empty ($error)) {
        // lets generate our new password
        //$random_password = wp_generate_password( 12, false );
        $random_password = wp_generate_password(); 
            
        // Get user data by field and data, fields are id, slug, email and login
        $user = get_user_by( $get_by, $account );
            
        $update_user = wp_update_user( array ( 'ID' => $user->ID, 'user_pass' => $random_password ) );
            
        // if  update user return true then lets send user an email containing the new password
        if( $update_user ) {
            
            $from = ''; // Set whatever you want like mail@yourdomain.com
            
            if(!(isset($from) && is_email($from))) {        
                $sitename = strtolower( $_SERVER['SERVER_NAME'] );
                if ( substr( $sitename, 0, 4 ) == 'www.' ) {
                    $sitename = substr( $sitename, 4 );                 
                }
                $from = 'admin@'.$sitename; 
            }
            
            $to = $user->user_email;
            $subject = __('Sua nova senha','menuto');
            $sender  = 'From: '.get_option('name').' <'.$from.'>' . "\r\n";
            
            $message = __('Sua nova senha: '.$random_password,'menuto');
                
            $headers[] = 'MIME-Version: 1.0' . "\r\n";
            $headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers[] = "X-Mailer: PHP \r\n";
            $headers[] = $sender;
                
            $mail = wp_mail( $to, $subject, $message, $headers );
            if( $mail ) {
                $success = __('Verifique seu endereço de e-mail para sua nova senha.','menuto');
            }
            else{
                $error = __('O sistema não pode enviar seu e-mail contendo sua nova senha.','menuto');  
                //$error = $random_password;                       
            }
        } else {
            $error = __('Oops! Algo deu errado ao atualizar sua conta.','menuto');
        }
    }
    
    if( ! empty( $error ) ){
        echo json_encode(
            array(
                'loggedin' => false, 
                'message'  => __($error)
            )
        );
    }
            
    if( ! empty( $success ) ){
        echo json_encode(
            array(
                'loggedin' => true, 
                'message'  => __($success)
            )
        );
    }
                
    die();
}
add_action( 'wp_ajax_lostpassword', 'handler_lostpassword' );
add_action( 'wp_ajax_nopriv_lostpassword', 'handler_lostpassword' );

/**
 * Callback ajax registro de usuário
 *
 */
function checkdatauser(){
    global $wpdb;
    $user_exists = false;

    $name     = $_POST['name']; 
    $email    = $_POST['email'];
    $c_email    = $_POST['c_email'];
    $password = $_POST['password'];

    if ( $c_email != $email ) {
        $user_exists = true;
        $msg = 'E-mail não confere.';    
    }

    if ( username_exists($name) ) {
        $user_exists = true;
        $msg = 'Este nome de usuário já está registrado.';
    
    }

    if ( !is_email( $email ) ) {
        $user_exists = true;
        $msg = 'Endereço de e-mail inválido.';    
    }

    if ( !is_email( $c_email ) ) {
        $user_exists = true;
        $msg = 'Endereço de e-mail inválido.';    
    }
    
    if ( email_exists( $email ) ) {
        $user_exists = true;
        $msg = 'Este endereço de e-mail já está registrado.';    
    }

    if ( email_exists( $c_email ) ) {
        $user_exists = true;
        $msg = 'Este endereço de e-mail já está registrado.';    
    }

    if( $user_exists ){
        echo json_encode(
            array(
                'userexists' => true,
                'message'    =>  __( $msg )
            )
        );
    } else{
        echo json_encode(
            array(
                'userexists' => false,
                'message'    =>  __( 'Vamos nessa!' )
            )
        );
    }
                
    die();
}
add_action( 'wp_ajax_checkdatauser', 'checkdatauser' );
add_action( 'wp_ajax_nopriv_checkdatauser', 'checkdatauser' );


function handle_registration(){
 
    global $wpdb;
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-register-nonce', 'security' );
    

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_nicename'] = $info['nickname'] = $info['display_name'] = $info['first_name'] = $info['user_login'] = sanitize_user($_POST['username']) ;
    $info['user_pass']     = sanitize_text_field($_POST['password']);
    $info['user_email']    = sanitize_email( $_POST['email']);
    
    $user_gender   = stripcslashes( $_POST['gender'] );
    
    // Register the user
    $user_register = wp_insert_user( $info );

    if ( is_wp_error($user_register) ){ 

        $error  = $user_register->get_error_codes() ;
        
        if(in_array('empty_user_login', $error)){
            echo json_encode(
                array(
                    'loggedin' => false,
                    'message'  =>  __( $user_register->get_error_message('empty_user_login') )
                )
            );
        }
        elseif(in_array('existing_user_login', $error)){
            echo json_encode(
                array(
                    'loggedin' => false,
                    'message'  =>  __( 'Este nome de usuário já está registrado.' )
                )
            );
        }
        elseif(in_array('existing_user_email', $error)){
            echo json_encode(
                array(
                    'loggedin' => false,
                    'message'  =>  __( 'Este endereço de e-mail já está registrado.' )
                )
            );
        }
    } else {
        update_field( 'user_gender', $user_gender, 'user_'.$user_register);

        // Nonce is checked, get the POST data and sign user on
        auth_user_login($info['nickname'], $info['user_pass'], 'Cadastro');       
    }
                
    die();
}
add_action( 'wp_ajax_ajaxregister', 'handle_registration' );
add_action( 'wp_ajax_nopriv_ajaxregister', 'handle_registration' );


function auth_user_login($user_login, $password, $login){
    $info = array();
    $info['user_login']    = $user_login;
    $info['user_password'] = $password;
    $info['remember']      = true;
    
    $user_signon = wp_signon( $info, false );

    if ( is_wp_error($user_signon) ){
        echo json_encode(
            array(
                'loggedin' => false, 
                'message'  => __('Verifique as informações fornecidas')
            )
        );
    } else {
        //setting the user variables so the globals have content without an actual page load
        wp_set_current_user( $user_signon->ID );
        wp_set_auth_cookie( $user_signon->ID, true, false );
        do_action( 'wp_login', $user_signon->user_login );

        $url = home_url('/?modal=quiz');
        echo json_encode(
            array(
                'loggedin'    => true, 
                'user_id'     => $user_signon->ID,
                'message'     => __($login.' bem sucedido.'),
                'redirectmsg' => __($login.' feito com sucesso.'),
                'redirecturl' => $url
            )
        );
    }
    
    die();
}

/**
 * Check data users facebook
 *
 */
function handler_social_connect(){
    global $wp_query;
    global $wpdb;

    $social_id          = $_POST['id']; 
    $social_first_name  = $_POST['first_name']; 
    $social_last_name   = $_POST['last_name'];
    $social_email       = $_POST['email'];
    $social_gender      = $_POST['gender'];
    $social_locale      = $_POST['locale'];
    $social_avatar      = $_POST['avatar'];
    $social_link        = $_POST['link'];    

    $user_exists = false;

    if(isset($social_email)){
        $user_name  = $social_email;
        $user_email = $social_email;
        $user_id    = username_exists( $user_name );

        if (username_exists($social_first_name)) {
            $user_exists = true;
            $user_data   = get_userdatabylogin($social_first_name);
        } 
        elseif (email_exists($social_email)) {
            $user_exists = true;
            $user        = get_user_by_email($social_email);
        } else {
            $user_exists = false;
        }


        if ($user_exists){
            $user_login = $user->user_login;
            $user_email = $user->user_email;

            $user = get_user_by('login', $user_email );
            $user_id = $user->ID;

            wp_set_current_user( $user_id );
            wp_set_auth_cookie( $user_id, true );

            if ( is_wp_error($user_id) ){
                echo json_encode(
                    array(
                        'loggedin' => false, 
                        'message'  => __( 'Nome de usuário ou senha incorretos.' )
                    )
                );
            } else {
                echo json_encode(
                    array(
                        'loggedin' => true, 
                        'message'  => __( 'Login feito com sucesso!' )
                    )
                );             
            }
        } else{
            $random_password = wp_generate_password( 10, true, true );
            $user_id         = wp_create_user( $user_name, $random_password, $user_email ); 

            update_user_meta($user_id, 'avtar_image', 'https://graph.facebook.com/' . $social_id . '/picture?type=large');

            if($social_gender == 'male'){
                $social_gender = 'Masculino';
            }
            if($social_gender == 'female'){
                $social_gender = 'Feminino';
            }

            update_field( 'user_gender', $social_gender, 'user_'.$user_id);

            wp_update_user( array(
                'ID'           => $user_id,
                'display_name' => $social_first_name,
                'first_name'   => $social_first_name,
                'last_name'    => $social_last_name
            ));

            wp_set_current_user( $user_id );
            wp_set_auth_cookie( $user_id, true );

            if ( is_wp_error($user_id) ){
                echo json_encode(
                    array(
                        'loggedin' => false, 
                        'message'  => __( 'O cadastro não pode ser realizado.' )
                    )
                );
            } else {
                echo json_encode(
                    array(
                        'loggedin' => true, 
                        'message'  => __( 'Cadastro feito com sucesso!' )
                    )
                );             
            }
        }
    }

}
add_action('wp_ajax_social_connect', 'handler_social_connect');
add_action('wp_ajax_nopriv_social_connect', 'handler_social_connect');

/**
 * Callback ajax questionário
 *
 */
function handler_quiz(){
    global $wpdb;

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-quiz-nonce', 'security' );

    if ( empty($_POST['user_id']) ){
        $error = __('Oops! Algo deu errado, não encontramos o seu usuário.', 'menuto');
    } else{

        $user_id = $_POST['user_id'];

        wp_set_current_user( $user_id );
        wp_set_auth_cookie( $user_id );

        if ( !is_wp_error($user_id) ){

            if( $_POST['user__question_1'] ){
                $question_1 = update_field( 'user__question_1', $_POST['user__question_1'], 'user_'.$user_id );
                if( $_POST['user__question_1'] == 3 ){
                    if( $hitech ){
                        $hitech += 1;
                    }else{
                        $hitech = 1;
                    }
                }
                if( $_POST['user__question_1'] == 1 ){
                    if( $pracombinar ){
                        $pracombinar += 1;
                    }else{
                        $pracombinar = 1;
                    }
                }
                if( $_POST['user__question_1'] == 2 ){
                    if( $seutempo ){
                        $seutempo += 1;
                    }else{
                        $seutempo = 1;
                    }
                }
            }
            if( $_POST['user__question_2'] ){
                $question_2 = update_field( 'user__question_2', $_POST['user__question_2'], 'user_'.$user_id);
                if( $_POST['user__question_2'] == 3 ){
                    if( $hitech ){
                        $hitech += 1;
                    }else{
                        $hitech = 1;
                    }
                }
                if( $_POST['user__question_2'] == 2 ){
                    if( $pracombinar ){
                        $pracombinar += 1;
                    }else{
                        $pracombinar = 1;
                    }
                }
                if( $_POST['user__question_2'] == 1 ){
                    if( $seutempo ){
                        $seutempo += 1;
                    }else{
                        $seutempo = 1;
                    }
                }
            }
            if( $_POST['user__question_3'] ){
                $question_3 = update_field( 'user__question_3', $_POST['user__question_3'], 'user_'.$user_id);
                if( $_POST['user__question_3'] == 3 ){
                    if( $hitech ){
                        $hitech += 1;
                    }else{
                        $hitech = 1;
                    }
                }
                if( $_POST['user__question_3'] == 2 ){
                    if( $pracombinar ){
                        $pracombinar += 1;
                    }else{
                        $pracombinar = 1;
                    }
                }
                if( $_POST['user__question_3'] == 1 ){
                    if( $seutempo ){
                        $seutempo += 1;
                    }else{
                        $seutempo = 1;
                    }
                }
            }
            if( $_POST['user__question_4'] ){
                $question_4 = update_field( 'user__question_4', $_POST['user__question_4'], 'user_'.$user_id);
                if( $_POST['user__question_4'] == 1 ){
                    if( $hitech ){
                        $hitech += 1;
                    }else{
                        $hitech = 1;
                    }
                }
                if( $_POST['user__question_3'] == 2 ){
                    if( $pracombinar ){
                        $pracombinar += 1;
                    }else{
                        $pracombinar = 1;
                    }
                }
                if( $_POST['user__question_3'] == 3 ){
                    if( $seutempo ){
                        $seutempo += 1;
                    }else{
                        $seutempo = 1;
                    }
                }
            }
            if( $_POST['user__question_5'] ){
                $question_5 = update_field( 'user__question_5', $_POST['user__question_5'], 'user_'.$user_id);
                if( $_POST['user__question_5'] == 2 ){
                    if( $hitech ){
                        $hitech += 1;
                    }else{
                        $hitech = 1;
                    }
                }
                if( $_POST['user__question_5'] == 3 ){
                    if( $pracombinar ){
                        $pracombinar += 1;
                    }else{
                        $pracombinar = 1;
                    }
                }
                if( $_POST['user__question_5'] == 1 ){
                    if( $seutempo ){
                        $seutempo += 1;
                    }else{
                        $seutempo = 1;
                    }
                }
            }
            if( $_POST['user__question_6'] ){
                $question_6 = update_field( 'user__question_6', $_POST['user__question_6'], 'user_'.$user_id);
                if( $_POST['user__question_6'] == 1 ){
                    if( $hitech ){
                        $hitech += 1;
                    }else{
                        $hitech = 1;
                    }
                }
                if( $_POST['user__question_6'] == 3 ){
                    if( $pracombinar ){
                        $pracombinar += 1;
                    }else{
                        $pracombinar = 1;
                    }
                }
                if( $_POST['user__question_6'] == 2 ){
                    if( $seutempo ){
                        $seutempo += 1;
                    }else{
                        $seutempo = 1;
                    }
                }
            }
            if( $_POST['user__question_7'] ){
                $question_7 = update_field( 'user__question_7', $_POST['user__question_7'], 'user_'.$user_id);
                if( $_POST['user__question_7'] == 1 ){
                    if( $hitech ){
                        $hitech += 1;
                    }else{
                        $hitech = 1;
                    }
                }
                if( $_POST['user__question_7'] == 3 ){
                    if( $pracombinar ){
                        $pracombinar += 1;
                    }else{
                        $pracombinar = 1;
                    }
                }
                if( $_POST['user__question_7'] == 2 ){
                    if( $seutempo ){
                        $seutempo += 1;
                    }else{
                        $seutempo = 1;
                    }
                }
            }

            $total = array();
            $total['hitech']      = $hitech;
            $total['pracombinar'] = $pracombinar;
            $total['seutempo']    = $seutempo;

            $cat_hitech_id      = 3;
            $cat_pracombinar_id = 5;
            $cat_seutempo_id    = 4;

            if( $total['hitech'] > $total['pracombinar'] && $total['hitech'] > $total['seutempo'] ){
               $user__categoria = $cat_hitech_id;
            }
            elseif ( $total['pracombinar'] > $total['hitech'] && $total['pracombinar'] > $total['seutempo'] ) {
                $user__categoria = $cat_pracombinar_id;
            }
            elseif ( $total['seutempo'] > $total['hitech'] && $total['seutempo'] > $total['pracombinar'] ) {
                $user__categoria = $cat_seutempo_id;
            }
            else {
                $user__categoria = $cat_hitech_id;
            }

            update_field( 'user_categoria', $user__categoria, 'user_'.$user_id);

            $table_name = $wpdb->prefix.'dash_most_popular_post_user';
            $field_name = 'post_read';
            $post_read  = 1;
            $post_share = 0;

            $key       = 'post_views_user_'.$user_id;

            $args = array( 
                'post_type'        => 'post',
                'post_status'      => 'publish',
                'orderby'          => 'date',
                'order'            => 'DESC'
            );

            $args['numberposts'] = 6;
            $recent_posts = wp_get_recent_posts($args, OBJECT);
            foreach( $recent_posts as $recent ){

                $cat_ids    = array();
                $categories = get_the_category( $recent->ID );
                if ( $categories && !is_wp_error( $categories ) ) {
                    foreach ( $categories as $category ) {     
                        array_push( $cat_ids, $category->term_id );     
                    }
                }
                $post_category = serialize($cat_ids);

                $wpdb->insert( $table_name, array(
                    'user_id'       => $user_id, 
                    'post_id'       => $recent->ID, 
                    'post_read'     => $post_read, 
                    'post_share'    => $post_share,
                    'post_category' => $post_category
                ));
                update_post_meta( $recent->ID, $key, $post_read );
            }

            if($user__categoria){
                $args['category']    = $user__categoria;
                $args['numberposts'] = 1;
                $recent_posts = wp_get_recent_posts($args, OBJECT);
                foreach( $recent_posts as $recent ){
                    $prepared  = $wpdb->prepare( "SELECT {$field_name} FROM {$table_name} WHERE post_id = %d AND user_id = %d", $recent->ID, $user_id );
                    $post_read = $wpdb->get_col( $prepared );
                    $count_read = $post_read[0]; 
                    $count_read += 1;

                    $wpdb->update( $table_name,  
                        array( 
                            'post_read'  => $count_read
                        ), 
                        array( 
                            'post_id' => $recent->ID,
                            'user_id' => $user_id
                        )                        
                    );
                    update_post_meta( $recent->ID, $key, $count_read );
                }
            }
        
            $success = __( 'Obrigado por responder o nosso questionario' ); 
        } else{
            $error = __('Não encontramos o registro do usuário.', 'menuto');
        }

    }

    if( ! empty( $error ) ){
        echo json_encode(
            array(
                'loggedin' => false, 
                'message'  => __($error)
            )
        );
    }
            
    if( ! empty( $success ) ){
        echo json_encode(
            array(
                'loggedin'        => true,
                'infos'           => $total,
                'user__categoria' => $user__categoria,
                'message'         => __($success)
            )
        );
    }

    // return proper result
    die();
}
add_action( 'wp_ajax_ajaxquiz', 'handler_quiz' );
add_action( 'wp_ajax_nopriv_ajaxquiz', 'handler_quiz' );

/**
 * Callback ajax próximo post
 *
 */
function handler_post_next(){
    global $wp_query;

    $_id = $_POST['postid'];

    $args = array(
        'p'              => $_id,
        'post_type'      => 'post',
        'posts_per_page' => 1
    );

    query_posts( $args );

    if( have_posts() ) :
        while( have_posts() ): the_post();
            get_template_part( 'content', 'article' );
            get_template_part( 'content', 'next' );
        endwhile;
    endif;
    
    //wp_die();
    die();
}
add_action('wp_ajax_nextpost', 'handler_post_next');
add_action('wp_ajax_nopriv_nextpost', 'handler_post_next');

/**
 * Callback ajax pontuação por tempo na página
 *
 */
function handler_most_time_in_post(){
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-time-nonce', 'security' );

    if ( is_single() ) {
    
        // Precisamos da variável $post global para obter o ID do post
        global $post, $wpdb;

        if( is_user_logged_in() ){
            $table_name = $wpdb->prefix.'dash_most_popular_post_user'; 
            $field_name = 'post_read';

            $user_id    = get_current_user_id();
            $post_id    = $_POST['post_id'];
            $post_read  = 0;
            $post_share = 0;
            $cat_ids    = array();

            // Cria ou obtém o valor da chave para contarmos
            $key       = 'post_views_user_'.$user_id;
            $key_value = get_post_meta( $post->ID, $key, true );

            $categories = get_the_category( $post_id );
            if ( $categories && !is_wp_error( $categories ) ) {
                foreach ( $categories as $category ) {     
                    array_push( $cat_ids, $category->term_id );     
                }
            }
            $post_category = serialize($cat_ids);
             
            $prepared  = $wpdb->prepare( "SELECT {$field_name} FROM {$table_name} WHERE post_id = %d AND user_id = %d", $post_id, $user_id );
            $post_read = $wpdb->get_col( $prepared );

            if( $post_read ){
                $count_read = $post_read[0];            
            
                // Caso contrário, o valor atual + 1
                $count_read += 1;
                $key_value  += 1;
                
                update_post_meta( $post->ID, $key, $key_value );

                $wpdb->update( $table_name,  
                    array( 
                        'post_read'  => $count_read
                    ), 
                    array( 
                        'post_id' => $post_id 
                    )                        
                );
            } else{
                $count_read = 1;
                $key_value  = 1;
                
                update_post_meta( $post->ID, $key, $key_value );

                $wpdb->insert( $table_name, array(
                    'user_id'       => $user_id, 
                    'post_id'       => $post_id, 
                    'post_read'     => $count_read, 
                    'post_share'    => $post_share,
                    'post_category' => $post_category
                ));
            }


        } else{
        
            // Cria ou obtém o valor da chave para contarmos
            $key       = 'post_views_general';
            $key_value = get_post_meta( $post->ID, $key, true );
            
            // Se a chave estiver vazia, valor será 1
            if ( empty( $key_value ) ) { // Verifica o valor
                $key_value = 1;
                update_post_meta( $post->ID, $key, $key_value );
            } else {
                // Caso contrário, o valor atual + 1
                $key_value += 1;
                update_post_meta( $post->ID, $key, $key_value );
            } // Verifica o valor

        }
        
    } // is_single

    // return proper result
    die();
}
add_action( 'wp_ajax_most_time', 'handler_most_time_in_post' );
add_action( 'wp_ajax_nopriv_most_time', 'handler_most_time_in_post' );




/*
 * The AJAX handler function
 */

function wp_deleteMyAccount(){
    global $wpdb;

    require_once(ABSPATH.'wp-admin/includes/user.php' );

    $user_id = $_POST['id'];
    
    if ( is_wp_error($user_id) ){
        echo json_encode(
            array(
                'status'  => false,
                'message' => __( 'Oops! Não foi possivel excluir seu perfil.', 'menuto' )
            )
        );
    } else{
        wp_delete_user( $user_id );

        echo json_encode(
            array(
                'status'  => true,
                'message' => __( 'Até já! Volta logo!', 'menuto' ),
                'urlhome' => home_url('/')
            )
        );
    }

    wp_die();
}
add_action( 'wp_ajax_user_deletemyaccount', 'wp_deleteMyAccount' );
add_action( 'wp_ajax_nopriv_user_deletemyaccount', 'wp_deleteMyAccount' );

/*
 * The AJAX handler function
 */

function checkEmailUser(){
    global $wpdb;

    $status  = false;

    $user_id = $_POST['edit_user_id'];
    $email   = $_POST['youremail'];

    if ( !is_email( $email ) ) {
        $status = true;
        $msg = 'Endereço de e-mail inválido.';    
    }
    
    if ( email_exists( $email ) ) {
        $status = true;
        $msg = 'Este endereço de e-mail já está registrado.';    
    }

    if( $status ){
        echo json_encode(
            array(
                'status'   => false,
                'message' =>  __( $msg, 'menuto' )
            )
        );
    } else{
        echo json_encode(
            array(
                'status'  => true
            )
        );
    }

    wp_die();
}
add_action( 'wp_ajax_checkemail', 'checkEmailUser' );
add_action( 'wp_ajax_nopriv_checkemail', 'checkEmailUser' );

function wp_editEmailUser(){
    global $wpdb;

    $user_id = $_POST['edit_user_id'];

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-editemail-nonce', 'security' );

     if ( is_wp_error($user_id) ){   
        echo json_encode(
            array(
                'status'  => false,
                'message' =>  __( 'Não foi possível editar seu email.', 'menuto' )
            )
        ); 
    } else{          
        $args = array(
            'ID'         => $user_id,
            'user_email' => esc_attr( $_POST['youremail'] )
        );
        wp_update_user( $args );

        echo json_encode(
            array(
                'status'   => true,
                'message'  =>  __( 'E-mail alterado com sucesso.', 'menuto' ),
                'redirect' => get_permalink(1196)
            )
        );
    }

    wp_die();
}
add_action( 'wp_ajax_editEmailUser', 'wp_editEmailUser' );
add_action( 'wp_ajax_nopriv_editEmailUser', 'wp_editEmailUser' );



function myaccount_check_password(){
    global $wpdb;

    $userid          = $_POST['userid'];
    $currentpassword = $_POST['currentpassword'];
    $newpassword     = $_POST['newpassword'];

    $user = get_user_by('id', $userid);

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-changepassword-nonce', 'security' );

    $passdata = $_POST;
    unset($_POST);

    $check = wp_check_password( $passdata['currentpassword'], $user->data->user_pass, $user->ID );

    if($check) {
        if( !empty($passdata['currentpassword']) && !empty($passdata['newpassword'])) {
            $passupdatetype = 'successed';
        } else {
            $passupdatemsg = "Por favor preencha os campos.";
            $passupdatetype = 'errored';
        }
    } else {
        $passupdatemsg = "Senha antiga não corresponde à senha existente";
        $passupdatetype = 'errored';
    }

    if($passupdatetype == 'successed'){
        echo json_encode(
            array(
                'status'  => true,
                'message' =>  __( $passupdatemsg, 'menuto' )
            )
        );
    }else{
        echo json_encode(
            array(
                'status'  => false,
                'message' =>  __( $passupdatemsg, 'menuto' )
            )
        );        
    }

    wp_die();
}
add_action( 'wp_ajax_check_password', 'myaccount_check_password' );
add_action( 'wp_ajax_nopriv_check_password', 'myaccount_check_password' );

function myaccount_change_password(){
    global $wpdb;

    $userid          = $_POST['userid'];
    $currentpassword = $_POST['currentpassword'];
    $newpassword     = $_POST['newpassword'];
    $c_newpassword   = $_POST['c_newpassword'];

    $user = get_user_by('id', $userid);

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-changepassword-nonce', 'security' );

    $passdata = $_POST;
    unset($_POST);
    $udata = array();

    if( !empty($passdata['newpassword']) && !empty($passdata['c_newpassword'])) {
        if($passdata['newpassword'] == $passdata['c_newpassword']) {
            $udata['ID']        = $user->data->ID;
            $udata['user_pass'] = $passdata['newpassword'];
            $uid = wp_update_user( $udata );
            if($uid) {
                $passupdatemsg = "A senha foi atualizada com sucesso";
                $passupdatetype = 'successed';
                unset($passdata);
            } else {
                $passupdatemsg = "Desculpa! Falha ao atualizar os detalhes da sua conta.";
                $passupdatetype = 'errored';
            }
        }
        else {
            $passupdatemsg = "Confirmação de senha não corresponde à nova senha";
            $passupdatetype = 'errored';
        }
    }
    else {
        $passupdatemsg = "Preencha o campo senha.";
        $passupdatetype = 'errored';
    }

    if($passupdatetype == 'successed'){
        echo json_encode(
            array(
                'status'  => true,
                'message' =>  __( $passupdatemsg, 'menuto' )
            )
        );
    }else{
        echo json_encode(
            array(
                'status'  => false,
                'message' =>  __( $passupdatemsg, 'menuto' )
            )
        );        
    }

    wp_die();
}
add_action( 'wp_ajax_change_password', 'myaccount_change_password' );
add_action( 'wp_ajax_nopriv_change_password', 'myaccount_change_password' );