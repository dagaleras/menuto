<?php 
// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function checkIfValuesExistsDB($loggedin = false, $tbname, $colname){
	global $wpdb;

	$table_name = $wpdb->prefix.$tbname; 
    $field_name = $colname;

    if($loggedin){
    	$user_id   = get_current_user_id();
		$prepared  = $wpdb->prepare( "SELECT {$field_name} FROM {$table_name} WHERE post_id = %d AND user_id = %d", $post_id, $user_id );    	
    } else{
    	$prepared  = $wpdb->prepare( "SELECT {$field_name} FROM {$table_name} WHERE post_id = %d", $post_id );
    }

    return $wpdb->get_col( $prepared );
}

// Consulta se existe
function setPostViewsTotal($post_id){
	global $wpdb;

	$tbname  = $wpdb->prefix.'dash_total_interaction';
    $colname = 'post_total';
    $query   = $wpdb->prepare( "SELECT {$colname} FROM {$tbname} WHERE post_id = %d", $post_id );
    $return  = $wpdb->get_col( $query );

    if($return){
    	$count = $return[0];
        $count += 1;
        $wpdb->update( $tbname,  
            array( 
                'post_total' => $count
            ), 
            array( 
                'post_id' => $post_id
            )                        
        );    	
    } else{
        $count = 1;
        $wpdb->insert( $tbname, array( 
			'post_id'    => $post_id,
			'post_total' => $count
        ));    	
    }
}

// Consulta se existe
function setCategoryViewsTotal( $post_id ){
    global $wpdb;

    $category_id    = array();
    $categories = get_the_category( $post_id );
    if ( $categories && !is_wp_error( $categories ) ) {
        foreach ( $categories as $category ) {     
            array_push( $category_id, $category->term_id );
        }
    }

    $tbname  = $wpdb->prefix.'dash_total_interaction';
    $colname = 'category_total';

    foreach( $category_id as $key => $id ) {
        $query   = $wpdb->prepare( "SELECT {$colname} FROM {$tbname} WHERE category_id = %d", $id );
        $return  = $wpdb->get_col( $query );

        if($return){
            $count = $return[0];
            $count += 1;
            $wpdb->update( $tbname,  
                array( 
                    'category_total' => $count
                ), 
                array( 
                    'category_id' => $id
                )                        
            );      
        } else{
            $count = 1;
            $wpdb->insert( $tbname, array(
                'category_id'    => $id,
                'category_total' => $count
            ));     
        }
    }    
}

// Conta os views do post
function setPostViewsNotUserLoggedIn($post_id){   
    // Cria ou obtém o valor da chave para contarmos
    global $wpdb;

    $table_name = $wpdb->prefix.'dash_most_popular_post_user';
    $field_name = 'post_count';    

    $meta_key   = 'post_views_general';
    $meta_value = get_post_meta( $post_id, $meta_key, true );

    $prepared   = $wpdb->prepare( "SELECT {$field_name} FROM {$table_name} WHERE post_id = %d", $post_id );
    $post_exist = $wpdb->get_col( $prepared );

    $cat_ids    = array();
    $categories = get_the_category( $post_id );
    if ( $categories && !is_wp_error( $categories ) ) {
        foreach ( $categories as $category ) {     
            array_push( $cat_ids, $category->term_id );
        }
    }
    $post_category = serialize($cat_ids);

    if( $post_exist ){
        $count_read = $post_exist[0];
        $count_read += 1;            
        $wpdb->update( $table_name,  
            array( 
                'post_count'     => $count_read
            ), 
            array( 
                'post_id'       => $post_id
            )                        
        );
    } else{
        $count_read = 1;
        $wpdb->insert( $table_name, array( 
            'post_id'       => $post_id, 
            'post_count'     => $count_read,
            'post_category' => $post_category
        ));
    }
    
    // Se a chave estiver vazia, valor será 1
    if ( empty( $meta_key ) ) { // Verifica o valor
        $meta_key = 1;
        update_post_meta( $post_id, $meta_key, $meta_value );
    } else {
        // Caso contrário, o valor atual + 1
        $meta_key += 1;
        update_post_meta( $post_id, $meta_key, $meta_value );
    }
}

// Conta os views do post
function setPostViewsIsUserLoggedIn($post_id){
    // Precisamos da variável $post global para obter o ID do post
    global $wpdb;

    $user_id    = get_current_user_id();

    $table_name = $wpdb->prefix.'dash_most_popular_post_user';
    $field_name = 'post_count';

    $post_read  = 0;
    $post_share = 0;
    $cat_ids    = array();

    // Cria ou obtém o valor da chave para contarmos
    $meta_key  = 'post_views_user_'.$user_id;
    $meta_value = get_post_meta( $post_id, $meta_key, true );

    $categories = get_the_category( $post_id );

    if ( $categories && !is_wp_error( $categories ) ) {
        foreach ( $categories as $category ) {     
            array_push( $cat_ids, $category->term_id );     
        }
    }
    $post_category = serialize($cat_ids);
     
    $prepared  = $wpdb->prepare( "SELECT {$field_name} FROM {$table_name} WHERE post_id = %d AND user_id = %d", $post_id, $user_id );
    $post_read = $wpdb->get_col( $prepared );

    if( $post_read ){
        $count_read = $post_read[0];    
        $count_read += 1;
        $wpdb->update( $table_name,  
            array( 
                'post_count' => $count_read
            ), 
            array( 
                'post_id' => $post_id
            )                        
        );

    } else{
        $count_read = 1;
        $wpdb->insert( $table_name, array(
            'user_id'       => $user_id, 
            'post_id'       => $post_id, 
            'post_count'     => $count_read, 
            'share_count'   => $post_share,
            'post_category' => $post_category
        ));
    }

    // Se a chave estiver vazia, valor será 1
    if ( empty( $meta_value ) ) { // Verifica o valor
        $meta_value = 1;
        update_post_meta( $post_id, $meta_key, $meta_value );
    } else {
        // Caso contrário, o valor atual + 1
        $meta_value += 1;
        update_post_meta( $post_id, $meta_key, $meta_value );
    }

}


function getPostViews(){
    global $post;

    if ( is_category() ) {
        setCategoryViewsTotal( $post->ID );
    }

    if ( is_single() ) {
    	setPostViewsTotal( $post->ID );

        if( is_user_logged_in() ){
            setPostViewsIsUserLoggedIn( $post->ID );
        } else{
            setPostViewsNotUserLoggedIn( $post->ID );
        }
    }
    return;
}
add_action( 'get_header',  'getPostViews' );

?>