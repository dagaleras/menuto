<?php
function tutsup_session_start() {
    // Inicia uma sessão PHP
    if ( ! session_id() ) session_start();
}
// Executa a ação
add_action( 'init', 'tutsup_session_start' );



function getArticlesMoreLeadsByUser($offset = 0, $logged = false){
    global $wpdb;
    $table_name = $wpdb->prefix.'dash_most_popular_post_user';
    
    if($logged){
        if( is_user_logged_in() ){        
            $user_id    = get_current_user_id();
            $resultado  = $wpdb->get_results( "SELECT * FROM {$table_name} WHERE user_id = {$user_id}" );
        } else{
            $resultado  = $wpdb->get_results( "SELECT * FROM {$table_name}" );
        }
    } else{
        $resultado  = $wpdb->get_results( "SELECT * FROM {$table_name}" );
    }

    $pegaArtigosMaisLidos = array();

    foreach ($resultado as $key => $res) {
        if( isset($pegaArtigosMaisLidos[$res->post_id]) ){
            $pegaArtigosMaisLidos[$res->post_id] += $res->post_read;
        } else{
            $pegaArtigosMaisLidos[$res->post_id] = $res->post_read;
        }
        arsort($pegaArtigosMaisLidos);                                                      
    }

    if( $offset == 1 ){
        return array_slice($pegaArtigosMaisLidos, 0, -4, true);
    } 
    elseif ( $offset == 2 ) {
        return array_slice($pegaArtigosMaisLidos, 1, -3, true);
    } 
    elseif ( $offset == 3 ) {
        return array_slice($pegaArtigosMaisLidos, 2, -2, true);
    } 
    elseif ( $offset == 4 ) {
        return array_slice($pegaArtigosMaisLidos, 3, -1, true);
    } 
    elseif ( $offset == 5 ) {
        return array_slice($pegaArtigosMaisLidos, 4, 1, true);
    } else{
        return $pegaArtigosMaisLidos;
    }

}

function getUserInteraction(){
    global $wpdb;
    $table_name = $wpdb->prefix.'dash_most_popular_post_user';
    
    $resultado  = $wpdb->get_results( "SELECT * FROM {$table_name}" );

    $aUsers = array();

    foreach ($resultado as $key => $res) {
        if( isset($aUsers[$res->user_id][$res->post_id]) ){
            $aUsers[$res->user_id][$res->post_id] += $res->post_read;
        } else{
            $aUsers[$res->user_id][$res->post_id] = $res->post_read;
        }
        arsort($aUsers);                                                      
    }

    return $aUsers;

}

function getPostByCategory(){
    global $wpdb;
    $table_name = $wpdb->prefix.'dash_most_popular_post_user';
    
    $resultado  = $wpdb->get_results( "SELECT * FROM {$table_name}" );

    $aCategory = array();

    foreach ($resultado as $key => $res) {
        foreach ( unserialize($res->post_category) as $cat) {
            if( isset($aCategory[$cat][$res->post_id]) ){
                $aCategory[$cat][$res->post_id] += $res->post_read;
            } else{
                $aCategory[$cat][$res->post_id] = $res->post_read;
            }
            arsort($aCategory);
        }                                                          
    }

    return $aCategory;

}


function getArticlesMoreLeadsGeneral($offset = 0){
    global $wpdb;
    $table_name = $wpdb->prefix.'postmeta';
    $meta       = 'post_views_general';
    $resultado  = $wpdb->get_results( " SELECT * FROM {$table_name} WHERE meta_key = '{$meta}' " );

    $pegaArtigosMaisLidos = array();

    foreach ($resultado as $key => $res) {
        if( isset($pegaArtigosMaisLidos[$res->post_id]) ){
            $pegaArtigosMaisLidos[$res->post_id] += $res->meta_value;
        } else{
            $pegaArtigosMaisLidos[$res->post_id] = $res->meta_value;
        }
        arsort($pegaArtigosMaisLidos);                                                      
    }
    if( $offset == 1 ){
        return array_slice($pegaArtigosMaisLidos, 0, -4, true);
    } 
    elseif ( $offset == 2 ) {
        return array_slice($pegaArtigosMaisLidos, 1, -3, true);
    } 
    elseif ( $offset == 3 ) {
        return array_slice($pegaArtigosMaisLidos, 2, -2, true);
    } 
    elseif ( $offset == 4 ) {
        return array_slice($pegaArtigosMaisLidos, 3, -1, true);
    } 
    elseif ( $offset == 5 ) {
        return array_slice($pegaArtigosMaisLidos, 4, 1, true);
    } else{
        return $pegaArtigosMaisLidos;
    }
}