<?php 

//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
    return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
}
add_filter('language_attributes', 'add_opengraph_doctype');
 
//Lets add Open Graph Meta Info
 
function insert_meta_in_head() {
    global $post;
    if ( !is_singular()) //if it is not a post or a page
        return;

        $og = '<meta property="fb:app_id" content="1651753734937810" />';
        $og .= '<meta property="fb:admins" content="1651753734937810" />';
        $og .= '<meta property="og:title" content="' . get_the_title($post->ID) . '"/>';
        $og .= '<meta property="og:type" content="article"/>';
        $og .= '<meta property="og:url" content="' . get_permalink($post->ID) . '"/>';
        $og .= '<meta property="og:site_name" content="'.get_bloginfo('name').'"/>';

        $card  = '<meta name="twitter:card" content="summary" />';
        $card .= '<meta name="twitter:site" content="'.get_bloginfo('name').'" />';
        $card .= '<meta name="twitter:url" content="' . get_permalink($post->ID) . '" />';
        $card .= '<meta name="twitter:creator" content="" />';
        $card .= '<meta name="twitter:title" content="' . get_the_title($post->ID) . '" />';
        $card .= '<meta name="twitter:description" content="' . get_permalink($post->ID) . '" />';

        if(!has_post_thumbnail( $post->ID )) { 
            $default_image = "http://example.com/image.jpg";
            $og .=  '<meta property="og:image" content="' . $default_image . '"/>';
            $card .= '<meta name="twitter:image" content="' . $default_image . '" />';
        }

        else{
            $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
            $og .= '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
            $card .= '<meta name="twitter:image" content="' . esc_attr( $thumbnail_src[0] ) . '" />';
        }

        echo $og.$card;

    echo "";
}
add_action( 'wp_head', 'insert_meta_in_head', 5 );

function get_response_body( $url ) {     
    $response = wp_remote_get( $url );
    $body     = wp_remote_retrieve_body( $response );
 
    return json_decode( $body );
}

function facebook_share( $url ) {
    $api_call = 'http://graph.facebook.com/?id=' . $url;

    $body = get_response_body( $api_call );
    
    $count = $body->share->share_count;

    return $count;
}


function twitter_share( $url ) {
    $api_call = 'http://urls.api.twitter.com/1/urls/count.json?url=' . $url;

    $body = get_response_body( $api_call );
    
    $count = $body->count;

    return $count;
}