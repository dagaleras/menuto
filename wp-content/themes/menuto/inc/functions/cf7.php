<?php /**
 * Formulário para integração com a newsletter
 * @author Silvio Monnerat <silvio@cabanacriacao.com>
 * @date 29/03/2018
 */
function send_email_integrations_newsletter ($WPCF7_ContactForm) {
    $submission = WPCF7_Submission::get_instance();
    $form       = $submission->get_posted_data();

    // Se o formulário for apenas o de franquias (ID: 168)
    if (in_array($form['_wpcf7'], array(122))) {
        try {
            // Remove campos do wpcf7, esses campos precisam ser removidos, se não dá pau
            foreach ($form as $key => $value) {
                // Se no formulário contiver campos que não contenham frn_, apagamos esses campos
                if (strpos($key, '_wpcf7') !== false) {
                // Apaga o campo
                    unset($form[$key]);
                }
            }

            $json = array(
                'evento'   => 'Novo Cadastro',
                'nm_email' => $form['nm_email'],
                'vars'     => array(
                    'nome' => $form['nome']
                ),
                "lista"     => array(
                    'nm_lista'    => 'base_optin',
                    'atualizar'   => "1",
                    'nome'        => $form['nome'],
                    'dt_cadastro' => date('Y-m-d')
                )
            );
            // Cria o JSON para ser enviado
            $dados = json_encode( $json );
            $body  = array( 'dados' => $dados );

            // Se JSON foi encodado com sucesso
            if ($body) {

                $tokenURL = 'https://transacional.allin.com.br/api/';
                $request = wp_remote_get($tokenURL, array(
                    'timeout' => 30,
                    'headers' => array(
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ),
                    'body'    => array(
                        'method'   => 'get_token',
                        'output'   => 'json',
                        'username' => 'gt_menuto_tallinapi',
                        'password' => 'Edu6UTY3'
                    )
                ));

                if ( is_wp_error( $request ) ) {
                    $error_message = $request->get_error_message();
                } else{ 
                    $token = json_decode($request['body']);
                    $url = 'https://transacional.allin.com.br/api/?method=workflow&token='.$token->token;

                    $response = wp_remote_post($url, array(
                        'method'  => 'POST',
                        'timeout' => 30,
                        'headers' => array(
                            'Content-Type' => 'application/x-www-form-urlencoded'
                        ),
                        'body'    => $body
                    ));
                }
            }
        }
        catch (Exception $e) {
            // Ignora os erros, simplesmente o formulário não vai ser enviado para as franquias
            // Porém acredito que será cadastrado e enviado normalmente pelo Wordpress
        }
    }
}
add_action("wpcf7_before_send_mail", "send_email_integrations_newsletter");

add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
    return $content;
});