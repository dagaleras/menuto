<?php

function harun_mce_button() {
    // Check if user have permission
    if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
        return;
    }
    // Check if WYSIWYG is enabled
    if ( 'true' == get_user_option( 'rich_editing' ) ) {
        add_filter( 'mce_external_plugins', 'harun_tinymce_plugin' );
        add_filter( 'mce_buttons', 'harun_register_mce_button' );
    }
}
add_action('admin_head', 'harun_mce_button');

// Function for new button
function harun_tinymce_plugin( $plugin_array ) {
    $plugin_array['harun_mce_button'] = get_template_directory_uri().'/js/tinymce_buttons.js';
    return $plugin_array;
}

// Register new button in the editor
function harun_register_mce_button( $buttons ) {
    array_push( $buttons, 'harun_mce_button' );
    return $buttons;
}


function add_style_select_button($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'add_style_select_button');


function add_editor_buttons($buttons) {
    $buttons[] = 'styleselect';
    $buttons[] = 'hr';

    return $buttons;
}
//add_filter('mce_buttons_3', 'add_editor_buttons');

function my_mce_before_init_insert_formats( $init_array ) {

    $style_formats = array(
        // These are the custom styles
        array(
            'title'   => 'Highlighter',
            'inline'  => 'span',
            'classes' => 'highlighter',
            'wrapper' => true,
        ),
        array(
            'title'   => 'Green Button',
            'block'   => 'span',
            'classes' => 'green-button',
            'wrapper' => true,
        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
//add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );