<?php

// Hooks your functions into the correct filters
function twd_mce_button() {
    // check user permissions
    if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
        return;
    }
    // check if WYSIWYG is enabled
    if ( 'true' == get_user_option( 'rich_editing' ) ) {
        add_filter( 'mce_external_plugins', 'twd_add_mce_plugin' );
        add_filter( 'mce_buttons', 'twd_register_mce_button' );
    }
}
add_action( 'admin_head', 'twd_mce_button' );

// Script for our mce button
function twd_add_mce_plugin( $plugin_array ) {
    $plugin_array['twd_mce_button'] = get_template_directory_uri().'/js/tinymce_buttons.js';
    return $plugin_array;
}

// Register our button in the editor
function twd_register_mce_button( $buttons ) {
    array_push( $buttons, 'twd_mce_button' );
    return $buttons;
}


/**
 * Function to fetch cpt posts list
 * @return json
 */
function twd_posts( $post_type ) {

    global $wpdb;
    $cpt_type = $post_type;
    $cpt_post_status = 'publish';
        $cpt = $wpdb->get_results( $wpdb->prepare(
        "SELECT ID, post_title
            FROM $wpdb->posts 
            WHERE $wpdb->posts.post_type = %s
            AND $wpdb->posts.post_status = %s
            ORDER BY ID DESC",
        $cpt_type,
        $cpt_post_status
    ) );

    $list = array();

    foreach ( $cpt as $post ) {
        $selected = '';
        $post_id = $post->ID;
        $post_name = $post->post_title;
        $list[] = array(
            'text' =>   $post_name,
            'value' =>  $post_id
        );
    }

    wp_send_json( $list );
}

/**
 * Function to fetch buttons
 * @since  1.6
 * @return string
 */
function twd_list_ajax() {
    // check for nonce
    check_ajax_referer( 'twd-nonce', 'security' );
    $posts = twd_posts( 'product' );
    return $posts;
}
add_action( 'wp_ajax_twd_cpt_list', 'twd_list_ajax' );

/**
 * Function to output button list ajax script
 * @since  1.6
 * @return string
 */
function twd_cpt_list() {
    // create nonce
    global $pagenow;
    if( $pagenow != 'admin.php' ){
        $nonce = wp_create_nonce( 'twd-nonce' );
        ?><script type="text/javascript">
            jQuery( document ).ready( function( $ ) {
                var data = {
                    'action'    : 'twd_cpt_list', // wp ajax action
                    'security'  : '<?php echo $nonce; ?>' // nonce value created earlier
                };
                // fire ajax
                jQuery.post( ajaxurl, data, function( response ) {
                    // if nonce fails then not authorized else settings saved
                    if( response === '-1' ){
                        // do nothing
                        console.log('error');
                    } else {
                        if (typeof(tinyMCE) != 'undefined') {
                            if (tinyMCE.activeEditor != null) {
                                tinyMCE.activeEditor.settings.cptPostsList = response;
                            }
                        }
                    }
                });
            });
        </script>
<?php 
    }
}
add_action( 'admin_footer', 'twd_cpt_list' );