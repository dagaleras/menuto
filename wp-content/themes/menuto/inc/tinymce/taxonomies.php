<?php

// Hooks your functions into the correct filters
function mce_button_taxonomies() {
    // check user permissions
    if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
        return;
    }
    // check if WYSIWYG is enabled
    if ( 'true' == get_user_option( 'rich_editing' ) ) {
        add_filter( 'mce_external_plugins', 'add_mce_plugin_taxonomies' );
        add_filter( 'mce_buttons', 'register_mce_button_taxonomies' );
    }
}
add_action( 'admin_head', 'mce_button_taxonomies' );

// Script for our mce button
function add_mce_plugin_taxonomies( $plugin_array ) {
    $plugin_array['mce_button_taxonomies'] = get_template_directory_uri().'/js/tinymce_buttons.js';
    return $plugin_array;
}

// Register our button in the editor
function register_mce_button_taxonomies( $buttons ) {
    array_push( $buttons, 'mce_button_taxonomies' );
    return $buttons;
}


/**
 * Function to fetch cpt posts list
 * @return json
 */
function custom_get_taxonomy( $post_type, $taxonomy ) {

    global $wpdb;
    $cpt_type        = $post_type;
    $cpt_taxonomy    = $taxonomy;
    $cpt_post_status = 'publish';

    $query = "
        SELECT t.term_id, t.name, t.slug  
        FROM $wpdb->terms AS t 
        INNER JOIN $wpdb->term_taxonomy AS tt ON (t.term_id = tt.term_id) 
        WHERE tt.taxonomy IN ('product_cat') 
        ORDER BY t.name ASC
    ";

    $res = $wpdb->get_results($query);

    $list = array();

    foreach ( $res as $tax ) {
        $selected  = '';
        $value = $tax->term_id;
        $text  = $tax->name;
        $list[] = array(
            'text' =>   $text,
            'value' =>  $value
        );
    }
    wp_send_json( $list );
    
}

/**
 * Function to fetch buttons
 * @since  1.6
 * @return string
 */
function custom_taxonomy_list_ajax() {
    // check for nonce
    check_ajax_referer( 'tax-nonce', 'security' );
    $tax = custom_get_taxonomy( 'product', 'product_cat' );
    return $tax;
}
add_action( 'wp_ajax_taxonomy_list_ajax', 'custom_taxonomy_list_ajax' );

/**
 * Function to output button list ajax script
 * @since  1.6
 * @return string
 */
function taxonomy_list() {
    // create nonce
    global $pagenow;
    if( $pagenow != 'admin.php' ){
        $nonce = wp_create_nonce( 'tax-nonce' );
?>
        <script type="text/javascript">
            jQuery( document ).ready( function( $ ) {
                var data = {
                    'action'    : 'taxonomy_list_ajax', // wp ajax action
                    'security'  : '<?php echo $nonce; ?>' // nonce value created earlier
                };
                // fire ajax
                jQuery.post( ajaxurl, data, function( response ) {
                    // if nonce fails then not authorized else settings saved
                    if( response === '-1' ){
                        // do nothing
                        console.log('error');
                    } else {
                        if (typeof(tinyMCE) != 'undefined') {
                            if (tinyMCE.activeEditor != null) {
                                tinyMCE.activeEditor.settings.cptTaxonomyList = response;
                            }
                        }
                    }
                });
            });
        </script>
<?php 
    }
}
add_action( 'admin_footer', 'taxonomy_list' );