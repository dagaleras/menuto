<?php

register_post_type(
	'carousel', array(	
		'label'               => 'Carrosel',
		'description'         => 'Create carousel that can be used in posts, pages and widgets.',
		'public'              => true,
    	'menu_icon'           => 'dashicons-format-gallery',
		'show_ui'             => true,
		'show_in_menu'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => true,
		'rewrite'             => array('slug' => ''),
		'query_var'           => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'supports'            => array('title'),
		'labels'              => array (
			'name'               => 'Carrosel',
			'singular_name'      => 'Carrosel',
			'menu_name'          => 'Carrosel',
			'add_new'            => 'Add',
			'add_new_item'       => 'Add New',
			'edit'               => 'Edit',
			'edit_item'          => 'Edit',
			'new_item'           => 'New',
			'view'               => 'View',
			'view_item'          => 'View',
			'search_items'       => 'Search',
			'not_found'          => 'No Found',
			'not_found_in_trash' => 'No Found in Trash',
			'parent'             => 'Parent',
		)
	) 
);

register_taxonomy( 
	'carousel_cat', 
	'carousel', array(
		'public'            => true,
		'hierarchical'      => true,
		'rewrite'           => array( 'slug' => __('categoria-carrousel'), 'hierarchical' => true, 'with_front' => false ),
		'show_in_nav_menus' => true,
		'query_var'         => true,
		'labels'            => array(
		    'name'              => __('Categorias', 'menuto'),
		    'singular_name'     => __('Categoria', 'menuto'),
		    'search_items'      => __('Buscar', 'menuto'),
		    'all_items'         => __('Todas', 'menuto'),
		    'parent_item'       => __('Pai', 'menuto'),
		    'parent_item_colon' => __('Pai:', 'menuto'),
		    'edit_item'         => __('Editar', 'menuto'),
		    'update_item'       => __('Atualizar', 'menuto'),
		    'add_new_item'      => __('Add Nova', 'menuto'),
		    'new_item_name'     => __('Nova', 'menuto'),
		)
	)
);

add_filter( 'manage_edit-carousel_columns', 'my_edit_carousel_columns' ) ;

function my_edit_carousel_columns( $columns ) {

	$columns = array(
		'cb'        => '<input type="checkbox" />',
		'title'     => __( 'Title' ),
		'shortcode' => __( 'Shortcode' ),
		'date'      => __( 'Date' )
	);

	return $columns;
}



add_action( 'manage_carousel_posts_custom_column', 'my_manage_carousel_columns', 10, 2 );
function my_manage_carousel_columns( $column, $post_id ) {
	global $post;

	$post_data = get_post($post_id, ARRAY_A);
	$slug = $post_data['post_name'];

	switch( $column ) {
		case 'shortcode' :
			echo '<span style="background:#eee;font-weight:bold;"> [carousel id="'.$slug.'"] </span>';
		break;
	}
}


/* ADD SHORTCODE */

// [carousel id=""]
function carousel_shortcode_func($atts, $content = null) {	
	extract( shortcode_atts( array(
    	'id' => ''
  	), $atts ) );

	// get content by slug
	global $wpdb;
	$post_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '$id'");

	if($post_id){
		$html =	get_post_field('post_content', $post_id);

		// add edit link for admins
		if (current_user_can('edit_posts')) {
		   $edit_link = get_edit_post_link( $post_id ); 
	 	   $html = '<div class="ux_block"><a class="edit-link" href="'.$edit_link.'">Edit Block</a>'.$html.'</div>';
		}

		$html = do_shortcode( $html );

	} else{
		
		$html = '<p><mark>UX Block <b>"'.$id.'"</b> not found! Wrong ID?</mark></p>';	
	
	}

	return $html;
}
add_shortcode('carousel', 'carousel_shortcode_func');