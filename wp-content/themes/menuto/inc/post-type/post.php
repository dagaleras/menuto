<?php 
	// Add the custom columns to the book post type:
	function set_custom_edit_most_read_columns($columns) {		
		$columns['post_views_user'] = __( 'Views User', 'menuto' );
		$columns['post_views']      = __( 'Views', 'menuto' );
		$columns['data_share_post'] = __( 'Shares', 'menuto' );

	    return $columns;
	}
	add_filter( 'manage_posts_columns', 'set_custom_edit_most_read_columns' );

	// Add the data to the custom columns for the book post type:
	function custom_most_read_column( $column, $post_id ) {
	    switch ( $column ) {

	        case 'post_views_user' :
				$user_id = get_current_user_id();
				$views   = get_post_meta( $post_id , 'post_views_user_'.$user_id , true );

	            if ( $views > 1 ) {
	                echo $views . ' views'; 
	            } 
	            elseif ( $views == 1 ) {
	                echo $views . ' view'; 
	            }else {
	                echo '0 view';
	            }
	            
	            break;

	        case 'post_views' :
	        	$views = get_post_meta( $post_id , 'post_views_general' , true );
	        	
	            if ( $views > 1 ) {
	                echo $views . ' views'; 
	            } 
	            elseif ( $views == 1 ) {
	                echo $views . ' view'; 
	            }else {
	                echo '0 view';
	            }
	            
	            break;

	        case 'data_share_post' :
	            $views = get_post_meta( $post_id , 'data_share_post' , true );
	            if ( $views > 1 ) {
	                echo $views . ' shares'; 
	            } 
	            elseif ( $views == 1 ) {
	                echo $views . ' share'; 
	            }else {
	                echo '0 share';
	            }
	            
	            break;
	    }
	}
	add_action( 'manage_posts_custom_column' , 'custom_most_read_column', 10, 2 );