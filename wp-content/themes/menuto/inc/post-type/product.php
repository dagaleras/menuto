<?php

/* ---------------------------------------------------------------------------
 * Create new post type
 * --------------------------------------------------------------------------- */
$labels = array(
    'name'                  => __('Produtos', 'menuto'),
    'singular_name'         => __('Produto'),
    'add_new'               => __('Add Novo'),
    'add_new_item'          => __('Add Novo'),
    'edit_item'             => __('Editar'),
    'new_item'              => __('Novo'),
    'view_item'             => __('Ver'),
    'search_items'          => __('Buscar'),
    'not_found'             => __('Nada encontrado'),
    'not_found_in_trash'    => __('Nada encontrando na lixeira'), 
    'parent_item_colon'     => ''
  );
    
$args = array(
    'labels'                => $labels,
    'menu_icon'             => 'dashicons-cart',
    'public'                => true,
    'publicly_queryable'    => true,
    'show_ui'               => true, 
    'query_var'             => true,
    'capability_type'       => 'post',
    'hierarchical'          => false,
    'menu_position'         => null,
    'rewrite'               => array( 'slug' => __('produto'), 'hierarchical' => true, 'with_front' => false ),
    'has_archive'           => false,
    'supports'              => array( 'thumbnail', 'title' ),
); 
  
register_post_type( 'product', $args );

$labels = array(
    'name'              => __('Categorias', 'menuto'),
    'singular_name'     => __('Categoria', 'menuto'),
    'search_items'      => __('Buscar', 'menuto'),
    'all_items'         => __('Todas', 'menuto'),
    'parent_item'       => __('Pai', 'menuto'),
    'parent_item_colon' => __('Pai:', 'menuto'),
    'edit_item'         => __('Editar', 'menuto'),
    'update_item'       => __('Atualizar', 'menuto'),
    'add_new_item'      => __('Add Nova', 'menuto'),
    'new_item_name'     => __('Nova', 'menuto'),
);

$args = array(
    'public'            => true,
    'hierarchical'      => true,
    'rewrite'           => array('slug' => __('categoria-produto'), 'hierarchical' => true, 'with_front' => false ),
    'show_in_nav_menus' => true,
    'labels'            => $labels,
    'query_var'         => true
);

register_taxonomy( 'product_cat', 'product', $args);

unset($labels);
unset($args);

/* ---------------------------------------------------------------------------
 * Edit columns
 * --------------------------------------------------------------------------- */
function edit_columns_product($columns){
    $newcolumns = array(
        "cb"            => "<input type=\"checkbox\" />",
        "title"         => __('Título', 'menuto'),
        "product_cat"   => __('Categorias', 'menuto'),
        "order_product" => __('Order', 'menuto')
    );
    $columns = array_merge($newcolumns, $columns);  
    
    return $columns;
}
add_filter("manage_edit-product_columns", "edit_columns_product");  


/* ---------------------------------------------------------------------------
 * Custom columns
 * --------------------------------------------------------------------------- */
function custom_columns_product($column){
    global $post;
    switch ($column){
        case "product_cat":
            echo get_the_term_list($post->ID, 'product_cat', '', ', ','');
            break;
        case "order_product":
            echo $post->menu_order;
            break;  
    }
}
add_action("manage_posts_custom_column",  "custom_columns_product");

/* ---------------------------------------------------------------------------
 * Sort table columns
 * --------------------------------------------------------------------------- */
function set_custom_product_sortable_columns( $columns ) {
    $columns['product_cat']   = 'product_cat';
    $columns['order_product'] = 'order_product';

    return $columns;
}

add_filter( 'manage_edit-product_sortable_columns', 'set_custom_product_sortable_columns' );

/* ---------------------------------------------------------------------------
 * Filter
 * --------------------------------------------------------------------------- */
function tsm_filter_post_type_by_taxonomy() {
    global $typenow;
    $post_type = 'product'; // change to your post type
    $taxonomy  = 'product_cat'; // change to your taxonomy
    if ($typenow == $post_type) {
        $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
        $info_taxonomy = get_taxonomy($taxonomy);
        wp_dropdown_categories(array(
            'show_option_all' => __("Todas as {$info_taxonomy->label}", "menuto"),
            'taxonomy'        => $taxonomy,
            'name'            => $taxonomy,
            'orderby'         => 'name',
            'selected'        => $selected,
            'show_count'      => true,
            'hide_empty'      => true,
        ));
    };
}
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');

/* ---------------------------------------------------------------------------
 * Query converty
 * --------------------------------------------------------------------------- */
function tsm_convert_id_to_term_in_query($query) {
    global $pagenow;
    $post_type = 'product'; // change to your post type
    $taxonomy  = 'product_cat'; // change to your taxonomy
    $q_vars    = &$query->query_vars;
    if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
        $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
        $q_vars[$taxonomy] = $term->slug;
    }
}
add_filter('parse_query', 'tsm_convert_id_to_term_in_query');