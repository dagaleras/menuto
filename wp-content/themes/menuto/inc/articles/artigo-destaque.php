<?php
function artigo_destaque() {
    global $wpdb, $post;

    $status = false;

    $args = array(
        'post_type'           => 'post',
        'posts_per_page'      => 1,
        'no_found_rows'       => true,
        'post_status'         => 'publish',
        'ignore_sticky_posts' => true
    );
        
    if( is_user_logged_in() ){
        $table_name = $wpdb->prefix.'dash_most_popular_post_user';
        $user_id    = get_current_user_id();

        $key   = 'post_views_user_'.$user_id;
        $views = get_post_meta( $post->ID, $key, true );

        if ( metadata_exists( 'post', $post->ID, $key ) ) {
            $args['orderby']  = 'meta_value_num';
            $args['meta_key'] = $key;
            $args['order']    = 'ASC';

        } else{
            $args['orderby']  = 'date';
            $args['order']    = 'DESC';
        }
        
        $resultado = $wpdb->get_results( "SELECT * FROM {$table_name} WHERE user_id = {$user_id}" );
        $ArtigosMaisLidos       = array();
        $CategoriaMaisAcessada  = array();
        foreach ( $resultado as $res ){
            $ArtigosMaisLidos[$res->post_id] = $res->post_count;
            arsort($ArtigosMaisLidos);
            foreach ( unserialize($res->post_category) as $cat) {
                $CategoriaMaisAcessada[$cat] = $res->post_count;
                arsort($CategoriaMaisAcessada);
            }
        }                       

        if($ArtigosMaisLidos){
            $ValorArtigoMaisLido = max($ArtigosMaisLidos);
            $IDArtigoMaisLido    = array_search($ValorArtigoMaisLido, $ArtigosMaisLidos);
            $status = true;
        } else{
            $status = false;
        }

        if($CategoriaMaisAcessada){
            $ValorCategoriaMaisLido = max($CategoriaMaisAcessada);
            $IDCategoriaMaisLido    = array_search($ValorCategoriaMaisLido, $CategoriaMaisAcessada);

            $status = true;
        } else{
            $status = false;
        }

    } else{
        $key   = 'post_views_general';
        if ( metadata_exists( 'post', $post->ID, $key ) ) {
            $args['orderby']  = 'meta_value_num';
            $args['meta_key'] = $key;
            $args['order']    = 'ASC';
        } else{
            $args['orderby']  = 'date';
            $args['order']    = 'DESC';
        }
    }

    ?>
    <?php if( $status ) : ?>
        <?php if( $ValorArtigoMaisLido ) : ?>
            <div class="banner--area-loop">
                <a href="<?php echo esc_url(get_permalink($IDArtigoMaisLido)); ?>">
                    <div class="mask"></div>
                    <?php if( get_field('banner__image', $IDArtigoMaisLido) ) : ?>
                        <figure class="figure">
                            <img class="figure-img" src="<?php the_field('banner__image', $IDArtigoMaisLido) ; ?>" alt="" />
                        </figure>
                    <?php else: ?>
                        <?php if( has_post_thumbnail() ) : ?>
                            <figure class="figure">
                                <?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id( $IDArtigoMaisLido ), 'full'); ?>
                                <img class="figure-img" src="<?php echo $thumbnail[0]; ?>" alt="" />
                            </figure>
                        <?php endif; ?>
                    <?php endif; ?> 
                    <div class="content">                                                       
                        <h2 class="content--title"><?php echo get_the_title($IDArtigoMaisLido); ?></h2>
                        <div class="content--paragraph">
                            <div class="arrow-up"></div>
                            <div class="content--paragraph-summary">
                                <?php 
                                    $content_post = get_post($IDArtigoMaisLido);
                                    $content      = $content_post->post_content;
                                    $content      = apply_filters('the_content', $content);
                                    $content      = str_replace(']]>', ']]&gt;', $content);
                                    echo wp_trim_words( do_shortcode($content), 17, '...' ); 
                                ?>
                            </div>
                            <button class="content--paragraph-button"><?php _e('leia mais'); ?></button>
                        </div>                                                      
                    </div>
                    <?php if( $IDCategoriaMaisLido): ?>
                        <div class="category">
                            <label for="category"><?php echo get_cat_name($IDCategoriaMaisLido); ?></label>
                        </div>
                    <?php endif; ?>                                             
                </a>
            </div>

        <?php elseif ( $ValorArtigoMaisLido < 2 ) : ?>
            <?php
                $catUser = get_field( 'user__categoria', 'user_'.$user_id);
                if( !empty($_GET['tipo']) ) {
                    $args['tax_query'][] = array(
                        'taxonomy' => 'category',
                        'field'    => 'term_id',
                        'terms'    => array($catUser[0]->term_id)
                    );
                }
                $featured = new WP_Query($args); 
                while( $featured->have_posts() ) : $featured->the_post();
                    get_template_part( 'content', 'featured' );
                endwhile; 
                wp_reset_postdata(); 
            ?>

        <?php else : ?>
            <?php 
                $featured = new WP_Query($args);  
                while( $featured->have_posts() ) : $featured->the_post();
                    get_template_part( 'content', 'featured' );
                endwhile; 
                wp_reset_postdata(); 
            ?>

        <?php endif; ?>
    
    <?php else :
        $featured = new WP_Query($args);  
        while( $featured->have_posts() ) : $featured->the_post();
            get_template_part( 'content', 'featured' );
        endwhile; 
        wp_reset_postdata();
    endif;

}
add_action('machine_learn_loop_featured', 'artigo_destaque', 15);