<div class="form-default">
    <form action="<?php echo site_url('/'); ?>"> 
        <div class="inputText">         
            <input type="text" name="s" id="s" autocomplete="off" value="<?php echo esc_attr( get_search_query() ) ?>" required />
            <span class="floating-label"><?php echo esc_attr( __( 'Pesquisar', 'menuto' ) ); ?></span>
            <input type="submit" value="<?php echo esc_attr( __( 'Buscar', 'menuto' ) ); ?>" />
        </div>
    </form>
</div>